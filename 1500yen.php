<?php include('header.php'); ?>

<div class="container-fluid">
    <div class="row kachigumiWrap">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/interview-grey.png" alt="img">
                    <h3>価値組生活<small>1,500円Contents</small></h3>
                </div>
                <p class="inner-page-header-date">2015.05.01</p>
            </div> 
            <div class="col-md-12">
                <div class="row">
                    <div class="interview-content">
                        <div class="col-xs-9">
                            <div class="interview-banner">
                                <img class="img-1500" src="assets/img/images/interview/img-banner-1500.png" alt="img">
                            </div>
                            <!--<div class="interview-banner-text">
                                <img src="assets/img/images/interview/icon-brand-interview.png" alt="img">
                                <h3>“NEW YORK MINUTE”を生きる<br>男たちのために</h3>

                            </div>-->
                            <div class="question-answer">
                                <p class="question">そもそも投資信託とは？</p>
                                <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                    運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                                <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                                <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                    運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                    それぞれの投資額に応じてすべて投資家に帰属します。</p>
                            </div>

                            <div id="additional-QuesAns" style="display:none">
                                <div class="question-answer">
                                    <p class="question">そもそも投資信託とは？</p>
                                    <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                        運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                                    <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                                    <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                        運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                        それぞれの投資額に応じてすべて投資家に帰属します。</p>
                                </div>
                                <div class="question-answer">
                                    <div class="center margin-20">
                                        <img src="assets/img/images/interview/img-yen.png" alt="img">
                                    </div>
                                    <p class="question">そもそも投資信託とは？</p>
                                    <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                        運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                                    <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                                    <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                        運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                        それぞれの投資額に応じてすべて投資家に帰属します。</p>
                                </div>
                                <div class="question-answer">
                                    <p class="question">そもそも投資信託とは？</p>
                                    <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                        運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                                    <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                                    <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                        運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                        それぞれの投資額に応じてすべて投資家に帰属します。</p>
                                </div>
                                <div class="center">
                                    <a class="btn-previous"><i class="fa fa-caret-left"></i> 前の記事へ</a>
                                    <a class="btn-next">次の記事へ <i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                            <div class="center">
                                <a id="showAdditionalQuesAns" class="btn-read-more">もっと読む</a>
                            </div>







                        </div>
                        <div class="col-xs-3">
                            <ul class="other-interviews">
                                <li class="other-interviews-heading">
                                    1,500円コラム一覧
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img5.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で買える 驚きの商品！ コピー</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img6.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で食べられる 大満足の食事！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img7.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で買える 驚きの商品！ コピー</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img8.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で食べられる 大満足の食事！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
