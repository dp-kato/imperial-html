function FastClick(e, t) {
    function n(e, t) {
        return function() {
            return e.apply(t, arguments)
        }
    }
    var i;
    if (t = t || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = t.touchBoundary || 10, this.layer = e, this.tapDelay = t.tapDelay || 200, !FastClick.notNeeded(e)) {
        for (var o = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], a = this, r = 0, s = o.length; s > r; r++)
            a[o[r]] = n(a[o[r]], a);
        deviceIsAndroid && (e.addEventListener("mouseover", this.onMouse, !0), e.addEventListener("mousedown", this.onMouse, !0), e.addEventListener("mouseup", this.onMouse, !0)), e.addEventListener("click", this.onClick, !0), e.addEventListener("touchstart", this.onTouchStart, !1), e.addEventListener("touchmove", this.onTouchMove, !1), e.addEventListener("touchend", this.onTouchEnd, !1), e.addEventListener("touchcancel", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (e.removeEventListener = function(t, n, i) {
            var o = Node.prototype.removeEventListener;
            "click" === t ? o.call(e, t, n.hijacked || n, i) : o.call(e, t, n, i)
        }, e.addEventListener = function(t, n, i) {
            var o = Node.prototype.addEventListener;
            "click" === t ? o.call(e, t, n.hijacked || (n.hijacked = function(e) {
                e.propagationStopped || n(e)
            }), i) : o.call(e, t, n, i)
        }), "function" == typeof e.onclick && (i = e.onclick, e.addEventListener("click", function(e) {
            i(e)
        }, !1), e.onclick = null)
    }
}
requirejs.config({waitSeconds: 30, paths: {jquery: "libs/jquery", jqui: "libs/jquery-ui.custom", easing: "libs/jquery.easing", mousewheel: "libs/jquery.mousewheel", "jquery.placeholder": "libs/jquery.placeholder", "jquery.flexslider": "libs/jquery.flexslider", "jquery.waitforimages": "libs/jquery.waitforimages", underscore: "libs/underscore", "jquery.scrolltrigger": "libs/jquery.scrolltrigger", kinetic: "libs/jquery.kinetic", smoothScroll: "libs/jquery.smoothDivScroll", "jquery.dropkick": "libs/jquery.dropkick", "responsive-tables": "libs/responsive-tables", "jquery.animateNumbers": "libs/jquery.animateNumbers", fastclick: "libs/fastclick"}, shim: {underscore: {exports: "_"}, easing: {deps: ["jquery"], exports: "easing"}, "jquery.scrolltrigger": {deps: ["easing", "jquery"]}, jqui: {deps: ["jquery"], exports: "jqui"}, mousewheel: {deps: ["jquery"], exports: "mousewheel"}, kinetic: {deps: ["jquery"], exports: "kinetic"}, smoothScroll: {deps: ["jqui", "kinetic", "mousewheel"], exports: "smoothScroll"}, "responsive-tables": {deps: ["jquery"]}, "jquery.dropkick": {deps: ["jquery"]}, "jquery.animateNumbers": {deps: ["jquery"]}, "jquery.placeholder": {deps: ["jquery"]}, "jquery.flexslider": {deps: ["jquery"]}, "jquery.waitforimages": {deps: ["jquery"]}, fastclick: {exports: "FastClick"}}}), define("config", function() {
}), function(e, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document)
            throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function(e, t) {
    function n(e) {
        var t = e.length, n = et.type(e);
        return"function" === n || et.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
    }
    function i(e, t, n) {
        if (et.isFunction(t))
            return et.grep(e, function(e, i) {
                return!!t.call(e, i, e) !== n
            });
        if (t.nodeType)
            return et.grep(e, function(e) {
                return e === t !== n
            });
        if ("string" == typeof t) {
            if (st.test(t))
                return et.filter(t, e, n);
            t = et.filter(t, e)
        }
        return et.grep(e, function(e) {
            return U.call(t, e) >= 0 !== n
        })
    }
    function o(e, t) {
        for (; (e = e[t]) && 1 !== e.nodeType; )
            ;
        return e
    }
    function a(e) {
        var t = ht[e] = {};
        return et.each(e.match(mt) || [], function(e, n) {
            t[n] = !0
        }), t
    }
    function r() {
        J.removeEventListener("DOMContentLoaded", r, !1), e.removeEventListener("load", r, !1), et.ready()
    }
    function s() {
        Object.defineProperty(this.cache = {}, 0, {get: function() {
                return{}
            }}), this.expando = et.expando + Math.random()
    }
    function l(e, t, n) {
        var i;
        if (void 0 === n && 1 === e.nodeType)
            if (i = "data-" + t.replace(bt, "-$1").toLowerCase(), n = e.getAttribute(i), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : _t.test(n) ? et.parseJSON(n) : n
                } catch (o) {
                }
                yt.set(e, t, n)
            } else
                n = void 0;
        return n
    }
    function c() {
        return!0
    }
    function d() {
        return!1
    }
    function u() {
        try {
            return J.activeElement
        } catch (e) {
        }
    }
    function p(e, t) {
        return et.nodeName(e, "table") && et.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }
    function m(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }
    function h(e) {
        var t = Rt.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }
    function f(e, t) {
        for (var n = 0, i = e.length; i > n; n++)
            vt.set(e[n], "globalEval", !t || vt.get(t[n], "globalEval"))
    }
    function g(e, t) {
        var n, i, o, a, r, s, l, c;
        if (1 === t.nodeType) {
            if (vt.hasData(e) && (a = vt.access(e), r = vt.set(t, a), c = a.events)) {
                delete r.handle, r.events = {};
                for (o in c)
                    for (n = 0, i = c[o].length; i > n; n++)
                        et.event.add(t, o, c[o][n])
            }
            yt.hasData(e) && (s = yt.access(e), l = et.extend({}, s), yt.set(t, l))
        }
    }
    function v(e, t) {
        var n = e.getElementsByTagName ? e.getElementsByTagName(t || "*") : e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
        return void 0 === t || t && et.nodeName(e, t) ? et.merge([e], n) : n
    }
    function y(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && kt.test(e.type) ? t.checked = e.checked : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
    }
    function _(t, n) {
        var i = et(n.createElement(t)).appendTo(n.body), o = e.getDefaultComputedStyle ? e.getDefaultComputedStyle(i[0]).display : et.css(i[0], "display");
        return i.detach(), o
    }
    function b(e) {
        var t = J, n = zt[e];
        return n || (n = _(e, t), "none" !== n && n || (Ft = (Ft || et("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = Ft[0].contentDocument, t.write(), t.close(), n = _(e, t), Ft.detach()), zt[e] = n), n
    }
    function w(e, t, n) {
        var i, o, a, r, s = e.style;
        return n = n || Wt(e), n && (r = n.getPropertyValue(t) || n[t]), n && ("" !== r || et.contains(e.ownerDocument, e) || (r = et.style(e, t)), Vt.test(r) && jt.test(t) && (i = s.width, o = s.minWidth, a = s.maxWidth, s.minWidth = s.maxWidth = s.width = r, r = n.width, s.width = i, s.minWidth = o, s.maxWidth = a)), void 0 !== r ? r + "" : r
    }
    function C(e, t) {
        return{get: function() {
                return e() ? (delete this.get, void 0) : (this.get = t).apply(this, arguments)
            }}
    }
    function x(e, t) {
        if (t in e)
            return t;
        for (var n = t[0].toUpperCase() + t.slice(1), i = t, o = Qt.length; o--; )
            if (t = Qt[o] + n, t in e)
                return t;
        return i
    }
    function k(e, t, n) {
        var i = $t.exec(t);
        return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t
    }
    function S(e, t, n, i, o) {
        for (var a = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, r = 0; 4 > a; a += 2)
            "margin" === n && (r += et.css(e, n + Ct[a], !0, o)), i ? ("content" === n && (r -= et.css(e, "padding" + Ct[a], !0, o)), "margin" !== n && (r -= et.css(e, "border" + Ct[a] + "Width", !0, o))) : (r += et.css(e, "padding" + Ct[a], !0, o), "padding" !== n && (r += et.css(e, "border" + Ct[a] + "Width", !0, o)));
        return r
    }
    function E(e, t, n) {
        var i = !0, o = "width" === t ? e.offsetWidth : e.offsetHeight, a = Wt(e), r = "border-box" === et.css(e, "boxSizing", !1, a);
        if (0 >= o || null == o) {
            if (o = w(e, t, a), (0 > o || null == o) && (o = e.style[t]), Vt.test(o))
                return o;
            i = r && (X.boxSizingReliable() || o === e.style[t]), o = parseFloat(o) || 0
        }
        return o + S(e, t, n || (r ? "border" : "content"), i, a) + "px"
    }
    function T(e, t) {
        for (var n, i, o, a = [], r = 0, s = e.length; s > r; r++)
            i = e[r], i.style && (a[r] = vt.get(i, "olddisplay"), n = i.style.display, t ? (a[r] || "none" !== n || (i.style.display = ""), "" === i.style.display && xt(i) && (a[r] = vt.access(i, "olddisplay", b(i.nodeName)))) : a[r] || (o = xt(i), (n && "none" !== n || !o) && vt.set(i, "olddisplay", o ? n : et.css(i, "display"))));
        for (r = 0; s > r; r++)
            i = e[r], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? a[r] || "" : "none"));
        return e
    }
    function M(e, t, n, i, o) {
        return new M.prototype.init(e, t, n, i, o)
    }
    function A() {
        return setTimeout(function() {
            Gt = void 0
        }), Gt = et.now()
    }
    function P(e, t) {
        var n, i = 0, o = {height: e};
        for (t = t?1:0; 4 > i; i += 2 - t)
            n = Ct[i], o["margin" + n] = o["padding" + n] = e;
        return t && (o.opacity = o.width = e), o
    }
    function I(e, t, n) {
        for (var i, o = (nn[t] || []).concat(nn["*"]), a = 0, r = o.length; r > a; a++)
            if (i = o[a].call(n, t, e))
                return i
    }
    function N(e, t, n) {
        var i, o, a, r, s, l, c, d = this, u = {}, p = e.style, m = e.nodeType && xt(e), h = vt.get(e, "fxshow");
        n.queue || (s = et._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, l = s.empty.fire, s.empty.fire = function() {
            s.unqueued || l()
        }), s.unqueued++, d.always(function() {
            d.always(function() {
                s.unqueued--, et.queue(e, "fx").length || s.empty.fire()
            })
        })), 1 === e.nodeType && ("height"in t || "width"in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], c = et.css(e, "display"), "none" === c && (c = b(e.nodeName)), "inline" === c && "none" === et.css(e, "float") && (p.display = "inline-block")), n.overflow && (p.overflow = "hidden", d.always(function() {
            p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2]
        }));
        for (i in t)
            if (o = t[i], Jt.exec(o)) {
                if (delete t[i], a = a || "toggle" === o, o === (m ? "hide" : "show")) {
                    if ("show" !== o || !h || void 0 === h[i])
                        continue;
                    m = !0
                }
                u[i] = h && h[i] || et.style(e, i)
            }
        if (!et.isEmptyObject(u)) {
            h ? "hidden"in h && (m = h.hidden) : h = vt.access(e, "fxshow", {}), a && (h.hidden = !m), m ? et(e).show() : d.done(function() {
                et(e).hide()
            }), d.done(function() {
                var t;
                vt.remove(e, "fxshow");
                for (t in u)
                    et.style(e, t, u[t])
            });
            for (i in u)
                r = I(m ? h[i] : 0, i, d), i in h || (h[i] = r.start, m && (r.end = r.start, r.start = "width" === i || "height" === i ? 1 : 0))
        }
    }
    function D(e, t) {
        var n, i, o, a, r;
        for (n in e)
            if (i = et.camelCase(n), o = t[i], a = e[n], et.isArray(a) && (o = a[1], a = e[n] = a[0]), n !== i && (e[i] = a, delete e[n]), r = et.cssHooks[i], r && "expand"in r) {
                a = r.expand(a), delete e[i];
                for (n in a)
                    n in e || (e[n] = a[n], t[n] = o)
            } else
                t[i] = o
    }
    function L(e, t, n) {
        var i, o, a = 0, r = tn.length, s = et.Deferred().always(function() {
            delete l.elem
        }), l = function() {
            if (o)
                return!1;
            for (var t = Gt || A(), n = Math.max(0, c.startTime + c.duration - t), i = n / c.duration || 0, a = 1 - i, r = 0, l = c.tweens.length; l > r; r++)
                c.tweens[r].run(a);
            return s.notifyWith(e, [c, a, n]), 1 > a && l ? n : (s.resolveWith(e, [c]), !1)
        }, c = s.promise({elem: e, props: et.extend({}, t), opts: et.extend(!0, {specialEasing: {}}, n), originalProperties: t, originalOptions: n, startTime: Gt || A(), duration: n.duration, tweens: [], createTween: function(t, n) {
                var i = et.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                return c.tweens.push(i), i
            }, stop: function(t) {
                var n = 0, i = t ? c.tweens.length : 0;
                if (o)
                    return this;
                for (o = !0; i > n; n++)
                    c.tweens[n].run(1);
                return t ? s.resolveWith(e, [c, t]) : s.rejectWith(e, [c, t]), this
            }}), d = c.props;
        for (D(d, c.opts.specialEasing); r > a; a++)
            if (i = tn[a].call(c, e, d, c.opts))
                return i;
        return et.map(d, I, c), et.isFunction(c.opts.start) && c.opts.start.call(e, c), et.fx.timer(et.extend(l, {elem: e, anim: c, queue: c.opts.queue})), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }
    function B(e) {
        return function(t, n) {
            "string" != typeof t && (n = t, t = "*");
            var i, o = 0, a = t.toLowerCase().match(mt) || [];
            if (et.isFunction(n))
                for (; i = a[o++]; )
                    "+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
        }
    }
    function R(e, t, n, i) {
        function o(s) {
            var l;
            return a[s] = !0, et.each(e[s] || [], function(e, s) {
                var c = s(t, n, i);
                return"string" != typeof c || r || a[c] ? r ? !(l = c) : void 0 : (t.dataTypes.unshift(c), o(c), !1)
            }), l
        }
        var a = {}, r = e === Cn;
        return o(t.dataTypes[0]) || !a["*"] && o("*")
    }
    function H(e, t) {
        var n, i, o = et.ajaxSettings.flatOptions || {};
        for (n in t)
            void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
        return i && et.extend(!0, e, i), e
    }
    function O(e, t, n) {
        for (var i, o, a, r, s = e.contents, l = e.dataTypes; "*" === l[0]; )
            l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i)
            for (o in s)
                if (s[o] && s[o].test(i)) {
                    l.unshift(o);
                    break
                }
        if (l[0]in n)
            a = l[0];
        else {
            for (o in n) {
                if (!l[0] || e.converters[o + " " + l[0]]) {
                    a = o;
                    break
                }
                r || (r = o)
            }
            a = a || r
        }
        return a ? (a !== l[0] && l.unshift(a), n[a]) : void 0
    }
    function F(e, t, n, i) {
        var o, a, r, s, l, c = {}, d = e.dataTypes.slice();
        if (d[1])
            for (r in e.converters)
                c[r.toLowerCase()] = e.converters[r];
        for (a = d.shift(); a; )
            if (e.responseFields[a] && (n[e.responseFields[a]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = a, a = d.shift())
                if ("*" === a)
                    a = l;
                else if ("*" !== l && l !== a) {
                    if (r = c[l + " " + a] || c["* " + a], !r)
                        for (o in c)
                            if (s = o.split(" "), s[1] === a && (r = c[l + " " + s[0]] || c["* " + s[0]])) {
                                r === !0 ? r = c[o] : c[o] !== !0 && (a = s[0], d.unshift(s[1]));
                                break
                            }
                    if (r !== !0)
                        if (r && e["throws"])
                            t = r(t);
                        else
                            try {
                                t = r(t)
                            } catch (u) {
                                return{state: "parsererror", error: r ? u : "No conversion from " + l + " to " + a}
                            }
                }
        return{state: "success", data: t}
    }
    function z(e, t, n, i) {
        var o;
        if (et.isArray(t))
            et.each(t, function(t, o) {
                n || En.test(e) ? i(e, o) : z(e + "[" + ("object" == typeof o ? t : "") + "]", o, n, i)
            });
        else if (n || "object" !== et.type(t))
            i(e, t);
        else
            for (o in t)
                z(e + "[" + o + "]", t[o], n, i)
    }
    function j(e) {
        return et.isWindow(e) ? e : 9 === e.nodeType && e.defaultView
    }
    var V = [], W = V.slice, q = V.concat, $ = V.push, U = V.indexOf, K = {}, Y = K.toString, Q = K.hasOwnProperty, G = "".trim, X = {}, J = e.document, Z = "2.1.0", et = function(e, t) {
        return new et.fn.init(e, t)
    }, tt = /^-ms-/, nt = /-([\da-z])/gi, it = function(e, t) {
        return t.toUpperCase()
    };
    et.fn = et.prototype = {jquery: Z, constructor: et, selector: "", length: 0, toArray: function() {
            return W.call(this)
        }, get: function(e) {
            return null != e ? 0 > e ? this[e + this.length] : this[e] : W.call(this)
        }, pushStack: function(e) {
            var t = et.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        }, each: function(e, t) {
            return et.each(this, e, t)
        }, map: function(e) {
            return this.pushStack(et.map(this, function(t, n) {
                return e.call(t, n, t)
            }))
        }, slice: function() {
            return this.pushStack(W.apply(this, arguments))
        }, first: function() {
            return this.eq(0)
        }, last: function() {
            return this.eq(-1)
        }, eq: function(e) {
            var t = this.length, n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        }, end: function() {
            return this.prevObject || this.constructor(null)
        }, push: $, sort: V.sort, splice: V.splice}, et.extend = et.fn.extend = function() {
        var e, t, n, i, o, a, r = arguments[0] || {}, s = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof r && (c = r, r = arguments[s] || {}, s++), "object" == typeof r || et.isFunction(r) || (r = {}), s === l && (r = this, s--); l > s; s++)
            if (null != (e = arguments[s]))
                for (t in e)
                    n = r[t], i = e[t], r !== i && (c && i && (et.isPlainObject(i) || (o = et.isArray(i))) ? (o ? (o = !1, a = n && et.isArray(n) ? n : []) : a = n && et.isPlainObject(n) ? n : {}, r[t] = et.extend(c, a, i)) : void 0 !== i && (r[t] = i));
        return r
    }, et.extend({expando: "jQuery" + (Z + Math.random()).replace(/\D/g, ""), isReady: !0, error: function(e) {
            throw new Error(e)
        }, noop: function() {
        }, isFunction: function(e) {
            return"function" === et.type(e)
        }, isArray: Array.isArray, isWindow: function(e) {
            return null != e && e === e.window
        }, isNumeric: function(e) {
            return e - parseFloat(e) >= 0
        }, isPlainObject: function(e) {
            if ("object" !== et.type(e) || e.nodeType || et.isWindow(e))
                return!1;
            try {
                if (e.constructor && !Q.call(e.constructor.prototype, "isPrototypeOf"))
                    return!1
            } catch (t) {
                return!1
            }
            return!0
        }, isEmptyObject: function(e) {
            var t;
            for (t in e)
                return!1;
            return!0
        }, type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? K[Y.call(e)] || "object" : typeof e
        }, globalEval: function(e) {
            var t, n = eval;
            e = et.trim(e), e && (1 === e.indexOf("use strict") ? (t = J.createElement("script"), t.text = e, J.head.appendChild(t).parentNode.removeChild(t)) : n(e))
        }, camelCase: function(e) {
            return e.replace(tt, "ms-").replace(nt, it)
        }, nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        }, each: function(e, t, i) {
            var o, a = 0, r = e.length, s = n(e);
            if (i) {
                if (s)
                    for (; r > a && (o = t.apply(e[a], i), o !== !1); a++)
                        ;
                else
                    for (a in e)
                        if (o = t.apply(e[a], i), o === !1)
                            break
            } else if (s)
                for (; r > a && (o = t.call(e[a], a, e[a]), o !== !1); a++)
                    ;
            else
                for (a in e)
                    if (o = t.call(e[a], a, e[a]), o === !1)
                        break;
            return e
        }, trim: function(e) {
            return null == e ? "" : G.call(e)
        }, makeArray: function(e, t) {
            var i = t || [];
            return null != e && (n(Object(e)) ? et.merge(i, "string" == typeof e ? [e] : e) : $.call(i, e)), i
        }, inArray: function(e, t, n) {
            return null == t ? -1 : U.call(t, e, n)
        }, merge: function(e, t) {
            for (var n = +t.length, i = 0, o = e.length; n > i; i++)
                e[o++] = t[i];
            return e.length = o, e
        }, grep: function(e, t, n) {
            for (var i, o = [], a = 0, r = e.length, s = !n; r > a; a++)
                i = !t(e[a], a), i !== s && o.push(e[a]);
            return o
        }, map: function(e, t, i) {
            var o, a = 0, r = e.length, s = n(e), l = [];
            if (s)
                for (; r > a; a++)
                    o = t(e[a], a, i), null != o && l.push(o);
            else
                for (a in e)
                    o = t(e[a], a, i), null != o && l.push(o);
            return q.apply([], l)
        }, guid: 1, proxy: function(e, t) {
            var n, i, o;
            return"string" == typeof t && (n = e[t], t = e, e = n), et.isFunction(e) ? (i = W.call(arguments, 2), o = function() {
                return e.apply(t || this, i.concat(W.call(arguments)))
            }, o.guid = e.guid = e.guid || et.guid++, o) : void 0
        }, now: Date.now, support: X}), et.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        K["[object " + t + "]"] = t.toLowerCase()
    });
    var ot = function(e) {
        function t(e, t, n, i) {
            var o, a, r, s, l, c, u, h, f, g;
            if ((t ? t.ownerDocument || t : z) !== N && I(t), t = t || N, n = n || [], !e || "string" != typeof e)
                return n;
            if (1 !== (s = t.nodeType) && 9 !== s)
                return[];
            if (L && !i) {
                if (o = yt.exec(e))
                    if (r = o[1]) {
                        if (9 === s) {
                            if (a = t.getElementById(r), !a || !a.parentNode)
                                return n;
                            if (a.id === r)
                                return n.push(a), n
                        } else if (t.ownerDocument && (a = t.ownerDocument.getElementById(r)) && O(t, a) && a.id === r)
                            return n.push(a), n
                    } else {
                        if (o[2])
                            return Z.apply(n, t.getElementsByTagName(e)), n;
                        if ((r = o[3]) && x.getElementsByClassName && t.getElementsByClassName)
                            return Z.apply(n, t.getElementsByClassName(r)), n
                    }
                if (x.qsa && (!B || !B.test(e))) {
                    if (h = u = F, f = t, g = 9 === s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                        for (c = p(e), (u = t.getAttribute("id"))?h = u.replace(bt, "\\$&"):t.setAttribute("id", h), h = "[id='" + h + "'] ", l = c.length; l--; )
                            c[l] = h + m(c[l]);
                        f = _t.test(e) && d(t.parentNode) || t, g = c.join(",")
                    }
                    if (g)
                        try {
                            return Z.apply(n, f.querySelectorAll(g)), n
                        } catch (v) {
                        } finally {
                            u || t.removeAttribute("id")
                        }
                }
            }
            return w(e.replace(lt, "$1"), t, n, i)
        }
        function n() {
            function e(n, i) {
                return t.push(n + " ") > k.cacheLength && delete e[t.shift()], e[n + " "] = i
            }
            var t = [];
            return e
        }
        function i(e) {
            return e[F] = !0, e
        }
        function o(e) {
            var t = N.createElement("div");
            try {
                return!!e(t)
            } catch (n) {
                return!1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }
        function a(e, t) {
            for (var n = e.split("|"), i = e.length; i--; )
                k.attrHandle[n[i]] = t
        }
        function r(e, t) {
            var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || Y) - (~e.sourceIndex || Y);
            if (i)
                return i;
            if (n)
                for (; n = n.nextSibling; )
                    if (n === t)
                        return-1;
            return e ? 1 : -1
        }
        function s(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return"input" === n && t.type === e
            }
        }
        function l(e) {
            return function(t) {
                var n = t.nodeName.toLowerCase();
                return("input" === n || "button" === n) && t.type === e
            }
        }
        function c(e) {
            return i(function(t) {
                return t = +t, i(function(n, i) {
                    for (var o, a = e([], n.length, t), r = a.length; r--; )
                        n[o = a[r]] && (n[o] = !(i[o] = n[o]))
                })
            })
        }
        function d(e) {
            return e && typeof e.getElementsByTagName !== K && e
        }
        function u() {
        }
        function p(e, n) {
            var i, o, a, r, s, l, c, d = q[e + " "];
            if (d)
                return n ? 0 : d.slice(0);
            for (s = e, l = [], c = k.preFilter; s; ) {
                (!i || (o = ct.exec(s))) && (o && (s = s.slice(o[0].length) || s), l.push(a = [])), i = !1, (o = dt.exec(s)) && (i = o.shift(), a.push({value: i, type: o[0].replace(lt, " ")}), s = s.slice(i.length));
                for (r in k.filter)
                    !(o = ht[r].exec(s)) || c[r] && !(o = c[r](o)) || (i = o.shift(), a.push({value: i, type: r, matches: o}), s = s.slice(i.length));
                if (!i)
                    break
            }
            return n ? s.length : s ? t.error(e) : q(e, l).slice(0)
        }
        function m(e) {
            for (var t = 0, n = e.length, i = ""; n > t; t++)
                i += e[t].value;
            return i
        }
        function h(e, t, n) {
            var i = t.dir, o = n && "parentNode" === i, a = V++;
            return t.first ? function(t, n, a) {
                for (; t = t[i]; )
                    if (1 === t.nodeType || o)
                        return e(t, n, a)
            } : function(t, n, r) {
                var s, l, c = [j, a];
                if (r) {
                    for (; t = t[i]; )
                        if ((1 === t.nodeType || o) && e(t, n, r))
                            return!0
                } else
                    for (; t = t[i]; )
                        if (1 === t.nodeType || o) {
                            if (l = t[F] || (t[F] = {}), (s = l[i]) && s[0] === j && s[1] === a)
                                return c[2] = s[2];
                            if (l[i] = c, c[2] = e(t, n, r))
                                return!0
                        }
            }
        }
        function f(e) {
            return e.length > 1 ? function(t, n, i) {
                for (var o = e.length; o--; )
                    if (!e[o](t, n, i))
                        return!1;
                return!0
            } : e[0]
        }
        function g(e, t, n, i, o) {
            for (var a, r = [], s = 0, l = e.length, c = null != t; l > s; s++)
                (a = e[s]) && (!n || n(a, i, o)) && (r.push(a), c && t.push(s));
            return r
        }
        function v(e, t, n, o, a, r) {
            return o && !o[F] && (o = v(o)), a && !a[F] && (a = v(a, r)), i(function(i, r, s, l) {
                var c, d, u, p = [], m = [], h = r.length, f = i || b(t || "*", s.nodeType ? [s] : s, []), v = !e || !i && t ? f : g(f, p, e, s, l), y = n ? a || (i ? e : h || o) ? [] : r : v;
                if (n && n(v, y, s, l), o)
                    for (c = g(y, m), o(c, [], s, l), d = c.length; d--; )
                        (u = c[d]) && (y[m[d]] = !(v[m[d]] = u));
                if (i) {
                    if (a || e) {
                        if (a) {
                            for (c = [], d = y.length; d--; )
                                (u = y[d]) && c.push(v[d] = u);
                            a(null, y = [], c, l)
                        }
                        for (d = y.length; d--; )
                            (u = y[d]) && (c = a ? tt.call(i, u) : p[d]) > -1 && (i[c] = !(r[c] = u))
                    }
                } else
                    y = g(y === r ? y.splice(h, y.length) : y), a ? a(null, r, y, l) : Z.apply(r, y)
            })
        }
        function y(e) {
            for (var t, n, i, o = e.length, a = k.relative[e[0].type], r = a || k.relative[" "], s = a ? 1 : 0, l = h(function(e) {
                return e === t
            }, r, !0), c = h(function(e) {
                return tt.call(t, e) > -1
            }, r, !0), d = [function(e, n, i) {
                    return!a && (i || n !== M) || ((t = n).nodeType ? l(e, n, i) : c(e, n, i))
                }]; o > s; s++)
                if (n = k.relative[e[s].type])
                    d = [h(f(d), n)];
                else {
                    if (n = k.filter[e[s].type].apply(null, e[s].matches), n[F]) {
                        for (i = ++s; o > i && !k.relative[e[i].type]; i++)
                            ;
                        return v(s > 1 && f(d), s > 1 && m(e.slice(0, s - 1).concat({value: " " === e[s - 2].type ? "*" : ""})).replace(lt, "$1"), n, i > s && y(e.slice(s, i)), o > i && y(e = e.slice(i)), o > i && m(e))
                    }
                    d.push(n)
                }
            return f(d)
        }
        function _(e, n) {
            var o = n.length > 0, a = e.length > 0, r = function(i, r, s, l, c) {
                var d, u, p, m = 0, h = "0", f = i && [], v = [], y = M, _ = i || a && k.find.TAG("*", c), b = j += null == y ? 1 : Math.random() || .1, w = _.length;
                for (c && (M = r !== N && r); h !== w && null != (d = _[h]); h++) {
                    if (a && d) {
                        for (u = 0; p = e[u++]; )
                            if (p(d, r, s)) {
                                l.push(d);
                                break
                            }
                        c && (j = b)
                    }
                    o && ((d = !p && d) && m--, i && f.push(d))
                }
                if (m += h, o && h !== m) {
                    for (u = 0; p = n[u++]; )
                        p(f, v, r, s);
                    if (i) {
                        if (m > 0)
                            for (; h--; )
                                f[h] || v[h] || (v[h] = X.call(l));
                        v = g(v)
                    }
                    Z.apply(l, v), c && !i && v.length > 0 && m + n.length > 1 && t.uniqueSort(l)
                }
                return c && (j = b, M = y), f
            };
            return o ? i(r) : r
        }
        function b(e, n, i) {
            for (var o = 0, a = n.length; a > o; o++)
                t(e, n[o], i);
            return i
        }
        function w(e, t, n, i) {
            var o, a, r, s, l, c = p(e);
            if (!i && 1 === c.length) {
                if (a = c[0] = c[0].slice(0), a.length > 2 && "ID" === (r = a[0]).type && x.getById && 9 === t.nodeType && L && k.relative[a[1].type]) {
                    if (t = (k.find.ID(r.matches[0].replace(wt, Ct), t) || [])[0], !t)
                        return n;
                    e = e.slice(a.shift().value.length)
                }
                for (o = ht.needsContext.test(e)?0:a.length; o-- && (r = a[o], !k.relative[s = r.type]); )
                    if ((l = k.find[s]) && (i = l(r.matches[0].replace(wt, Ct), _t.test(a[0].type) && d(t.parentNode) || t))) {
                        if (a.splice(o, 1), e = i.length && m(a), !e)
                            return Z.apply(n, i), n;
                        break
                    }
            }
            return T(e, c)(i, t, !L, n, _t.test(e) && d(t.parentNode) || t), n
        }
        var C, x, k, S, E, T, M, A, P, I, N, D, L, B, R, H, O, F = "sizzle" + -new Date, z = e.document, j = 0, V = 0, W = n(), q = n(), $ = n(), U = function(e, t) {
            return e === t && (P = !0), 0
        }, K = "undefined", Y = 1 << 31, Q = {}.hasOwnProperty, G = [], X = G.pop, J = G.push, Z = G.push, et = G.slice, tt = G.indexOf || function(e) {
            for (var t = 0, n = this.length; n > t; t++)
                if (this[t] === e)
                    return t;
            return-1
        }, nt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", it = "[\\x20\\t\\r\\n\\f]", ot = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", at = ot.replace("w", "w#"), rt = "\\[" + it + "*(" + ot + ")" + it + "*(?:([*^$|!~]?=)" + it + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + at + ")|)|)" + it + "*\\]", st = ":(" + ot + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + rt.replace(3, 8) + ")*)|.*)\\)|)", lt = new RegExp("^" + it + "+|((?:^|[^\\\\])(?:\\\\.)*)" + it + "+$", "g"), ct = new RegExp("^" + it + "*," + it + "*"), dt = new RegExp("^" + it + "*([>+~]|" + it + ")" + it + "*"), ut = new RegExp("=" + it + "*([^\\]'\"]*?)" + it + "*\\]", "g"), pt = new RegExp(st), mt = new RegExp("^" + at + "$"), ht = {ID: new RegExp("^#(" + ot + ")"), CLASS: new RegExp("^\\.(" + ot + ")"), TAG: new RegExp("^(" + ot.replace("w", "w*") + ")"), ATTR: new RegExp("^" + rt), PSEUDO: new RegExp("^" + st), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + it + "*(even|odd|(([+-]|)(\\d*)n|)" + it + "*(?:([+-]|)" + it + "*(\\d+)|))" + it + "*\\)|)", "i"), bool: new RegExp("^(?:" + nt + ")$", "i"), needsContext: new RegExp("^" + it + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + it + "*((?:-\\d)?\\d*)" + it + "*\\)|)(?=[^-]|$)", "i")}, ft = /^(?:input|select|textarea|button)$/i, gt = /^h\d$/i, vt = /^[^{]+\{\s*\[native \w/, yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, _t = /[+~]/, bt = /'|\\/g, wt = new RegExp("\\\\([\\da-f]{1,6}" + it + "?|(" + it + ")|.)", "ig"), Ct = function(e, t, n) {
            var i = "0x" + t - 65536;
            return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(55296 | i >> 10, 56320 | 1023 & i)
        };
        try {
            Z.apply(G = et.call(z.childNodes), z.childNodes), G[z.childNodes.length].nodeType
        } catch (xt) {
            Z = {apply: G.length ? function(e, t) {
                    J.apply(e, et.call(t))
                } : function(e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++]; )
                        ;
                    e.length = n - 1
                }}
        }
        x = t.support = {}, E = t.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return t ? "HTML" !== t.nodeName : !1
        }, I = t.setDocument = function(e) {
            var t, n = e ? e.ownerDocument || e : z, i = n.defaultView;
            return n !== N && 9 === n.nodeType && n.documentElement ? (N = n, D = n.documentElement, L = !E(n), i && i !== i.top && (i.addEventListener ? i.addEventListener("unload", function() {
                I()
            }, !1) : i.attachEvent && i.attachEvent("onunload", function() {
                I()
            })), x.attributes = o(function(e) {
                return e.className = "i", !e.getAttribute("className")
            }), x.getElementsByTagName = o(function(e) {
                return e.appendChild(n.createComment("")), !e.getElementsByTagName("*").length
            }), x.getElementsByClassName = vt.test(n.getElementsByClassName) && o(function(e) {
                return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
            }), x.getById = o(function(e) {
                return D.appendChild(e).id = F, !n.getElementsByName || !n.getElementsByName(F).length
            }), x.getById ? (k.find.ID = function(e, t) {
                if (typeof t.getElementById !== K && L) {
                    var n = t.getElementById(e);
                    return n && n.parentNode ? [n] : []
                }
            }, k.filter.ID = function(e) {
                var t = e.replace(wt, Ct);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete k.find.ID, k.filter.ID = function(e) {
                var t = e.replace(wt, Ct);
                return function(e) {
                    var n = typeof e.getAttributeNode !== K && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }), k.find.TAG = x.getElementsByTagName ? function(e, t) {
                return typeof t.getElementsByTagName !== K ? t.getElementsByTagName(e) : void 0
            } : function(e, t) {
                var n, i = [], o = 0, a = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = a[o++]; )
                        1 === n.nodeType && i.push(n);
                    return i
                }
                return a
            }, k.find.CLASS = x.getElementsByClassName && function(e, t) {
                return typeof t.getElementsByClassName !== K && L ? t.getElementsByClassName(e) : void 0
            }, R = [], B = [], (x.qsa = vt.test(n.querySelectorAll)) && (o(function(e) {
                e.innerHTML = "<select t=''><option selected=''></option></select>", e.querySelectorAll("[t^='']").length && B.push("[*^$]=" + it + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || B.push("\\[" + it + "*(?:value|" + nt + ")"), e.querySelectorAll(":checked").length || B.push(":checked")
            }), o(function(e) {
                var t = n.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && B.push("name" + it + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || B.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), B.push(",.*:")
            })), (x.matchesSelector = vt.test(H = D.webkitMatchesSelector || D.mozMatchesSelector || D.oMatchesSelector || D.msMatchesSelector)) && o(function(e) {
                x.disconnectedMatch = H.call(e, "div"), H.call(e, "[s!='']:x"), R.push("!=", st)
            }), B = B.length && new RegExp(B.join("|")), R = R.length && new RegExp(R.join("|")), t = vt.test(D.compareDocumentPosition), O = t || vt.test(D.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
            } : function(e, t) {
                if (t)
                    for (; t = t.parentNode; )
                        if (t === e)
                            return!0;
                return!1
            }, U = t ? function(e, t) {
                if (e === t)
                    return P = !0, 0;
                var i = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return i ? i : (i = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & i || !x.sortDetached && t.compareDocumentPosition(e) === i ? e === n || e.ownerDocument === z && O(z, e) ? -1 : t === n || t.ownerDocument === z && O(z, t) ? 1 : A ? tt.call(A, e) - tt.call(A, t) : 0 : 4 & i ? -1 : 1)
            } : function(e, t) {
                if (e === t)
                    return P = !0, 0;
                var i, o = 0, a = e.parentNode, s = t.parentNode, l = [e], c = [t];
                if (!a || !s)
                    return e === n ? -1 : t === n ? 1 : a ? -1 : s ? 1 : A ? tt.call(A, e) - tt.call(A, t) : 0;
                if (a === s)
                    return r(e, t);
                for (i = e; i = i.parentNode; )
                    l.unshift(i);
                for (i = t; i = i.parentNode; )
                    c.unshift(i);
                for (; l[o] === c[o]; )
                    o++;
                return o ? r(l[o], c[o]) : l[o] === z ? -1 : c[o] === z ? 1 : 0
            }, n) : N
        }, t.matches = function(e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function(e, n) {
            if ((e.ownerDocument || e) !== N && I(e), n = n.replace(ut, "='$1']"), !(!x.matchesSelector || !L || R && R.test(n) || B && B.test(n)))
                try {
                    var i = H.call(e, n);
                    if (i || x.disconnectedMatch || e.document && 11 !== e.document.nodeType)
                        return i
                } catch (o) {
                }
            return t(n, N, null, [e]).length > 0
        }, t.contains = function(e, t) {
            return(e.ownerDocument || e) !== N && I(e), O(e, t)
        }, t.attr = function(e, t) {
            (e.ownerDocument || e) !== N && I(e);
            var n = k.attrHandle[t.toLowerCase()], i = n && Q.call(k.attrHandle, t.toLowerCase()) ? n(e, t, !L) : void 0;
            return void 0 !== i ? i : x.attributes || !L ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, t.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function(e) {
            var t, n = [], i = 0, o = 0;
            if (P = !x.detectDuplicates, A = !x.sortStable && e.slice(0), e.sort(U), P) {
                for (; t = e[o++]; )
                    t === e[o] && (i = n.push(o));
                for (; i--; )
                    e.splice(n[i], 1)
            }
            return A = null, e
        }, S = t.getText = function(e) {
            var t, n = "", i = 0, o = e.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof e.textContent)
                        return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling)
                        n += S(e)
                } else if (3 === o || 4 === o)
                    return e.nodeValue
            } else
                for (; t = e[i++]; )
                    n += S(t);
            return n
        }, k = t.selectors = {cacheLength: 50, createPseudo: i, match: ht, attrHandle: {}, find: {}, relative: {">": {dir: "parentNode", first: !0}, " ": {dir: "parentNode"}, "+": {dir: "previousSibling", first: !0}, "~": {dir: "previousSibling"}}, preFilter: {ATTR: function(e) {
                    return e[1] = e[1].replace(wt, Ct), e[3] = (e[4] || e[5] || "").replace(wt, Ct), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                }, CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                }, PSEUDO: function(e) {
                    var t, n = !e[5] && e[2];
                    return ht.CHILD.test(e[0]) ? null : (e[3] && void 0 !== e[4] ? e[2] = e[4] : n && pt.test(n) && (t = p(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }}, filter: {TAG: function(e) {
                    var t = e.replace(wt, Ct).toLowerCase();
                    return"*" === e ? function() {
                        return!0
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                }, CLASS: function(e) {
                    var t = W[e + " "];
                    return t || (t = new RegExp("(^|" + it + ")" + e + "(" + it + "|$)")) && W(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== K && e.getAttribute("class") || "")
                    })
                }, ATTR: function(e, n, i) {
                    return function(o) {
                        var a = t.attr(o, e);
                        return null == a ? "!=" === n : n ? (a += "", "=" === n ? a === i : "!=" === n ? a !== i : "^=" === n ? i && 0 === a.indexOf(i) : "*=" === n ? i && a.indexOf(i) > -1 : "$=" === n ? i && a.slice(-i.length) === i : "~=" === n ? (" " + a + " ").indexOf(i) > -1 : "|=" === n ? a === i || a.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                }, CHILD: function(e, t, n, i, o) {
                    var a = "nth" !== e.slice(0, 3), r = "last" !== e.slice(-4), s = "of-type" === t;
                    return 1 === i && 0 === o ? function(e) {
                        return!!e.parentNode
                    } : function(t, n, l) {
                        var c, d, u, p, m, h, f = a !== r ? "nextSibling" : "previousSibling", g = t.parentNode, v = s && t.nodeName.toLowerCase(), y = !l && !s;
                        if (g) {
                            if (a) {
                                for (; f; ) {
                                    for (u = t; u = u[f]; )
                                        if (s ? u.nodeName.toLowerCase() === v : 1 === u.nodeType)
                                            return!1;
                                    h = f = "only" === e && !h && "nextSibling"
                                }
                                return!0
                            }
                            if (h = [r ? g.firstChild : g.lastChild], r && y) {
                                for (d = g[F] || (g[F] = {}), c = d[e] || [], m = c[0] === j && c[1], p = c[0] === j && c[2], u = m && g.childNodes[m]; u = ++m && u && u[f] || (p = m = 0) || h.pop(); )
                                    if (1 === u.nodeType && ++p && u === t) {
                                        d[e] = [j, m, p];
                                        break
                                    }
                            } else if (y && (c = (t[F] || (t[F] = {}))[e]) && c[0] === j)
                                p = c[1];
                            else
                                for (; (u = ++m && u && u[f] || (p = m = 0) || h.pop()) && ((s?u.nodeName.toLowerCase() !== v:1 !== u.nodeType) || !++p || (y && ((u[F] || (u[F] = {}))[e] = [j, p]), u !== t)); )
                                    ;
                            return p -= o, p === i || 0 === p % i && p / i >= 0
                        }
                    }
                }, PSEUDO: function(e, n) {
                    var o, a = k.pseudos[e] || k.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return a[F] ? a(n) : a.length > 1 ? (o = [e, e, "", n], k.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function(e, t) {
                        for (var i, o = a(e, n), r = o.length; r--; )
                            i = tt.call(e, o[r]), e[i] = !(t[i] = o[r])
                    }) : function(e) {
                        return a(e, 0, o)
                    }) : a
                }}, pseudos: {not: i(function(e) {
                    var t = [], n = [], o = T(e.replace(lt, "$1"));
                    return o[F] ? i(function(e, t, n, i) {
                        for (var a, r = o(e, null, i, []), s = e.length; s--; )
                            (a = r[s]) && (e[s] = !(t[s] = a))
                    }) : function(e, i, a) {
                        return t[0] = e, o(t, null, a, n), !n.pop()
                    }
                }), has: i(function(e) {
                    return function(n) {
                        return t(e, n).length > 0
                    }
                }), contains: i(function(e) {
                    return function(t) {
                        return(t.textContent || t.innerText || S(t)).indexOf(e) > -1
                    }
                }), lang: i(function(e) {
                    return mt.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(wt, Ct).toLowerCase(), function(t) {
                        var n;
                        do
                            if (n = L ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))
                                return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-");
                        while ((t = t.parentNode) && 1 === t.nodeType);
                        return!1
                    }
                }), target: function(t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                }, root: function(e) {
                    return e === D
                }, focus: function(e) {
                    return e === N.activeElement && (!N.hasFocus || N.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                }, enabled: function(e) {
                    return e.disabled === !1
                }, disabled: function(e) {
                    return e.disabled === !0
                }, checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return"input" === t && !!e.checked || "option" === t && !!e.selected
                }, selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                }, empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeType < 6)
                            return!1;
                    return!0
                }, parent: function(e) {
                    return!k.pseudos.empty(e)
                }, header: function(e) {
                    return gt.test(e.nodeName)
                }, input: function(e) {
                    return ft.test(e.nodeName)
                }, button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return"input" === t && "button" === e.type || "button" === t
                }, text: function(e) {
                    var t;
                    return"input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                }, first: c(function() {
                    return[0]
                }), last: c(function(e, t) {
                    return[t - 1]
                }), eq: c(function(e, t, n) {
                    return[0 > n ? n + t : n]
                }), even: c(function(e, t) {
                    for (var n = 0; t > n; n += 2)
                        e.push(n);
                    return e
                }), odd: c(function(e, t) {
                    for (var n = 1; t > n; n += 2)
                        e.push(n);
                    return e
                }), lt: c(function(e, t, n) {
                    for (var i = 0 > n ? n + t : n; --i >= 0; )
                        e.push(i);
                    return e
                }), gt: c(function(e, t, n) {
                    for (var i = 0 > n ? n + t : n; ++i < t; )
                        e.push(i);
                    return e
                })}}, k.pseudos.nth = k.pseudos.eq;
        for (C in{radio:!0, checkbox:!0, file:!0, password:!0, image:!0})
            k.pseudos[C] = s(C);
        for (C in{submit:!0, reset:!0})
            k.pseudos[C] = l(C);
        return u.prototype = k.filters = k.pseudos, k.setFilters = new u, T = t.compile = function(e, t) {
            var n, i = [], o = [], a = $[e + " "];
            if (!a) {
                for (t || (t = p(e)), n = t.length; n--; )
                    a = y(t[n]), a[F] ? i.push(a) : o.push(a);
                a = $(e, _(o, i))
            }
            return a
        }, x.sortStable = F.split("").sort(U).join("") === F, x.detectDuplicates = !!P, I(), x.sortDetached = o(function(e) {
            return 1 & e.compareDocumentPosition(N.createElement("div"))
        }), o(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || a("type|href|height|width", function(e, t, n) {
            return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), x.attributes && o(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || a("value", function(e, t, n) {
            return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
        }), o(function(e) {
            return null == e.getAttribute("disabled")
        }) || a(nt, function(e, t, n) {
            var i;
            return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), t
    }(e);
    et.find = ot, et.expr = ot.selectors, et.expr[":"] = et.expr.pseudos, et.unique = ot.uniqueSort, et.text = ot.getText, et.isXMLDoc = ot.isXML, et.contains = ot.contains;
    var at = et.expr.match.needsContext, rt = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, st = /^.[^:#\[\.,]*$/;
    et.filter = function(e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? et.find.matchesSelector(i, e) ? [i] : [] : et.find.matches(e, et.grep(t, function(e) {
            return 1 === e.nodeType
        }))
    }, et.fn.extend({find: function(e) {
            var t, n = this.length, i = [], o = this;
            if ("string" != typeof e)
                return this.pushStack(et(e).filter(function() {
                    for (t = 0; n > t; t++)
                        if (et.contains(o[t], this))
                            return!0
                }));
            for (t = 0; n > t; t++)
                et.find(e, o[t], i);
            return i = this.pushStack(n > 1 ? et.unique(i) : i), i.selector = this.selector ? this.selector + " " + e : e, i
        }, filter: function(e) {
            return this.pushStack(i(this, e || [], !1))
        }, not: function(e) {
            return this.pushStack(i(this, e || [], !0))
        }, is: function(e) {
            return!!i(this, "string" == typeof e && at.test(e) ? et(e) : e || [], !1).length
        }});
    var lt, ct = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, dt = et.fn.init = function(e, t) {
        var n, i;
        if (!e)
            return this;
        if ("string" == typeof e) {
            if (n = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : ct.exec(e), !n || !n[1] && t)
                return!t || t.jquery ? (t || lt).find(e) : this.constructor(t).find(e);
            if (n[1]) {
                if (t = t instanceof et ? t[0] : t, et.merge(this, et.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : J, !0)), rt.test(n[1]) && et.isPlainObject(t))
                    for (n in t)
                        et.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                return this
            }
            return i = J.getElementById(n[2]), i && i.parentNode && (this.length = 1, this[0] = i), this.context = J, this.selector = e, this
        }
        return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : et.isFunction(e) ? "undefined" != typeof lt.ready ? lt.ready(e) : e(et) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), et.makeArray(e, this))
    };
    dt.prototype = et.fn, lt = et(J);
    var ut = /^(?:parents|prev(?:Until|All))/, pt = {children: !0, contents: !0, next: !0, prev: !0};
    et.extend({dir: function(e, t, n) {
            for (var i = [], o = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
                if (1 === e.nodeType) {
                    if (o && et(e).is(n))
                        break;
                    i.push(e)
                }
            return i
        }, sibling: function(e, t) {
            for (var n = []; e; e = e.nextSibling)
                1 === e.nodeType && e !== t && n.push(e);
            return n
        }}), et.fn.extend({has: function(e) {
            var t = et(e, this), n = t.length;
            return this.filter(function() {
                for (var e = 0; n > e; e++)
                    if (et.contains(this, t[e]))
                        return!0
            })
        }, closest: function(e, t) {
            for (var n, i = 0, o = this.length, a = [], r = at.test(e) || "string" != typeof e ? et(e, t || this.context) : 0; o > i; i++)
                for (n = this[i]; n && n !== t; n = n.parentNode)
                    if (n.nodeType < 11 && (r ? r.index(n) > -1 : 1 === n.nodeType && et.find.matchesSelector(n, e))) {
                        a.push(n);
                        break
                    }
            return this.pushStack(a.length > 1 ? et.unique(a) : a)
        }, index: function(e) {
            return e ? "string" == typeof e ? U.call(et(e), this[0]) : U.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function(e, t) {
            return this.pushStack(et.unique(et.merge(this.get(), et(e, t))))
        }, addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }}), et.each({parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        }, parents: function(e) {
            return et.dir(e, "parentNode")
        }, parentsUntil: function(e, t, n) {
            return et.dir(e, "parentNode", n)
        }, next: function(e) {
            return o(e, "nextSibling")
        }, prev: function(e) {
            return o(e, "previousSibling")
        }, nextAll: function(e) {
            return et.dir(e, "nextSibling")
        }, prevAll: function(e) {
            return et.dir(e, "previousSibling")
        }, nextUntil: function(e, t, n) {
            return et.dir(e, "nextSibling", n)
        }, prevUntil: function(e, t, n) {
            return et.dir(e, "previousSibling", n)
        }, siblings: function(e) {
            return et.sibling((e.parentNode || {}).firstChild, e)
        }, children: function(e) {
            return et.sibling(e.firstChild)
        }, contents: function(e) {
            return e.contentDocument || et.merge([], e.childNodes)
        }}, function(e, t) {
        et.fn[e] = function(n, i) {
            var o = et.map(this, t, n);
            return"Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (o = et.filter(i, o)), this.length > 1 && (pt[e] || et.unique(o), ut.test(e) && o.reverse()), this.pushStack(o)
        }
    });
    var mt = /\S+/g, ht = {};
    et.Callbacks = function(e) {
        e = "string" == typeof e ? ht[e] || a(e) : et.extend({}, e);
        var t, n, i, o, r, s, l = [], c = !e.once && [], d = function(a) {
            for (t = e.memory && a, n = !0, s = o || 0, o = 0, r = l.length, i = !0; l && r > s; s++)
                if (l[s].apply(a[0], a[1]) === !1 && e.stopOnFalse) {
                    t = !1;
                    break
                }
            i = !1, l && (c ? c.length && d(c.shift()) : t ? l = [] : u.disable())
        }, u = {add: function() {
                if (l) {
                    var n = l.length;
                    !function a(t) {
                        et.each(t, function(t, n) {
                            var i = et.type(n);
                            "function" === i ? e.unique && u.has(n) || l.push(n) : n && n.length && "string" !== i && a(n)
                        })
                    }(arguments), i ? r = l.length : t && (o = n, d(t))
                }
                return this
            }, remove: function() {
                return l && et.each(arguments, function(e, t) {
                    for (var n; (n = et.inArray(t, l, n)) > - 1; )
                        l.splice(n, 1), i && (r >= n && r--, s >= n && s--)
                }), this
            }, has: function(e) {
                return e ? et.inArray(e, l) > -1 : !(!l || !l.length)
            }, empty: function() {
                return l = [], r = 0, this
            }, disable: function() {
                return l = c = t = void 0, this
            }, disabled: function() {
                return!l
            }, lock: function() {
                return c = void 0, t || u.disable(), this
            }, locked: function() {
                return!c
            }, fireWith: function(e, t) {
                return!l || n && !c || (t = t || [], t = [e, t.slice ? t.slice() : t], i ? c.push(t) : d(t)), this
            }, fire: function() {
                return u.fireWith(this, arguments), this
            }, fired: function() {
                return!!n
            }};
        return u
    }, et.extend({Deferred: function(e) {
            var t = [["resolve", "done", et.Callbacks("once memory"), "resolved"], ["reject", "fail", et.Callbacks("once memory"), "rejected"], ["notify", "progress", et.Callbacks("memory")]], n = "pending", i = {state: function() {
                    return n
                }, always: function() {
                    return o.done(arguments).fail(arguments), this
                }, then: function() {
                    var e = arguments;
                    return et.Deferred(function(n) {
                        et.each(t, function(t, a) {
                            var r = et.isFunction(e[t]) && e[t];
                            o[a[1]](function() {
                                var e = r && r.apply(this, arguments);
                                e && et.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a[0] + "With"](this === i ? n.promise() : this, r ? [e] : arguments)
                            })
                        }), e = null
                    }).promise()
                }, promise: function(e) {
                    return null != e ? et.extend(e, i) : i
                }}, o = {};
            return i.pipe = i.then, et.each(t, function(e, a) {
                var r = a[2], s = a[3];
                i[a[1]] = r.add, s && r.add(function() {
                    n = s
                }, t[1 ^ e][2].disable, t[2][2].lock), o[a[0]] = function() {
                    return o[a[0] + "With"](this === o ? i : this, arguments), this
                }, o[a[0] + "With"] = r.fireWith
            }), i.promise(o), e && e.call(o, o), o
        }, when: function(e) {
            var t, n, i, o = 0, a = W.call(arguments), r = a.length, s = 1 !== r || e && et.isFunction(e.promise) ? r : 0, l = 1 === s ? e : et.Deferred(), c = function(e, n, i) {
                return function(o) {
                    n[e] = this, i[e] = arguments.length > 1 ? W.call(arguments) : o, i === t ? l.notifyWith(n, i) : --s || l.resolveWith(n, i)
                }
            };
            if (r > 1)
                for (t = new Array(r), n = new Array(r), i = new Array(r); r > o; o++)
                    a[o] && et.isFunction(a[o].promise) ? a[o].promise().done(c(o, i, a)).fail(l.reject).progress(c(o, n, t)) : --s;
            return s || l.resolveWith(i, a), l.promise()
        }});
    var ft;
    et.fn.ready = function(e) {
        return et.ready.promise().done(e), this
    }, et.extend({isReady: !1, readyWait: 1, holdReady: function(e) {
            e ? et.readyWait++ : et.ready(!0)
        }, ready: function(e) {
            (e === !0 ? --et.readyWait : et.isReady) || (et.isReady = !0, e !== !0 && --et.readyWait > 0 || (ft.resolveWith(J, [et]), et.fn.trigger && et(J).trigger("ready").off("ready")))
        }}), et.ready.promise = function(t) {
        return ft || (ft = et.Deferred(), "complete" === J.readyState ? setTimeout(et.ready) : (J.addEventListener("DOMContentLoaded", r, !1), e.addEventListener("load", r, !1))), ft.promise(t)
    }, et.ready.promise();
    var gt = et.access = function(e, t, n, i, o, a, r) {
        var s = 0, l = e.length, c = null == n;
        if ("object" === et.type(n)) {
            o = !0;
            for (s in n)
                et.access(e, t, s, n[s], !0, a, r)
        } else if (void 0 !== i && (o = !0, et.isFunction(i) || (r = !0), c && (r ? (t.call(e, i), t = null) : (c = t, t = function(e, t, n) {
            return c.call(et(e), n)
        })), t))
            for (; l > s; s++)
                t(e[s], n, r ? i : i.call(e[s], s, t(e[s], n)));
        return o ? e : c ? t.call(e) : l ? t(e[0], n) : a
    };
    et.acceptData = function(e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
    }, s.uid = 1, s.accepts = et.acceptData, s.prototype = {key: function(e) {
            if (!s.accepts(e))
                return 0;
            var t = {}, n = e[this.expando];
            if (!n) {
                n = s.uid++;
                try {
                    t[this.expando] = {value: n}, Object.defineProperties(e, t)
                } catch (i) {
                    t[this.expando] = n, et.extend(e, t)
                }
            }
            return this.cache[n] || (this.cache[n] = {}), n
        }, set: function(e, t, n) {
            var i, o = this.key(e), a = this.cache[o];
            if ("string" == typeof t)
                a[t] = n;
            else if (et.isEmptyObject(a))
                et.extend(this.cache[o], t);
            else
                for (i in t)
                    a[i] = t[i];
            return a
        }, get: function(e, t) {
            var n = this.cache[this.key(e)];
            return void 0 === t ? n : n[t]
        }, access: function(e, t, n) {
            var i;
            return void 0 === t || t && "string" == typeof t && void 0 === n ? (i = this.get(e, t), void 0 !== i ? i : this.get(e, et.camelCase(t))) : (this.set(e, t, n), void 0 !== n ? n : t)
        }, remove: function(e, t) {
            var n, i, o, a = this.key(e), r = this.cache[a];
            if (void 0 === t)
                this.cache[a] = {};
            else {
                et.isArray(t) ? i = t.concat(t.map(et.camelCase)) : (o = et.camelCase(t), t in r ? i = [t, o] : (i = o, i = i in r ? [i] : i.match(mt) || [])), n = i.length;
                for (; n--; )
                    delete r[i[n]]
            }
        }, hasData: function(e) {
            return!et.isEmptyObject(this.cache[e[this.expando]] || {})
        }, discard: function(e) {
            e[this.expando] && delete this.cache[e[this.expando]]
        }};
    var vt = new s, yt = new s, _t = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, bt = /([A-Z])/g;
    et.extend({hasData: function(e) {
            return yt.hasData(e) || vt.hasData(e)
        }, data: function(e, t, n) {
            return yt.access(e, t, n)
        }, removeData: function(e, t) {
            yt.remove(e, t)
        }, _data: function(e, t, n) {
            return vt.access(e, t, n)
        }, _removeData: function(e, t) {
            vt.remove(e, t)
        }}), et.fn.extend({data: function(e, t) {
            var n, i, o, a = this[0], r = a && a.attributes;
            if (void 0 === e) {
                if (this.length && (o = yt.get(a), 1 === a.nodeType && !vt.get(a, "hasDataAttrs"))) {
                    for (n = r.length; n--; )
                        i = r[n].name, 0 === i.indexOf("data-") && (i = et.camelCase(i.slice(5)), l(a, i, o[i]));
                    vt.set(a, "hasDataAttrs", !0)
                }
                return o
            }
            return"object" == typeof e ? this.each(function() {
                yt.set(this, e)
            }) : gt(this, function(t) {
                var n, i = et.camelCase(e);
                if (a && void 0 === t) {
                    if (n = yt.get(a, e), void 0 !== n)
                        return n;
                    if (n = yt.get(a, i), void 0 !== n)
                        return n;
                    if (n = l(a, i, void 0), void 0 !== n)
                        return n
                } else
                    this.each(function() {
                        var n = yt.get(this, i);
                        yt.set(this, i, t), -1 !== e.indexOf("-") && void 0 !== n && yt.set(this, e, t)
                    })
            }, null, t, arguments.length > 1, null, !0)
        }, removeData: function(e) {
            return this.each(function() {
                yt.remove(this, e)
            })
        }}), et.extend({queue: function(e, t, n) {
            var i;
            return e ? (t = (t || "fx") + "queue", i = vt.get(e, t), n && (!i || et.isArray(n) ? i = vt.access(e, t, et.makeArray(n)) : i.push(n)), i || []) : void 0
        }, dequeue: function(e, t) {
            t = t || "fx";
            var n = et.queue(e, t), i = n.length, o = n.shift(), a = et._queueHooks(e, t), r = function() {
                et.dequeue(e, t)
            };
            "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete a.stop, o.call(e, r, a)), !i && a && a.empty.fire()
        }, _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return vt.get(e, n) || vt.access(e, n, {empty: et.Callbacks("once memory").add(function() {
                    vt.remove(e, [t + "queue", n])
                })})
        }}), et.fn.extend({queue: function(e, t) {
            var n = 2;
            return"string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? et.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                var n = et.queue(this, e, t);
                et._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && et.dequeue(this, e)
            })
        }, dequeue: function(e) {
            return this.each(function() {
                et.dequeue(this, e)
            })
        }, clearQueue: function(e) {
            return this.queue(e || "fx", [])
        }, promise: function(e, t) {
            var n, i = 1, o = et.Deferred(), a = this, r = this.length, s = function() {
                --i || o.resolveWith(a, [a])
            };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; r--; )
                n = vt.get(a[r], e + "queueHooks"), n && n.empty && (i++, n.empty.add(s));
            return s(), o.promise(t)
        }});
    var wt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Ct = ["Top", "Right", "Bottom", "Left"], xt = function(e, t) {
        return e = t || e, "none" === et.css(e, "display") || !et.contains(e.ownerDocument, e)
    }, kt = /^(?:checkbox|radio)$/i;
    !function() {
        var e = J.createDocumentFragment(), t = e.appendChild(J.createElement("div"));
        t.innerHTML = "<input type='radio' checked='checked' name='t'/>", X.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", X.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
    }();
    var St = "undefined";
    X.focusinBubbles = "onfocusin"in e;
    var Et = /^key/, Tt = /^(?:mouse|contextmenu)|click/, Mt = /^(?:focusinfocus|focusoutblur)$/, At = /^([^.]*)(?:\.(.+)|)$/;
    et.event = {global: {}, add: function(e, t, n, i, o) {
            var a, r, s, l, c, d, u, p, m, h, f, g = vt.get(e);
            if (g)
                for (n.handler && (a = n, n = a.handler, o = a.selector), n.guid || (n.guid = et.guid++), (l = g.events) || (l = g.events = {}), (r = g.handle) || (r = g.handle = function(t) {
                    return typeof et !== St && et.event.triggered !== t.type ? et.event.dispatch.apply(e, arguments) : void 0
                }), t = (t || "").match(mt) || [""], c = t.length; c--; )
                    s = At.exec(t[c]) || [], m = f = s[1], h = (s[2] || "").split(".").sort(), m && (u = et.event.special[m] || {}, m = (o ? u.delegateType : u.bindType) || m, u = et.event.special[m] || {}, d = et.extend({type: m, origType: f, data: i, handler: n, guid: n.guid, selector: o, needsContext: o && et.expr.match.needsContext.test(o), namespace: h.join(".")}, a), (p = l[m]) || (p = l[m] = [], p.delegateCount = 0, u.setup && u.setup.call(e, i, h, r) !== !1 || e.addEventListener && e.addEventListener(m, r, !1)), u.add && (u.add.call(e, d), d.handler.guid || (d.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, d) : p.push(d), et.event.global[m] = !0)
        }, remove: function(e, t, n, i, o) {
            var a, r, s, l, c, d, u, p, m, h, f, g = vt.hasData(e) && vt.get(e);
            if (g && (l = g.events)) {
                for (t = (t || "").match(mt) || [""], c = t.length; c--; )
                    if (s = At.exec(t[c]) || [], m = f = s[1], h = (s[2] || "").split(".").sort(), m) {
                        for (u = et.event.special[m] || {}, m = (i?u.delegateType:u.bindType) || m, p = l[m] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), r = a = p.length; a--; )
                            d = p[a], !o && f !== d.origType || n && n.guid !== d.guid || s && !s.test(d.namespace) || i && i !== d.selector && ("**" !== i || !d.selector) || (p.splice(a, 1), d.selector && p.delegateCount--, u.remove && u.remove.call(e, d));
                        r && !p.length && (u.teardown && u.teardown.call(e, h, g.handle) !== !1 || et.removeEvent(e, m, g.handle), delete l[m])
                    } else
                        for (m in l)
                            et.event.remove(e, m + t[c], n, i, !0);
                et.isEmptyObject(l) && (delete g.handle, vt.remove(e, "events"))
            }
        }, trigger: function(t, n, i, o) {
            var a, r, s, l, c, d, u, p = [i || J], m = Q.call(t, "type") ? t.type : t, h = Q.call(t, "namespace") ? t.namespace.split(".") : [];
            if (r = s = i = i || J, 3 !== i.nodeType && 8 !== i.nodeType && !Mt.test(m + et.event.triggered) && (m.indexOf(".") >= 0 && (h = m.split("."), m = h.shift(), h.sort()), c = m.indexOf(":") < 0 && "on" + m, t = t[et.expando] ? t : new et.Event(m, "object" == typeof t && t), t.isTrigger = o ? 2 : 3, t.namespace = h.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : et.makeArray(n, [t]), u = et.event.special[m] || {}, o || !u.trigger || u.trigger.apply(i, n) !== !1)) {
                if (!o && !u.noBubble && !et.isWindow(i)) {
                    for (l = u.delegateType || m, Mt.test(l + m) || (r = r.parentNode); r; r = r.parentNode)
                        p.push(r), s = r;
                    s === (i.ownerDocument || J) && p.push(s.defaultView || s.parentWindow || e)
                }
                for (a = 0; (r = p[a++]) && !t.isPropagationStopped(); )
                    t.type = a > 1 ? l : u.bindType || m, d = (vt.get(r, "events") || {})[t.type] && vt.get(r, "handle"), d && d.apply(r, n), d = c && r[c], d && d.apply && et.acceptData(r) && (t.result = d.apply(r, n), t.result === !1 && t.preventDefault());
                return t.type = m, o || t.isDefaultPrevented() || u._default && u._default.apply(p.pop(), n) !== !1 || !et.acceptData(i) || c && et.isFunction(i[m]) && !et.isWindow(i) && (s = i[c], s && (i[c] = null), et.event.triggered = m, i[m](), et.event.triggered = void 0, s && (i[c] = s)), t.result
            }
        }, dispatch: function(e) {
            e = et.event.fix(e);
            var t, n, i, o, a, r = [], s = W.call(arguments), l = (vt.get(this, "events") || {})[e.type] || [], c = et.event.special[e.type] || {};
            if (s[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                for (r = et.event.handlers.call(this, e, l), t = 0; (o = r[t++]) && !e.isPropagationStopped(); )
                    for (e.currentTarget = o.elem, n = 0; (a = o.handlers[n++]) && !e.isImmediatePropagationStopped(); )
                        (!e.namespace_re || e.namespace_re.test(a.namespace)) && (e.handleObj = a, e.data = a.data, i = ((et.event.special[a.origType] || {}).handle || a.handler).apply(o.elem, s), void 0 !== i && (e.result = i) === !1 && (e.preventDefault(), e.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, e), e.result
            }
        }, handlers: function(e, t) {
            var n, i, o, a, r = [], s = t.delegateCount, l = e.target;
            if (s && l.nodeType && (!e.button || "click" !== e.type))
                for (; l !== this; l = l.parentNode || this)
                    if (l.disabled !== !0 || "click" !== e.type) {
                        for (i = [], n = 0; s > n; n++)
                            a = t[n], o = a.selector + " ", void 0 === i[o] && (i[o] = a.needsContext ? et(o, this).index(l) >= 0 : et.find(o, this, null, [l]).length), i[o] && i.push(a);
                        i.length && r.push({elem: l, handlers: i})
                    }
            return s < t.length && r.push({elem: this, handlers: t.slice(s)}), r
        }, props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks: {}, keyHooks: {props: "char charCode key keyCode".split(" "), filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }}, mouseHooks: {props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function(e, t) {
                var n, i, o, a = t.button;
                return null == e.pageX && null != t.clientX && (n = e.target.ownerDocument || J, i = n.documentElement, o = n.body, e.pageX = t.clientX + (i && i.scrollLeft || o && o.scrollLeft || 0) - (i && i.clientLeft || o && o.clientLeft || 0), e.pageY = t.clientY + (i && i.scrollTop || o && o.scrollTop || 0) - (i && i.clientTop || o && o.clientTop || 0)), e.which || void 0 === a || (e.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0), e
            }}, fix: function(e) {
            if (e[et.expando])
                return e;
            var t, n, i, o = e.type, a = e, r = this.fixHooks[o];
            for (r || (this.fixHooks[o] = r = Tt.test(o)?this.mouseHooks:Et.test(o)?this.keyHooks:{}), i = r.props?this.props.concat(r.props):this.props, e = new et.Event(a), t = i.length; t--; )
                n = i[t], e[n] = a[n];
            return e.target || (e.target = J), 3 === e.target.nodeType && (e.target = e.target.parentNode), r.filter ? r.filter(e, a) : e
        }, special: {load: {noBubble: !0}, focus: {trigger: function() {
                    return this !== u() && this.focus ? (this.focus(), !1) : void 0
                }, delegateType: "focusin"}, blur: {trigger: function() {
                    return this === u() && this.blur ? (this.blur(), !1) : void 0
                }, delegateType: "focusout"}, click: {trigger: function() {
                    return"checkbox" === this.type && this.click && et.nodeName(this, "input") ? (this.click(), !1) : void 0
                }, _default: function(e) {
                    return et.nodeName(e.target, "a")
                }}, beforeunload: {postDispatch: function(e) {
                    void 0 !== e.result && (e.originalEvent.returnValue = e.result)
                }}}, simulate: function(e, t, n, i) {
            var o = et.extend(new et.Event, n, {type: e, isSimulated: !0, originalEvent: {}});
            i ? et.event.trigger(o, null, t) : et.event.dispatch.call(t, o), o.isDefaultPrevented() && n.preventDefault()
        }}, et.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    }, et.Event = function(e, t) {
        return this instanceof et.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.getPreventDefault && e.getPreventDefault() ? c : d) : this.type = e, t && et.extend(this, t), this.timeStamp = e && e.timeStamp || et.now(), this[et.expando] = !0, void 0) : new et.Event(e, t)
    }, et.Event.prototype = {isDefaultPrevented: d, isPropagationStopped: d, isImmediatePropagationStopped: d, preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = c, e && e.preventDefault && e.preventDefault()
        }, stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = c, e && e.stopPropagation && e.stopPropagation()
        }, stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = c, this.stopPropagation()
        }}, et.each({mouseenter: "mouseover", mouseleave: "mouseout"}, function(e, t) {
        et.event.special[e] = {delegateType: t, bindType: t, handle: function(e) {
                var n, i = this, o = e.relatedTarget, a = e.handleObj;
                return(!o || o !== i && !et.contains(i, o)) && (e.type = a.origType, n = a.handler.apply(this, arguments), e.type = t), n
            }}
    }), X.focusinBubbles || et.each({focus: "focusin", blur: "focusout"}, function(e, t) {
        var n = function(e) {
            et.event.simulate(t, e.target, et.event.fix(e), !0)
        };
        et.event.special[t] = {setup: function() {
                var i = this.ownerDocument || this, o = vt.access(i, t);
                o || i.addEventListener(e, n, !0), vt.access(i, t, (o || 0) + 1)
            }, teardown: function() {
                var i = this.ownerDocument || this, o = vt.access(i, t) - 1;
                o ? vt.access(i, t, o) : (i.removeEventListener(e, n, !0), vt.remove(i, t))
            }}
    }), et.fn.extend({on: function(e, t, n, i, o) {
            var a, r;
            if ("object" == typeof e) {
                "string" != typeof t && (n = n || t, t = void 0);
                for (r in e)
                    this.on(r, t, n, e[r], o);
                return this
            }
            if (null == n && null == i ? (i = t, n = t = void 0) : null == i && ("string" == typeof t ? (i = n, n = void 0) : (i = n, n = t, t = void 0)), i === !1)
                i = d;
            else if (!i)
                return this;
            return 1 === o && (a = i, i = function(e) {
                return et().off(e), a.apply(this, arguments)
            }, i.guid = a.guid || (a.guid = et.guid++)), this.each(function() {
                et.event.add(this, e, i, n, t)
            })
        }, one: function(e, t, n, i) {
            return this.on(e, t, n, i, 1)
        }, off: function(e, t, n) {
            var i, o;
            if (e && e.preventDefault && e.handleObj)
                return i = e.handleObj, et(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof e) {
                for (o in e)
                    this.off(o, t, e[o]);
                return this
            }
            return(t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = d), this.each(function() {
                et.event.remove(this, e, n, t)
            })
        }, trigger: function(e, t) {
            return this.each(function() {
                et.event.trigger(e, t, this)
            })
        }, triggerHandler: function(e, t) {
            var n = this[0];
            return n ? et.event.trigger(e, t, n, !0) : void 0
        }});
    var Pt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, It = /<([\w:]+)/, Nt = /<|&#?\w+;/, Dt = /<(?:script|style|link)/i, Lt = /checked\s*(?:[^=]|=\s*.checked.)/i, Bt = /^$|\/(?:java|ecma)script/i, Rt = /^true\/(.*)/, Ht = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Ot = {option: [1, "<select multiple='multiple'>", "</select>"], thead: [1, "<table>", "</table>"], col: [2, "<table><colgroup>", "</colgroup></table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: [0, "", ""]};
    Ot.optgroup = Ot.option, Ot.tbody = Ot.tfoot = Ot.colgroup = Ot.caption = Ot.thead, Ot.th = Ot.td, et.extend({clone: function(e, t, n) {
            var i, o, a, r, s = e.cloneNode(!0), l = et.contains(e.ownerDocument, e);
            if (!(X.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || et.isXMLDoc(e)))
                for (r = v(s), a = v(e), i = 0, o = a.length; o > i; i++)
                    y(a[i], r[i]);
            if (t)
                if (n)
                    for (a = a || v(e), r = r || v(s), i = 0, o = a.length; o > i; i++)
                        g(a[i], r[i]);
                else
                    g(e, s);
            return r = v(s, "script"), r.length > 0 && f(r, !l && v(e, "script")), s
        }, buildFragment: function(e, t, n, i) {
            for (var o, a, r, s, l, c, d = t.createDocumentFragment(), u = [], p = 0, m = e.length; m > p; p++)
                if (o = e[p], o || 0 === o)
                    if ("object" === et.type(o))
                        et.merge(u, o.nodeType ? [o] : o);
                    else if (Nt.test(o)) {
                        for (a = a || d.appendChild(t.createElement("div")), r = (It.exec(o) || ["", ""])[1].toLowerCase(), s = Ot[r] || Ot._default, a.innerHTML = s[1] + o.replace(Pt, "<$1></$2>") + s[2], c = s[0]; c--; )
                            a = a.lastChild;
                        et.merge(u, a.childNodes), a = d.firstChild, a.textContent = ""
                    } else
                        u.push(t.createTextNode(o));
            for (d.textContent = "", p = 0; o = u[p++]; )
                if ((!i || -1 === et.inArray(o, i)) && (l = et.contains(o.ownerDocument, o), a = v(d.appendChild(o), "script"), l && f(a), n))
                    for (c = 0; o = a[c++]; )
                        Bt.test(o.type || "") && n.push(o);
            return d
        }, cleanData: function(e) {
            for (var t, n, i, o, a, r, s = et.event.special, l = 0; void 0 !== (n = e[l]); l++) {
                if (et.acceptData(n) && (a = n[vt.expando], a && (t = vt.cache[a]))) {
                    if (i = Object.keys(t.events || {}), i.length)
                        for (r = 0; void 0 !== (o = i[r]); r++)
                            s[o] ? et.event.remove(n, o) : et.removeEvent(n, o, t.handle);
                    vt.cache[a] && delete vt.cache[a]
                }
                delete yt.cache[n[yt.expando]]
            }
        }}), et.fn.extend({text: function(e) {
            return gt(this, function(e) {
                return void 0 === e ? et.text(this) : this.empty().each(function() {
                    (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = e)
                })
            }, null, e, arguments.length)
        }, append: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = p(this, e);
                    t.appendChild(e)
                }
            })
        }, prepend: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = p(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        }, before: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        }, after: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        }, remove: function(e, t) {
            for (var n, i = e ? et.filter(e, this) : this, o = 0; null != (n = i[o]); o++)
                t || 1 !== n.nodeType || et.cleanData(v(n)), n.parentNode && (t && et.contains(n.ownerDocument, n) && f(v(n, "script")), n.parentNode.removeChild(n));
            return this
        }, empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++)
                1 === e.nodeType && (et.cleanData(v(e, !1)), e.textContent = "");
            return this
        }, clone: function(e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function() {
                return et.clone(this, e, t)
            })
        }, html: function(e) {
            return gt(this, function(e) {
                var t = this[0] || {}, n = 0, i = this.length;
                if (void 0 === e && 1 === t.nodeType)
                    return t.innerHTML;
                if ("string" == typeof e && !Dt.test(e) && !Ot[(It.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = e.replace(Pt, "<$1></$2>");
                    try {
                        for (; i > n; n++)
                            t = this[n] || {}, 1 === t.nodeType && (et.cleanData(v(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (o) {
                    }
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function() {
            var e = arguments[0];
            return this.domManip(arguments, function(t) {
                e = this.parentNode, et.cleanData(v(this)), e && e.replaceChild(t, this)
            }), e && (e.length || e.nodeType) ? this : this.remove()
        }, detach: function(e) {
            return this.remove(e, !0)
        }, domManip: function(e, t) {
            e = q.apply([], e);
            var n, i, o, a, r, s, l = 0, c = this.length, d = this, u = c - 1, p = e[0], f = et.isFunction(p);
            if (f || c > 1 && "string" == typeof p && !X.checkClone && Lt.test(p))
                return this.each(function(n) {
                    var i = d.eq(n);
                    f && (e[0] = p.call(this, n, i.html())), i.domManip(e, t)
                });
            if (c && (n = et.buildFragment(e, this[0].ownerDocument, !1, this), i = n.firstChild, 1 === n.childNodes.length && (n = i), i)) {
                for (o = et.map(v(n, "script"), m), a = o.length; c > l; l++)
                    r = n, l !== u && (r = et.clone(r, !0, !0), a && et.merge(o, v(r, "script"))), t.call(this[l], r, l);
                if (a)
                    for (s = o[o.length - 1].ownerDocument, et.map(o, h), l = 0; a > l; l++)
                        r = o[l], Bt.test(r.type || "") && !vt.access(r, "globalEval") && et.contains(s, r) && (r.src ? et._evalUrl && et._evalUrl(r.src) : et.globalEval(r.textContent.replace(Ht, "")))
            }
            return this
        }}), et.each({appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith"}, function(e, t) {
        et.fn[e] = function(e) {
            for (var n, i = [], o = et(e), a = o.length - 1, r = 0; a >= r; r++)
                n = r === a ? this : this.clone(!0), et(o[r])[t](n), $.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var Ft, zt = {}, jt = /^margin/, Vt = new RegExp("^(" + wt + ")(?!px)[a-z%]+$", "i"), Wt = function(e) {
        return e.ownerDocument.defaultView.getComputedStyle(e, null)
    };
    !function() {
        function t() {
            s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%", a.appendChild(r);
            var t = e.getComputedStyle(s, null);
            n = "1%" !== t.top, i = "4px" === t.width, a.removeChild(r)
        }
        var n, i, o = "padding:0;margin:0;border:0;display:block;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box", a = J.documentElement, r = J.createElement("div"), s = J.createElement("div");
        s.style.backgroundClip = "content-box", s.cloneNode(!0).style.backgroundClip = "", X.clearCloneStyle = "content-box" === s.style.backgroundClip, r.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", r.appendChild(s), e.getComputedStyle && et.extend(X, {pixelPosition: function() {
                return t(), n
            }, boxSizingReliable: function() {
                return null == i && t(), i
            }, reliableMarginRight: function() {
                var t, n = s.appendChild(J.createElement("div"));
                return n.style.cssText = s.style.cssText = o, n.style.marginRight = n.style.width = "0", s.style.width = "1px", a.appendChild(r), t = !parseFloat(e.getComputedStyle(n, null).marginRight), a.removeChild(r), s.innerHTML = "", t
            }})
    }(), et.swap = function(e, t, n, i) {
        var o, a, r = {};
        for (a in t)
            r[a] = e.style[a], e.style[a] = t[a];
        o = n.apply(e, i || []);
        for (a in t)
            e.style[a] = r[a];
        return o
    };
    var qt = /^(none|table(?!-c[ea]).+)/, $t = new RegExp("^(" + wt + ")(.*)$", "i"), Ut = new RegExp("^([+-])=(" + wt + ")", "i"), Kt = {position: "absolute", visibility: "hidden", display: "block"}, Yt = {letterSpacing: 0, fontWeight: 400}, Qt = ["Webkit", "O", "Moz", "ms"];
    et.extend({cssHooks: {opacity: {get: function(e, t) {
                    if (t) {
                        var n = w(e, "opacity");
                        return"" === n ? "1" : n
                    }
                }}}, cssNumber: {columnCount: !0, fillOpacity: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0}, cssProps: {"float": "cssFloat"}, style: function(e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, a, r, s = et.camelCase(t), l = e.style;
                return t = et.cssProps[s] || (et.cssProps[s] = x(l, s)), r = et.cssHooks[t] || et.cssHooks[s], void 0 === n ? r && "get"in r && void 0 !== (o = r.get(e, !1, i)) ? o : l[t] : (a = typeof n, "string" === a && (o = Ut.exec(n)) && (n = (o[1] + 1) * o[2] + parseFloat(et.css(e, t)), a = "number"), null != n && n === n && ("number" !== a || et.cssNumber[s] || (n += "px"), X.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), r && "set"in r && void 0 === (n = r.set(e, n, i)) || (l[t] = "", l[t] = n)), void 0)
            }
        }, css: function(e, t, n, i) {
            var o, a, r, s = et.camelCase(t);
            return t = et.cssProps[s] || (et.cssProps[s] = x(e.style, s)), r = et.cssHooks[t] || et.cssHooks[s], r && "get"in r && (o = r.get(e, !0, n)), void 0 === o && (o = w(e, t, i)), "normal" === o && t in Yt && (o = Yt[t]), "" === n || n ? (a = parseFloat(o), n === !0 || et.isNumeric(a) ? a || 0 : o) : o
        }}), et.each(["height", "width"], function(e, t) {
        et.cssHooks[t] = {get: function(e, n, i) {
                return n ? 0 === e.offsetWidth && qt.test(et.css(e, "display")) ? et.swap(e, Kt, function() {
                    return E(e, t, i)
                }) : E(e, t, i) : void 0
            }, set: function(e, n, i) {
                var o = i && Wt(e);
                return k(e, n, i ? S(e, t, i, "border-box" === et.css(e, "boxSizing", !1, o), o) : 0)
            }}
    }), et.cssHooks.marginRight = C(X.reliableMarginRight, function(e, t) {
        return t ? et.swap(e, {display: "inline-block"}, w, [e, "marginRight"]) : void 0
    }), et.each({margin: "", padding: "", border: "Width"}, function(e, t) {
        et.cssHooks[e + t] = {expand: function(n) {
                for (var i = 0, o = {}, a = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++)
                    o[e + Ct[i] + t] = a[i] || a[i - 2] || a[0];
                return o
            }}, jt.test(e) || (et.cssHooks[e + t].set = k)
    }), et.fn.extend({css: function(e, t) {
            return gt(this, function(e, t, n) {
                var i, o, a = {}, r = 0;
                if (et.isArray(t)) {
                    for (i = Wt(e), o = t.length; o > r; r++)
                        a[t[r]] = et.css(e, t[r], !1, i);
                    return a
                }
                return void 0 !== n ? et.style(e, t, n) : et.css(e, t)
            }, e, t, arguments.length > 1)
        }, show: function() {
            return T(this, !0)
        }, hide: function() {
            return T(this)
        }, toggle: function(e) {
            return"boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                xt(this) ? et(this).show() : et(this).hide()
            })
        }}), et.Tween = M, M.prototype = {constructor: M, init: function(e, t, n, i, o, a) {
            this.elem = e, this.prop = n, this.easing = o || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = a || (et.cssNumber[n] ? "" : "px")
        }, cur: function() {
            var e = M.propHooks[this.prop];
            return e && e.get ? e.get(this) : M.propHooks._default.get(this)
        }, run: function(e) {
            var t, n = M.propHooks[this.prop];
            return this.pos = t = this.options.duration ? et.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : M.propHooks._default.set(this), this
        }}, M.prototype.init.prototype = M.prototype, M.propHooks = {_default: {get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = et.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            }, set: function(e) {
                et.fx.step[e.prop] ? et.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[et.cssProps[e.prop]] || et.cssHooks[e.prop]) ? et.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }}}, M.propHooks.scrollTop = M.propHooks.scrollLeft = {set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }}, et.easing = {linear: function(e) {
            return e
        }, swing: function(e) {
            return.5 - Math.cos(e * Math.PI) / 2
        }}, et.fx = M.prototype.init, et.fx.step = {};
    var Gt, Xt, Jt = /^(?:toggle|show|hide)$/, Zt = new RegExp("^(?:([+-])=|)(" + wt + ")([a-z%]*)$", "i"), en = /queueHooks$/, tn = [N], nn = {"*": [function(e, t) {
                var n = this.createTween(e, t), i = n.cur(), o = Zt.exec(t), a = o && o[3] || (et.cssNumber[e] ? "" : "px"), r = (et.cssNumber[e] || "px" !== a && +i) && Zt.exec(et.css(n.elem, e)), s = 1, l = 20;
                if (r && r[3] !== a) {
                    a = a || r[3], o = o || [], r = +i || 1;
                    do
                        s = s || ".5", r /= s, et.style(n.elem, e, r + a);
                    while (s !== (s = n.cur() / i) && 1 !== s && --l)
                }
                return o && (r = n.start = +r || +i || 0, n.unit = a, n.end = o[1] ? r + (o[1] + 1) * o[2] : +o[2]), n
            }]};
    et.Animation = et.extend(L, {tweener: function(e, t) {
            et.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            for (var n, i = 0, o = e.length; o > i; i++)
                n = e[i], nn[n] = nn[n] || [], nn[n].unshift(t)
        }, prefilter: function(e, t) {
            t ? tn.unshift(e) : tn.push(e)
        }}), et.speed = function(e, t, n) {
        var i = e && "object" == typeof e ? et.extend({}, e) : {complete: n || !n && t || et.isFunction(e) && e, duration: e, easing: n && t || t && !et.isFunction(t) && t};
        return i.duration = et.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in et.fx.speeds ? et.fx.speeds[i.duration] : et.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function() {
            et.isFunction(i.old) && i.old.call(this), i.queue && et.dequeue(this, i.queue)
        }, i
    }, et.fn.extend({fadeTo: function(e, t, n, i) {
            return this.filter(xt).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
        }, animate: function(e, t, n, i) {
            var o = et.isEmptyObject(e), a = et.speed(t, n, i), r = function() {
                var t = L(this, et.extend({}, e), a);
                (o || vt.get(this, "finish")) && t.stop(!0)
            };
            return r.finish = r, o || a.queue === !1 ? this.each(r) : this.queue(a.queue, r)
        }, stop: function(e, t, n) {
            var i = function(e) {
                var t = e.stop;
                delete e.stop, t(n)
            };
            return"string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                var t = !0, o = null != e && e + "queueHooks", a = et.timers, r = vt.get(this);
                if (o)
                    r[o] && r[o].stop && i(r[o]);
                else
                    for (o in r)
                        r[o] && r[o].stop && en.test(o) && i(r[o]);
                for (o = a.length; o--; )
                    a[o].elem !== this || null != e && a[o].queue !== e || (a[o].anim.stop(n), t = !1, a.splice(o, 1));
                (t || !n) && et.dequeue(this, e)
            })
        }, finish: function(e) {
            return e !== !1 && (e = e || "fx"), this.each(function() {
                var t, n = vt.get(this), i = n[e + "queue"], o = n[e + "queueHooks"], a = et.timers, r = i ? i.length : 0;
                for (n.finish = !0, et.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = a.length; t--; )
                    a[t].elem === this && a[t].queue === e && (a[t].anim.stop(!0), a.splice(t, 1));
                for (t = 0; r > t; t++)
                    i[t] && i[t].finish && i[t].finish.call(this);
                delete n.finish
            })
        }}), et.each(["toggle", "show", "hide"], function(e, t) {
        var n = et.fn[t];
        et.fn[t] = function(e, i, o) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(P(t, !0), e, i, o)
        }
    }), et.each({slideDown: P("show"), slideUp: P("hide"), slideToggle: P("toggle"), fadeIn: {opacity: "show"}, fadeOut: {opacity: "hide"}, fadeToggle: {opacity: "toggle"}}, function(e, t) {
        et.fn[e] = function(e, n, i) {
            return this.animate(t, e, n, i)
        }
    }), et.timers = [], et.fx.tick = function() {
        var e, t = 0, n = et.timers;
        for (Gt = et.now(); t < n.length; t++)
            e = n[t], e() || n[t] !== e || n.splice(t--, 1);
        n.length || et.fx.stop(), Gt = void 0
    }, et.fx.timer = function(e) {
        et.timers.push(e), e() ? et.fx.start() : et.timers.pop()
    }, et.fx.interval = 13, et.fx.start = function() {
        Xt || (Xt = setInterval(et.fx.tick, et.fx.interval))
    }, et.fx.stop = function() {
        clearInterval(Xt), Xt = null
    }, et.fx.speeds = {slow: 600, fast: 200, _default: 400}, et.fn.delay = function(e, t) {
        return e = et.fx ? et.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
            var i = setTimeout(t, e);
            n.stop = function() {
                clearTimeout(i)
            }
        })
    }, function() {
        var e = J.createElement("input"), t = J.createElement("select"), n = t.appendChild(J.createElement("option"));
        e.type = "checkbox", X.checkOn = "" !== e.value, X.optSelected = n.selected, t.disabled = !0, X.optDisabled = !n.disabled, e = J.createElement("input"), e.value = "t", e.type = "radio", X.radioValue = "t" === e.value
    }();
    var on, an, rn = et.expr.attrHandle;
    et.fn.extend({attr: function(e, t) {
            return gt(this, et.attr, e, t, arguments.length > 1)
        }, removeAttr: function(e) {
            return this.each(function() {
                et.removeAttr(this, e)
            })
        }}), et.extend({attr: function(e, t, n) {
            var i, o, a = e.nodeType;
            if (e && 3 !== a && 8 !== a && 2 !== a)
                return typeof e.getAttribute === St ? et.prop(e, t, n) : (1 === a && et.isXMLDoc(e) || (t = t.toLowerCase(), i = et.attrHooks[t] || (et.expr.match.bool.test(t) ? an : on)), void 0 === n ? i && "get"in i && null !== (o = i.get(e, t)) ? o : (o = et.find.attr(e, t), null == o ? void 0 : o) : null !== n ? i && "set"in i && void 0 !== (o = i.set(e, n, t)) ? o : (e.setAttribute(t, n + ""), n) : (et.removeAttr(e, t), void 0))
        }, removeAttr: function(e, t) {
            var n, i, o = 0, a = t && t.match(mt);
            if (a && 1 === e.nodeType)
                for (; n = a[o++]; )
                    i = et.propFix[n] || n, et.expr.match.bool.test(n) && (e[i] = !1), e.removeAttribute(n)
        }, attrHooks: {type: {set: function(e, t) {
                    if (!X.radioValue && "radio" === t && et.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }}}}), an = {set: function(e, t, n) {
            return t === !1 ? et.removeAttr(e, n) : e.setAttribute(n, n), n
        }}, et.each(et.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = rn[t] || et.find.attr;
        rn[t] = function(e, t, i) {
            var o, a;
            return i || (a = rn[t], rn[t] = o, o = null != n(e, t, i) ? t.toLowerCase() : null, rn[t] = a), o
        }
    });
    var sn = /^(?:input|select|textarea|button)$/i;
    et.fn.extend({prop: function(e, t) {
            return gt(this, et.prop, e, t, arguments.length > 1)
        }, removeProp: function(e) {
            return this.each(function() {
                delete this[et.propFix[e] || e]
            })
        }}), et.extend({propFix: {"for": "htmlFor", "class": "className"}, prop: function(e, t, n) {
            var i, o, a, r = e.nodeType;
            if (e && 3 !== r && 8 !== r && 2 !== r)
                return a = 1 !== r || !et.isXMLDoc(e), a && (t = et.propFix[t] || t, o = et.propHooks[t]), void 0 !== n ? o && "set"in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get"in o && null !== (i = o.get(e, t)) ? i : e[t]
        }, propHooks: {tabIndex: {get: function(e) {
                    return e.hasAttribute("tabindex") || sn.test(e.nodeName) || e.href ? e.tabIndex : -1
                }}}}), X.optSelected || (et.propHooks.selected = {get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        }}), et.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        et.propFix[this.toLowerCase()] = this
    });
    var ln = /[\t\r\n\f]/g;
    et.fn.extend({addClass: function(e) {
            var t, n, i, o, a, r, s = "string" == typeof e && e, l = 0, c = this.length;
            if (et.isFunction(e))
                return this.each(function(t) {
                    et(this).addClass(e.call(this, t, this.className))
                });
            if (s)
                for (t = (e || "").match(mt) || []; c > l; l++)
                    if (n = this[l], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ln, " ") : " ")) {
                        for (a = 0; o = t[a++]; )
                            i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                        r = et.trim(i), n.className !== r && (n.className = r)
                    }
            return this
        }, removeClass: function(e) {
            var t, n, i, o, a, r, s = 0 === arguments.length || "string" == typeof e && e, l = 0, c = this.length;
            if (et.isFunction(e))
                return this.each(function(t) {
                    et(this).removeClass(e.call(this, t, this.className))
                });
            if (s)
                for (t = (e || "").match(mt) || []; c > l; l++)
                    if (n = this[l], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ln, " ") : "")) {
                        for (a = 0; o = t[a++]; )
                            for (; i.indexOf(" " + o + " ") >= 0; )
                                i = i.replace(" " + o + " ", " ");
                        r = e ? et.trim(i) : "", n.className !== r && (n.className = r)
                    }
            return this
        }, toggleClass: function(e, t) {
            var n = typeof e;
            return"boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : et.isFunction(e) ? this.each(function(n) {
                et(this).toggleClass(e.call(this, n, this.className, t), t)
            }) : this.each(function() {
                if ("string" === n)
                    for (var t, i = 0, o = et(this), a = e.match(mt) || []; t = a[i++]; )
                        o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                else
                    (n === St || "boolean" === n) && (this.className && vt.set(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : vt.get(this, "__className__") || "")
            })
        }, hasClass: function(e) {
            for (var t = " " + e + " ", n = 0, i = this.length; i > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(ln, " ").indexOf(t) >= 0)
                    return!0;
            return!1
        }});
    var cn = /\r/g;
    et.fn.extend({val: function(e) {
            var t, n, i, o = this[0];
            {
                if (arguments.length)
                    return i = et.isFunction(e), this.each(function(n) {
                        var o;
                        1 === this.nodeType && (o = i ? e.call(this, n, et(this).val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : et.isArray(o) && (o = et.map(o, function(e) {
                            return null == e ? "" : e + ""
                        })), t = et.valHooks[this.type] || et.valHooks[this.nodeName.toLowerCase()], t && "set"in t && void 0 !== t.set(this, o, "value") || (this.value = o))
                    });
                if (o)
                    return t = et.valHooks[o.type] || et.valHooks[o.nodeName.toLowerCase()], t && "get"in t && void 0 !== (n = t.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(cn, "") : null == n ? "" : n)
            }
        }}), et.extend({valHooks: {select: {get: function(e) {
                    for (var t, n, i = e.options, o = e.selectedIndex, a = "select-one" === e.type || 0 > o, r = a ? null : [], s = a ? o + 1 : i.length, l = 0 > o ? s : a ? o : 0; s > l; l++)
                        if (n = i[l], !(!n.selected && l !== o || (X.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && et.nodeName(n.parentNode, "optgroup"))) {
                            if (t = et(n).val(), a)
                                return t;
                            r.push(t)
                        }
                    return r
                }, set: function(e, t) {
                    for (var n, i, o = e.options, a = et.makeArray(t), r = o.length; r--; )
                        i = o[r], (i.selected = et.inArray(et(i).val(), a) >= 0) && (n = !0);
                    return n || (e.selectedIndex = -1), a
                }}}}), et.each(["radio", "checkbox"], function() {
        et.valHooks[this] = {set: function(e, t) {
                return et.isArray(t) ? e.checked = et.inArray(et(e).val(), t) >= 0 : void 0
            }}, X.checkOn || (et.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    }), et.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        et.fn[t] = function(e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), et.fn.extend({hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }, bind: function(e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function(e, t) {
            return this.off(e, null, t)
        }, delegate: function(e, t, n, i) {
            return this.on(t, e, n, i)
        }, undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }});
    var dn = et.now(), un = /\?/;
    et.parseJSON = function(e) {
        return JSON.parse(e + "")
    }, et.parseXML = function(e) {
        var t, n;
        if (!e || "string" != typeof e)
            return null;
        try {
            n = new DOMParser, t = n.parseFromString(e, "text/xml")
        } catch (i) {
            t = void 0
        }
        return(!t || t.getElementsByTagName("parsererror").length) && et.error("Invalid XML: " + e), t
    };
    var pn, mn, hn = /#.*$/, fn = /([?&])_=[^&]*/, gn = /^(.*?):[ \t]*([^\r\n]*)$/gm, vn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, yn = /^(?:GET|HEAD)$/, _n = /^\/\//, bn = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, wn = {}, Cn = {}, xn = "*/".concat("*");
    try {
        mn = location.href
    } catch (kn) {
        mn = J.createElement("a"), mn.href = "", mn = mn.href
    }
    pn = bn.exec(mn.toLowerCase()) || [], et.extend({active: 0, lastModified: {}, etag: {}, ajaxSettings: {url: mn, type: "GET", isLocal: vn.test(pn[1]), global: !0, processData: !0, async: !0, contentType: "application/x-www-form-urlencoded; charset=UTF-8", accepts: {"*": xn, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript"}, contents: {xml: /xml/, html: /html/, json: /json/}, responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"}, converters: {"* text": String, "text html": !0, "text json": et.parseJSON, "text xml": et.parseXML}, flatOptions: {url: !0, context: !0}}, ajaxSetup: function(e, t) {
            return t ? H(H(e, et.ajaxSettings), t) : H(et.ajaxSettings, e)
        }, ajaxPrefilter: B(wn), ajaxTransport: B(Cn), ajax: function(e, t) {
            function n(e, t, n, r) {
                var l, d, v, y, b, C = t;
                2 !== _ && (_ = 2, s && clearTimeout(s), i = void 0, a = r || "", w.readyState = e > 0 ? 4 : 0, l = e >= 200 && 300 > e || 304 === e, n && (y = O(u, w, n)), y = F(u, y, w, l), l ? (u.ifModified && (b = w.getResponseHeader("Last-Modified"), b && (et.lastModified[o] = b), b = w.getResponseHeader("etag"), b && (et.etag[o] = b)), 204 === e || "HEAD" === u.type ? C = "nocontent" : 304 === e ? C = "notmodified" : (C = y.state, d = y.data, v = y.error, l = !v)) : (v = C, (e || !C) && (C = "error", 0 > e && (e = 0))), w.status = e, w.statusText = (t || C) + "", l ? h.resolveWith(p, [d, C, w]) : h.rejectWith(p, [w, C, v]), w.statusCode(g), g = void 0, c && m.trigger(l ? "ajaxSuccess" : "ajaxError", [w, u, l ? d : v]), f.fireWith(p, [w, C]), c && (m.trigger("ajaxComplete", [w, u]), --et.active || et.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var i, o, a, r, s, l, c, d, u = et.ajaxSetup({}, t), p = u.context || u, m = u.context && (p.nodeType || p.jquery) ? et(p) : et.event, h = et.Deferred(), f = et.Callbacks("once memory"), g = u.statusCode || {}, v = {}, y = {}, _ = 0, b = "canceled", w = {readyState: 0, getResponseHeader: function(e) {
                    var t;
                    if (2 === _) {
                        if (!r)
                            for (r = {}; t = gn.exec(a); )
                                r[t[1].toLowerCase()] = t[2];
                        t = r[e.toLowerCase()]
                    }
                    return null == t ? null : t
                }, getAllResponseHeaders: function() {
                    return 2 === _ ? a : null
                }, setRequestHeader: function(e, t) {
                    var n = e.toLowerCase();
                    return _ || (e = y[n] = y[n] || e, v[e] = t), this
                }, overrideMimeType: function(e) {
                    return _ || (u.mimeType = e), this
                }, statusCode: function(e) {
                    var t;
                    if (e)
                        if (2 > _)
                            for (t in e)
                                g[t] = [g[t], e[t]];
                        else
                            w.always(e[w.status]);
                    return this
                }, abort: function(e) {
                    var t = e || b;
                    return i && i.abort(t), n(0, t), this
                }};
            if (h.promise(w).complete = f.add, w.success = w.done, w.error = w.fail, u.url = ((e || u.url || mn) + "").replace(hn, "").replace(_n, pn[1] + "//"), u.type = t.method || t.type || u.method || u.type, u.dataTypes = et.trim(u.dataType || "*").toLowerCase().match(mt) || [""], null == u.crossDomain && (l = bn.exec(u.url.toLowerCase()), u.crossDomain = !(!l || l[1] === pn[1] && l[2] === pn[2] && (l[3] || ("http:" === l[1] ? "80" : "443")) === (pn[3] || ("http:" === pn[1] ? "80" : "443")))), u.data && u.processData && "string" != typeof u.data && (u.data = et.param(u.data, u.traditional)), R(wn, u, t, w), 2 === _)
                return w;
            c = u.global, c && 0 === et.active++ && et.event.trigger("ajaxStart"), u.type = u.type.toUpperCase(), u.hasContent = !yn.test(u.type), o = u.url, u.hasContent || (u.data && (o = u.url += (un.test(o) ? "&" : "?") + u.data, delete u.data), u.cache === !1 && (u.url = fn.test(o) ? o.replace(fn, "$1_=" + dn++) : o + (un.test(o) ? "&" : "?") + "_=" + dn++)), u.ifModified && (et.lastModified[o] && w.setRequestHeader("If-Modified-Since", et.lastModified[o]), et.etag[o] && w.setRequestHeader("If-None-Match", et.etag[o])), (u.data && u.hasContent && u.contentType !== !1 || t.contentType) && w.setRequestHeader("Content-Type", u.contentType), w.setRequestHeader("Accept", u.dataTypes[0] && u.accepts[u.dataTypes[0]] ? u.accepts[u.dataTypes[0]] + ("*" !== u.dataTypes[0] ? ", " + xn + "; q=0.01" : "") : u.accepts["*"]);
            for (d in u.headers)
                w.setRequestHeader(d, u.headers[d]);
            if (u.beforeSend && (u.beforeSend.call(p, w, u) === !1 || 2 === _))
                return w.abort();
            b = "abort";
            for (d in{success:1, error:1, complete:1})
                w[d](u[d]);
            if (i = R(Cn, u, t, w)) {
                w.readyState = 1, c && m.trigger("ajaxSend", [w, u]), u.async && u.timeout > 0 && (s = setTimeout(function() {
                    w.abort("timeout")
                }, u.timeout));
                try {
                    _ = 1, i.send(v, n)
                } catch (C) {
                    if (!(2 > _))
                        throw C;
                    n(-1, C)
                }
            } else
                n(-1, "No Transport");
            return w
        }, getJSON: function(e, t, n) {
            return et.get(e, t, n, "json")
        }, getScript: function(e, t) {
            return et.get(e, void 0, t, "script")
        }}), et.each(["get", "post"], function(e, t) {
        et[t] = function(e, n, i, o) {
            return et.isFunction(n) && (o = o || i, i = n, n = void 0), et.ajax({url: e, type: t, dataType: o, data: n, success: i})
        }
    }), et.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        et.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), et._evalUrl = function(e) {
        return et.ajax({url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
    }, et.fn.extend({wrapAll: function(e) {
            var t;
            return et.isFunction(e) ? this.each(function(t) {
                et(this).wrapAll(e.call(this, t))
            }) : (this[0] && (t = et(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                for (var e = this; e.firstElementChild; )
                    e = e.firstElementChild;
                return e
            }).append(this)), this)
        }, wrapInner: function(e) {
            return et.isFunction(e) ? this.each(function(t) {
                et(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = et(this), n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        }, wrap: function(e) {
            var t = et.isFunction(e);
            return this.each(function(n) {
                et(this).wrapAll(t ? e.call(this, n) : e)
            })
        }, unwrap: function() {
            return this.parent().each(function() {
                et.nodeName(this, "body") || et(this).replaceWith(this.childNodes)
            }).end()
        }}), et.expr.filters.hidden = function(e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0
    }, et.expr.filters.visible = function(e) {
        return!et.expr.filters.hidden(e)
    };
    var Sn = /%20/g, En = /\[\]$/, Tn = /\r?\n/g, Mn = /^(?:submit|button|image|reset|file)$/i, An = /^(?:input|select|textarea|keygen)/i;
    et.param = function(e, t) {
        var n, i = [], o = function(e, t) {
            t = et.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
        };
        if (void 0 === t && (t = et.ajaxSettings && et.ajaxSettings.traditional), et.isArray(e) || e.jquery && !et.isPlainObject(e))
            et.each(e, function() {
                o(this.name, this.value)
            });
        else
            for (n in e)
                z(n, e[n], t, o);
        return i.join("&").replace(Sn, "+")
    }, et.fn.extend({serialize: function() {
            return et.param(this.serializeArray())
        }, serializeArray: function() {
            return this.map(function() {
                var e = et.prop(this, "elements");
                return e ? et.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !et(this).is(":disabled") && An.test(this.nodeName) && !Mn.test(e) && (this.checked || !kt.test(e))
            }).map(function(e, t) {
                var n = et(this).val();
                return null == n ? null : et.isArray(n) ? et.map(n, function(e) {
                    return{name: t.name, value: e.replace(Tn, "\r\n")}
                }) : {name: t.name, value: n.replace(Tn, "\r\n")}
            }).get()
        }}), et.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest
        } catch (e) {
        }
    };
    var Pn = 0, In = {}, Nn = {0: 200, 1223: 204}, Dn = et.ajaxSettings.xhr();
    e.ActiveXObject && et(e).on("unload", function() {
        for (var e in In)
            In[e]()
    }), X.cors = !!Dn && "withCredentials"in Dn, X.ajax = Dn = !!Dn, et.ajaxTransport(function(e) {
        var t;
        return X.cors || Dn && !e.crossDomain ? {send: function(n, i) {
                var o, a = e.xhr(), r = ++Pn;
                if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                    for (o in e.xhrFields)
                        a[o] = e.xhrFields[o];
                e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                for (o in n)
                    a.setRequestHeader(o, n[o]);
                t = function(e) {
                    return function() {
                        t && (delete In[r], t = a.onload = a.onerror = null, "abort" === e ? a.abort() : "error" === e ? i(a.status, a.statusText) : i(Nn[a.status] || a.status, a.statusText, "string" == typeof a.responseText ? {text: a.responseText} : void 0, a.getAllResponseHeaders()))
                    }
                }, a.onload = t(), a.onerror = t("error"), t = In[r] = t("abort"), a.send(e.hasContent && e.data || null)
            }, abort: function() {
                t && t()
            }} : void 0
    }), et.ajaxSetup({accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"}, contents: {script: /(?:java|ecma)script/}, converters: {"text script": function(e) {
                return et.globalEval(e), e
            }}}), et.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), et.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var t, n;
            return{send: function(i, o) {
                    t = et("<script>").prop({async: !0, charset: e.scriptCharset, src: e.url}).on("load error", n = function(e) {
                        t.remove(), n = null, e && o("error" === e.type ? 404 : 200, e.type)
                    }), J.head.appendChild(t[0])
                }, abort: function() {
                    n && n()
                }}
        }
    });
    var Ln = [], Bn = /(=)\?(?=&|$)|\?\?/;
    et.ajaxSetup({jsonp: "callback", jsonpCallback: function() {
            var e = Ln.pop() || et.expando + "_" + dn++;
            return this[e] = !0, e
        }}), et.ajaxPrefilter("json jsonp", function(t, n, i) {
        var o, a, r, s = t.jsonp !== !1 && (Bn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && Bn.test(t.data) && "data");
        return s || "jsonp" === t.dataTypes[0] ? (o = t.jsonpCallback = et.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(Bn, "$1" + o) : t.jsonp !== !1 && (t.url += (un.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function() {
            return r || et.error(o + " was not called"), r[0]
        }, t.dataTypes[0] = "json", a = e[o], e[o] = function() {
            r = arguments
        }, i.always(function() {
            e[o] = a, t[o] && (t.jsonpCallback = n.jsonpCallback, Ln.push(o)), r && et.isFunction(a) && a(r[0]), r = a = void 0
        }), "script") : void 0
    }), et.parseHTML = function(e, t, n) {
        if (!e || "string" != typeof e)
            return null;
        "boolean" == typeof t && (n = t, t = !1), t = t || J;
        var i = rt.exec(e), o = !n && [];
        return i ? [t.createElement(i[1])] : (i = et.buildFragment([e], t, o), o && o.length && et(o).remove(), et.merge([], i.childNodes))
    };
    var Rn = et.fn.load;
    et.fn.load = function(e, t, n) {
        if ("string" != typeof e && Rn)
            return Rn.apply(this, arguments);
        var i, o, a, r = this, s = e.indexOf(" ");
        return s >= 0 && (i = e.slice(s), e = e.slice(0, s)), et.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), r.length > 0 && et.ajax({url: e, type: o, dataType: "html", data: t}).done(function(e) {
            a = arguments, r.html(i ? et("<div>").append(et.parseHTML(e)).find(i) : e)
        }).complete(n && function(e, t) {
            r.each(n, a || [e.responseText, t, e])
        }), this
    }, et.expr.filters.animated = function(e) {
        return et.grep(et.timers, function(t) {
            return e === t.elem
        }).length
    };
    var Hn = e.document.documentElement;
    et.offset = {setOffset: function(e, t, n) {
            var i, o, a, r, s, l, c, d = et.css(e, "position"), u = et(e), p = {};
            "static" === d && (e.style.position = "relative"), s = u.offset(), a = et.css(e, "top"), l = et.css(e, "left"), c = ("absolute" === d || "fixed" === d) && (a + l).indexOf("auto") > -1, c ? (i = u.position(), r = i.top, o = i.left) : (r = parseFloat(a) || 0, o = parseFloat(l) || 0), et.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (p.top = t.top - s.top + r), null != t.left && (p.left = t.left - s.left + o), "using"in t ? t.using.call(e, p) : u.css(p)
        }}, et.fn.extend({offset: function(e) {
            if (arguments.length)
                return void 0 === e ? this : this.each(function(t) {
                    et.offset.setOffset(this, e, t)
                });
            var t, n, i = this[0], o = {top: 0, left: 0}, a = i && i.ownerDocument;
            if (a)
                return t = a.documentElement, et.contains(t, i) ? (typeof i.getBoundingClientRect !== St && (o = i.getBoundingClientRect()), n = j(a), {top: o.top + n.pageYOffset - t.clientTop, left: o.left + n.pageXOffset - t.clientLeft}) : o
        }, position: function() {
            if (this[0]) {
                var e, t, n = this[0], i = {top: 0, left: 0};
                return"fixed" === et.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), et.nodeName(e[0], "html") || (i = e.offset()), i.top += et.css(e[0], "borderTopWidth", !0), i.left += et.css(e[0], "borderLeftWidth", !0)), {top: t.top - i.top - et.css(n, "marginTop", !0), left: t.left - i.left - et.css(n, "marginLeft", !0)}
            }
        }, offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || Hn; e && !et.nodeName(e, "html") && "static" === et.css(e, "position"); )
                    e = e.offsetParent;
                return e || Hn
            })
        }}), et.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function(t, n) {
        var i = "pageYOffset" === n;
        et.fn[t] = function(o) {
            return gt(this, function(t, o, a) {
                var r = j(t);
                return void 0 === a ? r ? r[n] : t[o] : (r ? r.scrollTo(i ? e.pageXOffset : a, i ? a : e.pageYOffset) : t[o] = a, void 0)
            }, t, o, arguments.length, null)
        }
    }), et.each(["top", "left"], function(e, t) {
        et.cssHooks[t] = C(X.pixelPosition, function(e, n) {
            return n ? (n = w(e, t), Vt.test(n) ? et(e).position()[t] + "px" : n) : void 0
        })
    }), et.each({Height: "height", Width: "width"}, function(e, t) {
        et.each({padding: "inner" + e, content: t, "": "outer" + e}, function(n, i) {
            et.fn[i] = function(i, o) {
                var a = arguments.length && (n || "boolean" != typeof i), r = n || (i === !0 || o === !0 ? "margin" : "border");
                return gt(this, function(t, n, i) {
                    var o;
                    return et.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? et.css(t, n, r) : et.style(t, n, i, r)
                }, t, a ? i : void 0, a, null)
            }
        })
    }), et.fn.size = function() {
        return this.length
    }, et.fn.andSelf = et.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return et
    });
    var On = e.jQuery, Fn = e.$;
    return et.noConflict = function(t) {
        return e.$ === et && (e.$ = Fn), t && e.jQuery === et && (e.jQuery = On), et
    }, typeof t === St && (e.jQuery = e.$ = et), et
}), define("app/utils/helpers", ["require", "exports", "module", "jquery"], function(e, t) {
    function n(e) {
        var t = Math.random() + "", n = 1e16 * t, o = i("<div/>"), a = i("<iframe/>"), r = "//4381626.fls.doubleclick.net/activityi;src=4381626;type=JetEd0;cat=" + e + ";ord=" + n + "?";
        o.css({position: "absolute", top: "0", left: "0", width: "1px", height: "1px", display: "none"}), a.attr({src: r, width: "1", height: "1", frameborder: "0"}), o.append(a), i("body").append(o)
    }
    var i = e("jquery");
    return t.isMobile = function() {
        var e = navigator.userAgent, t = new RegExp("Android|webOS|iPhone|iPad|iPod|BlackBerry", "i");
        return t.test(e)
    }, t.onScrollAnimate = function(e) {
        var t = i(window), n = e.delegate, o = e.target;
        i(n).find(o).each(function(e, t) {
            var n = i(t);
            n.visible(!0) && n.addClass("already-animated")
        }), t.on("scroll.onScrollAnimate", function() {
            i(n).find(o).each(function(e, t) {
                var n = i(t);
                n.visible(!0) && n.addClass("animate-in")
            })
        })
    }, t.trackEvent = function(e) {
        var t = e.floodlight && e.floodlight.catId, o = e.floodlight && e.floodlight.loadEvent;
        t && (o ? i(window).on("load", function() {
            n(t)
        }) : n(t))
    }, i.fn.visible = function(e) {
        var t = i(this), n = i(window), o = n.scrollTop(), a = o + n.height(), r = t.offset().top, s = r + t.height(), l = e === !0 ? s : r, c = e === !0 ? r : s;
        return a >= c && l >= o
    }, t
}), function() {
    var e = this, t = e._, n = {}, i = Array.prototype, o = Object.prototype, a = Function.prototype, r = i.push, s = i.slice, l = i.concat, c = o.toString, d = o.hasOwnProperty, u = i.forEach, p = i.map, m = i.reduce, h = i.reduceRight, f = i.filter, g = i.every, v = i.some, y = i.indexOf, _ = i.lastIndexOf, b = Array.isArray, w = Object.keys, C = a.bind, x = function(e) {
        return e instanceof x ? e : this instanceof x ? (this._wrapped = e, void 0) : new x(e)
    };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = x), exports._ = x) : e._ = x, x.VERSION = "1.4.3";
    var k = x.each = x.forEach = function(e, t, i) {
        if (null != e)
            if (u && e.forEach === u)
                e.forEach(t, i);
            else if (e.length === +e.length) {
                for (var o = 0, a = e.length; a > o; o++)
                    if (t.call(i, e[o], o, e) === n)
                        return
            } else
                for (var r in e)
                    if (x.has(e, r) && t.call(i, e[r], r, e) === n)
                        return
    };
    x.map = x.collect = function(e, t, n) {
        var i = [];
        return null == e ? i : p && e.map === p ? e.map(t, n) : (k(e, function(e, o, a) {
            i[i.length] = t.call(n, e, o, a)
        }), i)
    };
    var S = "Reduce of empty array with no initial value";
    x.reduce = x.foldl = x.inject = function(e, t, n, i) {
        var o = arguments.length > 2;
        if (null == e && (e = []), m && e.reduce === m)
            return i && (t = x.bind(t, i)), o ? e.reduce(t, n) : e.reduce(t);
        if (k(e, function(e, a, r) {
            o ? n = t.call(i, n, e, a, r) : (n = e, o = !0)
        }), !o)
            throw new TypeError(S);
        return n
    }, x.reduceRight = x.foldr = function(e, t, n, i) {
        var o = arguments.length > 2;
        if (null == e && (e = []), h && e.reduceRight === h)
            return i && (t = x.bind(t, i)), o ? e.reduceRight(t, n) : e.reduceRight(t);
        var a = e.length;
        if (a !== +a) {
            var r = x.keys(e);
            a = r.length
        }
        if (k(e, function(s, l, c) {
            l = r ? r[--a] : --a, o ? n = t.call(i, n, e[l], l, c) : (n = e[l], o = !0)
        }), !o)
            throw new TypeError(S);
        return n
    }, x.find = x.detect = function(e, t, n) {
        var i;
        return E(e, function(e, o, a) {
            return t.call(n, e, o, a) ? (i = e, !0) : void 0
        }), i
    }, x.filter = x.select = function(e, t, n) {
        var i = [];
        return null == e ? i : f && e.filter === f ? e.filter(t, n) : (k(e, function(e, o, a) {
            t.call(n, e, o, a) && (i[i.length] = e)
        }), i)
    }, x.reject = function(e, t, n) {
        return x.filter(e, function(e, i, o) {
            return!t.call(n, e, i, o)
        }, n)
    }, x.every = x.all = function(e, t, i) {
        t || (t = x.identity);
        var o = !0;
        return null == e ? o : g && e.every === g ? e.every(t, i) : (k(e, function(e, a, r) {
            return(o = o && t.call(i, e, a, r)) ? void 0 : n
        }), !!o)
    };
    var E = x.some = x.any = function(e, t, i) {
        t || (t = x.identity);
        var o = !1;
        return null == e ? o : v && e.some === v ? e.some(t, i) : (k(e, function(e, a, r) {
            return o || (o = t.call(i, e, a, r)) ? n : void 0
        }), !!o)
    };
    x.contains = x.include = function(e, t) {
        return null == e ? !1 : y && e.indexOf === y ? -1 != e.indexOf(t) : E(e, function(e) {
            return e === t
        })
    }, x.invoke = function(e, t) {
        var n = s.call(arguments, 2);
        return x.map(e, function(e) {
            return(x.isFunction(t) ? t : e[t]).apply(e, n)
        })
    }, x.pluck = function(e, t) {
        return x.map(e, function(e) {
            return e[t]
        })
    }, x.where = function(e, t) {
        return x.isEmpty(t) ? [] : x.filter(e, function(e) {
            for (var n in t)
                if (t[n] !== e[n])
                    return!1;
            return!0
        })
    }, x.max = function(e, t, n) {
        if (!t && x.isArray(e) && e[0] === +e[0] && e.length < 65535)
            return Math.max.apply(Math, e);
        if (!t && x.isEmpty(e))
            return-1 / 0;
        var i = {computed: -1 / 0, value: -1 / 0};
        return k(e, function(e, o, a) {
            var r = t ? t.call(n, e, o, a) : e;
            r >= i.computed && (i = {value: e, computed: r})
        }), i.value
    }, x.min = function(e, t, n) {
        if (!t && x.isArray(e) && e[0] === +e[0] && e.length < 65535)
            return Math.min.apply(Math, e);
        if (!t && x.isEmpty(e))
            return 1 / 0;
        var i = {computed: 1 / 0, value: 1 / 0};
        return k(e, function(e, o, a) {
            var r = t ? t.call(n, e, o, a) : e;
            r < i.computed && (i = {value: e, computed: r})
        }), i.value
    }, x.shuffle = function(e) {
        var t, n = 0, i = [];
        return k(e, function(e) {
            t = x.random(n++), i[n - 1] = i[t], i[t] = e
        }), i
    };
    var T = function(e) {
        return x.isFunction(e) ? e : function(t) {
            return t[e]
        }
    };
    x.sortBy = function(e, t, n) {
        var i = T(t);
        return x.pluck(x.map(e, function(e, t, o) {
            return{value: e, index: t, criteria: i.call(n, e, t, o)}
        }).sort(function(e, t) {
            var n = e.criteria, i = t.criteria;
            if (n !== i) {
                if (n > i || void 0 === n)
                    return 1;
                if (i > n || void 0 === i)
                    return-1
            }
            return e.index < t.index ? -1 : 1
        }), "value")
    };
    var M = function(e, t, n, i) {
        var o = {}, a = T(t || x.identity);
        return k(e, function(t, r) {
            var s = a.call(n, t, r, e);
            i(o, s, t)
        }), o
    };
    x.groupBy = function(e, t, n) {
        return M(e, t, n, function(e, t, n) {
            (x.has(e, t) ? e[t] : e[t] = []).push(n)
        })
    }, x.countBy = function(e, t, n) {
        return M(e, t, n, function(e, t) {
            x.has(e, t) || (e[t] = 0), e[t]++
        })
    }, x.sortedIndex = function(e, t, n, i) {
        n = null == n ? x.identity : T(n);
        for (var o = n.call(i, t), a = 0, r = e.length; r > a; ) {
            var s = a + r >>> 1;
            n.call(i, e[s]) < o ? a = s + 1 : r = s
        }
        return a
    }, x.toArray = function(e) {
        return e ? x.isArray(e) ? s.call(e) : e.length === +e.length ? x.map(e, x.identity) : x.values(e) : []
    }, x.size = function(e) {
        return null == e ? 0 : e.length === +e.length ? e.length : x.keys(e).length
    }, x.first = x.head = x.take = function(e, t, n) {
        return null == e ? void 0 : null == t || n ? e[0] : s.call(e, 0, t)
    }, x.initial = function(e, t, n) {
        return s.call(e, 0, e.length - (null == t || n ? 1 : t))
    }, x.last = function(e, t, n) {
        return null == e ? void 0 : null == t || n ? e[e.length - 1] : s.call(e, Math.max(e.length - t, 0))
    }, x.rest = x.tail = x.drop = function(e, t, n) {
        return s.call(e, null == t || n ? 1 : t)
    }, x.compact = function(e) {
        return x.filter(e, x.identity)
    };
    var A = function(e, t, n) {
        return k(e, function(e) {
            x.isArray(e) ? t ? r.apply(n, e) : A(e, t, n) : n.push(e)
        }), n
    };
    x.flatten = function(e, t) {
        return A(e, t, [])
    }, x.without = function(e) {
        return x.difference(e, s.call(arguments, 1))
    }, x.uniq = x.unique = function(e, t, n, i) {
        x.isFunction(t) && (i = n, n = t, t = !1);
        var o = n ? x.map(e, n, i) : e, a = [], r = [];
        return k(o, function(n, i) {
            (t ? i && r[r.length - 1] === n : x.contains(r, n)) || (r.push(n), a.push(e[i]))
        }), a
    }, x.union = function() {
        return x.uniq(l.apply(i, arguments))
    }, x.intersection = function(e) {
        var t = s.call(arguments, 1);
        return x.filter(x.uniq(e), function(e) {
            return x.every(t, function(t) {
                return x.indexOf(t, e) >= 0
            })
        })
    }, x.difference = function(e) {
        var t = l.apply(i, s.call(arguments, 1));
        return x.filter(e, function(e) {
            return!x.contains(t, e)
        })
    }, x.zip = function() {
        for (var e = s.call(arguments), t = x.max(x.pluck(e, "length")), n = new Array(t), i = 0; t > i; i++)
            n[i] = x.pluck(e, "" + i);
        return n
    }, x.object = function(e, t) {
        if (null == e)
            return{};
        for (var n = {}, i = 0, o = e.length; o > i; i++)
            t ? n[e[i]] = t[i] : n[e[i][0]] = e[i][1];
        return n
    }, x.indexOf = function(e, t, n) {
        if (null == e)
            return-1;
        var i = 0, o = e.length;
        if (n) {
            if ("number" != typeof n)
                return i = x.sortedIndex(e, t), e[i] === t ? i : -1;
            i = 0 > n ? Math.max(0, o + n) : n
        }
        if (y && e.indexOf === y)
            return e.indexOf(t, n);
        for (; o > i; i++)
            if (e[i] === t)
                return i;
        return-1
    }, x.lastIndexOf = function(e, t, n) {
        if (null == e)
            return-1;
        var i = null != n;
        if (_ && e.lastIndexOf === _)
            return i ? e.lastIndexOf(t, n) : e.lastIndexOf(t);
        for (var o = i ? n : e.length; o--; )
            if (e[o] === t)
                return o;
        return-1
    }, x.range = function(e, t, n) {
        arguments.length <= 1 && (t = e || 0, e = 0), n = arguments[2] || 1;
        for (var i = Math.max(Math.ceil((t - e) / n), 0), o = 0, a = new Array(i); i > o; )
            a[o++] = e, e += n;
        return a
    };
    var P = function() {
    };
    x.bind = function(e, t) {
        var n, i;
        if (e.bind === C && C)
            return C.apply(e, s.call(arguments, 1));
        if (!x.isFunction(e))
            throw new TypeError;
        return n = s.call(arguments, 2), i = function() {
            if (!(this instanceof i))
                return e.apply(t, n.concat(s.call(arguments)));
            P.prototype = e.prototype;
            var o = new P;
            P.prototype = null;
            var a = e.apply(o, n.concat(s.call(arguments)));
            return Object(a) === a ? a : o
        }
    }, x.bindAll = function(e) {
        var t = s.call(arguments, 1);
        return 0 == t.length && (t = x.functions(e)), k(t, function(t) {
            e[t] = x.bind(e[t], e)
        }), e
    }, x.memoize = function(e, t) {
        var n = {};
        return t || (t = x.identity), function() {
            var i = t.apply(this, arguments);
            return x.has(n, i) ? n[i] : n[i] = e.apply(this, arguments)
        }
    }, x.delay = function(e, t) {
        var n = s.call(arguments, 2);
        return setTimeout(function() {
            return e.apply(null, n)
        }, t)
    }, x.defer = function(e) {
        return x.delay.apply(x, [e, 1].concat(s.call(arguments, 1)))
    }, x.throttle = function(e, t) {
        var n, i, o, a, r = 0, s = function() {
            r = new Date, o = null, a = e.apply(n, i)
        };
        return function() {
            var l = new Date, c = t - (l - r);
            return n = this, i = arguments, 0 >= c ? (clearTimeout(o), o = null, r = l, a = e.apply(n, i)) : o || (o = setTimeout(s, c)), a
        }
    }, x.debounce = function(e, t, n) {
        var i, o;
        return function() {
            var a = this, r = arguments, s = function() {
                i = null, n || (o = e.apply(a, r))
            }, l = n && !i;
            return clearTimeout(i), i = setTimeout(s, t), l && (o = e.apply(a, r)), o
        }
    }, x.once = function(e) {
        var t, n = !1;
        return function() {
            return n ? t : (n = !0, t = e.apply(this, arguments), e = null, t)
        }
    }, x.wrap = function(e, t) {
        return function() {
            var n = [e];
            return r.apply(n, arguments), t.apply(this, n)
        }
    }, x.compose = function() {
        var e = arguments;
        return function() {
            for (var t = arguments, n = e.length - 1; n >= 0; n--)
                t = [e[n].apply(this, t)];
            return t[0]
        }
    }, x.after = function(e, t) {
        return 0 >= e ? t() : function() {
            return--e < 1 ? t.apply(this, arguments) : void 0
        }
    }, x.keys = w || function(e) {
        if (e !== Object(e))
            throw new TypeError("Invalid object");
        var t = [];
        for (var n in e)
            x.has(e, n) && (t[t.length] = n);
        return t
    }, x.values = function(e) {
        var t = [];
        for (var n in e)
            x.has(e, n) && t.push(e[n]);
        return t
    }, x.pairs = function(e) {
        var t = [];
        for (var n in e)
            x.has(e, n) && t.push([n, e[n]]);
        return t
    }, x.invert = function(e) {
        var t = {};
        for (var n in e)
            x.has(e, n) && (t[e[n]] = n);
        return t
    }, x.functions = x.methods = function(e) {
        var t = [];
        for (var n in e)
            x.isFunction(e[n]) && t.push(n);
        return t.sort()
    }, x.extend = function(e) {
        return k(s.call(arguments, 1), function(t) {
            if (t)
                for (var n in t)
                    e[n] = t[n]
        }), e
    }, x.pick = function(e) {
        var t = {}, n = l.apply(i, s.call(arguments, 1));
        return k(n, function(n) {
            n in e && (t[n] = e[n])
        }), t
    }, x.omit = function(e) {
        var t = {}, n = l.apply(i, s.call(arguments, 1));
        for (var o in e)
            x.contains(n, o) || (t[o] = e[o]);
        return t
    }, x.defaults = function(e) {
        return k(s.call(arguments, 1), function(t) {
            if (t)
                for (var n in t)
                    null == e[n] && (e[n] = t[n])
        }), e
    }, x.clone = function(e) {
        return x.isObject(e) ? x.isArray(e) ? e.slice() : x.extend({}, e) : e
    }, x.tap = function(e, t) {
        return t(e), e
    };
    var I = function(e, t, n, i) {
        if (e === t)
            return 0 !== e || 1 / e == 1 / t;
        if (null == e || null == t)
            return e === t;
        e instanceof x && (e = e._wrapped), t instanceof x && (t = t._wrapped);
        var o = c.call(e);
        if (o != c.call(t))
            return!1;
        switch (o) {
            case"[object String]":
                return e == String(t);
            case"[object Number]":
                return e != +e ? t != +t : 0 == e ? 1 / e == 1 / t : e == +t;
            case"[object Date]":
            case"[object Boolean]":
                return+e == +t;
            case"[object RegExp]":
                return e.source == t.source && e.global == t.global && e.multiline == t.multiline && e.ignoreCase == t.ignoreCase
        }
        if ("object" != typeof e || "object" != typeof t)
            return!1;
        for (var a = n.length; a--; )
            if (n[a] == e)
                return i[a] == t;
        n.push(e), i.push(t);
        var r = 0, s = !0;
        if ("[object Array]" == o) {
            if (r = e.length, s = r == t.length)
                for (; r-- && (s = I(e[r], t[r], n, i)); )
                    ;
        } else {
            var l = e.constructor, d = t.constructor;
            if (l !== d && !(x.isFunction(l) && l instanceof l && x.isFunction(d) && d instanceof d))
                return!1;
            for (var u in e)
                if (x.has(e, u) && (r++, !(s = x.has(t, u) && I(e[u], t[u], n, i))))
                    break;
            if (s) {
                for (u in t)
                    if (x.has(t, u) && !r--)
                        break;
                s = !r
            }
        }
        return n.pop(), i.pop(), s
    };
    x.isEqual = function(e, t) {
        return I(e, t, [], [])
    }, x.isEmpty = function(e) {
        if (null == e)
            return!0;
        if (x.isArray(e) || x.isString(e))
            return 0 === e.length;
        for (var t in e)
            if (x.has(e, t))
                return!1;
        return!0
    }, x.isElement = function(e) {
        return!(!e || 1 !== e.nodeType)
    }, x.isArray = b || function(e) {
        return"[object Array]" == c.call(e)
    }, x.isObject = function(e) {
        return e === Object(e)
    }, k(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(e) {
        x["is" + e] = function(t) {
            return c.call(t) == "[object " + e + "]"
        }
    }), x.isArguments(arguments) || (x.isArguments = function(e) {
        return!(!e || !x.has(e, "callee"))
    }), "function" != typeof /./ && (x.isFunction = function(e) {
        return"function" == typeof e
    }), x.isFinite = function(e) {
        return isFinite(e) && !isNaN(parseFloat(e))
    }, x.isNaN = function(e) {
        return x.isNumber(e) && e != +e
    }, x.isBoolean = function(e) {
        return e === !0 || e === !1 || "[object Boolean]" == c.call(e)
    }, x.isNull = function(e) {
        return null === e
    }, x.isUndefined = function(e) {
        return void 0 === e
    }, x.has = function(e, t) {
        return d.call(e, t)
    }, x.noConflict = function() {
        return e._ = t, this
    }, x.identity = function(e) {
        return e
    }, x.times = function(e, t, n) {
        for (var i = Array(e), o = 0; e > o; o++)
            i[o] = t.call(n, o);
        return i
    }, x.random = function(e, t) {
        return null == t && (t = e, e = 0), e + (0 | Math.random() * (t - e + 1))
    };
    var N = {escape: {"&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;", "/": "&#x2F;"}};
    N.unescape = x.invert(N.escape);
    var D = {escape: new RegExp("[" + x.keys(N.escape).join("") + "]", "g"), unescape: new RegExp("(" + x.keys(N.unescape).join("|") + ")", "g")};
    x.each(["escape", "unescape"], function(e) {
        x[e] = function(t) {
            return null == t ? "" : ("" + t).replace(D[e], function(t) {
                return N[e][t]
            })
        }
    }), x.result = function(e, t) {
        if (null == e)
            return null;
        var n = e[t];
        return x.isFunction(n) ? n.call(e) : n
    }, x.mixin = function(e) {
        k(x.functions(e), function(t) {
            var n = x[t] = e[t];
            x.prototype[t] = function() {
                var e = [this._wrapped];
                return r.apply(e, arguments), O.call(this, n.apply(x, e))
            }
        })
    };
    var L = 0;
    x.uniqueId = function(e) {
        var t = "" + ++L;
        return e ? e + t : t
    }, x.templateSettings = {evaluate: /<%([\s\S]+?)%>/g, interpolate: /<%=([\s\S]+?)%>/g, escape: /<%-([\s\S]+?)%>/g};
    var B = /(.)^/, R = {"'": "'", "\\": "\\", "\r": "r", "\n": "n", "	": "t", "\u2028": "u2028", "\u2029": "u2029"}, H = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    x.template = function(e, t, n) {
        n = x.defaults({}, n, x.templateSettings);
        var i = new RegExp([(n.escape || B).source, (n.interpolate || B).source, (n.evaluate || B).source].join("|") + "|$", "g"), o = 0, a = "__p+='";
        e.replace(i, function(t, n, i, r, s) {
            return a += e.slice(o, s).replace(H, function(e) {
                return"\\" + R[e]
            }), n && (a += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'"), i && (a += "'+\n((__t=(" + i + "))==null?'':__t)+\n'"), r && (a += "';\n" + r + "\n__p+='"), o = s + t.length, t
        }), a += "';\n", n.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
        try {
            var r = new Function(n.variable || "obj", "_", a)
        } catch (s) {
            throw s.source = a, s
        }
        if (t)
            return r(t, x);
        var l = function(e) {
            return r.call(this, e, x)
        };
        return l.source = "function(" + (n.variable || "obj") + "){\n" + a + "}", l
    }, x.chain = function(e) {
        return x(e).chain()
    };
    var O = function(e) {
        return this._chain ? x(e).chain() : e
    };
    x.mixin(x), k(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(e) {
        var t = i[e];
        x.prototype[e] = function() {
            var n = this._wrapped;
            return t.apply(n, arguments), "shift" != e && "splice" != e || 0 !== n.length || delete n[0], O.call(this, n)
        }
    }), k(["concat", "join", "slice"], function(e) {
        var t = i[e];
        x.prototype[e] = function() {
            return O.call(this, t.apply(this._wrapped, arguments))
        }
    }), x.extend(x.prototype, {chain: function() {
            return this._chain = !0, this
        }, value: function() {
            return this._wrapped
        }})
}.call(this), define("underscore", function(e) {
    return function() {
        var t;
        return t || e._
    }
}(this)), define("app/modules/formUtils", ["require", "jquery"], function(e) {
    function t(e) {
        this.ui = {}, this.initialize = function(e) {
            this.ui.form = e.form, this.ui.btnSubmit = n(".btn-submit", this.ui.form), this.confirmMsg = e.confirmMsg || "Message sent, thank you.", this.successCallback = e.showConfirmation || this.showConfirmation, this.bindUIEvents()
        }, this.bindUIEvents = function() {
            this.ui.form.on("submit", n.proxy(this.onFormSubmit, this))
        }, this.onFormSubmit = function(e) {
            e.preventDefault(), this.validateRequiredFields({form: this.ui.form, requiredFields: n("[required]", this.ui.form), callback: n.proxy(this.ajaxPost, this)})
        }, this.ajaxPost = function() {
            n.ajax({url: this.ui.form.attr("action"), type: "POST", dataType: "json", data: this.ui.form.serializeArray(), success: n.proxy(this.ajaxPostComplete, this)})
        }, this.validateRequiredFields = function(e) {
            var t = e.requiredFields, i = e.callback, o = !1;
            n(".error", e.form).removeClass("error"), _.each(t, function(e) {
                n(e).val().length <= 0 && (n(e).addClass("error"), o = !0)
            }), i && !o && i()
        }, this.ajaxPostComplete = function(e) {
            e.success ? this.successCallback() : "undefined" != typeof e.errors && _.each(e.errors, n.proxy(function(e, t) {
                n("input[name=" + t + "], textarea[name=" + t + "]", this.ui.form).addClass("error").val("")
            }, this))
        }, this.showConfirmation = function() {
            this.ui.form[0].reset();
            var e = '<p class="confirmation-message">' + this.confirmMsg + "</p>";
            this.ui.btnSubmit.after(n(e).fadeIn(500)), _.delay(n.proxy(this.hideConfirmation, this), 5e3)
        }, this.hideConfirmation = function() {
            n(".confirmation-message", this.ui.form).fadeOut(250, function() {
                n(this).remove()
            })
        }, this.initialize(e)
    }
    var n = e("jquery");
    return t
}), define("app/utils/constants", ["require", "exports", "module", "jquery"], function(e, t) {
    return e("jquery"), t.floodlight = {LANDING_PAGE: "JetEd0", FLEET_PAGE: "Flyer0", ASIA_JET_PAGE: "AsiaJ0", CAREERS_PAGE: "Caree0", CONTACT_PAGE: "Conta0", JET_MANAGEMENT_PAGE: "JetMa0", MAINTENANCE_PAGE: "Owner0", ONE_WAY_PAGE: "One-W0", NEWS_PRESS_PAGE: "Press0", PROGRAM_PAGE: "Progr0", PROMOTION_PAGE: "Promo0", SAFETY_PAGE: "Safet0", WHAT_WE_STAND_FOR_PAGE: "Compa0", WHO_WE_ARE_PAGE: "WhoWe0", BOOK_FLIGHT_OPEN: "BookA0", BOOK_FLIGHT_SUCCESS: "BookA00", JET_FALCON_PAGE: "Falco0", JET_GULFSTREEM_PAGE: "Gulfs0"}, t
}), define("app/modules/bookFlight", ["require", "../modules/formUtils", "app/utils/helpers", "app/utils/constants"], function(e) {
    var t = e("../modules/formUtils"), n = e("app/utils/helpers"), i = e("app/utils/constants");
    return{ui: {html: $("html"), body: $("body"), wrapper: $(".book-flight"), header: $(".book-flight header"), form: $(".form-book-flight"), btnToggle: $(".btn-toggle, [type=reset]"), btnContinue: $(".btn-continue"), btnTrip: $(".btn-noborder"), roundTripRow: $(".return-trip"), responsePanel: $(".panel.response"), pageDot: $(".pagination .dot"), quoteSourceSelect: $(".quote-source"), otherInput: $("#other")}, initialize: function() {
            var e = this;
            this.bindUIEvents(), this.initDatePicker(), this.updatePagination(), this.bookFlightForm = new t({form: e.ui.form, showConfirmation: $.proxy(e.onSuccess, this)}), "isOpen" === sessionStorage.getItem("baf") && this.open()
        }, bindUIEvents: function() {
            var e = this;
            this.ui.btnToggle.on("click", $.proxy(this.onClickBtnToggle, this)), this.ui.btnContinue.on("click", $.proxy(this.onClickContinue, this)), this.ui.btnTrip.on("click", $.proxy(this.onClickBtnTrip, this)), this.ui.pageDot.on("click", $.proxy(this.onClickPageDot, this)), this.ui.quoteSourceSelect.on("change", $.proxy(this.onChangequoteSourceSelect, this)), this.ui.wrapper.on("click", function(t) {
                e.onClickWrapper(t)
            })
        }, onChangequoteSourceSelect: function(e) {
            "other" === $(e.target).val() ? this.enableOtherInput() : this.disableOtherInput()
        }, enableOtherInput: function() {
            this.ui.otherInput.prop("disabled", !1).prop("placeholder", "Please specify").prop("required", !0).focus()
        }, disableOtherInput: function() {
            this.ui.otherInput.prop("disabled", !0).prop("placeholder", "").prop("required", !1).removeClass("error").blur()
        }, initDatePicker: function() {
            var e = this;
            $.datepicker.setDefaults({showOn: "both", buttonImageOnly: !1, buttonImage: $("link[rel=media-url]").attr("href") + "img/sprites/icons/calendar.png", buttonText: "Calendar", dateFormat: "m/d/y", minDate: 0}), this.toDatePicker = $(".datepicker-to").datepicker();
            var t = $(".datepicker-from").datepicker({onClose: function(t) {
                    e.toDatePicker.datepicker("option", "minDate", t)
                }});
            return t
        }, updatePagination: function(e) {
            var t = e || 1, n = t - 1;
            this.ui.pageDot.removeClass("current").eq(n).addClass("current")
        }, onClickPageDot: function(e) {
            e.preventDefault();
            var t = $(e.currentTarget).data("page");
            2 == t ? this.onClickContinue(e) : this.goToStep(t)
        }, onClickBtnToggle: function(e) {
            e.preventDefault(), this.ui.html.hasClass("form-is-visible") ? this.close() : this.open()
        }, onClickContinue: function(e) {
            e.preventDefault();
            var t = this;
            this.bookFlightForm.validateRequiredFields({requiredFields: $(".panel:visible [required]", t.ui.form), callback: function() {
                    t.goToStep(2)
                }})
        }, onClickWrapper: function(e) {
            $(e.target)[0] === this.ui.wrapper[0] && this.close()
        }, onClickBtnTrip: function(e) {
            e.preventDefault();
            var t = $(e.currentTarget);
            "round" !== t.attr("data-trip") ? (this.ui.roundTripRow.addClass("disabled"), $("[required]", this.ui.roundTripRow).removeAttr("required")) : (this.ui.roundTripRow.removeClass("disabled"), $("[name=return_date],[name=return_time]", this.ui.roundTripRow).attr("required", "")), t.addClass("active").siblings(".active").removeClass("active")
        }, open: function() {
            this.ui.header.removeClass("invisible"), this.ui.html.addClass("form-is-visible"), this.ui.wrapper.addClass("visible"), sessionStorage.setItem("baf", "isOpen"), n.trackEvent({floodlight: {catId: i.floodlight.BOOK_FLIGHT_OPEN, loadEvent: !1}})
        }, close: function() {
            this.ui.body.hasClass("page-landing") && $(window).scrollTop(0), this.ui.html.removeClass("form-is-visible").css("margin-left", "inherit"), this.ui.wrapper.removeClass("visible"), this.ui.form[0].reset(), this.goToStep(1), sessionStorage.removeItem("baf")
        }, goToStep: function(e) {
            this.ui.form.attr("data-step", e), this.updatePagination(e)
        }, onSuccess: function() {
            var e = this;
            this.goToStep(3), _.delay(function() {
                e.close()
            }, 3e3), n.trackEvent({floodlight: {catId: i.floodlight.BOOK_FLIGHT_SUCCESS, loadEvent: !1}}), ga("send", "event", "Book A Flight", "submit", "form")
        }}
}), function(e) {
    var t = "waitForImages";
    e.waitForImages = {hasImageProperties: ["backgroundImage", "listStyleImage", "borderImage", "borderCornerImage", "cursor"]}, e.expr[":"].uncached = function(t) {
        if (!e(t).is('img[src!=""]'))
            return!1;
        var n = new Image;
        return n.src = t.src, !n.complete
    }, e.fn.waitForImages = function(n, i, o) {
        var a = 0, r = 0;
        if (e.isPlainObject(arguments[0]) && (o = arguments[0].waitForAll, i = arguments[0].each, n = arguments[0].finished), n = n || e.noop, i = i || e.noop, o = !!o, !e.isFunction(n) || !e.isFunction(i))
            throw new TypeError("An invalid callback was supplied.");
        return this.each(function() {
            var s = e(this), l = [], c = e.waitForImages.hasImageProperties || [], d = /url\(\s*(['"]?)(.*?)\1\s*\)/g;
            o ? s.find("*").addBack().each(function() {
                var t = e(this);
                t.is("img:uncached") && l.push({src: t.attr("src"), element: t[0]}), e.each(c, function(e, n) {
                    var i, o = t.css(n);
                    if (!o)
                        return!0;
                    for (; i = d.exec(o); )
                        l.push({src: i[2], element: t[0]})
                })
            }) : s.find("img:uncached").each(function() {
                l.push({src: this.src, element: this})
            }), a = l.length, r = 0, 0 === a && n.call(s[0]), e.each(l, function(o, l) {
                var c = new Image;
                e(c).on("load." + t + " error." + t, function(e) {
                    return r++, i.call(l.element, r, a, "load" == e.type), r == a ? (n.call(s[0]), !1) : void 0
                }), c.src = l.src
            })
        })
    }
}(jQuery), define("jquery.waitforimages", ["jquery"], function() {
}), function(e) {
    e.flexslider = function(t, n) {
        var i = e(t), o = e.extend({}, e.flexslider.defaults, n), a = o.namespace, r = "ontouchstart"in window || window.DocumentTouch && document instanceof DocumentTouch, s = r ? "touchend" : "click", l = "vertical" === o.direction, c = o.reverse, d = o.itemWidth > 0, u = "fade" === o.animation, p = "" !== o.asNavFor, m = {};
        e.data(t, "flexslider", i), m = {init: function() {
                i.animating = !1, i.currentSlide = o.startAt, i.animatingTo = i.currentSlide, i.atEnd = 0 === i.currentSlide || i.currentSlide === i.last, i.containerSelector = o.selector.substr(0, o.selector.search(" ")), i.slides = e(o.selector, i), i.container = e(i.containerSelector, i), i.count = i.slides.length, i.syncExists = e(o.sync).length > 0, "slide" === o.animation && (o.animation = "swing"), i.prop = l ? "top" : "marginLeft", i.args = {}, i.manualPause = !1, i.transitions = !o.video && !u && o.useCSS && function() {
                    var e = document.createElement("div"), t = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                    for (var n in t)
                        if (void 0 !== e.style[t[n]])
                            return i.pfx = t[n].replace("Perspective", "").toLowerCase(), i.prop = "-" + i.pfx + "-transform", !0;
                    return!1
                }(), "" !== o.controlsContainer && (i.controlsContainer = e(o.controlsContainer).length > 0 && e(o.controlsContainer)), "" !== o.manualControls && (i.manualControls = e(o.manualControls).length > 0 && e(o.manualControls)), o.randomize && (i.slides.sort(function() {
                    return Math.round(Math.random()) - .5
                }), i.container.empty().append(i.slides)), i.doMath(), p && m.asNav.setup(), i.setup("init"), o.controlNav && m.controlNav.setup(), o.directionNav && m.directionNav.setup(), o.keyboard && (1 === e(i.containerSelector).length || o.multipleKeyboard) && e(document).bind("keyup", function(e) {
                    var t = e.keyCode;
                    if (!i.animating && (39 === t || 37 === t)) {
                        var n = 39 === t ? i.getTarget("next") : 37 === t ? i.getTarget("prev") : !1;
                        i.flexAnimate(n, o.pauseOnAction)
                    }
                }), o.mousewheel && i.bind("mousewheel", function(e, t) {
                    e.preventDefault();
                    var n = 0 > t ? i.getTarget("next") : i.getTarget("prev");
                    i.flexAnimate(n, o.pauseOnAction)
                }), o.pausePlay && m.pausePlay.setup(), o.slideshow && (o.pauseOnHover && i.hover(function() {
                    i.pause()
                }, function() {
                    i.manualPause || i.play()
                }), o.initDelay > 0 ? setTimeout(i.play, o.initDelay) : i.play()), r && o.touch && m.touch(), (!u || u && o.smoothHeight) && e(window).bind("resize focus", m.resize), setTimeout(function() {
                    o.start(i)
                }, 200)
            }, asNav: {setup: function() {
                    i.asNav = !0, i.animatingTo = Math.floor(i.currentSlide / i.move), i.currentItem = i.currentSlide, i.slides.removeClass(a + "active-slide").eq(i.currentItem).addClass(a + "active-slide"), i.slides.click(function(t) {
                        t.preventDefault();
                        var n = e(this), a = n.index();
                        e(o.asNavFor).data("flexslider").animating || n.hasClass("active") || (i.direction = i.currentItem < a ? "next" : "prev", i.flexAnimate(a, o.pauseOnAction, !1, !0, !0))
                    })
                }}, controlNav: {setup: function() {
                    i.manualControls ? m.controlNav.setupManual() : m.controlNav.setupPaging()
                }, setupPaging: function() {
                    var t, n = "thumbnails" === o.controlNav ? "control-thumbs" : "control-paging", l = 1;
                    if (i.controlNavScaffold = e('<ol class="' + a + "control-nav " + a + n + '"></ol>'), i.pagingCount > 1)
                        for (var c = 0; c < i.pagingCount; c++)
                            t = "thumbnails" === o.controlNav ? '<img src="' + i.slides.eq(c).attr("data-thumb") + '"/>' : "<a>" + l + "</a>", i.controlNavScaffold.append("<li>" + t + "</li>"), l++;
                    i.controlsContainer ? e(i.controlsContainer).append(i.controlNavScaffold) : i.append(i.controlNavScaffold), m.controlNav.set(), m.controlNav.active(), i.controlNavScaffold.delegate("a, img", s, function(t) {
                        t.preventDefault();
                        var n = e(this), r = i.controlNav.index(n);
                        n.hasClass(a + "active") || (i.direction = r > i.currentSlide ? "next" : "prev", i.flexAnimate(r, o.pauseOnAction))
                    }), r && i.controlNavScaffold.delegate("a", "click touchstart", function(e) {
                        e.preventDefault()
                    })
                }, setupManual: function() {
                    i.controlNav = i.manualControls, m.controlNav.active(), i.controlNav.live(s, function(t) {
                        t.preventDefault();
                        var n = e(this), r = i.controlNav.index(n);
                        n.hasClass(a + "active") || (i.direction = r > i.currentSlide ? "next" : "prev", i.flexAnimate(r, o.pauseOnAction))
                    }), r && i.controlNav.live("click touchstart", function(e) {
                        e.preventDefault()
                    })
                }, set: function() {
                    var t = "thumbnails" === o.controlNav ? "img" : "a";
                    i.controlNav = e("." + a + "control-nav li " + t, i.controlsContainer ? i.controlsContainer : i)
                }, active: function() {
                    i.controlNav.removeClass(a + "active").eq(i.animatingTo).addClass(a + "active")
                }, update: function(t, n) {
                    i.pagingCount > 1 && "add" === t ? i.controlNavScaffold.append(e("<li><a>" + i.count + "</a></li>")) : 1 === i.pagingCount ? i.controlNavScaffold.find("li").remove() : i.controlNav.eq(n).closest("li").remove(), m.controlNav.set(), i.pagingCount > 1 && i.pagingCount !== i.controlNav.length ? i.update(n, t) : m.controlNav.active()
                }}, directionNav: {setup: function() {
                    var t = e('<ul class="' + a + 'direction-nav"><li><a class="' + a + 'prev" href="#">' + o.prevText + '</a></li><li><a class="' + a + 'next" href="#">' + o.nextText + "</a></li></ul>");
                    i.controlsContainer ? (e(i.controlsContainer).append(t), i.directionNav = e("." + a + "direction-nav li a", i.controlsContainer)) : (i.append(t), i.directionNav = e("." + a + "direction-nav li a", i)), m.directionNav.update(), i.directionNav.bind(s, function(t) {
                        t.preventDefault();
                        var n = e(this).hasClass(a + "next") ? i.getTarget("next") : i.getTarget("prev");
                        i.flexAnimate(n, o.pauseOnAction)
                    }), r && i.directionNav.bind("click touchstart", function(e) {
                        e.preventDefault()
                    })
                }, update: function() {
                    var e = a + "disabled";
                    o.animationLoop || (1 === i.pagingCount ? i.directionNav.addClass(e) : 0 === i.animatingTo ? i.directionNav.removeClass(e).filter("." + a + "prev").addClass(e) : i.animatingTo === i.last ? i.directionNav.removeClass(e).filter("." + a + "next").addClass(e) : i.directionNav.removeClass(e))
                }}, pausePlay: {setup: function() {
                    var t = e('<div class="' + a + 'pauseplay"><a></a></div>');
                    i.controlsContainer ? (i.controlsContainer.append(t), i.pausePlay = e("." + a + "pauseplay a", i.controlsContainer)) : (i.append(t), i.pausePlay = e("." + a + "pauseplay a", i)), m.pausePlay.update(o.slideshow ? a + "pause" : a + "play"), i.pausePlay.bind(s, function(t) {
                        t.preventDefault(), e(this).hasClass(a + "pause") ? (i.pause(), i.manualPause = !0) : (i.play(), i.manualPause = !1)
                    }), r && i.pausePlay.bind("click touchstart", function(e) {
                        e.preventDefault()
                    })
                }, update: function(e) {
                    "play" === e ? i.pausePlay.removeClass(a + "pause").addClass(a + "play").text(o.playText) : i.pausePlay.removeClass(a + "play").addClass(a + "pause").text(o.pauseText)
                }}, touch: function() {
                function e(e) {
                    i.animating ? e.preventDefault() : 1 === e.touches.length && (i.pause(), m = l ? i.h : i.w, f = Number(new Date), p = d && c && i.animatingTo === i.last ? 0 : d && c ? i.limit - (i.itemW + o.itemMargin) * i.move * i.animatingTo : d && i.currentSlide === i.last ? i.limit : d ? (i.itemW + o.itemMargin) * i.move * i.currentSlide : c ? (i.last - i.currentSlide + i.cloneOffset) * m : (i.currentSlide + i.cloneOffset) * m, r = l ? e.touches[0].pageY : e.touches[0].pageX, s = l ? e.touches[0].pageX : e.touches[0].pageY, t.addEventListener("touchmove", n, !1), t.addEventListener("touchend", a, !1))
                }
                function n(e) {
                    h = l ? r - e.touches[0].pageY : r - e.touches[0].pageX, g = l ? Math.abs(h) < Math.abs(e.touches[0].pageX - s) : Math.abs(h) < Math.abs(e.touches[0].pageY - s), (!g || Number(new Date) - f > 500) && (e.preventDefault(), !u && i.transitions && (o.animationLoop || (h /= 0 === i.currentSlide && 0 > h || i.currentSlide === i.last && h > 0 ? Math.abs(h) / m + 2 : 1), i.setProps(p + h, "setTouch")))
                }
                function a() {
                    if (i.animatingTo === i.currentSlide && !g && null !== h) {
                        var e = c ? -h : h, l = e > 0 ? i.getTarget("next") : i.getTarget("prev");
                        i.canAdvance(l) && (Number(new Date) - f < 550 && Math.abs(e) > 20 || Math.abs(e) > m / 2) ? i.flexAnimate(l, o.pauseOnAction) : i.flexAnimate(i.currentSlide, o.pauseOnAction, !0)
                    }
                    t.removeEventListener("touchmove", n, !1), t.removeEventListener("touchend", a, !1), r = null, s = null, h = null, p = null
                }
                var r, s, p, m, h, f, g = !1;
                t.addEventListener("touchstart", e, !1)
            }, resize: function() {
                !i.animating && i.is(":visible") && (d || i.doMath(), u ? m.smoothHeight() : d ? (i.slides.width(i.computedW), i.update(i.pagingCount), i.setProps()) : l ? (i.viewport.height(i.h), i.setProps(i.h, "setTotal")) : (o.smoothHeight && m.smoothHeight(), i.newSlides.width(i.computedW), i.setProps(i.computedW, "setTotal")))
            }, smoothHeight: function(e) {
                if (!l || u) {
                    var t = u ? i : i.viewport;
                    e ? t.animate({height: i.slides.eq(i.animatingTo).height()}, e) : t.height(i.slides.eq(i.animatingTo).height())
                }
            }, sync: function(t) {
                var n = e(o.sync).data("flexslider"), a = i.animatingTo;
                switch (t) {
                    case"animate":
                        n.flexAnimate(a, o.pauseOnAction, !1, !0);
                        break;
                    case"play":
                        n.playing || n.asNav || n.play();
                        break;
                    case"pause":
                        n.pause()
                    }
            }}, i.flexAnimate = function(t, n, r, s, h) {
            if (!i.animating && (i.canAdvance(t) || r) && i.is(":visible")) {
                if (p && s) {
                    var f = e(o.asNavFor).data("flexslider");
                    if (i.atEnd = 0 === t || t === i.count - 1, f.flexAnimate(t, !0, !1, !0, h), i.direction = i.currentItem < t ? "next" : "prev", f.direction = i.direction, Math.ceil((t + 1) / i.visible) - 1 === i.currentSlide || 0 === t)
                        return i.currentItem = t, i.slides.removeClass(a + "active-slide").eq(t).addClass(a + "active-slide"), !1;
                    i.currentItem = t, i.slides.removeClass(a + "active-slide").eq(t).addClass(a + "active-slide"), t = Math.floor(t / i.visible)
                }
                if (i.animating = !0, i.animatingTo = t, o.before(i), n && i.pause(), i.syncExists && !h && m.sync("animate"), o.controlNav && m.controlNav.active(), d || i.slides.removeClass(a + "active-slide").eq(t).addClass(a + "active-slide"), i.atEnd = 0 === t || t === i.last, o.directionNav && m.directionNav.update(), t === i.last && (o.end(i), o.animationLoop || i.pause()), u)
                    i.slides.eq(i.currentSlide).fadeOut(o.animationSpeed, o.easing), i.slides.eq(t).fadeIn(o.animationSpeed, o.easing, i.wrapup);
                else {
                    var g, v, y, _ = l ? i.slides.filter(":first").height() : i.computedW;
                    d ? (g = o.itemWidth > i.w ? 2 * o.itemMargin : o.itemMargin, y = (i.itemW + g) * i.move * i.animatingTo, v = y > i.limit && 1 !== i.visible ? i.limit : y) : v = 0 === i.currentSlide && t === i.count - 1 && o.animationLoop && "next" !== i.direction ? c ? (i.count + i.cloneOffset) * _ : 0 : i.currentSlide === i.last && 0 === t && o.animationLoop && "prev" !== i.direction ? c ? 0 : (i.count + 1) * _ : c ? (i.count - 1 - t + i.cloneOffset) * _ : (t + i.cloneOffset) * _, i.setProps(v, "", o.animationSpeed), i.transitions ? (o.animationLoop && i.atEnd || (i.animating = !1, i.currentSlide = i.animatingTo), i.container.unbind("webkitTransitionEnd transitionend"), i.container.bind("webkitTransitionEnd transitionend", function() {
                        i.wrapup(_)
                    })) : i.container.animate(i.args, o.animationSpeed, o.easing, function() {
                        i.wrapup(_)
                    })
                }
                o.smoothHeight && m.smoothHeight(o.animationSpeed)
            }
        }, i.wrapup = function(e) {
            u || d || (0 === i.currentSlide && i.animatingTo === i.last && o.animationLoop ? i.setProps(e, "jumpEnd") : i.currentSlide === i.last && 0 === i.animatingTo && o.animationLoop && i.setProps(e, "jumpStart")), i.animating = !1, i.currentSlide = i.animatingTo, o.after(i)
        }, i.animateSlides = function() {
            i.animating || i.flexAnimate(i.getTarget("next"))
        }, i.pause = function() {
            clearInterval(i.animatedSlides), i.playing = !1, o.pausePlay && m.pausePlay.update("play"), i.syncExists && m.sync("pause")
        }, i.play = function() {
            i.animatedSlides = setInterval(i.animateSlides, o.slideshowSpeed), i.playing = !0, o.pausePlay && m.pausePlay.update("pause"), i.syncExists && m.sync("play")
        }, i.canAdvance = function(e) {
            var t = p ? i.pagingCount - 1 : i.last;
            return p && 0 === i.currentItem && e === i.pagingCount - 1 && "next" !== i.direction ? !1 : e !== i.currentSlide || p ? o.animationLoop ? !0 : i.atEnd && 0 === i.currentSlide && e === t && "next" !== i.direction ? !1 : i.atEnd && i.currentSlide === t && 0 === e && "next" === i.direction ? !1 : !0 : !1
        }, i.getTarget = function(e) {
            return i.direction = e, "next" === e ? i.currentSlide === i.last ? 0 : i.currentSlide + 1 : 0 === i.currentSlide ? i.last : i.currentSlide - 1
        }, i.setProps = function(e, t, n) {
            var a = function() {
                var n = e ? e : (i.itemW + o.itemMargin) * i.move * i.animatingTo, a = function() {
                    if (d)
                        return"setTouch" === t ? e : c && i.animatingTo === i.last ? 0 : c ? i.limit - (i.itemW + o.itemMargin) * i.move * i.animatingTo : i.animatingTo === i.last ? i.limit : n;
                    switch (t) {
                        case"setTotal":
                            return c ? (i.count - 1 - i.currentSlide + i.cloneOffset) * e : (i.currentSlide + i.cloneOffset) * e;
                        case"setTouch":
                            return c ? e : e;
                        case"jumpEnd":
                            return c ? e : i.count * e;
                        case"jumpStart":
                            return c ? i.count * e : e;
                        default:
                            return e
                        }
                }();
                return-1 * a + "px"
            }();
            i.transitions && (a = l ? "translate3d(0," + a + ",0)" : "translate3d(" + a + ",0,0)", n = void 0 !== n ? n / 1e3 + "s" : "0s", i.container.css("-" + i.pfx + "-transition-duration", n)), i.args[i.prop] = a, (i.transitions || void 0 === n) && i.container.css(i.args)
        }, i.setup = function(t) {
            if (u)
                i.slides.css({width: "100%", "float": "left", marginRight: "-100%", position: "relative"}), "init" === t && i.slides.eq(i.currentSlide).fadeIn(o.animationSpeed, o.easing), o.smoothHeight && m.smoothHeight();
            else {
                var n, r;
                "init" === t && (i.viewport = e('<div class="flex-viewport"></div>').css({overflow: "hidden", position: "relative"}).appendTo(i).append(i.container), i.cloneCount = 0, i.cloneOffset = 0, c && (r = e.makeArray(i.slides).reverse(), i.slides = e(r), i.container.empty().append(i.slides))), o.animationLoop && !d && (i.cloneCount = 2, i.cloneOffset = 1, "init" !== t && i.container.find(".clone").remove(), i.container.append(i.slides.first().clone().addClass("clone")).prepend(i.slides.last().clone().addClass("clone"))), i.newSlides = e(o.selector, i), n = c ? i.count - 1 - i.currentSlide + i.cloneOffset : i.currentSlide + i.cloneOffset, l && !d ? (i.container.height(200 * (i.count + i.cloneCount) + "%").css("position", "absolute").width("100%"), setTimeout(function() {
                    i.newSlides.css({display: "block"}), i.doMath(), i.viewport.height(i.h), i.setProps(n * i.h, "init")
                }, "init" === t ? 100 : 0)) : (i.container.width(200 * (i.count + i.cloneCount) + "%"), i.setProps(n * i.computedW, "init"), setTimeout(function() {
                    i.doMath(), i.newSlides.css({width: i.computedW, "float": "left", display: "block"}), o.smoothHeight && m.smoothHeight()
                }, "init" === t ? 100 : 0))
            }
            d || i.slides.removeClass(a + "active-slide").eq(i.currentSlide).addClass(a + "active-slide")
        }, i.doMath = function() {
            var e = i.slides.first(), t = o.itemMargin, n = o.minItems, a = o.maxItems;
            i.w = i.width(), i.h = e.height(), i.boxPadding = e.outerWidth() - e.width(), d ? (i.itemT = o.itemWidth + t, i.minW = n ? n * i.itemT : i.w, i.maxW = a ? a * i.itemT : i.w, i.itemW = i.minW > i.w ? (i.w - t * n) / n : i.maxW < i.w ? (i.w - t * a) / a : o.itemWidth > i.w ? i.w : o.itemWidth, i.visible = Math.floor(i.w / (i.itemW + t)), i.move = o.move > 0 && o.move < i.visible ? o.move : i.visible, i.pagingCount = Math.ceil((i.count - i.visible) / i.move + 1), i.last = i.pagingCount - 1, i.limit = 1 === i.pagingCount ? 0 : o.itemWidth > i.w ? (i.itemW + 2 * t) * i.count - i.w - t : (i.itemW + t) * i.count - i.w) : (i.itemW = i.w, i.pagingCount = i.count, i.last = i.count - 1), i.computedW = i.itemW - i.boxPadding
        }, i.update = function(e, t) {
            i.doMath(), d || (e < i.currentSlide ? i.currentSlide += 1 : e <= i.currentSlide && 0 !== e && (i.currentSlide -= 1), i.animatingTo = i.currentSlide), o.controlNav && !i.manualControls && ("add" === t && !d || i.pagingCount > i.controlNav.length ? m.controlNav.update("add") : ("remove" === t && !d || i.pagingCount < i.controlNav.length) && (d && i.currentSlide > i.last && (i.currentSlide -= 1, i.animatingTo -= 1), m.controlNav.update("remove", i.last))), o.directionNav && m.directionNav.update()
        }, i.addSlide = function(t, n) {
            var a = e(t);
            i.count += 1, i.last = i.count - 1, l && c ? void 0 !== n ? i.slides.eq(i.count - n).after(a) : i.container.prepend(a) : void 0 !== n ? i.slides.eq(n).before(a) : i.container.append(a), i.update(n, "add"), i.slides = e(o.selector + ":not(.clone)", i), i.setup(), o.added(i)
        }, i.removeSlide = function(t) {
            var n = isNaN(t) ? i.slides.index(e(t)) : t;
            i.count -= 1, i.last = i.count - 1, isNaN(t) ? e(t, i.slides).remove() : l && c ? i.slides.eq(i.last).remove() : i.slides.eq(t).remove(), i.doMath(), i.update(n, "remove"), i.slides = e(o.selector + ":not(.clone)", i), i.setup(), o.removed(i)
        }, m.init()
    }, e.flexslider.defaults = {namespace: "flex-", selector: ".slides > li", animation: "fade", easing: "swing", direction: "horizontal", reverse: !1, animationLoop: !0, smoothHeight: !1, startAt: 0, slideshow: !0, slideshowSpeed: 7e3, animationSpeed: 600, initDelay: 0, randomize: !1, pauseOnAction: !0, pauseOnHover: !1, useCSS: !0, touch: !0, video: !1, controlNav: !0, directionNav: !0, prevText: "Previous", nextText: "Next", keyboard: !0, multipleKeyboard: !1, mousewheel: !1, pausePlay: !1, pauseText: "Pause", playText: "Play", controlsContainer: "", manualControls: "", sync: "", asNavFor: "", itemWidth: 0, itemMargin: 0, minItems: 0, maxItems: 0, move: 0, start: function() {
        }, before: function() {
        }, after: function() {
        }, end: function() {
        }, added: function() {
        }, removed: function() {
        }}, e.fn.flexslider = function(t) {
        if (t = t || {}, "object" == typeof t)
            return this.each(function() {
                var n = e(this), i = t.selector ? t.selector : ".slides > li", o = n.find(i);
                1 === o.length ? (o.fadeIn(400), t.start && t.start(n)) : void 0 === n.data("flexslider") && new e.flexslider(this, t)
            });
        var n = e(this).data("flexslider");
        switch (t) {
            case"play":
                n.play();
                break;
            case"pause":
                n.pause();
                break;
            case"next":
                n.flexAnimate(n.getTarget("next"), !0);
                break;
            case"prev":
            case"previous":
                n.flexAnimate(n.getTarget("prev"), !0);
                break;
            default:
                "number" == typeof t && n.flexAnimate(t, !0)
            }
    }
}(jQuery), define("jquery.flexslider", ["jquery"], function() {
}), define("app/modules/heroSlider", ["require", "jquery", "jquery.waitforimages", "jquery.flexslider"], function(e) {
    var t = e("jquery");
    return e("jquery.waitforimages"), e("jquery.flexslider"), {ui: {heroSlider: t(".hero-slider"), flexSlider: t(".flexslider"), slides: t(".slides"), images: t(".slides").find("[data-loaded=false]").find("img"), loader: t(".loader-bar"), loaderBar: t(".loader-bar").find("div")}, initialize: function() {
            this.ui.heroSlider.length && (this.initSlider(), this.initLoadingIndicator())
        }, initLoadingIndicator: function() {
            var e = this;
            this.ui.loader.show(), t("body").waitForImages(function() {
                setTimeout(function() {
                    t("body").removeClass("loading")
                }, 1e3)
            }, function(t, n) {
                var i = Math.floor(100 * (t / n));
                e.ui.loaderBar.css("width", i + "%")
            }, !0)
        }, initSlider: function() {
            this.ui.flexSlider.flexslider({animation: "slide", animationLoop: !0, itemMargin: 0, pausePlay: !1, useCSS: !0, slideshow: !0, touch: !0})
        }}
}), define("app/modules/header", ["require", "jquery", "./bookFlight"], function(e) {
    var t = e("jquery"), n = e("./bookFlight");
    return{ui: {win: t(window), doc: t(document), html: t("html"), body: t("body"), hdr: t(".header"), nav: t(".nav"), navBtn: t(".navicon"), bafBtn: t(".baf")}, initialize: function() {
            this.bindUIEvents(), this.setMenuHeight()
        }, bindUIEvents: function() {
            var e = this;
            this.ui.navBtn.on("click", function() {
                e.onClickNavBtn()
            }), this.ui.win.on("resize", function() {
                e.setMenuHeight()
            }), this.ui.bafBtn.on("click", function() {
                e.onClickBAFBtn()
            }), this.ui.win.on("scroll.checkHeaderState", function() {
                e.onScrollPage()
            })
        }, onScrollPage: function() {
            this.ui.win.scrollTop() >= this.ui.hdr.outerHeight(!0) && !this.ui.body.hasClass("page-landing") ? this.ui.hdr.addClass("is-collapsed") : this.ui.hdr.removeClass("is-collapsed")
        }, navIsOpen: function() {
            return this.ui.body.hasClass("nav-is-open")
        }, onClickNavBtn: function() {
            this.navIsOpen() ? this.closeMenu() : (this.openMenu(), n.close())
        }, onClickBAFBtn: function() {
            n.open(), this.closeMenu()
        }, openMenu: function() {
            this.setMenuHeight(), this.ui.body.addClass("nav-is-open"), this.preventNaturalScroll()
        }, closeMenu: function() {
            this.ui.body.removeClass("nav-is-open"), this.resumeNaturalScroll()
        }, preventNaturalScroll: function() {
            var e = function(e) {
                e.preventDefault()
            }, t = function(e) {
                e.stopPropagation()
            };
            this.ui.doc.on("touchmove", e), this.ui.doc.on("touchmove", ".scrollable", t)
        }, resumeNaturalScroll: function() {
            this.ui.doc.off("touchmove")
        }, setMenuHeight: function() {
            var e = window.innerHeight;
            //this.ui.nav.css("height", 'auto');
        }}
}), define("app/modules/banner", ["require", "jquery"], function(e) {
    var t = e("jquery");
    return{ui: {banner: t(".banner-wrapper")}, initialize: function() {
            this.ui.banner.load("/banner/")
        }}
}), jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, {def: "easeOutQuad", swing: function(e, t, n, i, o) {
        return jQuery.easing[jQuery.easing.def](e, t, n, i, o)
    }, easeInQuad: function(e, t, n, i, o) {
        return i * (t /= o) * t + n
    }, easeOutQuad: function(e, t, n, i, o) {
        return-i * (t /= o) * (t - 2) + n
    }, easeInOutQuad: function(e, t, n, i, o) {
        return(t /= o / 2) < 1 ? i / 2 * t * t + n : -i / 2 * (--t * (t - 2) - 1) + n
    }, easeInCubic: function(e, t, n, i, o) {
        return i * (t /= o) * t * t + n
    }, easeOutCubic: function(e, t, n, i, o) {
        return i * ((t = t / o - 1) * t * t + 1) + n
    }, easeInOutCubic: function(e, t, n, i, o) {
        return(t /= o / 2) < 1 ? i / 2 * t * t * t + n : i / 2 * ((t -= 2) * t * t + 2) + n
    }, easeInQuart: function(e, t, n, i, o) {
        return i * (t /= o) * t * t * t + n
    }, easeOutQuart: function(e, t, n, i, o) {
        return-i * ((t = t / o - 1) * t * t * t - 1) + n
    }, easeInOutQuart: function(e, t, n, i, o) {
        return(t /= o / 2) < 1 ? i / 2 * t * t * t * t + n : -i / 2 * ((t -= 2) * t * t * t - 2) + n
    }, easeInQuint: function(e, t, n, i, o) {
        return i * (t /= o) * t * t * t * t + n
    }, easeOutQuint: function(e, t, n, i, o) {
        return i * ((t = t / o - 1) * t * t * t * t + 1) + n
    }, easeInOutQuint: function(e, t, n, i, o) {
        return(t /= o / 2) < 1 ? i / 2 * t * t * t * t * t + n : i / 2 * ((t -= 2) * t * t * t * t + 2) + n
    }, easeInSine: function(e, t, n, i, o) {
        return-i * Math.cos(t / o * (Math.PI / 2)) + i + n
    }, easeOutSine: function(e, t, n, i, o) {
        return i * Math.sin(t / o * (Math.PI / 2)) + n
    }, easeInOutSine: function(e, t, n, i, o) {
        return-i / 2 * (Math.cos(Math.PI * t / o) - 1) + n
    }, easeInExpo: function(e, t, n, i, o) {
        return 0 == t ? n : i * Math.pow(2, 10 * (t / o - 1)) + n
    }, easeOutExpo: function(e, t, n, i, o) {
        return t == o ? n + i : i * (-Math.pow(2, -10 * t / o) + 1) + n
    }, easeInOutExpo: function(e, t, n, i, o) {
        return 0 == t ? n : t == o ? n + i : (t /= o / 2) < 1 ? i / 2 * Math.pow(2, 10 * (t - 1)) + n : i / 2 * (-Math.pow(2, -10 * --t) + 2) + n
    }, easeInCirc: function(e, t, n, i, o) {
        return-i * (Math.sqrt(1 - (t /= o) * t) - 1) + n
    }, easeOutCirc: function(e, t, n, i, o) {
        return i * Math.sqrt(1 - (t = t / o - 1) * t) + n
    }, easeInOutCirc: function(e, t, n, i, o) {
        return(t /= o / 2) < 1 ? -i / 2 * (Math.sqrt(1 - t * t) - 1) + n : i / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + n
    }, easeInElastic: function(e, t, n, i, o) {
        var a = 1.70158, r = 0, s = i;
        if (0 == t)
            return n;
        if (1 == (t /= o))
            return n + i;
        if (r || (r = .3 * o), s < Math.abs(i)) {
            s = i;
            var a = r / 4
        } else
            var a = r / (2 * Math.PI) * Math.asin(i / s);
        return-(s * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * o - a) * 2 * Math.PI / r)) + n
    }, easeOutElastic: function(e, t, n, i, o) {
        var a = 1.70158, r = 0, s = i;
        if (0 == t)
            return n;
        if (1 == (t /= o))
            return n + i;
        if (r || (r = .3 * o), s < Math.abs(i)) {
            s = i;
            var a = r / 4
        } else
            var a = r / (2 * Math.PI) * Math.asin(i / s);
        return s * Math.pow(2, -10 * t) * Math.sin((t * o - a) * 2 * Math.PI / r) + i + n
    }, easeInOutElastic: function(e, t, n, i, o) {
        var a = 1.70158, r = 0, s = i;
        if (0 == t)
            return n;
        if (2 == (t /= o / 2))
            return n + i;
        if (r || (r = o * .3 * 1.5), s < Math.abs(i)) {
            s = i;
            var a = r / 4
        } else
            var a = r / (2 * Math.PI) * Math.asin(i / s);
        return 1 > t ? -.5 * s * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * o - a) * 2 * Math.PI / r) + n : .5 * s * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * o - a) * 2 * Math.PI / r) + i + n
    }, easeInBack: function(e, t, n, i, o, a) {
        return void 0 == a && (a = 1.70158), i * (t /= o) * t * ((a + 1) * t - a) + n
    }, easeOutBack: function(e, t, n, i, o, a) {
        return void 0 == a && (a = 1.70158), i * ((t = t / o - 1) * t * ((a + 1) * t + a) + 1) + n
    }, easeInOutBack: function(e, t, n, i, o, a) {
        return void 0 == a && (a = 1.70158), (t /= o / 2) < 1 ? i / 2 * t * t * (((a *= 1.525) + 1) * t - a) + n : i / 2 * ((t -= 2) * t * (((a *= 1.525) + 1) * t + a) + 2) + n
    }, easeInBounce: function(e, t, n, i, o) {
        return i - jQuery.easing.easeOutBounce(e, o - t, 0, i, o) + n
    }, easeOutBounce: function(e, t, n, i, o) {
        return(t /= o) < 1 / 2.75 ? i * 7.5625 * t * t + n : 2 / 2.75 > t ? i * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + n : 2.5 / 2.75 > t ? i * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + n : i * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + n
    }, easeInOutBounce: function(e, t, n, i, o) {
        return o / 2 > t ? .5 * jQuery.easing.easeInBounce(e, 2 * t, 0, i, o) + n : .5 * jQuery.easing.easeOutBounce(e, 2 * t - o, 0, i, o) + .5 * i + n
    }}), define("easing", ["jquery"], function(e) {
    return function() {
        var t;
        return t || e.easing
    }
}(this)), define("app/modules/scrollToAnchor", ["require", "jquery", "underscore", "easing"], function(e) {
    var t = e("jquery"), n = e("underscore");
    return e("easing"), {hasHash: function(e) {
            n.delay(t.proxy(function() {
                this.go(e)
            }, this), 1e3)
        }, go: function(e) {
            var n = "string" == typeof e ? e : t(e.currentTarget).attr("href"), i = /^(#|.)/.test(n) ? t("[name=" + n.slice(1) + "]") : t("[name=" + n + "]");
            return t("html,body").animate({scrollTop: i.offset().top}, 1e3, "easeOutExpo"), !1
        }}
}), function(e, t, n) {
    var i = navigator.userAgent.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/), o = !!i, a = o && parseFloat(i[1]) < 7, r = navigator.userAgent.match(/iPad|iPhone|Android|IEMobile|BlackBerry/i), s = {}, l = [], c = {left: 37, up: 38, right: 39, down: 40, enter: 13, tab: 9, zero: 48, z: 90, last: 221}, d = ['<div class="dk_container" id="dk_container_{{ id }}" tabindex="{{ tabindex }}" aria-hidden="true">', '<a class="dk_toggle dk_label">{{ label }}</a>', '<div class="dk_options">', '<ul class="dk_options_inner" role="main" aria-hidden="true">', "</ul>", "</div>", "</div>"].join(""), u = '<li><a data-dk-dropdown-value="{{ value }}">{{ text }}</a></li>', p = {startSpeed: 400, theme: !1, changes: !1, syncReverse: !0, nativeMobile: !0, autoWidth: !0}, m = null, h = null, f = function(e, t, n) {
        var i, o, a, r;
        i = e.attr("data-dk-dropdown-value"), o = e.text(), a = t.data("dropkick"), r = a.$select, r.val(i).trigger("change"), t.find(".dk_label").text(o), n = n || !1, !a.settings.change || n || a.settings.syncReverse || a.settings.change.call(r, i, o)
    }, g = function(e) {
        e.removeClass("dk_open dk_open_top"), m = null
    }, v = function(n) {
        var i = n.find(".dk_toggle"), o = n.find(".dk_options").outerHeight(), a = e(t).height() - i.outerHeight() - i.offset().top + e(t).scrollTop(), r = i.offset().top - e(t).scrollTop();
        return r > o ? a > o : !0
    }, y = function(e, t, n) {
        var i = e.find(".dk_options_inner"), o = t.prevAll("li").outerHeight() * t.prevAll("li").length, a = i.scrollTop(), r = i.height() + i.scrollTop() - t.outerHeight();
        (n && "keydown" === n.type || a > o || o > r) && i.scrollTop(o)
    }, _ = function(e, t) {
        var n = v(e);
        m = e.toggleClass("dk_open");
        var n = v(e), i = n ? "dk_open" : "dk_open_top dk_open";
        e.find(".dk_options").css({top: n ? e.find(".dk_toggle").outerHeight() - 1 : "", bottom: n ? "" : e.find(".dk_toggle").outerHeight() - 1}), m = e.addClass(i), y(e, e.find(".dk_option_current"), t)
    }, b = function(e, t, n) {
        t.find(".dk_option_current").removeClass("dk_option_current"), e.addClass("dk_option_current"), y(t, e, n)
    }, w = function(t, n) {
        var i, o, a, r, s, l, d, u = t.keyCode, p = n.data("dropkick"), m = String.fromCharCode(u), h = n.find(".dk_options"), v = n.hasClass("dk_open"), y = h.find("li"), w = n.find(".dk_option_current"), C = y.first(), x = y.last();
        switch (u) {
            case c.enter:
                v ? w.hasClass("disabled") || (f(w.find("a"), n), g(n)) : _(n, t), t.preventDefault();
                break;
            case c.tab:
                v && (w.length && f(w.find("a"), n), g(n));
                break;
            case c.up:
                o = w.prev("li"), v ? o.length ? b(o, n, t) : b(x, n, t) : _(n, t), t.preventDefault();
                break;
            case c.down:
                v ? (i = w.next("li").first(), i.length ? b(i, n, t) : b(C, n, t)) : _(n, t), t.preventDefault()
        }
        if (u >= c.zero && u <= c.z) {
            for (a = (new Date).getTime(), null === p.finder || void 0 === p.finder?(p.finder = m.toUpperCase(), p.timer = a):a > parseInt(p.timer, 10) + 1e3?(p.finder = m.toUpperCase(), p.timer = a):(p.finder = p.finder + m.toUpperCase(), p.timer = a), r = y.find("a"), s = 0, l = r.length; l > s; s++)
                if (d = e(r[s]), 0 === d.html().toUpperCase().indexOf(p.finder)) {
                    f(d, n), b(d.parent(), n, t);
                    break
                }
            n.data("dropkick", p)
        }
    }, C = function(t) {
        return e.trim(t).length > 0 ? t : !1
    }, x = function(t, n) {
        var i, o, a, r, s, l = t.replace("{{ id }}", n.id).replace("{{ label }}", n.label).replace("{{ tabindex }}", n.tabindex), c = [];
        if (n.options && n.options.length)
            for (o = 0, a = n.options.length; a > o; o++)
                r = e(n.options[o]), s = 0 === o && void 0 !== r.attr("selected") && void 0 !== r.attr("disabled") ? null : u.replace("{{ value }}", r.val()).replace("{{ current }}", C(r.val()) === n.value ? "dk_option_current" : "").replace("{{ disabled }}", void 0 !== r.attr("disabled") ? "disabled" : "").replace("{{ text }}", e.trim(r.html())), c[c.length] = s;
        return i = e(l), i.find(".dk_options_inner").html(c.join("")), i
    };
    a || (n.documentElement.className = n.documentElement.className + " dk_fouc"), s.init = function(t) {
        return t = e.extend({}, p, t), d = t.dropdownTemplate ? t.dropdownTemplate : d, u = t.optionTemplate ? t.optionTemplate : u, this.each(function() {
            var n, i, o = e(this), a = o.find(":selected").first(), s = o.find("option"), c = o.data("dropkick") || {}, u = o.attr("id") || o.attr("name"), p = (t.width || o.outerWidth(), o.attr("tabindex") || "0"), m = !1;
            return c.id ? o : (c.settings = t, c.tabindex = p, c.id = u, c.$original = a, c.$select = o, c.value = C(o.val()) || C(a.attr("value")), c.label = a.text(), c.options = s, m = x(d, c), c.settings.autoWidth && m.find(".dk_toggle").css({width: "100%"}), o.before(m).appendTo(m), m = e('div[id="dk_container_' + u + '"]').fadeIn(t.startSpeed), n = t.theme || "default", m.addClass("dk_theme_" + n), c.theme = n, c.$dk = m, o.data("dropkick", c), m.addClass(o.attr("class")), m.data("dropkick", c), l[l.length] = o, m.on("focus.dropkick", function() {
                h = m.addClass("dk_focus")
            }).on("blur.dropkick", function() {
                m.removeClass("dk_focus"), h = null
            }), r && c.settings.nativeMobile && m.addClass("dk_mobile"), c.settings.syncReverse && o.on("change", function(t) {
                var n = o.val(), i = e('a[data-dk-dropdown-value="' + n + '"]', m), a = i.text();
                m.find(".dk_label").text(a), c.settings.change && c.settings.change.call(o, n, a), b(i.parent(), m, t)
            }), i = o.attr("form") ? e("#" + o.attr("form").replace(" ", ", #")) : o.closest("form"), i.length && i.on("reset", function() {
                o.dropkick("reset")
            }), void 0)
        })
    }, s.theme = function(t) {
        var n = e(this).data("dropkick"), i = n.$dk, o = "dk_theme_" + n.theme;
        i.removeClass(o).addClass("dk_theme_" + t), n.theme = t
    }, s.reset = function() {
        return this.each(function() {
            var t = e(this).data("dropkick"), n = t.$dk, i = e('a[data-dk-dropdown-value="' + t.$original.attr("value") + '"]', n);
            t.$original.prop("selected", !0), n.find(".dk_label").text(t.label), b(i.parent(), n)
        })
    }, s.setValue = function(t) {
        return this.each(function() {
            var n = e(this).data("dropkick").$dk, i = e('.dk_options a[data-dk-dropdown-value="' + t + '"]', n);
            i.length ? f(i, n) | b(i.parent(), n) : console.warn("There is no option with this value in " + n.selector)
        })
    }, s.refresh = function() {
        return this.each(function() {
            var t, n, i = e(this).data("dropkick"), o = i.$select, a = i.$dk;
            i.options = o.find("option"), n = x(d, i).find(".dk_options_inner"), a.find(".dk_options_inner").replaceWith(n), t = e('a[data-dk-dropdown-value="' + o.val() + '"]', a), b(t.parent(), a)
        })
    }, e.fn.dropkick = function(e) {
        if (!a) {
            if (s[e])
                return s[e].apply(this, Array.prototype.slice.call(arguments, 1));
            if ("object" == typeof e || !e)
                return s.init.apply(this, arguments)
        }
    }, e(function() {
        e(n).on(o ? "mousedown" : "click", ".dk_options a", function() {
            var t = e(this), n = t.parents(".dk_container").first();
            return t.parent().hasClass("disabled") || (f(t, n), b(t.parent(), n), g(n)), !1
        }), e(n).on("keydown.dk_nav", function(e) {
            var t;
            m ? t = m : h && (t = h), t && w(e, t)
        }), e(n).on("click", null, function(t) {
            var n, i = e(t.target);
            if (m && 0 === i.closest(".dk_container").length)
                g(m);
            else {
                if (i.is(".dk_toggle, .dk_label"))
                    return n = i.parents(".dk_container").first(), n.hasClass("dk_open") ? g(n) : (m && g(m), _(n, t)), !1;
                i.attr("for") && e("#dk_container_" + i.attr("for"))[0] && e("#dk_container_" + i.attr("for")).trigger("focus.dropkick")
            }
        });
        var i = "onwheel"in t ? "wheel" : "onmousewheel"in n ? "mousewheel" : "MouseScrollEvent"in t ? "DOMMouseScroll MozMousePixelScroll" : !1;
        i && e(n).on(i, ".dk_options_inner", function(e) {
            var t = e.originalEvent.wheelDelta || -e.originalEvent.deltaY || -e.originalEvent.detail;
            return o ? (this.scrollTop -= Math.round(t / 10), !1) : t > 0 && this.scrollTop <= 0 || 0 > t && this.scrollTop >= this.scrollHeight - this.offsetHeight ? !1 : !0
        })
    })
}(jQuery, window, document), define("jquery.dropkick", ["jquery"], function() {
}), define("app/views/SiteView", ["require", "jquery", "underscore", "app/modules/bookFlight", "app/modules/heroSlider", "app/modules/header", "app/modules/banner", "app/modules/scrollToAnchor", "app/utils/helpers", "app/utils/constants", "jquery.dropkick"], function(e) {
    var t = e("jquery"), n = (e("underscore"), e("app/modules/bookFlight")), i = e("app/modules/heroSlider"), o = e("app/modules/header"), a = e("app/modules/banner"), r = e("app/modules/scrollToAnchor"), s = e("app/utils/helpers"), l = e("app/utils/constants");
    return e("jquery.dropkick"), {ui: {win: t(window), doc: t(document), body: t("body"), selects: t("select"), anchors: t("a[href*=#]:not([href=#])")}, initialize: function() {
            n.initialize(), i.initialize(), o.initialize(), a.initialize(), this.bindUIEvents(), this.initFauxSelects(), s.isMobile() || s.onScrollAnimate({delegate: ".inner", target: ".card, .bio, .oneway-flight"}), this.ui.body.hasClass("initing") && this.ui.body.removeClass("initing"), location.hash && (window.scrollTo(0, 0), r.hasHash(location.hash)), this.trackPageLoad()
        }, bindUIEvents: function() {
            this.ui.anchors.on("click", r.go)
        }, initFauxSelects: function() {
            this.ui.selects.dropkick()
        }, trackPageLoad: function() {
            var e, t, n = l.floodlight, i = document.location.pathname;
            switch (!0) {
                case-1 !== i.indexOf("oneway"):
                    e = n.ONE_WAY_PAGE;
                    break;
                case-1 !== i.indexOf("programs"):
                    e = n.PROGRAM_PAGE;
                    break;
                case-1 !== i.indexOf("safety"):
                    e = n.SAFETY_PAGE;
                    break;
                case-1 !== i.indexOf("jet"):
                    t = this.ui.body.data("jet-model"), t && (-1 !== t.indexOf("Gulfstream") ? e = n.JET_GULFSTREEM_PAGE : -1 !== t.indexOf("Falcon") && (e = n.JET_FALCON_PAGE));
                    break;
                case-1 !== i.indexOf("fleet"):
                    e = n.FLEET_PAGE;
                    break;
                case-1 !== i.indexOf("maintenance"):
                    e = n.MAINTENANCE_PAGE;
                    break;
                case-1 !== i.indexOf("owners"):
                    e = n.JET_MANAGEMENT_PAGE;
                    break;
                case-1 !== i.indexOf("asia"):
                    e = n.ASIA_JET_PAGE;
                    break;
                case-1 !== i.indexOf("career"):
                    e = n.CAREERS_PAGE;
                    break;
                case-1 !== i.indexOf("contact"):
                    e = n.CONTACT_PAGE;
                    break;
                case-1 !== i.indexOf("news"):
                    e = n.NEWS_PRESS_PAGE;
                    break;
                case-1 !== i.indexOf("promotion"):
                    e = n.PROMOTION_PAGE;
                    break;
                case-1 !== i.indexOf("who_we_are"):
                    e = n.WHO_WE_ARE_PAGE;
                    break;
                case-1 !== i.indexOf("company"):
                    e = n.WHAT_WE_STAND_FOR_PAGE;
                    break;
                default:
                    e = n.LANDING_PAGE
            }
            s.trackEvent({floodlight: {catId: e, loadEvent: !0}})
        }}
}), function(e) {
    var t = {sectionContainer: "section", easing: "ease", animationTime: 1e3, keyboard: !0, beforeMove: null, afterMove: null, loop: !1, afterLoad: null, transitionEndTarget: null, transitionEndProperty: "opacity", bodyDisableClass: "disabled-onepage-scroll"};
    e.fn.scrollTrigger = function(n) {
        function i() {
            e("body").addClass("viewing-page-1"), c.addClass("onepage-wrapper"), e.each(d, function(t) {
                e(this).addClass("section").attr("data-index", t + 1)
            }), "function" == typeof l.afterLoad && l.afterLoad(), e(l.sectionContainer + "[data-index='1']").addClass("active"), s()
        }
        function o(t) {
            var n = {moveTo: t.moveTo || null, direction: t.direction || null}, i = e(l.sectionContainer + ".active"), o = i.data("index"), a = null !== n.moveTo ? n.moveTo : "up" === n.direction ? o - 1 : o + 1, s = e(l.sectionContainer + "[data-index='" + a + "']"), c = e("body");
            s.length > 0 && (r(i, s), "function" == typeof l.beforeMove && l.beforeMove(a), i.removeClass("active"), s.addClass("active"), c[0].className = c[0].className.replace(/\bviewing-page-\d.*?\b/g, ""), c.addClass("viewing-page-" + s.data("index")))
        }
        function a(e, t) {
            var n = (new Date).getTime();
            return n - u < p + l.animationTime ? (e.preventDefault(), void 0) : (0 > t ? o({direction: "down"}) : o({direction: "up"}), u = n, void 0)
        }
        function r(t, n) {
            var i = function(t) {
                var n = e(this);
                n.find(l.transitionEndTarget).get(0) === t.target && n.off("webkitTransitionEnd.scrolltrigger transitionend.scrolltrigger").removeClass("animating animate-out animate-in").trigger("animationEnd.scrolltrigger")
            };
            t.addClass("animating animate-out"), n.addClass("animating animate-in"), n.on("webkitTransitionEnd.scrolltrigger transitionend.scrolltrigger", i), t.on("webkitTransitionEnd.scrolltrigger transitionend.scrolltrigger", i)
        }
        function s() {
            var t = !1;
            void 0 !== document.onmousewheel && (t = "mousewheel");
            try {
                WheelEvent("wheel"), t = "wheel"
            } catch (n) {
            }
            t || (t = "MozMousePixelScroll"), e(document).on(t, ".app", function(n) {
                var i, o = n.originalEvent;
                n.preventDefault(), i = "mousewheel" == t ? o.wheelDelta && .025 * o.wheelDelta || -o.deltaY : "wheel" == t ? -o.deltaY : -o.detail, e("html,body").hasClass(l.bodyDisableClass) || a(n, i)
            }), Modernizr.touch && c.swipeEvents().bind("swipeDown", function(t) {
                e("html,body").hasClass(l.bodyDisableClass) || t.preventDefault(), o({direction: "up"})
            }).bind("swipeUp", function(t) {
                e("html,body").hasClass(l.bodyDisableClass) || t.preventDefault(), o({direction: "down"})
            }), l.keyboard === !0 && e(document).keydown(function(e) {
                38 === e.which && o({direction: "up"}), 40 === e.which && o({direction: "down"})
            })
        }
        var l = e.extend({}, t, n), c = e(this), d = e(l.sectionContainer), u = (d.length, 0), p = 1e3;
        return e.fn.moveTo = function(e) {
            o({moveTo: e})
        }, i(), !1
    }, e.fn.swipeEvents = function() {
        return this.each(function() {
            function t(e) {
                var t = e.originalEvent.touches;
                t && t.length && (i = t[0].pageX, o = t[0].pageY, a.bind("touchmove", n))
            }
            function n(e) {
                var t = e.originalEvent.touches;
                if (t && t.length) {
                    var r = i - t[0].pageX, s = o - t[0].pageY;
                    r >= 50 && a.trigger("swipeLeft"), -50 >= r && a.trigger("swipeRight"), s >= 50 && a.trigger("swipeUp"), -50 >= s && a.trigger("swipeDown"), (Math.abs(r) >= 50 || Math.abs(s) >= 50) && a.unbind("touchmove", n)
                }
            }
            var i, o, a = e(this);
            a.bind("touchstart", t)
        })
    }
}(jQuery), define("jquery.scrolltrigger", ["easing", "jquery"], function() {
}), function(e) {
    e.fn.animateNumbers = function(t, n, i, o) {
        return this.each(function() {
            var a = e(this), r = parseInt(a.text().replace(/,/g, ""));
            n = void 0 === n ? !0 : n, e({value: r}).animate({value: t}, {duration: void 0 == i ? 1e3 : i, easing: void 0 == o ? "swing" : o, step: function() {
                    a.text(Math.floor(this.value)), n && a.text(a.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"))
                }, complete: function() {
                    parseInt(a.text()) !== t && (a.text(t), n && a.text(a.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")))
                }})
        })
    }
}(jQuery), define("jquery.animateNumbers", ["jquery"], function() {
}), define("app/views/landingPage", ["require", "jquery.scrolltrigger", "app/utils/helpers", "jquery.waitforimages", "jquery.animateNumbers"], function(e) {
    var t = (e("jquery.scrolltrigger"), e("app/utils/helpers"));
    return e("jquery.waitforimages"), e("jquery.animateNumbers"), {ui: {whisp: $(".whisp"), scrollStory: $("#scroll-story"), elevationWrapper: $(".elevation"), elevation: $(".altimeter"), elevationValue: $(".height"), elevationStops: $(".stop"), loadingIndicator: $(".loading-indicator"), percentage: $(".percentage"), progressBar: $(".progress-bar"), downArrow: $(".down-arrow")}, initialize: function() {
            var e = this;
            this.initLoadingIndicator(), this.preloadStaticImages(), Modernizr.csstransitions && (this.initScrollStory(), this.ui.elevationWrapper.on("click", ".stop", function() {
                var t = $(this).attr("class").replace("stop stop-", "");
                e.ui.scrollStory.moveTo(t)
            }), this.ui.downArrow.on("click", function() {
                var t = $("body").attr("class").replace("page-landing viewing-page-", ""), n = parseInt(t, 10) + 1;
                e.ui.scrollStory.moveTo(n)
            }), t.isMobile() || this.responsiveImages()), $(window).on("resize", _.bind(e.setScrollStoryHeight, this))
        }, setScrollStoryHeight: function() {
            var e = window.innerHeight;
            this.ui.scrollStory.css({minHeight: e}), window.scrollTo(0, 0)
        }, preloadStaticImages: function() {
            /*for (var e = new Image, t = staticUrl + "img/", n = [t + "fleet/bg-hero.jpg"], i = n.length - 1; i >= 0; i--)
                e.src = n[i]*/
        }, initLoadingIndicator: function() {
            var e = this;
            $("body").waitForImages(function() {
                $(this).removeClass("loading"), e.setScrollStoryHeight()
            }, function(t, n) {
                var i = Math.floor(100 * (t / n));
                e.ui.percentage.html(i)
            }, !0)
        }, initScrollStory: function() {
            var e = $("section", this.ui.scrollStory), n = e.size(), i = [0, 1e3, 1e4, 4e4], o = (i[0], i[1], 0), a = this;
            t.isMobile() && document.addEventListener("touchmove", function(e) {
                var t = $(e.target);
                t.parents(".nav").length || t.parents(".book-flight").length || e.preventDefault()
            }, !1), this.ui.scrollStory.scrollTrigger({animationTime: 950, keyboard: !0, transitionEndTarget: ".bg", bodyDisableClass: "form-is-visible", beforeMove: function(r) {
                    var s = a.ui.scrollStory.find(".active"), l = e.eq(r - 1), c = o > r - 1 ? "up" : "down", d = "up" == c ? "up" : "down";
                    s.attr("data-transition", d + "Out"), l.attr("data-transition", d + "In"), t.isMobile() || (1 == r && r != n ? a.ui.whisp.removeClass("active") : a.ui.whisp.addClass("active"), l.one("animationEnd.scrolltrigger", function() {
                        $(".move").removeClass("move"), r != n && 1 != r && l.addClass("move")
                    }), _.delay(function() {
                        a.ui.elevation.attr("data-tilt", "")
                    }, 1300)), a.ui.elevationValue.animateNumbers(i[r - 1], !0, 2e3), a.ui.elevationWrapper.attr("data-slide", r), a.ui.elevation.attr("data-tilt", d), o = r
                }})
        }, responsiveImages: function() {
            $("[data-responsive-img]").each(function() {
                var e = $(this), t = $("<img/>");
                t.attr({"class": e.attr("class"), src: e.attr("img-src")}), t.insertAfter(e), e.remove()
            })
        }}
}), define("app/modules/heroFull", ["require", "jquery"], function(e) {
    var t = e("jquery");
    return{ui: {win: t(window), hdr: t(".header"), wrapper: t(".hero")}, initialize: function() {
            this.onResize(), this.ui.win.on("resize.heroFull", t.proxy(this.onResize, this))
        }, onResize: function() {
            var e = this.ui.win.height() - this.ui.hdr.outerHeight(!0);
            this.ui.wrapper.css("min-height", e)
        }}
}), function(e, t) {
    function n(t, n) {
        var o, a, r, s = t.nodeName.toLowerCase();
        return"area" === s ? (o = t.parentNode, a = o.name, t.href && a && "map" === o.nodeName.toLowerCase() ? (r = e("img[usemap=#" + a + "]")[0], !!r && i(r)) : !1) : (/input|select|textarea|button|object/.test(s) ? !t.disabled : "a" === s ? t.href || n : n) && i(t)
    }
    function i(t) {
        return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function() {
            return"hidden" === e.css(this, "visibility")
        }).length
    }
    var o = 0, a = /^ui-id-\d+$/;
    e.ui = e.ui || {}, e.extend(e.ui, {version: "1.10.4", keyCode: {BACKSPACE: 8, COMMA: 188, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, LEFT: 37, NUMPAD_ADD: 107, NUMPAD_DECIMAL: 110, NUMPAD_DIVIDE: 111, NUMPAD_ENTER: 108, NUMPAD_MULTIPLY: 106, NUMPAD_SUBTRACT: 109, PAGE_DOWN: 34, PAGE_UP: 33, PERIOD: 190, RIGHT: 39, SPACE: 32, TAB: 9, UP: 38}}), e.fn.extend({focus: function(t) {
            return function(n, i) {
                return"number" == typeof n ? this.each(function() {
                    var t = this;
                    setTimeout(function() {
                        e(t).focus(), i && i.call(t)
                    }, n)
                }) : t.apply(this, arguments)
            }
        }(e.fn.focus), scrollParent: function() {
            var t;
            return t = e.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                return/(relative|absolute|fixed)/.test(e.css(this, "position")) && /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
            }).eq(0) : this.parents().filter(function() {
                return/(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
            }).eq(0), /fixed/.test(this.css("position")) || !t.length ? e(document) : t
        }, zIndex: function(n) {
            if (n !== t)
                return this.css("zIndex", n);
            if (this.length)
                for (var i, o, a = e(this[0]); a.length && a[0] !== document; ) {
                    if (i = a.css("position"), ("absolute" === i || "relative" === i || "fixed" === i) && (o = parseInt(a.css("zIndex"), 10), !isNaN(o) && 0 !== o))
                        return o;
                    a = a.parent()
                }
            return 0
        }, uniqueId: function() {
            return this.each(function() {
                this.id || (this.id = "ui-id-" + ++o)
            })
        }, removeUniqueId: function() {
            return this.each(function() {
                a.test(this.id) && e(this).removeAttr("id")
            })
        }}), e.extend(e.expr[":"], {data: e.expr.createPseudo ? e.expr.createPseudo(function(t) {
            return function(n) {
                return!!e.data(n, t)
            }
        }) : function(t, n, i) {
            return!!e.data(t, i[3])
        }, focusable: function(t) {
            return n(t, !isNaN(e.attr(t, "tabindex")))
        }, tabbable: function(t) {
            var i = e.attr(t, "tabindex"), o = isNaN(i);
            return(o || i >= 0) && n(t, !o)
        }}), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function(n, i) {
        function o(t, n, i, o) {
            return e.each(a, function() {
                n -= parseFloat(e.css(t, "padding" + this)) || 0, i && (n -= parseFloat(e.css(t, "border" + this + "Width")) || 0), o && (n -= parseFloat(e.css(t, "margin" + this)) || 0)
            }), n
        }
        var a = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"], r = i.toLowerCase(), s = {innerWidth: e.fn.innerWidth, innerHeight: e.fn.innerHeight, outerWidth: e.fn.outerWidth, outerHeight: e.fn.outerHeight};
        e.fn["inner" + i] = function(n) {
            return n === t ? s["inner" + i].call(this) : this.each(function() {
                e(this).css(r, o(this, n) + "px")
            })
        }, e.fn["outer" + i] = function(t, n) {
            return"number" != typeof t ? s["outer" + i].call(this, t) : this.each(function() {
                e(this).css(r, o(this, t, !0, n) + "px")
            })
        }
    }), e.fn.addBack || (e.fn.addBack = function(e) {
        return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
    }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function(t) {
        return function(n) {
            return arguments.length ? t.call(this, e.camelCase(n)) : t.call(this)
        }
    }(e.fn.removeData)), e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.support.selectstart = "onselectstart"in document.createElement("div"), e.fn.extend({disableSelection: function() {
            return this.bind((e.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(e) {
                e.preventDefault()
            })
        }, enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        }}), e.extend(e.ui, {plugin: {add: function(t, n, i) {
                var o, a = e.ui[t].prototype;
                for (o in i)
                    a.plugins[o] = a.plugins[o] || [], a.plugins[o].push([n, i[o]])
            }, call: function(e, t, n) {
                var i, o = e.plugins[t];
                if (o && e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType)
                    for (i = 0; o.length > i; i++)
                        e.options[o[i][0]] && o[i][1].apply(e.element, n)
            }}, hasScroll: function(t, n) {
            if ("hidden" === e(t).css("overflow"))
                return!1;
            var i = n && "left" === n ? "scrollLeft" : "scrollTop", o = !1;
            return t[i] > 0 ? !0 : (t[i] = 1, o = t[i] > 0, t[i] = 0, o)
        }})
}(jQuery), function(e, t) {
    var n = 0, i = Array.prototype.slice, o = e.cleanData;
    e.cleanData = function(t) {
        for (var n, i = 0; null != (n = t[i]); i++)
            try {
                e(n).triggerHandler("remove")
            } catch (a) {
            }
        o(t)
    }, e.widget = function(n, i, o) {
        var a, r, s, l, c = {}, d = n.split(".")[0];
        n = n.split(".")[1], a = d + "-" + n, o || (o = i, i = e.Widget), e.expr[":"][a.toLowerCase()] = function(t) {
            return!!e.data(t, a)
        }, e[d] = e[d] || {}, r = e[d][n], s = e[d][n] = function(e, n) {
            return this._createWidget ? (arguments.length && this._createWidget(e, n), t) : new s(e, n)
        }, e.extend(s, r, {version: o.version, _proto: e.extend({}, o), _childConstructors: []}), l = new i, l.options = e.widget.extend({}, l.options), e.each(o, function(n, o) {
            return e.isFunction(o) ? (c[n] = function() {
                var e = function() {
                    return i.prototype[n].apply(this, arguments)
                }, t = function(e) {
                    return i.prototype[n].apply(this, e)
                };
                return function() {
                    var n, i = this._super, a = this._superApply;
                    return this._super = e, this._superApply = t, n = o.apply(this, arguments), this._super = i, this._superApply = a, n
                }
            }(), t) : (c[n] = o, t)
        }), s.prototype = e.widget.extend(l, {widgetEventPrefix: r ? l.widgetEventPrefix || n : n}, c, {constructor: s, namespace: d, widgetName: n, widgetFullName: a}), r ? (e.each(r._childConstructors, function(t, n) {
            var i = n.prototype;
            e.widget(i.namespace + "." + i.widgetName, s, n._proto)
        }), delete r._childConstructors) : i._childConstructors.push(s), e.widget.bridge(n, s)
    }, e.widget.extend = function(n) {
        for (var o, a, r = i.call(arguments, 1), s = 0, l = r.length; l > s; s++)
            for (o in r[s])
                a = r[s][o], r[s].hasOwnProperty(o) && a !== t && (n[o] = e.isPlainObject(a) ? e.isPlainObject(n[o]) ? e.widget.extend({}, n[o], a) : e.widget.extend({}, a) : a);
        return n
    }, e.widget.bridge = function(n, o) {
        var a = o.prototype.widgetFullName || n;
        e.fn[n] = function(r) {
            var s = "string" == typeof r, l = i.call(arguments, 1), c = this;
            return r = !s && l.length ? e.widget.extend.apply(null, [r].concat(l)) : r, s ? this.each(function() {
                var i, o = e.data(this, a);
                return o ? e.isFunction(o[r]) && "_" !== r.charAt(0) ? (i = o[r].apply(o, l), i !== o && i !== t ? (c = i && i.jquery ? c.pushStack(i.get()) : i, !1) : t) : e.error("no such method '" + r + "' for " + n + " widget instance") : e.error("cannot call methods on " + n + " prior to initialization; " + "attempted to call method '" + r + "'")
            }) : this.each(function() {
                var t = e.data(this, a);
                t ? t.option(r || {})._init() : e.data(this, a, new o(r, this))
            }), c
        }
    }, e.Widget = function() {
    }, e.Widget._childConstructors = [], e.Widget.prototype = {widgetName: "widget", widgetEventPrefix: "", defaultElement: "<div>", options: {disabled: !1, create: null}, _createWidget: function(t, i) {
            i = e(i || this.defaultElement || this)[0], this.element = e(i), this.uuid = n++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = e.widget.extend({}, this.options, this._getCreateOptions(), t), this.bindings = e(), this.hoverable = e(), this.focusable = e(), i !== this && (e.data(i, this.widgetFullName, this), this._on(!0, this.element, {remove: function(e) {
                    e.target === i && this.destroy()
                }}), this.document = e(i.style ? i.ownerDocument : i.document || i), this.window = e(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        }, _getCreateOptions: e.noop, _getCreateEventData: e.noop, _create: e.noop, _init: e.noop, destroy: function() {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled " + "ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        }, _destroy: e.noop, widget: function() {
            return this.element
        }, option: function(n, i) {
            var o, a, r, s = n;
            if (0 === arguments.length)
                return e.widget.extend({}, this.options);
            if ("string" == typeof n)
                if (s = {}, o = n.split("."), n = o.shift(), o.length) {
                    for (a = s[n] = e.widget.extend({}, this.options[n]), r = 0; o.length - 1 > r; r++)
                        a[o[r]] = a[o[r]] || {}, a = a[o[r]];
                    if (n = o.pop(), 1 === arguments.length)
                        return a[n] === t ? null : a[n];
                    a[n] = i
                } else {
                    if (1 === arguments.length)
                        return this.options[n] === t ? null : this.options[n];
                    s[n] = i
                }
            return this._setOptions(s), this
        }, _setOptions: function(e) {
            var t;
            for (t in e)
                this._setOption(t, e[t]);
            return this
        }, _setOption: function(e, t) {
            return this.options[e] = t, "disabled" === e && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!t).attr("aria-disabled", t), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
        }, enable: function() {
            return this._setOption("disabled", !1)
        }, disable: function() {
            return this._setOption("disabled", !0)
        }, _on: function(n, i, o) {
            var a, r = this;
            "boolean" != typeof n && (o = i, i = n, n = !1), o ? (i = a = e(i), this.bindings = this.bindings.add(i)) : (o = i, i = this.element, a = this.widget()), e.each(o, function(o, s) {
                function l() {
                    return n || r.options.disabled !== !0 && !e(this).hasClass("ui-state-disabled") ? ("string" == typeof s ? r[s] : s).apply(r, arguments) : t
                }
                "string" != typeof s && (l.guid = s.guid = s.guid || l.guid || e.guid++);
                var c = o.match(/^(\w+)\s*(.*)$/), d = c[1] + r.eventNamespace, u = c[2];
                u ? a.delegate(u, d, l) : i.bind(d, l)
            })
        }, _off: function(e, t) {
            t = (t || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.unbind(t).undelegate(t)
        }, _delay: function(e, t) {
            function n() {
                return("string" == typeof e ? i[e] : e).apply(i, arguments)
            }
            var i = this;
            return setTimeout(n, t || 0)
        }, _hoverable: function(t) {
            this.hoverable = this.hoverable.add(t), this._on(t, {mouseenter: function(t) {
                    e(t.currentTarget).addClass("ui-state-hover")
                }, mouseleave: function(t) {
                    e(t.currentTarget).removeClass("ui-state-hover")
                }})
        }, _focusable: function(t) {
            this.focusable = this.focusable.add(t), this._on(t, {focusin: function(t) {
                    e(t.currentTarget).addClass("ui-state-focus")
                }, focusout: function(t) {
                    e(t.currentTarget).removeClass("ui-state-focus")
                }})
        }, _trigger: function(t, n, i) {
            var o, a, r = this.options[t];
            if (i = i || {}, n = e.Event(n), n.type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(), n.target = this.element[0], a = n.originalEvent)
                for (o in a)
                    o in n || (n[o] = a[o]);
            return this.element.trigger(n, i), !(e.isFunction(r) && r.apply(this.element[0], [n].concat(i)) === !1 || n.isDefaultPrevented())
        }}, e.each({show: "fadeIn", hide: "fadeOut"}, function(t, n) {
        e.Widget.prototype["_" + t] = function(i, o, a) {
            "string" == typeof o && (o = {effect: o});
            var r, s = o ? o === !0 || "number" == typeof o ? n : o.effect || n : t;
            o = o || {}, "number" == typeof o && (o = {duration: o}), r = !e.isEmptyObject(o), o.complete = a, o.delay && i.delay(o.delay), r && e.effects && e.effects.effect[s] ? i[t](o) : s !== t && i[s] ? i[s](o.duration, o.easing, a) : i.queue(function(n) {
                e(this)[t](), a && a.call(i[0]), n()
            })
        }
    })
}(jQuery), function(e, t) {
    function n() {
        this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {closeText: "Done", prevText: "Prev", nextText: "Next", currentText: "Today", monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], weekHeader: "Wk", dateFormat: "mm/dd/yy", firstDay: 0, isRTL: !1, showMonthAfterYear: !1, yearSuffix: ""}, this._defaults = {showOn: "focus", showAnim: "fadeIn", showOptions: {}, defaultDate: null, appendText: "", buttonText: "...", buttonImage: "", buttonImageOnly: !1, hideIfNoPrevNext: !1, navigationAsDateFormat: !1, gotoCurrent: !1, changeMonth: !1, changeYear: !1, yearRange: "c-10:c+10", showOtherMonths: !1, selectOtherMonths: !1, showWeek: !1, calculateWeek: this.iso8601Week, shortYearCutoff: "+10", minDate: null, maxDate: null, duration: "fast", beforeShowDay: null, beforeShow: null, onSelect: null, onChangeMonthYear: null, onClose: null, numberOfMonths: 1, showCurrentAtPos: 0, stepMonths: 1, stepBigMonths: 12, altField: "", altFormat: "", constrainInput: !0, showButtonPanel: !1, autoSize: !1, disabled: !1}, e.extend(this._defaults, this.regional[""]), this.dpDiv = i(e("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
    }
    function i(t) {
        var n = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return t.delegate(n, "mouseout", function() {
            e(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && e(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && e(this).removeClass("ui-datepicker-next-hover")
        }).delegate(n, "mouseover", function() {
            e.datepicker._isDisabledDatepicker(a.inline ? t.parent()[0] : a.input[0]) || (e(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), e(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && e(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && e(this).addClass("ui-datepicker-next-hover"))
        })
    }
    function o(t, n) {
        e.extend(t, n);
        for (var i in n)
            null == n[i] && (t[i] = n[i]);
        return t
    }
    e.extend(e.ui, {datepicker: {version: "1.10.4"}});
    var a, r = "datepicker";
    e.extend(n.prototype, {markerClassName: "hasDatepicker", maxRows: 4, _widgetDatepicker: function() {
            return this.dpDiv
        }, setDefaults: function(e) {
            return o(this._defaults, e || {}), this
        }, _attachDatepicker: function(t, n) {
            var i, o, a;
            i = t.nodeName.toLowerCase(), o = "div" === i || "span" === i, t.id || (this.uuid += 1, t.id = "dp" + this.uuid), a = this._newInst(e(t), o), a.settings = e.extend({}, n || {}), "input" === i ? this._connectDatepicker(t, a) : o && this._inlineDatepicker(t, a)
        }, _newInst: function(t, n) {
            var o = t[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
            return{id: o, input: t, selectedDay: 0, selectedMonth: 0, selectedYear: 0, drawMonth: 0, drawYear: 0, inline: n, dpDiv: n ? i(e("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv}
        }, _connectDatepicker: function(t, n) {
            var i = e(t);
            n.append = e([]), n.trigger = e([]), i.hasClass(this.markerClassName) || (this._attachments(i, n), i.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), this._autoSize(n), e.data(t, r, n), n.settings.disabled && this._disableDatepicker(t))
        }, _attachments: function(t, n) {
            var i, o, a, r = this._get(n, "appendText"), s = this._get(n, "isRTL");
            n.append && n.append.remove(), r && (n.append = e("<span class='" + this._appendClass + "'>" + r + "</span>"), t[s ? "before" : "after"](n.append)), t.unbind("focus", this._showDatepicker), n.trigger && n.trigger.remove(), i = this._get(n, "showOn"), ("focus" === i || "both" === i) && t.focus(this._showDatepicker), ("button" === i || "both" === i) && (o = this._get(n, "buttonText"), a = this._get(n, "buttonImage"), n.trigger = e(this._get(n, "buttonImageOnly") ? e("<img/>").addClass(this._triggerClass).attr({src: a, alt: o, title: o}) : e("<button type='button'></button>").addClass(this._triggerClass).html(a ? e("<img/>").attr({src: a, alt: o, title: o}) : o)), t[s ? "before" : "after"](n.trigger), n.trigger.click(function() {
                return e.datepicker._datepickerShowing && e.datepicker._lastInput === t[0] ? e.datepicker._hideDatepicker() : e.datepicker._datepickerShowing && e.datepicker._lastInput !== t[0] ? (e.datepicker._hideDatepicker(), e.datepicker._showDatepicker(t[0])) : e.datepicker._showDatepicker(t[0]), !1
            }))
        }, _autoSize: function(e) {
            if (this._get(e, "autoSize") && !e.inline) {
                var t, n, i, o, a = new Date(2009, 11, 20), r = this._get(e, "dateFormat");
                r.match(/[DM]/) && (t = function(e) {
                    for (n = 0, i = 0, o = 0; e.length > o; o++)
                        e[o].length > n && (n = e[o].length, i = o);
                    return i
                }, a.setMonth(t(this._get(e, r.match(/MM/) ? "monthNames" : "monthNamesShort"))), a.setDate(t(this._get(e, r.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - a.getDay())), e.input.attr("size", this._formatDate(e, a).length)
            }
        }, _inlineDatepicker: function(t, n) {
            var i = e(t);
            i.hasClass(this.markerClassName) || (i.addClass(this.markerClassName).append(n.dpDiv), e.data(t, r, n), this._setDate(n, this._getDefaultDate(n), !0), this._updateDatepicker(n), this._updateAlternate(n), n.settings.disabled && this._disableDatepicker(t), n.dpDiv.css("display", "block"))
        }, _dialogDatepicker: function(t, n, i, a, s) {
            var l, c, d, u, p, m = this._dialogInst;
            return m || (this.uuid += 1, l = "dp" + this.uuid, this._dialogInput = e("<input type='text' id='" + l + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.keydown(this._doKeyDown), e("body").append(this._dialogInput), m = this._dialogInst = this._newInst(this._dialogInput, !1), m.settings = {}, e.data(this._dialogInput[0], r, m)), o(m.settings, a || {}), n = n && n.constructor === Date ? this._formatDate(m, n) : n, this._dialogInput.val(n), this._pos = s ? s.length ? s : [s.pageX, s.pageY] : null, this._pos || (c = document.documentElement.clientWidth, d = document.documentElement.clientHeight, u = document.documentElement.scrollLeft || document.body.scrollLeft, p = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [c / 2 - 100 + u, d / 2 - 150 + p]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), m.settings.onSelect = i, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), e.blockUI && e.blockUI(this.dpDiv), e.data(this._dialogInput[0], r, m), this
        }, _destroyDatepicker: function(t) {
            var n, i = e(t), o = e.data(t, r);
            i.hasClass(this.markerClassName) && (n = t.nodeName.toLowerCase(), e.removeData(t, r), "input" === n ? (o.append.remove(), o.trigger.remove(), i.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === n || "span" === n) && i.removeClass(this.markerClassName).empty())
        }, _enableDatepicker: function(t) {
            var n, i, o = e(t), a = e.data(t, r);
            o.hasClass(this.markerClassName) && (n = t.nodeName.toLowerCase(), "input" === n ? (t.disabled = !1, a.trigger.filter("button").each(function() {
                this.disabled = !1
            }).end().filter("img").css({opacity: "1.0", cursor: ""})) : ("div" === n || "span" === n) && (i = o.children("." + this._inlineClass), i.children().removeClass("ui-state-disabled"), i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = e.map(this._disabledInputs, function(e) {
                return e === t ? null : e
            }))
        }, _disableDatepicker: function(t) {
            var n, i, o = e(t), a = e.data(t, r);
            o.hasClass(this.markerClassName) && (n = t.nodeName.toLowerCase(), "input" === n ? (t.disabled = !0, a.trigger.filter("button").each(function() {
                this.disabled = !0
            }).end().filter("img").css({opacity: "0.5", cursor: "default"})) : ("div" === n || "span" === n) && (i = o.children("." + this._inlineClass), i.children().addClass("ui-state-disabled"), i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = e.map(this._disabledInputs, function(e) {
                return e === t ? null : e
            }), this._disabledInputs[this._disabledInputs.length] = t)
        }, _isDisabledDatepicker: function(e) {
            if (!e)
                return!1;
            for (var t = 0; this._disabledInputs.length > t; t++)
                if (this._disabledInputs[t] === e)
                    return!0;
            return!1
        }, _getInst: function(t) {
            try {
                return e.data(t, r)
            } catch (n) {
                throw"Missing instance data for this datepicker"
            }
        }, _optionDatepicker: function(n, i, a) {
            var r, s, l, c, d = this._getInst(n);
            return 2 === arguments.length && "string" == typeof i ? "defaults" === i ? e.extend({}, e.datepicker._defaults) : d ? "all" === i ? e.extend({}, d.settings) : this._get(d, i) : null : (r = i || {}, "string" == typeof i && (r = {}, r[i] = a), d && (this._curInst === d && this._hideDatepicker(), s = this._getDateDatepicker(n, !0), l = this._getMinMaxDate(d, "min"), c = this._getMinMaxDate(d, "max"), o(d.settings, r), null !== l && r.dateFormat !== t && r.minDate === t && (d.settings.minDate = this._formatDate(d, l)), null !== c && r.dateFormat !== t && r.maxDate === t && (d.settings.maxDate = this._formatDate(d, c)), "disabled"in r && (r.disabled ? this._disableDatepicker(n) : this._enableDatepicker(n)), this._attachments(e(n), d), this._autoSize(d), this._setDate(d, s), this._updateAlternate(d), this._updateDatepicker(d)), t)
        }, _changeDatepicker: function(e, t, n) {
            this._optionDatepicker(e, t, n)
        }, _refreshDatepicker: function(e) {
            var t = this._getInst(e);
            t && this._updateDatepicker(t)
        }, _setDateDatepicker: function(e, t) {
            var n = this._getInst(e);
            n && (this._setDate(n, t), this._updateDatepicker(n), this._updateAlternate(n))
        }, _getDateDatepicker: function(e, t) {
            var n = this._getInst(e);
            return n && !n.inline && this._setDateFromField(n, t), n ? this._getDate(n) : null
        }, _doKeyDown: function(t) {
            var n, i, o, a = e.datepicker._getInst(t.target), r = !0, s = a.dpDiv.is(".ui-datepicker-rtl");
            if (a._keyEvent = !0, e.datepicker._datepickerShowing)
                switch (t.keyCode) {
                    case 9:
                        e.datepicker._hideDatepicker(), r = !1;
                        break;
                    case 13:
                        return o = e("td." + e.datepicker._dayOverClass + ":not(." + e.datepicker._currentClass + ")", a.dpDiv), o[0] && e.datepicker._selectDay(t.target, a.selectedMonth, a.selectedYear, o[0]), n = e.datepicker._get(a, "onSelect"), n ? (i = e.datepicker._formatDate(a), n.apply(a.input ? a.input[0] : null, [i, a])) : e.datepicker._hideDatepicker(), !1;
                    case 27:
                        e.datepicker._hideDatepicker();
                        break;
                    case 33:
                        e.datepicker._adjustDate(t.target, t.ctrlKey ? -e.datepicker._get(a, "stepBigMonths") : -e.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 34:
                        e.datepicker._adjustDate(t.target, t.ctrlKey ? +e.datepicker._get(a, "stepBigMonths") : +e.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 35:
                        (t.ctrlKey || t.metaKey) && e.datepicker._clearDate(t.target), r = t.ctrlKey || t.metaKey;
                        break;
                    case 36:
                        (t.ctrlKey || t.metaKey) && e.datepicker._gotoToday(t.target), r = t.ctrlKey || t.metaKey;
                        break;
                    case 37:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, s ? 1 : -1, "D"), r = t.ctrlKey || t.metaKey, t.originalEvent.altKey && e.datepicker._adjustDate(t.target, t.ctrlKey ? -e.datepicker._get(a, "stepBigMonths") : -e.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 38:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, -7, "D"), r = t.ctrlKey || t.metaKey;
                        break;
                    case 39:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, s ? -1 : 1, "D"), r = t.ctrlKey || t.metaKey, t.originalEvent.altKey && e.datepicker._adjustDate(t.target, t.ctrlKey ? +e.datepicker._get(a, "stepBigMonths") : +e.datepicker._get(a, "stepMonths"), "M");
                        break;
                    case 40:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, 7, "D"), r = t.ctrlKey || t.metaKey;
                        break;
                    default:
                        r = !1
                }
            else
                36 === t.keyCode && t.ctrlKey ? e.datepicker._showDatepicker(this) : r = !1;
            r && (t.preventDefault(), t.stopPropagation())
        }, _doKeyPress: function(n) {
            var i, o, a = e.datepicker._getInst(n.target);
            return e.datepicker._get(a, "constrainInput") ? (i = e.datepicker._possibleChars(e.datepicker._get(a, "dateFormat")), o = String.fromCharCode(null == n.charCode ? n.keyCode : n.charCode), n.ctrlKey || n.metaKey || " " > o || !i || i.indexOf(o) > -1) : t
        }, _doKeyUp: function(t) {
            var n, i = e.datepicker._getInst(t.target);
            if (i.input.val() !== i.lastVal)
                try {
                    n = e.datepicker.parseDate(e.datepicker._get(i, "dateFormat"), i.input ? i.input.val() : null, e.datepicker._getFormatConfig(i)), n && (e.datepicker._setDateFromField(i), e.datepicker._updateAlternate(i), e.datepicker._updateDatepicker(i))
                } catch (o) {
                }
            return!0
        }, _showDatepicker: function(t) {
            if (t = t.target || t, "input" !== t.nodeName.toLowerCase() && (t = e("input", t.parentNode)[0]), !e.datepicker._isDisabledDatepicker(t) && e.datepicker._lastInput !== t) {
                var n, i, a, r, s, l, c;
                n = e.datepicker._getInst(t), e.datepicker._curInst && e.datepicker._curInst !== n && (e.datepicker._curInst.dpDiv.stop(!0, !0), n && e.datepicker._datepickerShowing && e.datepicker._hideDatepicker(e.datepicker._curInst.input[0])), i = e.datepicker._get(n, "beforeShow"), a = i ? i.apply(t, [t, n]) : {}, a !== !1 && (o(n.settings, a), n.lastVal = null, e.datepicker._lastInput = t, e.datepicker._setDateFromField(n), e.datepicker._inDialog && (t.value = ""), e.datepicker._pos || (e.datepicker._pos = e.datepicker._findPos(t), e.datepicker._pos[1] += t.offsetHeight), r = !1, e(t).parents().each(function() {
                    return r |= "fixed" === e(this).css("position"), !r
                }), s = {left: e.datepicker._pos[0], top: e.datepicker._pos[1]}, e.datepicker._pos = null, n.dpDiv.empty(), n.dpDiv.css({position: "absolute", display: "block", top: "-1000px"}), e.datepicker._updateDatepicker(n), s = e.datepicker._checkOffset(n, s, r), n.dpDiv.css({position: e.datepicker._inDialog && e.blockUI ? "static" : r ? "fixed" : "absolute", display: "none", left: s.left + "px", top: s.top + "px"}), n.inline || (l = e.datepicker._get(n, "showAnim"), c = e.datepicker._get(n, "duration"), n.dpDiv.zIndex(e(t).zIndex() + 1), e.datepicker._datepickerShowing = !0, e.effects && e.effects.effect[l] ? n.dpDiv.show(l, e.datepicker._get(n, "showOptions"), c) : n.dpDiv[l || "show"](l ? c : null), e.datepicker._shouldFocusInput(n) && n.input.focus(), e.datepicker._curInst = n))
            }
        }, _updateDatepicker: function(t) {
            this.maxRows = 4, a = t, t.dpDiv.empty().append(this._generateHTML(t)), this._attachHandlers(t), t.dpDiv.find("." + this._dayOverClass + " a").mouseover();
            var n, i = this._getNumberOfMonths(t), o = i[1], r = 17;
            t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), o > 1 && t.dpDiv.addClass("ui-datepicker-multi-" + o).css("width", r * o + "em"), t.dpDiv[(1 !== i[0] || 1 !== i[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), t.dpDiv[(this._get(t, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), t === e.datepicker._curInst && e.datepicker._datepickerShowing && e.datepicker._shouldFocusInput(t) && t.input.focus(), t.yearshtml && (n = t.yearshtml, setTimeout(function() {
                n === t.yearshtml && t.yearshtml && t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml), n = t.yearshtml = null
            }, 0))
        }, _shouldFocusInput: function(e) {
            return e.input && e.input.is(":visible") && !e.input.is(":disabled") && !e.input.is(":focus")
        }, _checkOffset: function(t, n, i) {
            var o = t.dpDiv.outerWidth(), a = t.dpDiv.outerHeight(), r = t.input ? t.input.outerWidth() : 0, s = t.input ? t.input.outerHeight() : 0, l = document.documentElement.clientWidth + (i ? 0 : e(document).scrollLeft()), c = document.documentElement.clientHeight + (i ? 0 : e(document).scrollTop());
            return n.left -= this._get(t, "isRTL") ? o - r : 0, n.left -= i && n.left === t.input.offset().left ? e(document).scrollLeft() : 0, n.top -= i && n.top === t.input.offset().top + s ? e(document).scrollTop() : 0, n.left -= Math.min(n.left, n.left + o > l && l > o ? Math.abs(n.left + o - l) : 0), n.top -= Math.min(n.top, n.top + a > c && c > a ? Math.abs(a + s) : 0), n
        }, _findPos: function(t) {
            for (var n, i = this._getInst(t), o = this._get(i, "isRTL"); t && ("hidden" === t.type || 1 !== t.nodeType || e.expr.filters.hidden(t)); )
                t = t[o ? "previousSibling" : "nextSibling"];
            return n = e(t).offset(), [n.left, n.top]
        }, _hideDatepicker: function(t) {
            var n, i, o, a, s = this._curInst;
            !s || t && s !== e.data(t, r) || this._datepickerShowing && (n = this._get(s, "showAnim"), i = this._get(s, "duration"), o = function() {
                e.datepicker._tidyDialog(s)
            }, e.effects && (e.effects.effect[n] || e.effects[n]) ? s.dpDiv.hide(n, e.datepicker._get(s, "showOptions"), i, o) : s.dpDiv["slideDown" === n ? "slideUp" : "fadeIn" === n ? "fadeOut" : "hide"](n ? i : null, o), n || o(), this._datepickerShowing = !1, a = this._get(s, "onClose"), a && a.apply(s.input ? s.input[0] : null, [s.input ? s.input.val() : "", s]), this._lastInput = null, this._inDialog && (this._dialogInput.css({position: "absolute", left: "0", top: "-100px"}), e.blockUI && (e.unblockUI(), e("body").append(this.dpDiv))), this._inDialog = !1)
        }, _tidyDialog: function(e) {
            e.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
        }, _checkExternalClick: function(t) {
            if (e.datepicker._curInst) {
                var n = e(t.target), i = e.datepicker._getInst(n[0]);
                (n[0].id !== e.datepicker._mainDivId && 0 === n.parents("#" + e.datepicker._mainDivId).length && !n.hasClass(e.datepicker.markerClassName) && !n.closest("." + e.datepicker._triggerClass).length && e.datepicker._datepickerShowing && (!e.datepicker._inDialog || !e.blockUI) || n.hasClass(e.datepicker.markerClassName) && e.datepicker._curInst !== i) && e.datepicker._hideDatepicker()
            }
        }, _adjustDate: function(t, n, i) {
            var o = e(t), a = this._getInst(o[0]);
            this._isDisabledDatepicker(o[0]) || (this._adjustInstDate(a, n + ("M" === i ? this._get(a, "showCurrentAtPos") : 0), i), this._updateDatepicker(a))
        }, _gotoToday: function(t) {
            var n, i = e(t), o = this._getInst(i[0]);
            this._get(o, "gotoCurrent") && o.currentDay ? (o.selectedDay = o.currentDay, o.drawMonth = o.selectedMonth = o.currentMonth, o.drawYear = o.selectedYear = o.currentYear) : (n = new Date, o.selectedDay = n.getDate(), o.drawMonth = o.selectedMonth = n.getMonth(), o.drawYear = o.selectedYear = n.getFullYear()), this._notifyChange(o), this._adjustDate(i)
        }, _selectMonthYear: function(t, n, i) {
            var o = e(t), a = this._getInst(o[0]);
            a["selected" + ("M" === i ? "Month" : "Year")] = a["draw" + ("M" === i ? "Month" : "Year")] = parseInt(n.options[n.selectedIndex].value, 10), this._notifyChange(a), this._adjustDate(o)
        }, _selectDay: function(t, n, i, o) {
            var a, r = e(t);
            e(o).hasClass(this._unselectableClass) || this._isDisabledDatepicker(r[0]) || (a = this._getInst(r[0]), a.selectedDay = a.currentDay = e("a", o).html(), a.selectedMonth = a.currentMonth = n, a.selectedYear = a.currentYear = i, this._selectDate(t, this._formatDate(a, a.currentDay, a.currentMonth, a.currentYear)))
        }, _clearDate: function(t) {
            var n = e(t);
            this._selectDate(n, "")
        }, _selectDate: function(t, n) {
            var i, o = e(t), a = this._getInst(o[0]);
            n = null != n ? n : this._formatDate(a), a.input && a.input.val(n), this._updateAlternate(a), i = this._get(a, "onSelect"), i ? i.apply(a.input ? a.input[0] : null, [n, a]) : a.input && a.input.trigger("change"), a.inline ? this._updateDatepicker(a) : (this._hideDatepicker(), this._lastInput = a.input[0], "object" != typeof a.input[0] && a.input.focus(), this._lastInput = null)
        }, _updateAlternate: function(t) {
            var n, i, o, a = this._get(t, "altField");
            a && (n = this._get(t, "altFormat") || this._get(t, "dateFormat"), i = this._getDate(t), o = this.formatDate(n, i, this._getFormatConfig(t)), e(a).each(function() {
                e(this).val(o)
            }))
        }, noWeekends: function(e) {
            var t = e.getDay();
            return[t > 0 && 6 > t, ""]
        }, iso8601Week: function(e) {
            var t, n = new Date(e.getTime());
            return n.setDate(n.getDate() + 4 - (n.getDay() || 7)), t = n.getTime(), n.setMonth(0), n.setDate(1), Math.floor(Math.round((t - n) / 864e5) / 7) + 1
        }, parseDate: function(n, i, o) {
            if (null == n || null == i)
                throw"Invalid arguments";
            if (i = "object" == typeof i ? "" + i : i + "", "" === i)
                return null;
            var a, r, s, l, c = 0, d = (o ? o.shortYearCutoff : null) || this._defaults.shortYearCutoff, u = "string" != typeof d ? d : (new Date).getFullYear() % 100 + parseInt(d, 10), p = (o ? o.dayNamesShort : null) || this._defaults.dayNamesShort, m = (o ? o.dayNames : null) || this._defaults.dayNames, h = (o ? o.monthNamesShort : null) || this._defaults.monthNamesShort, f = (o ? o.monthNames : null) || this._defaults.monthNames, g = -1, v = -1, y = -1, _ = -1, b = !1, w = function(e) {
                var t = n.length > a + 1 && n.charAt(a + 1) === e;
                return t && a++, t
            }, C = function(e) {
                var t = w(e), n = "@" === e ? 14 : "!" === e ? 20 : "y" === e && t ? 4 : "o" === e ? 3 : 2, o = RegExp("^\\d{1," + n + "}"), a = i.substring(c).match(o);
                if (!a)
                    throw"Missing number at position " + c;
                return c += a[0].length, parseInt(a[0], 10)
            }, x = function(n, o, a) {
                var r = -1, s = e.map(w(n) ? a : o, function(e, t) {
                    return[[t, e]]
                }).sort(function(e, t) {
                    return-(e[1].length - t[1].length)
                });
                if (e.each(s, function(e, n) {
                    var o = n[1];
                    return i.substr(c, o.length).toLowerCase() === o.toLowerCase() ? (r = n[0], c += o.length, !1) : t
                }), -1 !== r)
                    return r + 1;
                throw"Unknown name at position " + c
            }, k = function() {
                if (i.charAt(c) !== n.charAt(a))
                    throw"Unexpected literal at position " + c;
                c++
            };
            for (a = 0; n.length > a; a++)
                if (b)
                    "'" !== n.charAt(a) || w("'") ? k() : b = !1;
                else
                    switch (n.charAt(a)) {
                        case"d":
                            y = C("d");
                            break;
                        case"D":
                            x("D", p, m);
                            break;
                        case"o":
                            _ = C("o");
                            break;
                        case"m":
                            v = C("m");
                            break;
                        case"M":
                            v = x("M", h, f);
                            break;
                        case"y":
                            g = C("y");
                            break;
                        case"@":
                            l = new Date(C("@")), g = l.getFullYear(), v = l.getMonth() + 1, y = l.getDate();
                            break;
                        case"!":
                            l = new Date((C("!") - this._ticksTo1970) / 1e4), g = l.getFullYear(), v = l.getMonth() + 1, y = l.getDate();
                            break;
                        case"'":
                            w("'") ? k() : b = !0;
                            break;
                        default:
                            k()
                    }
            if (i.length > c && (s = i.substr(c), !/^\s+/.test(s)))
                throw"Extra/unparsed characters found in date: " + s;
            if (-1 === g ? g = (new Date).getFullYear() : 100 > g && (g += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (u >= g ? 0 : -100)), _ > -1)
                for (v = 1, y = _; r = this._getDaysInMonth(g, v - 1), !(r >= y); )
                    v++, y -= r;
            if (l = this._daylightSavingAdjust(new Date(g, v - 1, y)), l.getFullYear() !== g || l.getMonth() + 1 !== v || l.getDate() !== y)
                throw"Invalid date";
            return l
        }, ATOM: "yy-mm-dd", COOKIE: "D, dd M yy", ISO_8601: "yy-mm-dd", RFC_822: "D, d M y", RFC_850: "DD, dd-M-y", RFC_1036: "D, d M y", RFC_1123: "D, d M yy", RFC_2822: "D, d M yy", RSS: "D, d M y", TICKS: "!", TIMESTAMP: "@", W3C: "yy-mm-dd", _ticksTo1970: 864e9 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)), formatDate: function(e, t, n) {
            if (!t)
                return"";
            var i, o = (n ? n.dayNamesShort : null) || this._defaults.dayNamesShort, a = (n ? n.dayNames : null) || this._defaults.dayNames, r = (n ? n.monthNamesShort : null) || this._defaults.monthNamesShort, s = (n ? n.monthNames : null) || this._defaults.monthNames, l = function(t) {
                var n = e.length > i + 1 && e.charAt(i + 1) === t;
                return n && i++, n
            }, c = function(e, t, n) {
                var i = "" + t;
                if (l(e))
                    for (; n > i.length; )
                        i = "0" + i;
                return i
            }, d = function(e, t, n, i) {
                return l(e) ? i[t] : n[t]
            }, u = "", p = !1;
            if (t)
                for (i = 0; e.length > i; i++)
                    if (p)
                        "'" !== e.charAt(i) || l("'") ? u += e.charAt(i) : p = !1;
                    else
                        switch (e.charAt(i)) {
                            case"d":
                                u += c("d", t.getDate(), 2);
                                break;
                            case"D":
                                u += d("D", t.getDay(), o, a);
                                break;
                            case"o":
                                u += c("o", Math.round((new Date(t.getFullYear(), t.getMonth(), t.getDate()).getTime() - new Date(t.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                                break;
                            case"m":
                                u += c("m", t.getMonth() + 1, 2);
                                break;
                            case"M":
                                u += d("M", t.getMonth(), r, s);
                                break;
                            case"y":
                                u += l("y") ? t.getFullYear() : (10 > t.getYear() % 100 ? "0" : "") + t.getYear() % 100;
                                break;
                            case"@":
                                u += t.getTime();
                                break;
                            case"!":
                                u += 1e4 * t.getTime() + this._ticksTo1970;
                                break;
                            case"'":
                                l("'") ? u += "'" : p = !0;
                                break;
                            default:
                                u += e.charAt(i)
                        }
            return u
        }, _possibleChars: function(e) {
            var t, n = "", i = !1, o = function(n) {
                var i = e.length > t + 1 && e.charAt(t + 1) === n;
                return i && t++, i
            };
            for (t = 0; e.length > t; t++)
                if (i)
                    "'" !== e.charAt(t) || o("'") ? n += e.charAt(t) : i = !1;
                else
                    switch (e.charAt(t)) {
                        case"d":
                        case"m":
                        case"y":
                        case"@":
                            n += "0123456789";
                            break;
                        case"D":
                        case"M":
                            return null;
                        case"'":
                            o("'") ? n += "'" : i = !0;
                            break;
                        default:
                            n += e.charAt(t)
                    }
            return n
        }, _get: function(e, n) {
            return e.settings[n] !== t ? e.settings[n] : this._defaults[n]
        }, _setDateFromField: function(e, t) {
            if (e.input.val() !== e.lastVal) {
                var n = this._get(e, "dateFormat"), i = e.lastVal = e.input ? e.input.val() : null, o = this._getDefaultDate(e), a = o, r = this._getFormatConfig(e);
                try {
                    a = this.parseDate(n, i, r) || o
                } catch (s) {
                    i = t ? "" : i
                }
                e.selectedDay = a.getDate(), e.drawMonth = e.selectedMonth = a.getMonth(), e.drawYear = e.selectedYear = a.getFullYear(), e.currentDay = i ? a.getDate() : 0, e.currentMonth = i ? a.getMonth() : 0, e.currentYear = i ? a.getFullYear() : 0, this._adjustInstDate(e)
            }
        }, _getDefaultDate: function(e) {
            return this._restrictMinMax(e, this._determineDate(e, this._get(e, "defaultDate"), new Date))
        }, _determineDate: function(t, n, i) {
            var o = function(e) {
                var t = new Date;
                return t.setDate(t.getDate() + e), t
            }, a = function(n) {
                try {
                    return e.datepicker.parseDate(e.datepicker._get(t, "dateFormat"), n, e.datepicker._getFormatConfig(t))
                } catch (i) {
                }
                for (var o = (n.toLowerCase().match(/^c/) ? e.datepicker._getDate(t) : null) || new Date, a = o.getFullYear(), r = o.getMonth(), s = o.getDate(), l = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, c = l.exec(n); c; ) {
                    switch (c[2] || "d") {
                        case"d":
                        case"D":
                            s += parseInt(c[1], 10);
                            break;
                        case"w":
                        case"W":
                            s += 7 * parseInt(c[1], 10);
                            break;
                        case"m":
                        case"M":
                            r += parseInt(c[1], 10), s = Math.min(s, e.datepicker._getDaysInMonth(a, r));
                            break;
                        case"y":
                        case"Y":
                            a += parseInt(c[1], 10), s = Math.min(s, e.datepicker._getDaysInMonth(a, r))
                    }
                    c = l.exec(n)
                }
                return new Date(a, r, s)
            }, r = null == n || "" === n ? i : "string" == typeof n ? a(n) : "number" == typeof n ? isNaN(n) ? i : o(n) : new Date(n.getTime());
            return r = r && "Invalid Date" == "" + r ? i : r, r && (r.setHours(0), r.setMinutes(0), r.setSeconds(0), r.setMilliseconds(0)), this._daylightSavingAdjust(r)
        }, _daylightSavingAdjust: function(e) {
            return e ? (e.setHours(e.getHours() > 12 ? e.getHours() + 2 : 0), e) : null
        }, _setDate: function(e, t, n) {
            var i = !t, o = e.selectedMonth, a = e.selectedYear, r = this._restrictMinMax(e, this._determineDate(e, t, new Date));
            e.selectedDay = e.currentDay = r.getDate(), e.drawMonth = e.selectedMonth = e.currentMonth = r.getMonth(), e.drawYear = e.selectedYear = e.currentYear = r.getFullYear(), o === e.selectedMonth && a === e.selectedYear || n || this._notifyChange(e), this._adjustInstDate(e), e.input && e.input.val(i ? "" : this._formatDate(e))
        }, _getDate: function(e) {
            var t = !e.currentYear || e.input && "" === e.input.val() ? null : this._daylightSavingAdjust(new Date(e.currentYear, e.currentMonth, e.currentDay));
            return t
        }, _attachHandlers: function(t) {
            var n = this._get(t, "stepMonths"), i = "#" + t.id.replace(/\\\\/g, "\\");
            t.dpDiv.find("[data-handler]").map(function() {
                var t = {prev: function() {
                        e.datepicker._adjustDate(i, -n, "M")
                    }, next: function() {
                        e.datepicker._adjustDate(i, +n, "M")
                    }, hide: function() {
                        e.datepicker._hideDatepicker()
                    }, today: function() {
                        e.datepicker._gotoToday(i)
                    }, selectDay: function() {
                        return e.datepicker._selectDay(i, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                    }, selectMonth: function() {
                        return e.datepicker._selectMonthYear(i, this, "M"), !1
                    }, selectYear: function() {
                        return e.datepicker._selectMonthYear(i, this, "Y"), !1
                    }};
                e(this).bind(this.getAttribute("data-event"), t[this.getAttribute("data-handler")])
            })
        }, _generateHTML: function(e) {
            var t, n, i, o, a, r, s, l, c, d, u, p, m, h, f, g, v, y, _, b, w, C, x, k, S, E, T, M, A, P, I, N, D, L, B, R, H, O, F, z = new Date, j = this._daylightSavingAdjust(new Date(z.getFullYear(), z.getMonth(), z.getDate())), V = this._get(e, "isRTL"), W = this._get(e, "showButtonPanel"), q = this._get(e, "hideIfNoPrevNext"), $ = this._get(e, "navigationAsDateFormat"), U = this._getNumberOfMonths(e), K = this._get(e, "showCurrentAtPos"), Y = this._get(e, "stepMonths"), Q = 1 !== U[0] || 1 !== U[1], G = this._daylightSavingAdjust(e.currentDay ? new Date(e.currentYear, e.currentMonth, e.currentDay) : new Date(9999, 9, 9)), X = this._getMinMaxDate(e, "min"), J = this._getMinMaxDate(e, "max"), Z = e.drawMonth - K, et = e.drawYear;
            if (0 > Z && (Z += 12, et--), J)
                for (t = this._daylightSavingAdjust(new Date(J.getFullYear(), J.getMonth() - U[0] * U[1] + 1, J.getDate())), t = X && X > t?X:t; this._daylightSavingAdjust(new Date(et, Z, 1)) > t; )
                    Z--, 0 > Z && (Z = 11, et--);
            for (e.drawMonth = Z, e.drawYear = et, n = this._get(e, "prevText"), n = $ ? this.formatDate(n, this._daylightSavingAdjust(new Date(et, Z - Y, 1)), this._getFormatConfig(e)) : n, i = this._canAdjustMonth(e, -1, et, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (V ? "e" : "w") + "'>" + n + "</span></a>" : q ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (V ? "e" : "w") + "'>" + n + "</span></a>", o = this._get(e, "nextText"), o = $ ? this.formatDate(o, this._daylightSavingAdjust(new Date(et, Z + Y, 1)), this._getFormatConfig(e)) : o, a = this._canAdjustMonth(e, 1, et, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + o + "'><span class='ui-icon ui-icon-circle-triangle-" + (V ? "w" : "e") + "'>" + o + "</span></a>" : q ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + o + "'><span class='ui-icon ui-icon-circle-triangle-" + (V ? "w" : "e") + "'>" + o + "</span></a>", r = this._get(e, "currentText"), s = this._get(e, "gotoCurrent") && e.currentDay ? G : j, r = $ ? this.formatDate(r, s, this._getFormatConfig(e)) : r, l = e.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(e, "closeText") + "</button>", c = W ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (V ? l : "") + (this._isInRange(e, s) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + r + "</button>" : "") + (V ? "" : l) + "</div>" : "", d = parseInt(this._get(e, "firstDay"), 10), d = isNaN(d) ? 0 : d, u = this._get(e, "showWeek"), p = this._get(e, "dayNames"), m = this._get(e, "dayNamesMin"), h = this._get(e, "monthNames"), f = this._get(e, "monthNamesShort"), g = this._get(e, "beforeShowDay"), v = this._get(e, "showOtherMonths"), y = this._get(e, "selectOtherMonths"), _ = this._getDefaultDate(e), b = "", C = 0; U[0] > C; C++) {
                for (x = "", this.maxRows = 4, k = 0; U[1] > k; k++) {
                    if (S = this._daylightSavingAdjust(new Date(et, Z, e.selectedDay)), E = " ui-corner-all", T = "", Q) {
                        if (T += "<div class='ui-datepicker-group", U[1] > 1)
                            switch (k) {
                                case 0:
                                    T += " ui-datepicker-group-first", E = " ui-corner-" + (V ? "right" : "left");
                                    break;
                                case U[1] - 1:
                                    T += " ui-datepicker-group-last", E = " ui-corner-" + (V ? "left" : "right");
                                    break;
                                default:
                                    T += " ui-datepicker-group-middle", E = ""
                            }
                        T += "'>"
                    }
                    for (T += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + E + "'>" + (/all|left/.test(E) && 0 === C?V?a:i:"") + (/all|right/.test(E) && 0 === C?V?i:a:"") + this._generateMonthYearHeader(e, Z, et, X, J, C > 0 || k > 0, h, f) + "</div><table class='ui-datepicker-calendar'><thead>" + "<tr>", M = u?"<th class='ui-datepicker-week-col'>" + this._get(e, "weekHeader") + "</th>":"", w = 0; 7 > w; w++)
                        A = (w + d) % 7, M += "<th" + ((w + d + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" + "<span title='" + p[A] + "'>" + m[A] + "</span></th>";
                    for (T += M + "</tr></thead><tbody>", P = this._getDaysInMonth(et, Z), et === e.selectedYear && Z === e.selectedMonth && (e.selectedDay = Math.min(e.selectedDay, P)), I = (this._getFirstDayOfMonth(et, Z) - d + 7) % 7, N = Math.ceil((I + P) / 7), D = Q ? this.maxRows > N ? this.maxRows : N : N, this.maxRows = D, L = this._daylightSavingAdjust(new Date(et, Z, 1 - I)), B = 0; D > B; B++) {
                        for (T += "<tr>", R = u?"<td class='ui-datepicker-week-col'>" + this._get(e, "calculateWeek")(L) + "</td>":"", w = 0; 7 > w; w++)
                            H = g ? g.apply(e.input ? e.input[0] : null, [L]) : [!0, ""], O = L.getMonth() !== Z, F = O && !y || !H[0] || X && X > L || J && L > J, R += "<td class='" + ((w + d + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (O ? " ui-datepicker-other-month" : "") + (L.getTime() === S.getTime() && Z === e.selectedMonth && e._keyEvent || _.getTime() === L.getTime() && _.getTime() === S.getTime() ? " " + this._dayOverClass : "") + (F ? " " + this._unselectableClass + " ui-state-disabled" : "") + (O && !v ? "" : " " + H[1] + (L.getTime() === G.getTime() ? " " + this._currentClass : "") + (L.getTime() === j.getTime() ? " ui-datepicker-today" : "")) + "'" + (O && !v || !H[2] ? "" : " title='" + H[2].replace(/'/g, "&#39;") + "'") + (F ? "" : " data-handler='selectDay' data-event='click' data-month='" + L.getMonth() + "' data-year='" + L.getFullYear() + "'") + ">" + (O && !v ? "&#xa0;" : F ? "<span class='ui-state-default'>" + L.getDate() + "</span>" : "<a class='ui-state-default" + (L.getTime() === j.getTime() ? " ui-state-highlight" : "") + (L.getTime() === G.getTime() ? " ui-state-active" : "") + (O ? " ui-priority-secondary" : "") + "' href='#'>" + L.getDate() + "</a>") + "</td>", L.setDate(L.getDate() + 1), L = this._daylightSavingAdjust(L);
                        T += R + "</tr>"
                    }
                    Z++, Z > 11 && (Z = 0, et++), T += "</tbody></table>" + (Q ? "</div>" + (U[0] > 0 && k === U[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), x += T
                }
                b += x
            }
            return b += c, e._keyEvent = !1, b
        }, _generateMonthYearHeader: function(e, t, n, i, o, a, r, s) {
            var l, c, d, u, p, m, h, f, g = this._get(e, "changeMonth"), v = this._get(e, "changeYear"), y = this._get(e, "showMonthAfterYear"), _ = "<div class='ui-datepicker-title'>", b = "";
            if (a || !g)
                b += "<span class='ui-datepicker-month'>" + r[t] + "</span>";
            else {
                for (l = i && i.getFullYear() === n, c = o && o.getFullYear() === n, b += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", d = 0; 12 > d; d++)
                    (!l || d >= i.getMonth()) && (!c || o.getMonth() >= d) && (b += "<option value='" + d + "'" + (d === t ? " selected='selected'" : "") + ">" + s[d] + "</option>");
                b += "</select>"
            }
            if (y || (_ += b + (!a && g && v ? "" : "&#xa0;")), !e.yearshtml)
                if (e.yearshtml = "", a || !v)
                    _ += "<span class='ui-datepicker-year'>" + n + "</span>";
                else {
                    for (u = this._get(e, "yearRange").split(":"), p = (new Date).getFullYear(), m = function(e) {
                        var t = e.match(/c[+\-].*/) ? n + parseInt(e.substring(1), 10) : e.match(/[+\-].*/) ? p + parseInt(e, 10) : parseInt(e, 10);
                        return isNaN(t) ? p : t
                    }, h = m(u[0]), f = Math.max(h, m(u[1] || "")), h = i?Math.max(h, i.getFullYear()):h, f = o?Math.min(f, o.getFullYear()):f, e.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; f >= h; h++)
                        e.yearshtml += "<option value='" + h + "'" + (h === n ? " selected='selected'" : "") + ">" + h + "</option>";
                    e.yearshtml += "</select>", _ += e.yearshtml, e.yearshtml = null
                }
            return _ += this._get(e, "yearSuffix"), y && (_ += (!a && g && v ? "" : "&#xa0;") + b), _ += "</div>"
        }, _adjustInstDate: function(e, t, n) {
            var i = e.drawYear + ("Y" === n ? t : 0), o = e.drawMonth + ("M" === n ? t : 0), a = Math.min(e.selectedDay, this._getDaysInMonth(i, o)) + ("D" === n ? t : 0), r = this._restrictMinMax(e, this._daylightSavingAdjust(new Date(i, o, a)));
            e.selectedDay = r.getDate(), e.drawMonth = e.selectedMonth = r.getMonth(), e.drawYear = e.selectedYear = r.getFullYear(), ("M" === n || "Y" === n) && this._notifyChange(e)
        }, _restrictMinMax: function(e, t) {
            var n = this._getMinMaxDate(e, "min"), i = this._getMinMaxDate(e, "max"), o = n && n > t ? n : t;
            return i && o > i ? i : o
        }, _notifyChange: function(e) {
            var t = this._get(e, "onChangeMonthYear");
            t && t.apply(e.input ? e.input[0] : null, [e.selectedYear, e.selectedMonth + 1, e])
        }, _getNumberOfMonths: function(e) {
            var t = this._get(e, "numberOfMonths");
            return null == t ? [1, 1] : "number" == typeof t ? [1, t] : t
        }, _getMinMaxDate: function(e, t) {
            return this._determineDate(e, this._get(e, t + "Date"), null)
        }, _getDaysInMonth: function(e, t) {
            return 32 - this._daylightSavingAdjust(new Date(e, t, 32)).getDate()
        }, _getFirstDayOfMonth: function(e, t) {
            return new Date(e, t, 1).getDay()
        }, _canAdjustMonth: function(e, t, n, i) {
            var o = this._getNumberOfMonths(e), a = this._daylightSavingAdjust(new Date(n, i + (0 > t ? t : o[0] * o[1]), 1));
            return 0 > t && a.setDate(this._getDaysInMonth(a.getFullYear(), a.getMonth())), this._isInRange(e, a)
        }, _isInRange: function(e, t) {
            var n, i, o = this._getMinMaxDate(e, "min"), a = this._getMinMaxDate(e, "max"), r = null, s = null, l = this._get(e, "yearRange");
            return l && (n = l.split(":"), i = (new Date).getFullYear(), r = parseInt(n[0], 10), s = parseInt(n[1], 10), n[0].match(/[+\-].*/) && (r += i), n[1].match(/[+\-].*/) && (s += i)), (!o || t.getTime() >= o.getTime()) && (!a || t.getTime() <= a.getTime()) && (!r || t.getFullYear() >= r) && (!s || s >= t.getFullYear())
        }, _getFormatConfig: function(e) {
            var t = this._get(e, "shortYearCutoff");
            return t = "string" != typeof t ? t : (new Date).getFullYear() % 100 + parseInt(t, 10), {shortYearCutoff: t, dayNamesShort: this._get(e, "dayNamesShort"), dayNames: this._get(e, "dayNames"), monthNamesShort: this._get(e, "monthNamesShort"), monthNames: this._get(e, "monthNames")}
        }, _formatDate: function(e, t, n, i) {
            t || (e.currentDay = e.selectedDay, e.currentMonth = e.selectedMonth, e.currentYear = e.selectedYear);
            var o = t ? "object" == typeof t ? t : this._daylightSavingAdjust(new Date(i, n, t)) : this._daylightSavingAdjust(new Date(e.currentYear, e.currentMonth, e.currentDay));
            return this.formatDate(this._get(e, "dateFormat"), o, this._getFormatConfig(e))
        }}), e.fn.datepicker = function(t) {
        if (!this.length)
            return this;
        e.datepicker.initialized || (e(document).mousedown(e.datepicker._checkExternalClick), e.datepicker.initialized = !0), 0 === e("#" + e.datepicker._mainDivId).length && e("body").append(e.datepicker.dpDiv);
        var n = Array.prototype.slice.call(arguments, 1);
        return"string" != typeof t || "isDisabled" !== t && "getDate" !== t && "widget" !== t ? "option" === t && 2 === arguments.length && "string" == typeof arguments[1] ? e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this[0]].concat(n)) : this.each(function() {
            "string" == typeof t ? e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this].concat(n)) : e.datepicker._attachDatepicker(this, t)
        }) : e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this[0]].concat(n))
    }, e.datepicker = new n, e.datepicker.initialized = !1, e.datepicker.uuid = (new Date).getTime(), e.datepicker.version = "1.10.4"
}(jQuery), define("jqui", ["jquery"], function(e) {
    return function() {
        var t;
        return t || e.jqui
    }
}(this)), function(e) {
    var t = {cursor: "move", decelerate: !0, triggerHardware: !1, y: !0, x: !0, slowdown: .9, maxvelocity: 40, throttleFPS: 60, movingClass: {up: "kinetic-moving-up", down: "kinetic-moving-down", left: "kinetic-moving-left", right: "kinetic-moving-right"}, deceleratingClass: {up: "kinetic-decelerating-up", down: "kinetic-decelerating-down", left: "kinetic-decelerating-left", right: "kinetic-decelerating-right"}}, n = "kinetic-settings", i = "kinetic-active";
    window.requestAnimationFrame || (window.requestAnimationFrame = function() {
        return window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(e) {
            window.setTimeout(e, 1e3 / 60)
        }
    }()), e.support = e.support || {}, e.extend(e.support, {touch: "ontouchend"in document});
    var o = function() {
        return!1
    }, a = function(e, t) {
        return 0 === Math.floor(Math.abs(e)) ? 0 : e * t
    }, r = function(e, t) {
        var n = e;
        return e > 0 ? e > t && (n = t) : 0 - t > e && (n = 0 - t), n
    }, s = function(e, t) {
        this.removeClass(e.movingClass.up).removeClass(e.movingClass.down).removeClass(e.movingClass.left).removeClass(e.movingClass.right).removeClass(e.deceleratingClass.up).removeClass(e.deceleratingClass.down).removeClass(e.deceleratingClass.left).removeClass(e.deceleratingClass.right), e.velocity > 0 && this.addClass(t.right), 0 > e.velocity && this.addClass(t.left), e.velocityY > 0 && this.addClass(t.down), 0 > e.velocityY && this.addClass(t.up)
    }, l = function(e, t) {
        t.velocity = 0, t.velocityY = 0, t.decelerate = !0, "function" == typeof t.stopped && t.stopped.call(e, t)
    }, c = function(e, t) {
        var n = e[0];
        t.x && n.scrollWidth > 0 ? (n.scrollLeft = t.scrollLeft = n.scrollLeft + t.velocity, Math.abs(t.velocity) > 0 && (t.velocity = t.decelerate ? a(t.velocity, t.slowdown) : t.velocity)) : t.velocity = 0, t.y && n.scrollHeight > 0 ? (n.scrollTop = t.scrollTop = n.scrollTop + t.velocityY, Math.abs(t.velocityY) > 0 && (t.velocityY = t.decelerate ? a(t.velocityY, t.slowdown) : t.velocityY)) : t.velocityY = 0, s.call(e, t, t.deceleratingClass), "function" == typeof t.moved && t.moved.call(e, t), Math.abs(t.velocity) > 0 || Math.abs(t.velocityY) > 0 ? window.requestAnimationFrame(function() {
            c(e, t)
        }) : l(e, t)
    }, d = function(t) {
        var i = e.kinetic.callMethods[t], o = Array.prototype.slice.call(arguments);
        i && this.each(function() {
            var t = o.slice(1), a = e(this).data(n);
            t.unshift(a), i.apply(this, t)
        })
    }, u = function(t, n) {
        t[0], e.support.touch ? t.bind("touchstart", n.events.touchStart).bind("touchend", n.events.inputEnd).bind("touchmove", n.events.touchMove) : t.mousedown(n.events.inputDown).mouseup(n.events.inputEnd).mousemove(n.events.inputMove), t.click(n.events.inputClick).scroll(n.events.scroll).bind("selectstart", o).bind("dragstart", n.events.dragStart)
    }, p = function(t, n) {
        t[0], e.support.touch ? t.unbind("touchstart", n.events.touchStart).unbind("touchend", n.events.inputEnd).unbind("touchmove", n.events.touchMove) : t.unbind("mousedown", n.events.inputDown).unbind("mouseup", n.events.inputEnd).unbind("mousemove", n.events.inputMove).unbind("scroll", n.events.scroll), t.unbind("click", n.events.inputClick).unbind("selectstart", o), t.unbind("dragstart", n.events.dragStart)
    }, m = function(o) {
        this.addClass(i).each(function() {
            var i = this, a = e(this);
            if (!a.data(n)) {
                var l, d, p, m, h = e.extend({}, t, o), f = !1, g = !1, v = !1, y = 1e3 / h.throttleFPS;
                h.velocity = 0, h.velocityY = 0;
                var _ = function() {
                    l = !1, d = !1, v = !1
                };
                e(document).mouseup(_).click(_);
                var b = function() {
                    h.velocity = r(f - l, h.maxvelocity), h.velocityY = r(g - d, h.maxvelocity)
                }, w = function(t, n) {
                    return e.isFunction(h.filterTarget) ? h.filterTarget.call(i, t, n) !== !1 : n.which && n.which > 1 ? !1 : !0
                }, C = function(e, t) {
                    v = !0, h.velocity = f = 0, h.velocityY = g = 0, l = e, d = t
                }, x = function() {
                    l && f && h.decelerate === !1 && (h.decelerate = !0, b(), l = f = v = !1, c(a, h))
                }, k = function(t, n) {
                    (!p || new Date > new Date(p.getTime() + y)) && (p = new Date, v && (l || d) && (m && (e(m).blur(), m = null, a.focus()), h.decelerate = !1, h.velocity = h.velocityY = 0, a[0].scrollLeft = h.scrollLeft = h.x ? a[0].scrollLeft - (t - l) : a[0].scrollLeft, a[0].scrollTop = h.scrollTop = h.y ? a[0].scrollTop - (n - d) : a[0].scrollTop, f = l, g = d, l = t, d = n, b(), s.call(a, h, h.movingClass), "function" == typeof h.moved && h.moved.call(a, h)))
                };
                h.events = {touchStart: function(e) {
                        var t;
                        w(e.target, e) && (t = e.originalEvent.touches[0], C(t.clientX, t.clientY), e.stopPropagation())
                    }, touchMove: function(e) {
                        var t;
                        v && (t = e.originalEvent.touches[0], k(t.clientX, t.clientY), e.preventDefault && e.preventDefault())
                    }, inputDown: function(e) {
                        w(e.target, e) && (C(e.clientX, e.clientY), m = e.target, "IMG" === e.target.nodeName && e.preventDefault(), e.stopPropagation())
                    }, inputEnd: function(e) {
                        x(), m = null, e.preventDefault && e.preventDefault()
                    }, inputMove: function(e) {
                        v && (k(e.clientX, e.clientY), e.preventDefault && e.preventDefault())
                    }, scroll: function(e) {
                        "function" == typeof h.moved && h.moved.call(a, h), e.preventDefault && e.preventDefault()
                    }, inputClick: function(e) {
                        return Math.abs(h.velocity) > 0 ? (e.preventDefault(), !1) : void 0
                    }, dragStart: function() {
                        return m ? !1 : void 0
                    }}, u(a, h), a.data(n, h).css("cursor", h.cursor), h.triggerHardware && a.css({"-webkit-transform": "translate3d(0,0,0)", "-webkit-perspective": "1000", "-webkit-backface-visibility": "hidden"})
            }
        })
    };
    e.kinetic = {settingsKey: n, callMethods: {start: function(t, n) {
                var i = e(this);
                t = e.extend(t, n), t && (t.decelerate = !1, c(i, t))
            }, end: function(t) {
                e(this), t && (t.decelerate = !0)
            }, stop: function(t) {
                var n = e(this);
                l(n, t)
            }, detach: function(t) {
                var n = e(this);
                p(n, t), n.removeClass(i).css("cursor", "")
            }, attach: function(t) {
                var n = e(this);
                u(n, t), n.addClass(i).css("cursor", "move")
            }}}, e.fn.kinetic = function(e) {
        return"string" == typeof e ? d.apply(this, arguments) : m.call(this, e), this
    }
}(window.jQuery || window.Zepto), define("kinetic", ["jquery"], function(e) {
    return function() {
        var t;
        return t || e.kinetic
    }
}(this)), function(e) {
    "function" == typeof define && define.amd ? define("mousewheel", ["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
}(function(e) {
    function t(t) {
        var o, a = t || window.event, r = [].slice.call(arguments, 1), s = 0, l = 0, c = 0, d = 0, u = 0;
        return t = e.event.fix(a), t.type = "mousewheel", a.wheelDelta && (s = a.wheelDelta), a.detail && (s = -1 * a.detail), c = s, void 0 !== a.axis && a.axis === a.HORIZONTAL_AXIS && (c = 0, l = -1 * s), a.deltaY && (c = -1 * a.deltaY, s = c), a.deltaX && (l = a.deltaX, s = -1 * l), void 0 !== a.wheelDeltaY && (c = a.wheelDeltaY), void 0 !== a.wheelDeltaX && (l = -1 * a.wheelDeltaX), d = Math.abs(s), (!n || n > d) && (n = d), u = Math.max(Math.abs(c), Math.abs(l)), (!i || i > u) && (i = u), o = s > 0 ? "floor" : "ceil", s = Math[o](s / n), l = Math[o](l / i), c = Math[o](c / i), r.unshift(t, s, l, c), (e.event.dispatch || e.event.handle).apply(this, r)
    }
    var n, i, o = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], a = "onwheel"in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"];
    if (e.event.fixHooks)
        for (var r = o.length; r; )
            e.event.fixHooks[o[--r]] = e.event.mouseHooks;
    e.event.special.mousewheel = {setup: function() {
            if (this.addEventListener)
                for (var e = a.length; e; )
                    this.addEventListener(a[--e], t, !1);
            else
                this.onmousewheel = t
        }, teardown: function() {
            if (this.removeEventListener)
                for (var e = a.length; e; )
                    this.removeEventListener(a[--e], t, !1);
            else
                this.onmousewheel = null
        }}, e.fn.extend({mousewheel: function(e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
        }, unmousewheel: function(e) {
            return this.unbind("mousewheel", e)
        }})
}), function(e) {
    e.widget("thomaskahn.smoothDivScroll", {options: {scrollingHotSpotLeftClass: "scrollingHotSpotLeft", scrollingHotSpotRightClass: "scrollingHotSpotRight", scrollingHotSpotLeftVisibleClass: "scrollingHotSpotLeftVisible", scrollingHotSpotRightVisibleClass: "scrollingHotSpotRightVisible", scrollableAreaClass: "scrollableArea", scrollWrapperClass: "scrollWrapper", hiddenOnStart: !1, getContentOnLoad: {}, countOnlyClass: "", startAtElementId: "", hotSpotScrolling: !0, hotSpotScrollingStep: 15, hotSpotScrollingInterval: 10, hotSpotMouseDownSpeedBooster: 3, visibleHotSpotBackgrounds: "hover", hotSpotsVisibleTime: 5e3, easingAfterHotSpotScrolling: !0, easingAfterHotSpotScrollingDistance: 10, easingAfterHotSpotScrollingDuration: 300, easingAfterHotSpotScrollingFunction: "easeOutQuart", mousewheelScrolling: "", mousewheelScrollingStep: 70, easingAfterMouseWheelScrolling: !0, easingAfterMouseWheelScrollingDuration: 300, easingAfterMouseWheelScrollingFunction: "easeOutQuart", manualContinuousScrolling: !1, autoScrollingMode: "", autoScrollingDirection: "endlessLoopRight", autoScrollingStep: 1, autoScrollingInterval: 10, touchScrolling: !1, scrollToAnimationDuration: 1e3, scrollToEasingFunction: "easeOutQuart"}, _create: function() {
            var t = this, n = this.options, i = this.element;
            i.data("scrollWrapper", i.find("." + n.scrollWrapperClass)), i.data("scrollingHotSpotRight", i.find("." + n.scrollingHotSpotRightClass)), i.data("scrollingHotSpotLeft", i.find("." + n.scrollingHotSpotLeftClass)), i.data("scrollableArea", i.find("." + n.scrollableAreaClass)), i.data("scrollingHotSpotRight").length > 0 && i.data("scrollingHotSpotRight").detach(), i.data("scrollingHotSpotLeft").length > 0 && i.data("scrollingHotSpotLeft").detach(), 0 === i.data("scrollableArea").length && 0 === i.data("scrollWrapper").length ? (i.wrapInner("<div class='" + n.scrollableAreaClass + "'>").wrapInner("<div class='" + n.scrollWrapperClass + "'>"), i.data("scrollWrapper", i.find("." + n.scrollWrapperClass)), i.data("scrollableArea", i.find("." + n.scrollableAreaClass))) : 0 === i.data("scrollWrapper").length ? (i.wrapInner("<div class='" + n.scrollWrapperClass + "'>"), i.data("scrollWrapper", i.find("." + n.scrollWrapperClass))) : 0 === i.data("scrollableArea").length && (i.data("scrollWrapper").wrapInner("<div class='" + n.scrollableAreaClass + "'>"), i.data("scrollableArea", i.find("." + n.scrollableAreaClass))), 0 === i.data("scrollingHotSpotRight").length ? (i.prepend("<div class='" + n.scrollingHotSpotRightClass + "'></div>"), i.data("scrollingHotSpotRight", i.find("." + n.scrollingHotSpotRightClass))) : i.prepend(i.data("scrollingHotSpotRight")), 0 === i.data("scrollingHotSpotLeft").length ? (i.prepend("<div class='" + n.scrollingHotSpotLeftClass + "'></div>"), i.data("scrollingHotSpotLeft", i.find("." + n.scrollingHotSpotLeftClass))) : i.prepend(i.data("scrollingHotSpotLeft")), i.data("speedBooster", 1), i.data("scrollXPos", 0), i.data("hotSpotWidth", i.data("scrollingHotSpotLeft").innerWidth()), i.data("scrollableAreaWidth", 0), i.data("startingPosition", 0), i.data("rightScrollingInterval", null), i.data("leftScrollingInterval", null), i.data("autoScrollingInterval", null), i.data("hideHotSpotBackgroundsInterval", null), i.data("previousScrollLeft", 0), i.data("pingPongDirection", "right"), i.data("getNextElementWidth", !0), i.data("swapAt", null), i.data("startAtElementHasNotPassed", !0), i.data("swappedElement", null), i.data("originalElements", i.data("scrollableArea").children(n.countOnlyClass)), i.data("visible", !0), i.data("enabled", !0), i.data("scrollableAreaHeight", i.data("scrollableArea").height()), i.data("scrollerOffset", i.offset()), n.touchScrolling && i.data("enabled") && i.data("scrollWrapper").kinetic({y: !1, moved: function() {
                    n.manualContinuousScrolling && (i.data("scrollWrapper").scrollLeft() <= 0 ? t._checkContinuousSwapLeft() : t._checkContinuousSwapRight()), t._trigger("touchMoved")
                }, stopped: function() {
                    i.data("scrollWrapper").stop(!0, !1), t.stopAutoScrolling(), t._trigger("touchStopped")
                }}), i.data("scrollingHotSpotRight").bind("mousemove", function(t) {
                if (n.hotSpotScrolling) {
                    var o = t.pageX - e(this).offset().left;
                    i.data("scrollXPos", Math.round(o / i.data("hotSpotWidth") * n.hotSpotScrollingStep)), (1 / 0 === i.data("scrollXPos") || i.data("scrollXPos") < 1) && i.data("scrollXPos", 1)
                }
            }), i.data("scrollingHotSpotRight").bind("mouseover", function() {
                n.hotSpotScrolling && (i.data("scrollWrapper").stop(!0, !1), t.stopAutoScrolling(), i.data("rightScrollingInterval", setInterval(function() {
                    i.data("scrollXPos") > 0 && i.data("enabled") && (i.data("scrollWrapper").scrollLeft(i.data("scrollWrapper").scrollLeft() + i.data("scrollXPos") * i.data("speedBooster")), n.manualContinuousScrolling && t._checkContinuousSwapRight(), t._showHideHotSpots())
                }, n.hotSpotScrollingInterval)), t._trigger("mouseOverRightHotSpot"))
            }), i.data("scrollingHotSpotRight").bind("mouseout", function() {
                n.hotSpotScrolling && (clearInterval(i.data("rightScrollingInterval")), i.data("scrollXPos", 0), n.easingAfterHotSpotScrolling && i.data("enabled") && i.data("scrollWrapper").animate({scrollLeft: i.data("scrollWrapper").scrollLeft() + n.easingAfterHotSpotScrollingDistance}, {duration: n.easingAfterHotSpotScrollingDuration, easing: n.easingAfterHotSpotScrollingFunction}))
            }), i.data("scrollingHotSpotRight").bind("mousedown", function() {
                i.data("speedBooster", n.hotSpotMouseDownSpeedBooster)
            }), e("body").bind("mouseup", function() {
                i.data("speedBooster", 1)
            }), i.data("scrollingHotSpotLeft").bind("mousemove", function(t) {
                if (n.hotSpotScrolling) {
                    var o = i.data("hotSpotWidth") - (t.pageX - e(this).offset().left);
                    i.data("scrollXPos", Math.round(o / i.data("hotSpotWidth") * n.hotSpotScrollingStep)), (1 / 0 === i.data("scrollXPos") || i.data("scrollXPos") < 1) && i.data("scrollXPos", 1)
                }
            }), i.data("scrollingHotSpotLeft").bind("mouseover", function() {
                n.hotSpotScrolling && (i.data("scrollWrapper").stop(!0, !1), t.stopAutoScrolling(), i.data("leftScrollingInterval", setInterval(function() {
                    i.data("scrollXPos") > 0 && i.data("enabled") && (i.data("scrollWrapper").scrollLeft(i.data("scrollWrapper").scrollLeft() - i.data("scrollXPos") * i.data("speedBooster")), n.manualContinuousScrolling && t._checkContinuousSwapLeft(), t._showHideHotSpots())
                }, n.hotSpotScrollingInterval)), t._trigger("mouseOverLeftHotSpot"))
            }), i.data("scrollingHotSpotLeft").bind("mouseout", function() {
                n.hotSpotScrolling && (clearInterval(i.data("leftScrollingInterval")), i.data("scrollXPos", 0), n.easingAfterHotSpotScrolling && i.data("enabled") && i.data("scrollWrapper").animate({scrollLeft: i.data("scrollWrapper").scrollLeft() - n.easingAfterHotSpotScrollingDistance}, {duration: n.easingAfterHotSpotScrollingDuration, easing: n.easingAfterHotSpotScrollingFunction}))
            }), i.data("scrollingHotSpotLeft").bind("mousedown", function() {
                i.data("speedBooster", n.hotSpotMouseDownSpeedBooster)
            }), i.data("scrollableArea").mousewheel(function(e, o, a, r) {
                if (i.data("enabled") && n.mousewheelScrolling.length > 0) {
                    var s;
                    "vertical" === n.mousewheelScrolling && 0 !== r ? (t.stopAutoScrolling(), e.preventDefault(), s = Math.round(-1 * n.mousewheelScrollingStep * r), t.move(s)) : "horizontal" === n.mousewheelScrolling && 0 !== a ? (t.stopAutoScrolling(), e.preventDefault(), s = Math.round(-1 * n.mousewheelScrollingStep * a), t.move(s)) : "allDirections" === n.mousewheelScrolling && (t.stopAutoScrolling(), e.preventDefault(), s = Math.round(-1 * n.mousewheelScrollingStep * o), t.move(s))
                }
            }), n.mousewheelScrolling && i.data("scrollingHotSpotLeft").add(i.data("scrollingHotSpotRight")).mousewheel(function(e) {
                e.preventDefault()
            }), e(window).bind("resize", function() {
                t._showHideHotSpots(), t._trigger("windowResized")
            }), jQuery.isEmptyObject(n.getContentOnLoad) || t[n.getContentOnLoad.method](n.getContentOnLoad.content, n.getContentOnLoad.manipulationMethod, n.getContentOnLoad.addWhere, n.getContentOnLoad.filterTag), n.hiddenOnStart && t.hide(), e(window).load(function() {
                if (n.hiddenOnStart || t.recalculateScrollableArea(), n.autoScrollingMode.length > 0 && !n.hiddenOnStart && t.startAutoScrolling(), "always" !== n.autoScrollingMode)
                    switch (n.visibleHotSpotBackgrounds) {
                        case"always":
                            t.showHotSpotBackgrounds();
                            break;
                        case"onStart":
                            t.showHotSpotBackgrounds(), i.data("hideHotSpotBackgroundsInterval", setTimeout(function() {
                                t.hideHotSpotBackgrounds(250)
                            }, n.hotSpotsVisibleTime));
                            break;
                        case"hover":
                            i.mouseenter(function(e) {
                                n.hotSpotScrolling && (e.stopPropagation(), t.showHotSpotBackgrounds(250))
                            }).mouseleave(function(e) {
                                n.hotSpotScrolling && (e.stopPropagation(), t.hideHotSpotBackgrounds(250))
                            })
                    }
                t._showHideHotSpots(), t._trigger("setupComplete")
            })
        }, _init: function() {
            var e = this;
            this.element, e.recalculateScrollableArea(), e._showHideHotSpots(), e._trigger("initializationComplete")
        }, _setOption: function(e, t) {
            var n = this, i = this.options, o = this.element;
            i[e] = t, "hotSpotScrolling" === e ? t === !0 ? n._showHideHotSpots() : (o.data("scrollingHotSpotLeft").hide(), o.data("scrollingHotSpotRight").hide()) : "autoScrollingStep" === e || "easingAfterHotSpotScrollingDistance" === e || "easingAfterHotSpotScrollingDuration" === e || "easingAfterMouseWheelScrollingDuration" === e ? i[e] = parseInt(t, 10) : "autoScrollingInterval" === e && (i[e] = parseInt(t, 10), n.startAutoScrolling())
        }, showHotSpotBackgrounds: function(e) {
            var t = this, n = this.element, i = this.options;
            void 0 !== e ? (n.data("scrollingHotSpotLeft").addClass(i.scrollingHotSpotLeftVisibleClass), n.data("scrollingHotSpotRight").addClass(i.scrollingHotSpotRightVisibleClass), n.data("scrollingHotSpotLeft").add(n.data("scrollingHotSpotRight")).fadeTo(e, .35)) : (n.data("scrollingHotSpotLeft").addClass(i.scrollingHotSpotLeftVisibleClass), n.data("scrollingHotSpotLeft").removeAttr("style"), n.data("scrollingHotSpotRight").addClass(i.scrollingHotSpotRightVisibleClass), n.data("scrollingHotSpotRight").removeAttr("style")), t._showHideHotSpots()
        }, hideHotSpotBackgrounds: function(e) {
            var t = this.element, n = this.options;
            void 0 !== e ? (t.data("scrollingHotSpotLeft").fadeTo(e, 0, function() {
                t.data("scrollingHotSpotLeft").removeClass(n.scrollingHotSpotLeftVisibleClass)
            }), t.data("scrollingHotSpotRight").fadeTo(e, 0, function() {
                t.data("scrollingHotSpotRight").removeClass(n.scrollingHotSpotRightVisibleClass)
            })) : (t.data("scrollingHotSpotLeft").removeClass(n.scrollingHotSpotLeftVisibleClass).removeAttr("style"), t.data("scrollingHotSpotRight").removeClass(n.scrollingHotSpotRightVisibleClass).removeAttr("style"))
        }, _showHideHotSpots: function() {
            var e = this, t = this.element, n = this.options;
            n.hotSpotScrolling ? n.hotSpotScrolling && "always" !== n.autoScrollingMode && null !== t.data("autoScrollingInterval") ? (t.data("scrollingHotSpotLeft").show(), t.data("scrollingHotSpotRight").show()) : "always" !== n.autoScrollingMode && n.hotSpotScrolling ? t.data("scrollableAreaWidth") <= t.data("scrollWrapper").innerWidth() ? (t.data("scrollingHotSpotLeft").hide(), t.data("scrollingHotSpotRight").hide()) : 0 === t.data("scrollWrapper").scrollLeft() ? (t.data("scrollingHotSpotLeft").hide(), t.data("scrollingHotSpotRight").show(), e._trigger("scrollerLeftLimitReached"), clearInterval(t.data("leftScrollingInterval")), t.data("leftScrollingInterval", null)) : t.data("scrollableAreaWidth") <= t.data("scrollWrapper").innerWidth() + t.data("scrollWrapper").scrollLeft() ? (t.data("scrollingHotSpotLeft").show(), t.data("scrollingHotSpotRight").hide(), e._trigger("scrollerRightLimitReached"), clearInterval(t.data("rightScrollingInterval")), t.data("rightScrollingInterval", null)) : (t.data("scrollingHotSpotLeft").show(), t.data("scrollingHotSpotRight").show()) : (t.data("scrollingHotSpotLeft").hide(), t.data("scrollingHotSpotRight").hide()) : (t.data("scrollingHotSpotLeft").hide(), t.data("scrollingHotSpotRight").hide())
        }, _setElementScrollPosition: function(t, n) {
            var i = this.element, o = this.options, a = 0;
            switch (t) {
                case"first":
                    return i.data("scrollXPos", 0), !0;
                case"start":
                    return"" !== o.startAtElementId && i.data("scrollableArea").has("#" + o.startAtElementId) ? (a = e("#" + o.startAtElementId).position().left, i.data("scrollXPos", a), !0) : !1;
                case"last":
                    return i.data("scrollXPos", i.data("scrollableAreaWidth") - i.data("scrollWrapper").innerWidth()), !0;
                case"number":
                    return isNaN(n) ? !1 : (a = i.data("scrollableArea").children(o.countOnlyClass).eq(n - 1).position().left, i.data("scrollXPos", a), !0);
                case"id":
                    return n.length > 0 && i.data("scrollableArea").has("#" + n) ? (a = e("#" + n).position().left, i.data("scrollXPos", a), !0) : !1;
                default:
                    return!1
                }
        }, jumpToElement: function(e, t) {
            var n = this, i = this.element;
            if (i.data("enabled") && n._setElementScrollPosition(e, t))
                switch (i.data("scrollWrapper").scrollLeft(i.data("scrollXPos")), n._showHideHotSpots(), e) {
                    case"first":
                        n._trigger("jumpedToFirstElement");
                        break;
                    case"start":
                        n._trigger("jumpedToStartElement");
                        break;
                    case"last":
                        n._trigger("jumpedToLastElement");
                        break;
                    case"number":
                        n._trigger("jumpedToElementNumber", null, {elementNumber: t});
                        break;
                    case"id":
                        n._trigger("jumpedToElementId", null, {elementId: t})
                    }
        }, scrollToElement: function(e, t) {
            var n = this, i = this.element, o = this.options, a = !1;
            i.data("enabled") && n._setElementScrollPosition(e, t) && (null !== i.data("autoScrollingInterval") && (n.stopAutoScrolling(), a = !0), i.data("scrollWrapper").stop(!0, !1), i.data("scrollWrapper").animate({scrollLeft: i.data("scrollXPos")}, {duration: o.scrollToAnimationDuration, easing: o.scrollToEasingFunction, complete: function() {
                    switch (a && n.startAutoScrolling(), n._showHideHotSpots(), e) {
                        case"first":
                            n._trigger("scrolledToFirstElement");
                            break;
                        case"start":
                            n._trigger("scrolledToStartElement");
                            break;
                        case"last":
                            n._trigger("scrolledToLastElement");
                            break;
                        case"number":
                            n._trigger("scrolledToElementNumber", null, {elementNumber: t});
                            break;
                        case"id":
                            n._trigger("scrolledToElementId", null, {elementId: t})
                        }
                }}))
        }, move: function(e) {
            var t = this, n = this.element, i = this.options;
            if (n.data("scrollWrapper").stop(!0, !0), 0 > e && n.data("scrollWrapper").scrollLeft() > 0 || e > 0 && n.data("scrollableAreaWidth") > n.data("scrollWrapper").innerWidth() + n.data("scrollWrapper").scrollLeft() || i.manualContinuousScrolling) {
                var o = n.data("scrollableArea").width() - n.data("scrollWrapper").width(), a = n.data("scrollWrapper").scrollLeft() + e;
                if (0 > a)
                    for (var r = function() {
                        n.data("swappedElement", n.data("scrollableArea").children(":last").detach()), n.data("scrollableArea").prepend(n.data("swappedElement")), n.data("scrollWrapper").scrollLeft(n.data("scrollWrapper").scrollLeft() + n.data("swappedElement").outerWidth(!0))
                    }; 0 > a; )
                        r(), a = n.data("scrollableArea").children(":first").outerWidth(!0) + a;
                else if (a - o > 0)
                    for (var s = function() {
                        n.data("swappedElement", n.data("scrollableArea").children(":first").detach()), n.data("scrollableArea").append(n.data("swappedElement"));
                        var e = n.data("scrollWrapper").scrollLeft();
                        n.data("scrollWrapper").scrollLeft(e - n.data("swappedElement").outerWidth(!0))
                    }; a - o > 0; )
                        s(), a -= n.data("scrollableArea").children(":last").outerWidth(!0);
                i.easingAfterMouseWheelScrolling ? n.data("scrollWrapper").animate({scrollLeft: n.data("scrollWrapper").scrollLeft() + e}, {duration: i.easingAfterMouseWheelScrollingDuration, easing: i.easingAfterMouseWheelFunction, complete: function() {
                        t._showHideHotSpots(), i.manualContinuousScrolling && (e > 0 ? t._checkContinuousSwapRight() : t._checkContinuousSwapLeft())
                    }}) : (n.data("scrollWrapper").scrollLeft(n.data("scrollWrapper").scrollLeft() + e), t._showHideHotSpots(), i.manualContinuousScrolling && (e > 0 ? t._checkContinuousSwapRight() : t._checkContinuousSwapLeft()))
            }
        }, getFlickrContent: function(t, n) {
            var i = this, o = this.element;
            e.getJSON(t, function(t) {
                function a(t, s) {
                    var m = t.media.m, h = m.replace("_m", l[s].letter), f = e("<img />").attr("src", h);
                    f.load(function() {
                        if (this.height < o.data("scrollableAreaHeight") ? s + 1 < l.length ? a(t, s + 1) : r(this) : r(this), p === u) {
                            switch (n) {
                                case"addFirst":
                                    o.data("scrollableArea").children(":first").before(c);
                                    break;
                                case"addLast":
                                    o.data("scrollableArea").children(":last").after(c);
                                    break;
                                default:
                                    o.data("scrollableArea").html(c)
                            }
                            i.recalculateScrollableArea(), i._showHideHotSpots(), i._trigger("addedFlickrContent", null, {addedElementIds: d})
                        }
                    })
                }
                function r(t) {
                    var n = o.data("scrollableAreaHeight") / t.height, i = Math.round(t.width * n), a = e(t).attr("src").split("/"), r = a.length - 1;
                    a = a[r].split("."), e(t).attr("id", a[0]), e(t).css({height: o.data("scrollableAreaHeight"), width: i}), d.push(a[0]), c.push(t), p++
                }
                var s, l = [{size: "small square", pixels: 75, letter: "_s"}, {size: "thumbnail", pixels: 100, letter: "_t"}, {size: "small", pixels: 240, letter: "_m"}, {size: "medium", pixels: 500, letter: ""}, {size: "medium 640", pixels: 640, letter: "_z"}, {size: "large", pixels: 1024, letter: "_b"}], c = [], d = [], u = t.items.length, p = 0;
                s = o.data("scrollableAreaHeight") <= 75 ? 0 : o.data("scrollableAreaHeight") <= 100 ? 1 : o.data("scrollableAreaHeight") <= 240 ? 2 : o.data("scrollableAreaHeight") <= 500 ? 3 : o.data("scrollableAreaHeight") <= 640 ? 4 : 5, e.each(t.items, function(e, t) {
                    a(t, s)
                })
            })
        }, getAjaxContent: function(t, n, i) {
            var o = this, a = this.element;
            e.ajaxSetup({cache: !1}), e.get(t, function(r) {
                var s;
                switch (s = void 0 !== i ? i.length > 0 ? e("<div>").html(r).find(i) : t : r, n) {
                    case"addFirst":
                        a.data("scrollableArea").children(":first").before(s);
                        break;
                    case"addLast":
                        a.data("scrollableArea").children(":last").after(s);
                        break;
                    default:
                        a.data("scrollableArea").html(s)
                }
                o.recalculateScrollableArea(), o._showHideHotSpots(), o._trigger("addedAjaxContent")
            })
        }, getHtmlContent: function(t, n, i) {
            var o, a = this, r = this.element;
            switch (o = void 0 !== i ? i.length > 0 ? e("<div>").html(t).find(i) : t : t, n) {
                case"addFirst":
                    r.data("scrollableArea").children(":first").before(o);
                    break;
                case"addLast":
                    r.data("scrollableArea").children(":last").after(o);
                    break;
                default:
                    r.data("scrollableArea").html(o)
            }
            a.recalculateScrollableArea(), a._showHideHotSpots(), a._trigger("addedHtmlContent")
        }, recalculateScrollableArea: function() {
            var t = 0, n = !1, i = this.options, o = this.element;
            o.data("scrollableArea").children(i.countOnlyClass).each(function() {
                i.startAtElementId.length > 0 && e(this).attr("id") === i.startAtElementId && (o.data("startingPosition", t), n = !0), t += e(this).outerWidth(!0)
            }), n || o.data("startAtElementId", ""), o.data("scrollableAreaWidth", t), o.data("scrollableArea").width(o.data("scrollableAreaWidth")), o.data("scrollWrapper").scrollLeft(o.data("startingPosition")), o.data("scrollXPos", o.data("startingPosition"))
        }, getScrollerOffset: function() {
            var e = this.element;
            return e.data("scrollWrapper").scrollLeft()
        }, stopAutoScrolling: function() {
            var e = this, t = this.element;
            null !== t.data("autoScrollingInterval") && (clearInterval(t.data("autoScrollingInterval")), t.data("autoScrollingInterval", null), e._showHideHotSpots(), e._trigger("autoScrollingStopped"))
        }, startAutoScrolling: function() {
            var e = this, t = this.element, n = this.options;
            t.data("enabled") && (e._showHideHotSpots(), clearInterval(t.data("autoScrollingInterval")), t.data("autoScrollingInterval", null), e._trigger("autoScrollingStarted"), t.data("autoScrollingInterval", setInterval(function() {
                if (!t.data("visible") || t.data("scrollableAreaWidth") <= t.data("scrollWrapper").innerWidth())
                    clearInterval(t.data("autoScrollingInterval")), t.data("autoScrollingInterval", null);
                else
                    switch (t.data("previousScrollLeft", t.data("scrollWrapper").scrollLeft()), n.autoScrollingDirection) {
                        case"right":
                            t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + n.autoScrollingStep), t.data("previousScrollLeft") === t.data("scrollWrapper").scrollLeft() && (e._trigger("autoScrollingRightLimitReached"), e.stopAutoScrolling());
                            break;
                        case"left":
                            t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() - n.autoScrollingStep), t.data("previousScrollLeft") === t.data("scrollWrapper").scrollLeft() && (e._trigger("autoScrollingLeftLimitReached"), e.stopAutoScrolling());
                            break;
                        case"backAndForth":
                            "right" === t.data("pingPongDirection") ? t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + n.autoScrollingStep) : t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() - n.autoScrollingStep), t.data("previousScrollLeft") === t.data("scrollWrapper").scrollLeft() && ("right" === t.data("pingPongDirection") ? (t.data("pingPongDirection", "left"), e._trigger("autoScrollingRightLimitReached")) : (t.data("pingPongDirection", "right"), e._trigger("autoScrollingLeftLimitReached")));
                            break;
                        case"endlessLoopRight":
                            t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + n.autoScrollingStep), e._checkContinuousSwapRight();
                            break;
                        case"endlessLoopLeft":
                            t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() - n.autoScrollingStep), e._checkContinuousSwapLeft()
                        }
            }, n.autoScrollingInterval)))
        }, _checkContinuousSwapRight: function() {
            var t = this.element, n = this.options;
            if (t.data("getNextElementWidth") && (n.startAtElementId.length > 0 && t.data("startAtElementHasNotPassed") ? (t.data("swapAt", e("#" + n.startAtElementId).outerWidth(!0)), t.data("startAtElementHasNotPassed", !1)) : t.data("swapAt", t.data("scrollableArea").children(":first").outerWidth(!0)), t.data("getNextElementWidth", !1)), t.data("swapAt") <= t.data("scrollWrapper").scrollLeft()) {
                t.data("swappedElement", t.data("scrollableArea").children(":first").detach()), t.data("scrollableArea").append(t.data("swappedElement"));
                var i = t.data("scrollWrapper").scrollLeft();
                t.data("scrollWrapper").scrollLeft(i - t.data("swappedElement").outerWidth(!0)), t.data("getNextElementWidth", !0)
            }
        }, _checkContinuousSwapLeft: function() {
            var t = this.element, n = this.options;
            t.data("getNextElementWidth") && (n.startAtElementId.length > 0 && t.data("startAtElementHasNotPassed") ? (t.data("swapAt", e("#" + n.startAtElementId).outerWidth(!0)), t.data("startAtElementHasNotPassed", !1)) : t.data("swapAt", t.data("scrollableArea").children(":first").outerWidth(!0)), t.data("getNextElementWidth", !1)), 0 === t.data("scrollWrapper").scrollLeft() && (t.data("swappedElement", t.data("scrollableArea").children(":last").detach()), t.data("scrollableArea").prepend(t.data("swappedElement")), t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + t.data("swappedElement").outerWidth(!0)), t.data("getNextElementWidth", !0))
        }, restoreOriginalElements: function() {
            var e = this, t = this.element;
            t.data("scrollableArea").html(t.data("originalElements")), e.recalculateScrollableArea(), e.jumpToElement("first")
        }, show: function() {
            var e = this.element;
            e.data("visible", !0), e.show()
        }, hide: function() {
            var e = this.element;
            e.data("visible", !1), e.hide()
        }, enable: function() {
            var e = this.element;
            this.options.touchScrolling && e.data("scrollWrapper").kinetic("attach"), e.data("enabled", !0)
        }, disable: function() {
            var e = this, t = this.element;
            e.stopAutoScrolling(), clearInterval(t.data("rightScrollingInterval")), clearInterval(t.data("leftScrollingInterval")), clearInterval(t.data("hideHotSpotBackgroundsInterval")), this.options.touchScrolling && t.data("scrollWrapper").kinetic("detach"), t.data("enabled", !1)
        }, destroy: function() {
            var t = this, n = this.element;
            t.stopAutoScrolling(), clearInterval(n.data("rightScrollingInterval")), clearInterval(n.data("leftScrollingInterval")), clearInterval(n.data("hideHotSpotBackgroundsInterval")), n.data("scrollingHotSpotRight").unbind("mouseover"), n.data("scrollingHotSpotRight").unbind("mouseout"), n.data("scrollingHotSpotRight").unbind("mousedown"), n.data("scrollingHotSpotLeft").unbind("mouseover"), n.data("scrollingHotSpotLeft").unbind("mouseout"), n.data("scrollingHotSpotLeft").unbind("mousedown"), n.unbind("mousenter"), n.unbind("mouseleave"), n.data("scrollingHotSpotRight").remove(), n.data("scrollingHotSpotLeft").remove(), n.data("scrollableArea").remove(), n.data("scrollWrapper").remove(), n.html(n.data("originalElements")), e.Widget.prototype.destroy.apply(this, arguments)
        }})
}(jQuery), define("smoothScroll", ["jqui", "kinetic", "mousewheel"], function(e) {
    return function() {
        var t;
        return t || e.smoothScroll
    }
}(this)), define("app/views/fleetPage", ["require", "../modules/heroFull", "app/modules/scrollToAnchor", "smoothScroll"], function(e) {
    var t = e("../modules/heroFull"), n = e("app/modules/scrollToAnchor");
    return e("smoothScroll"), {ui: {jetList: $(".jets"), slides: $(".jets").find(".slide"), subnavBtn: $(".page-fleet").find(".subnav > *")}, initialize: function() {
            t.initialize(), this.bindUIEvents(), this.onInitialLoad(), this.ui.slides.length && this.ui.slides.smoothDivScroll({manualContinuousScrolling: !1, hotSpotScrolling: !0, touchScrolling: !0, easingAfterMouseWheelScrollingDuration: 600})
        }, bindUIEvents: function() {
            this.ui.subnavBtn.on("click", $.proxy(this.onClickSubnavBtn, this))
        }, onInitialLoad: function() {
            this.updateNav(this.ui.subnavBtn[0]), this.showJetList(".large-cabin")
        }, onClickSubnavBtn: function(e) {
            var t = $(e.currentTarget);
            this.updateNav(t), this.showJetList(t.data("href")), n.go(t.data("href"))
        }, updateNav: function(e) {
            this.ui.subnavBtn.removeClass("active"), $(e).addClass("active")
        }, showJetList: function(e) {
            this.ui.jetList.removeClass("active"), $(e).addClass("active")
        }}
}), define("app/views/contactPage", ["require", "../modules/formUtils"], function(e) {
    var t = e("../modules/formUtils");
    return{ui: {wrapper: $(".contact-form-wrapper")}, initialize: function() {
            this.contactForm = new t({form: $("form", this.ui.wrapper)})
        }}
}), define("app/views/onewayPage", ["require", "jquery"], function(e) {
    var t = e("jquery");
    return{ui: {flights: t(".oneway-flight")}, initialize: function() {
            this.bindUIEvents()
        }, bindUIEvents: function() {
            t(".btn-red", this.ui.flights).on("click", this.onGetAQuoteClick)
        }, onGetAQuoteClick: function(e) {
            e.preventDefault();
            var n = t(e.currentTarget).parents(".oneway-flight"), i = {departure_city: n.data("departurecity"), destination_city: n.data("destinationcity"), departure_date: n.data("departuredate")};
            _.each(i, function(e, n) {
                t(".form-book-flight [name=" + n + "]").val(e).blur()
            }), t(".btn-toggle, [data-trip=oneway]").trigger("click")
        }}
}), define("app/views/newsPage", ["require", "jquery"], function(e) {
    var t = e("jquery");
    return{initialize: function() {
            t.getScript("//ws.sharethis.com/button/buttons.js?", function() {
                stLight.options({publisher: "ur-2a59c36c-5422-88f9-f6e1-4500f7184bc4", doNotHash: !0, doNotCopy: !0, hashAddressBar: !1, headerTitle: "Jet Edge News", exclusive_services: !0, services: "facebook,twitter,linkedin,pinterest,email", headerbg: "#000", tracking: "google"})
            })
        }}
});
var deviceIsAndroid = navigator.userAgent.indexOf("Android") > 0, deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent), deviceIsIOS4 = deviceIsIOS && /OS 4_\d(_\d)?/.test(navigator.userAgent), deviceIsIOSWithBadTarget = deviceIsIOS && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent), deviceIsBlackBerry10 = navigator.userAgent.indexOf("BB10") > 0;
FastClick.prototype.needsClick = function(e) {
    switch (e.nodeName.toLowerCase()) {
        case"button":
        case"select":
        case"textarea":
            if (e.disabled)
                return!0;
            break;
        case"input":
            if (deviceIsIOS && "file" === e.type || e.disabled)
                return!0;
            break;
        case"label":
        case"video":
            return!0
    }
    return/\bneedsclick\b/.test(e.className)
}, FastClick.prototype.needsFocus = function(e) {
    switch (e.nodeName.toLowerCase()) {
        case"textarea":
            return!0;
        case"select":
            return!deviceIsAndroid;
        case"input":
            switch (e.type) {
                case"button":
                case"checkbox":
                case"file":
                case"image":
                case"radio":
                case"submit":
                    return!1
            }
            return!e.disabled && !e.readOnly;
        default:
            return/\bneedsfocus\b/.test(e.className)
        }
}, FastClick.prototype.sendClick = function(e, t) {
    var n, i;
    document.activeElement && document.activeElement !== e && document.activeElement.blur(), i = t.changedTouches[0], n = document.createEvent("MouseEvents"), n.initMouseEvent(this.determineEventType(e), !0, !0, window, 1, i.screenX, i.screenY, i.clientX, i.clientY, !1, !1, !1, !1, 0, null), n.forwardedTouchEvent = !0, e.dispatchEvent(n)
}, FastClick.prototype.determineEventType = function(e) {
    return deviceIsAndroid && "select" === e.tagName.toLowerCase() ? "mousedown" : "click"
}, FastClick.prototype.focus = function(e) {
    var t;
    deviceIsIOS && e.setSelectionRange && 0 !== e.type.indexOf("date") && "time" !== e.type ? (t = e.value.length, e.setSelectionRange(t, t)) : e.focus()
}, FastClick.prototype.updateScrollParent = function(e) {
    var t, n;
    if (t = e.fastClickScrollParent, !t || !t.contains(e)) {
        n = e;
        do {
            if (n.scrollHeight > n.offsetHeight) {
                t = n, e.fastClickScrollParent = n;
                break
            }
            n = n.parentElement
        } while (n)
    }
    t && (t.fastClickLastScrollTop = t.scrollTop)
}, FastClick.prototype.getTargetElementFromEventTarget = function(e) {
    return e.nodeType === Node.TEXT_NODE ? e.parentNode : e
}, FastClick.prototype.onTouchStart = function(e) {
    var t, n, i;
    if (e.targetTouches.length > 1)
        return!0;
    if (t = this.getTargetElementFromEventTarget(e.target), n = e.targetTouches[0], deviceIsIOS) {
        if (i = window.getSelection(), i.rangeCount && !i.isCollapsed)
            return!0;
        if (!deviceIsIOS4) {
            if (n.identifier && n.identifier === this.lastTouchIdentifier)
                return e.preventDefault(), !1;
            this.lastTouchIdentifier = n.identifier, this.updateScrollParent(t)
        }
    }
    return this.trackingClick = !0, this.trackingClickStart = e.timeStamp, this.targetElement = t, this.touchStartX = n.pageX, this.touchStartY = n.pageY, e.timeStamp - this.lastClickTime < this.tapDelay && e.preventDefault(), !0
}, FastClick.prototype.touchHasMoved = function(e) {
    var t = e.changedTouches[0], n = this.touchBoundary;
    return Math.abs(t.pageX - this.touchStartX) > n || Math.abs(t.pageY - this.touchStartY) > n ? !0 : !1
}, FastClick.prototype.onTouchMove = function(e) {
    return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(e.target) || this.touchHasMoved(e)) && (this.trackingClick = !1, this.targetElement = null), !0) : !0
}, FastClick.prototype.findControl = function(e) {
    return void 0 !== e.control ? e.control : e.htmlFor ? document.getElementById(e.htmlFor) : e.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
}, FastClick.prototype.onTouchEnd = function(e) {
    var t, n, i, o, a, r = this.targetElement;
    if (!this.trackingClick)
        return!0;
    if (e.timeStamp - this.lastClickTime < this.tapDelay)
        return this.cancelNextClick = !0, !0;
    if (this.cancelNextClick = !1, this.lastClickTime = e.timeStamp, n = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, deviceIsIOSWithBadTarget && (a = e.changedTouches[0], r = document.elementFromPoint(a.pageX - window.pageXOffset, a.pageY - window.pageYOffset) || r, r.fastClickScrollParent = this.targetElement.fastClickScrollParent), i = r.tagName.toLowerCase(), "label" === i) {
        if (t = this.findControl(r)) {
            if (this.focus(r), deviceIsAndroid)
                return!1;
            r = t
        }
    } else if (this.needsFocus(r))
        return e.timeStamp - n > 100 || deviceIsIOS && window.top !== window && "input" === i ? (this.targetElement = null, !1) : (this.focus(r), this.sendClick(r, e), deviceIsIOS && "select" === i || (this.targetElement = null, e.preventDefault()), !1);
    return deviceIsIOS && !deviceIsIOS4 && (o = r.fastClickScrollParent, o && o.fastClickLastScrollTop !== o.scrollTop) ? !0 : (this.needsClick(r) || (e.preventDefault(), this.sendClick(r, e)), !1)
}, FastClick.prototype.onTouchCancel = function() {
    this.trackingClick = !1, this.targetElement = null
}, FastClick.prototype.onMouse = function(e) {
    return this.targetElement ? e.forwardedTouchEvent ? !0 : e.cancelable ? !this.needsClick(this.targetElement) || this.cancelNextClick ? (e.stopImmediatePropagation ? e.stopImmediatePropagation() : e.propagationStopped = !0, e.stopPropagation(), e.preventDefault(), !1) : !0 : !0 : !0
}, FastClick.prototype.onClick = function(e) {
    var t;
    return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : "submit" === e.target.type && 0 === e.detail ? !0 : (t = this.onMouse(e), t || (this.targetElement = null), t)
}, FastClick.prototype.destroy = function() {
    var e = this.layer;
    deviceIsAndroid && (e.removeEventListener("mouseover", this.onMouse, !0), e.removeEventListener("mousedown", this.onMouse, !0), e.removeEventListener("mouseup", this.onMouse, !0)), e.removeEventListener("click", this.onClick, !0), e.removeEventListener("touchstart", this.onTouchStart, !1), e.removeEventListener("touchmove", this.onTouchMove, !1), e.removeEventListener("touchend", this.onTouchEnd, !1), e.removeEventListener("touchcancel", this.onTouchCancel, !1)
}, FastClick.notNeeded = function(e) {
    var t, n, i;
    if ("undefined" == typeof window.ontouchstart)
        return!0;
    if (n = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {
        if (!deviceIsAndroid)
            return!0;
        if (t = document.querySelector("meta[name=viewport]")) {
            if (-1 !== t.content.indexOf("user-scalable=no"))
                return!0;
            if (n > 31 && document.documentElement.scrollWidth <= window.outerWidth)
                return!0
        }
    }
    if (deviceIsBlackBerry10 && (i = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), i[1] >= 10 && i[2] >= 3 && (t = document.querySelector("meta[name=viewport]")))) {
        if (-1 !== t.content.indexOf("user-scalable=no"))
            return!0;
        if (document.documentElement.scrollWidth <= window.outerWidth)
            return!0
    }
    return"none" === e.style.msTouchAction ? !0 : !1
}, FastClick.attach = function(e, t) {
    return new FastClick(e, t)
}, "function" == typeof define && "object" == typeof define.amd && define.amd ? define("fastclick", [], function() {
    return FastClick
}) : "undefined" != typeof module && module.exports ? (module.exports = FastClick.attach, module.exports.FastClick = FastClick) : window.FastClick = FastClick, function(e, t, n) {
    function i(e) {
        var t = {}, i = /^jQuery\d+$/;
        return n.each(e.attributes, function(e, n) {
            n.specified && !i.test(n.name) && (t[n.name] = n.value)
        }), t
    }
    function o(e, t) {
        var i = this, o = n(i);
        if (i.value == o.attr("placeholder") && o.hasClass("placeholder"))
            if (o.data("placeholder-password")) {
                if (o = o.hide().next().show().attr("id", o.removeAttr("id").data("placeholder-id")), e === !0)
                    return o[0].value = t;
                o.focus()
            } else
                i.value = "", o.removeClass("placeholder"), i == r() && i.select()
    }
    function a() {
        var e, t = this, a = n(t), r = this.id;
        if ("" == t.value) {
            if ("password" == t.type) {
                if (!a.data("placeholder-textinput")) {
                    try {
                        e = a.clone().attr({type: "text"})
                    } catch (s) {
                        e = n("<input>").attr(n.extend(i(this), {type: "text"}))
                    }
                    e.removeAttr("name").data({"placeholder-password": a, "placeholder-id": r}).bind("focus.placeholder", o), a.data({"placeholder-textinput": e, "placeholder-id": r}).before(e)
                }
                a = a.removeAttr("id").hide().prev().attr("id", r).show()
            }
            a.addClass("placeholder"), a[0].value = a.attr("placeholder")
        } else
            a.removeClass("placeholder")
    }
    function r() {
        try {
            return t.activeElement
        } catch (e) {
        }
    }
    var s, l, c = "[object OperaMini]" == Object.prototype.toString.call(e.operamini), d = "placeholder"in t.createElement("input") && !c, u = "placeholder"in t.createElement("textarea") && !c, p = n.fn, m = n.valHooks, h = n.propHooks;
    d && u ? (l = p.placeholder = function() {
        return this
    }, l.input = l.textarea = !0) : (l = p.placeholder = function() {
        var e = this;
        return e.filter((d ? "textarea" : ":input") + "[placeholder]").not(".placeholder").bind({"focus.placeholder": o, "blur.placeholder": a}).data("placeholder-enabled", !0).trigger("blur.placeholder"), e
    }, l.input = d, l.textarea = u, s = {get: function(e) {
            var t = n(e), i = t.data("placeholder-password");
            return i ? i[0].value : t.data("placeholder-enabled") && t.hasClass("placeholder") ? "" : e.value
        }, set: function(e, t) {
            var i = n(e), s = i.data("placeholder-password");
            return s ? s[0].value = t : i.data("placeholder-enabled") ? ("" == t ? (e.value = t, e != r() && a.call(e)) : i.hasClass("placeholder") ? o.call(e, !0, t) || (e.value = t) : e.value = t, i) : e.value = t
        }}, d || (m.input = s, h.value = s), u || (m.textarea = s, h.value = s), n(function() {
        n(t).delegate("form", "submit.placeholder", function() {
            var e = n(".placeholder", this).each(o);
            setTimeout(function() {
                e.each(a)
            }, 10)
        })
    }), n(e).bind("beforeunload.placeholder", function() {
        n(".placeholder").each(function() {
            this.value = ""
        })
    }))
}(this, document, jQuery), define("jquery.placeholder", ["jquery"], function() {
}), define("app/app", ["require", "jquery", "app/utils/helpers", "app/views/SiteView", "app/views/landingPage", "app/views/fleetPage", "app/views/contactPage", "app/views/onewayPage", "app/views/newsPage", "fastclick", "jquery.placeholder"], function(e) {
    var t = e("jquery"), n = (e("app/utils/helpers"), e("app/views/SiteView")), i = e("app/views/landingPage"), o = e("app/views/fleetPage"), a = e("app/views/contactPage"), r = e("app/views/onewayPage"), s = e("app/views/newsPage"), l = e("fastclick");
    Modernizr.input.placeholder || (e("jquery.placeholder"), t("input,textarea").placeholder()), n.initialize(), t(".page-landing").length && i.initialize(), t(".page-fleet").length && o.initialize(), t(".page-contact").length && a.initialize(), t(".page-oneway").length && r.initialize(), t(".page-news").length && s.initialize(), document.addEventListener("touchstart", function() {
    }, !0), l.attach(document.body)
}), require(["./config"], function() {
    require(["app/app"], function(e) {
        "function" == typeof e && e()
    })
}), define("main", function() {
});