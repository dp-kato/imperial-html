<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header margin-30">                                
                    <img src="assets/img/images/campaign-grey.png" alt="img">
                    <h3>キャンペーン応募フォーム</h3>
                </div>
            </div>
            <div class="col-md-12">
                <div class="campaigning">
                    <div class="campaigning-img">
                        <img src="assets/img/images/campaign/img-campaigning.jpg" alt="img">
                        <div class="img-campaigning-text">
                            <p><b>キャンペーンタイトル</b></p>
                            <p>2015.10.10</p>
                        </div>
                    </div>
                    <div class="campaigning-text">
                        <p>これはダミー文章です。キャンペーンの記事が入ります。これはダミー文章です。キャンペーンの記事が入ります。これはダミー文章です。キャンペーンの
                            記事が入ります。これはダミー文章です。キャンペーンの記事が入ります。これはダミー文章です。キャンペーンの記事が入ります。これはダミー文章です。
                            キャンペーンの記事が入ります。これはダミー文章です。キャンペーンの記事が入ります。これはダミー文章です。キャンペーンの記事が入ります。これは
                            ダミー文章です。キャンペーンの記事が入ります。これはダミー文章です。キャンペーンの記事が入ります。
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="below-top-list no-border margin-40"> 
                    <table class="table">                                    
                        <tbody>
                            <tr>
                                <th scope="row">ダミーテキスト</th>
                                <td>
                                    <div class="inner-table-item">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">                                      
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">ダミーテキスト</th>
                                <td>
                                    <div class="inner-table-item">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">                                      
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">ダミーテキスト</th>
                                <td>
                                    <div class="inner-table-item">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">                                      
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">ダミーテキスト</th>
                                <td>
                                    <div class="inner-table-item">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">                                      
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">ダミーテキスト</th>
                                <td>
                                    <div class="inner-table-item">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">項目</p>
                                        <input class="form-control" type="text">                                      
                                    </div>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </div>
                <div class="change-password-span margin-40">
                    <a class="white-proceed btn-refresh">送信する</a>
                </div>













































            </div>

        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    $('.btn-modal').click(function() {
        var dataTarget = $(this).attr('data-target');
        setTimeout(function() {
            $(dataTarget).find('div.modal-body').removeClass('modal-loading');
            $(dataTarget).find('div.single-item').removeClass('disappear');
            $(dataTarget).find('div.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });

        }, 500);
    });

</script>
</body>

</html>
