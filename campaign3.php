<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header margin-30">                                
                    <img src="assets/img/images/campaign-grey.png" alt="img">
                    <h3>キャンペーン</h3>
                </div> 
            </div>
            <div class="col-md-12">
                <ul class="banner-top-list">
                    <li>抽選で当たる</li>
                    <li>使用ポイント：300pt</li>
                </ul>
                <div class="campaign-banner">
                    <div class="img-banner-campaign">
                        <img src="assets/img/images/campaign/img-banner-3.png" alt="ïmg"/>
                    </div>
                </div>
                <div class="campaign-content">
                    <h3>あなただけのコミックウエストプレゼント！</h3>
                    <p class="head">コミックウエストで人気の『タナカダファミリア』と『アーノルズはせがわ』が</p>
                    <p class="head">あなたのために書き下ろした漫画を色紙でプレゼント！</p>
                    <p>ダミー文章ダミー文章ダミー文章ダミー文章ダミー文章ダミー文章</p>
                    <p>応募の際に入力いただいたフリーアンサーを元に、『タナカダファミリア』と『アーノルズはせがわ』が</p>
                    <p>あなたのために漫画を描き下ろします！</p>
                </div>  
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="campaign-form">
                        <div class="content-top-part-1">
                            <h4 class="margin-15">ご希望のコースを選択してください。</h4>
                            <div class="margin-15">
                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio01" name="radio">
                                <label  for="radio02"><span></span> タナカダファミリア コース</label>
                            </div>
                            <div class="margin-15">
                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio02" name="radio">
                                <label  for="radio02"><span></span> アーノルズはせがわ コース</label>
                            </div>
                        </div>
                        <div class="content-middle-part-2">
                            <h4 class="margin-15">あなたのプロフィールを入力してください。</h4>
                            <div class="margin-15">
                                <label>性別</label>
                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio03" name="radio">
                                <label for="radio02"><span></span> 男性</label>
                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio04" name="radio">
                                <label for="radio02"><span></span>女性</label>
                            </div>
                            <div class="margin-15">
                                <label>年齢</label>
                                <input type="text" name="questions" rel="percent-1" id="text01" name="text">
                                <label>歳</label>
                            </div>
                            <div class="margin-15">
                                <label>職業</label>
                                <input type="text" name="questions" rel="percent-1" id="text02" name="text">
                            </div>
                        </div>
                        <div class="content-third-part-3">
                            <h3>あなたの身の回りで起きたありえない出来事を教えてください。</h3>
                            <textarea></textarea>
                        </div>
                    </div>

                </div>
                <div class="change-password-span margin-40 border-bottom">
                    <a class="white-proceed btn-refresh">応募する</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="campaign-footer">
                        <h3>キャンペーン一覧</h3>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/campaign/img-foot-1.png" alt="img">                                            
                                </div>
                                <div class="new-entry">
                                    <img src="assets/img/images/comics/new-tag.png" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a> <i class="fa fa-angle-right"></i></a>
                                    <p>西さん監修！<br>癒しのひとときプレゼント</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/campaign/img-foot-2.png" alt="img">                                            
                                </div>                        
                                <div class="comics-listing-text">
                                    <a> <i class="fa fa-angle-right"></i></a>
                                    <p>あなただけのコミック<br>ウエストプレゼント！</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>









        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    $('.btn-modal').click(function() {
        var dataTarget = $(this).attr('data-target');
        setTimeout(function() {
            $(dataTarget).find('div.modal-body').removeClass('modal-loading');
            $(dataTarget).find('div.single-item').removeClass('disappear');
            $(dataTarget).find('div.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });

        }, 500);
    });

</script>
</body>

</html>
