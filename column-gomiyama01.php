<?php include('header.php'); ?>

<div class="container-fluid">
    <div class="row kachigumiWrap">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/column-grey.png" height="34" width="25" alt="img">
                    <h3>価値組生活<small>No.1 「WESTで浮いたお金で子供の頃に夢みた大人買いを堪能してみる」</small></h3>
                </div>
                <p class="inner-page-header-date">2015.05.01</p>
            </div> 
            <div class="col-md-12">
                <div class="row">
                    <div class="interview-content">
                        <div class="col-xs-9">
                            <div class="kachigumi-title">
                                <h3>WESTで浮いたお金で子供の頃に夢みた<br>大人買いを堪能してみる</h3>
                            </div>
                            <div class="kachigumi-content margin-20">
                                <p>どうも、ゴミ山地球です！
                                <br>「世界をちょっぴり面白くしよう！！」をコンセプトに
                                <br>脳みそを空っぽにして、活動しています。</p>
                                <p>今回は、国産タバコからWESTに乗り換えて浮いたお金で、
                                <br>駄菓子屋で大人買いをすることにしました。</p>
                            </div>
                            <div class="center margin-20">
                                <img src="assets/img/images/column/gomiyama/img_vol01_01.jpg" height="330" width="560" alt="img">
                            </div>

                            <div id="additional-QuesAns" style="display:none">
                                
                                <div class="kachigumi-content margin-20">
                                    <p>深夜２時すぎ…
                                    <br>大人買いをするために訪れたのはここ。
                                    <br>下北沢のはずれに佇む
                                    <br>「真夜中の駄菓子屋　悪童処」</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_02.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>な、な、な、なんとも怪しい…！
                                    <br>19:00〜翌05:00に営業している
                                    <br>大人のための駄菓子屋です！</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_03.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>では、店内に入ってみることに…</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_04.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>うぉー！！懐かしの駄菓子がずらり！！</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_05.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>こちらが店主の東久条さん。
                                    <br>いざ喋りかけてみると…
                                    <br>喋る！喋る！喋りが止まらない！</p>
                                    <p>なぜ深夜営業にしているのか気になったので
                                    <br>聞いてみたところ…少子高齢化で子供が減ってしまったので
                                    <br>大人向けに営業しているとのこと！
                                    <br>（子供をたくさん作らなければ…！！）</p>
                                    <p>さらに話し続けてみると、衝撃の事実が発覚！</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_06.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>東久条さん：三丁目の夕日に出てる茶川さんは俺だよ</p>
                                    <p>な、な、な、なにぃぃぃ！？！？！？</p>
                                    <p>なんとあの茶川さんは東九条さんがモデルだったのです！
                                    <br>（確かに雰囲気も似ていました！）</p>
                                    <p>それだけじゃありません！
                                    <br>なんと競馬でG1レース30連勝をしたことがある
                                    <br>正真正銘のすごい人だったのです！</p>
                                </div>
                                <div class="center margin-20">
                                    <p>
                                        <img src="assets/img/images/column/gomiyama/img_vol01_07.jpg" height="330" width="560" alt="img">
                                    </p>
                                     <p>
                                        <img src="assets/img/images/column/gomiyama/img_vol01_08.jpg" height="330" width="560" alt="img">
                                    </p>
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>そんな凄腕、東九条さんと喋ること２時間半（！）</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_09.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>ようやく1000円の駄菓子を買い、
                                    <br>家でたらふく食べることにしました！</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_10.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>最後は東九条さんと一緒にパチャリ。</p>
                                    <p>帰宅後。</p>
                                    <p>いざ大人買いをしてみたものの
                                    <br>家に帰って1000円分のお菓子を
                                    <br>食べるだけではつまらない…</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_11.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>そこで今回はこんな企画を考えてみました。</p>
                                    <p>子供の頃に一度はやったことがある
                                    <br>「これとこれを一緒に食ったら美味いんじゃね？」
                                    <br>通称『掛け合わせ駄菓子』を贅沢に検証してみます！</p>
                                </div>
                                <div class="center margin-20">
                                    <p>
                                        <img src="assets/img/images/column/gomiyama/img_vol01_12.jpg" height="330" width="560" alt="img">
                                    </p>
                                     <p>
                                        <img src="assets/img/images/column/gomiyama/img_vol01_13.jpg" height="398" width="560" alt="img">
                                    </p>
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p><b>第３位「パチパチばくだん」×「蒲焼さん」</b></p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_14.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>ぱりっぱりの蒲焼さんの上で
                                    <br>パチパチと稲妻が駆け巡る！！！</p>
                                    <p><b>第２位「ソフト菓子」×「みつあんず」</b></p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_15.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>「この上になんか乗っけてくれ！」と
                                    <br>言わんばかりのソフト菓子に酸っぱいみつあんずを
                                    <br>乗っけて食べる！見た目で堂々の２位にランクイン！</p>
                                    <p>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        運命の…第１位は！
                                        <br><b>「チョコバット」×「モロッコヨーグルト」</b>

                                    </p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_16.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>チョコバットがモロッコヨーグルトに
                                    <br>すっぽり入るという奇跡！しかも
                                    <br>普通に美味い！！こりゃ堂々の１位でしょ！</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_17.jpg" height="411" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>もはや美味い、不味いかじゃない…！
                                    <br>子供のように全力で楽しむことが大切なんだ！</p>
                                    <p>ゴミ山：いや〜楽しんだ！楽しんだ！</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_18.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>ゴミ山：そういや…駄菓子屋の店主に
                                    <br>色々と競馬の話してもらったなぁ〜</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_19.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>そうだ！次は浮いたお金でギャンブルでもしてみるか！</p>
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>ゴミ山：そういや…駄菓子屋の店主に
                                    <br>色々と競馬の話してもらったなぁ〜</p>
                                </div>
                                <div class="center margin-20">
                                    <img src="assets/img/images/column/gomiyama/img_vol01_20.jpg" height="330" width="560" alt="img">
                                </div>
                                <div class="kachigumi-content margin-20">
                                    <p>ゴミ山…次回「ギャンブル編」に続く！</p>
                                </div>
                                


                                <div class="center">
                                    <a class="btn-previous"><i class="fa fa-caret-left"></i> 前の記事へ</a>
                                    <a class="btn-next">次の記事へ <i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                            <div class="center">
                                <a id="showAdditionalQuesAns" class="btn-read-more">もっと読む</a>
                            </div>







                        </div>
                        <div class="col-xs-3">
                            <ul class="other-interviews">
                                <li class="other-interviews-heading">
                                    1,500円コラム一覧
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img5.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で買える 驚きの商品！ コピー</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img6.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で食べられる 大満足の食事！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img7.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で買える 驚きの商品！ コピー</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="interview-listing">                                                    
                                            <div class="interview-listing-text">
                                                <img src="assets/img/images/interview/img8.jpg" alt="img">
                                                <p class="text-small">2015.05.01</p>
                                                <p>1,500円で食べられる 大満足の食事！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
