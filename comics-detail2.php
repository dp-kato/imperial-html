<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/comics-grey.png" alt="img">
                    <h3>コミック<small>喫煙所物語</small></h3>
                    <a  data-toggle="modal" data-target="#modal-comics" class="btn-modal grey-bg btn-inner-page-header btn-comics">サラリーマンヒーロー西さんと東さんのプロフィール</a>
                </div>

            </div>
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="#">アーノルズはせがわ </a></li>
                    <li><a href="#">2015.05.01</a></li>
                    <li class="active">1服目　遭遇</li>
                </ol>
                <div class="comics-slider">
                    <div class="col-md-12">
                        <div class="slider single-item">
                            <div>
                                <img src="assets/img/images/comics/main-hasegawa02.jpg" alt="img"/>
                            </div>
                            <div>
                                <img src="assets/img/images/comics/main-haegawa01.jpg" alt="img"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">                         
                <div class="modal-description">
                    <p>この文章はダミーです。お笑い芸人・田中 光がネットで発表し、フジテレビ『人狼~嘘つきは誰だ?~』のTVCMでのコラボなど、</p>
                    <p>   密かに話題を集め始めているシュール系一コママンガ『サラリーマン山崎シゲル』。 </p>
                </div>
                <div class="comic-row-1">
                    <div class="row">
                        <h3 class="author-description-comics2">アーノルズはせがわ <span>- 喫煙所物語</span></h3>
                        <div class="slider slides-nav multiple-items hasegawaList">
                            <div class="col-md-2-5">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/comics/img-hasegawa02.jpg" alt="img">                                            
                                    </div>
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                        <p>2服目<br>なぞ</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2-5">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/comics/img-hasegawa01.jpg" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <a href="#" data-toggle="modal" data-target="#modal-comics2-11" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                        <p>1服目<br>遭遇</p>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>  
                </div>
                <div class="comic-row-2">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="main-story">
                                <a href="comics-detail.php">
                                    <div class="main-story-img">
                                        <img src="assets/img/images/comics/img-banner-02.jpg" alt="img">
                                    </div>
                                    <div class="main-story-text">
                                        <i class="fa fa-angle-right"></i>
                                        <p class="head"><b>田中光</b>東さんの叫び</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright c 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>



<div class="modal fade" id="modal-comics" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>サラリーマンヒーロー西さんと東さんのプロフィール</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <img src="assets/img/images/modal-images/comic-modal-1.jpg" alt="img">                                         
                </div>
                <div class="col-xs-12">
                    <img src="assets/img/images/modal-images/comic-modal-2.jpg" alt="img">                                         
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>

<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true,
        asNavFor: '.slides-nav'
    });
     $('.slides-nav').slick({
      dots: false,
      slidesToShow: 5,
      centerMode: false,
      focusOnSelect: true,
      asNavFor: '.single-item',
    });
    
    
</script>
</body>

</html>
