<?php

/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles (default)                         :*/
/*::                  'K' is kilometers                                      :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at http://www.geodatasource.com                          :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: http://www.geodatasource.com                        :*/
/*::                                                                         :*/
/*::         GeoDataSource.com (C) All Rights Reserved 2015		   		     :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}
$locations = array(
  'h'=>'43.064615,141.346807',
  't'=>'37.756239,139.976807',
  'kt'=>'35.689487,139.691706',
  'cb'=>'35.181446,136.906398',
  'ks'=>'34.693738,135.502165',
  'cg'=>'34.385203,132.455293',
  's'=>'33.841624,132.765681',
  'ky'=>'33.590355,130.401716',
);

$results = array();

$ip = $_SERVER['REMOTE_ADDR'];
$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
$location = explode(',',$details->loc);
if($location){
    $curLat = $location[0];
    $curLon = $location[1];
}


$curLat = 35.6833;
$curLon = 139.6833;
foreach($locations as $ind => $locs):
  $LatLon = explode(',', $locs);
  //array_push($results, distance($curLat, $curLon, $LatLon[0], $LatLon[1], "K"));
  $results[$ind] = distance($curLat, $curLon, $LatLon[0], $LatLon[1], "K");
endforeach;
//echo distance(-27.0000, 133.0000, 43.064615, 141.346807, "K") . " Kilometer<br>";
date_default_timezone_set('Asia/Tokyo');
$imgPath = 'assets/img/landing/pc';
$areaCode = array_search(min($results), $results);
$hour = date('H');
$randNum = rand(1, 3).'.jpg';
if ($hour >= 7 && $hour <= 18) {
     echo  $imgPath.$areaCode.'_d_'.$randNum;
} else if ($hour >= 19) {
    echo  $imgPath.$areaCode.'_n_'.$randNum;
} else {
    echo  $imgPath.$areaCode.'_n_'.$randNum;
}




?>