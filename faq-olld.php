<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="header-faq">
                    <div class="inner-page-header margin-15 faq-title">                                
                        <img src="../../../../../../Users/Hassan/Downloads/Telegram Desktop/assets/img/images/faq-grey.png" alt="img">
                        <h3>FAQ</h3>
                    </div> 
                    <ul class="faq-links">
                        <li><a href="#link-1" rel='link-1'>ＩＤについて</a></li>
                        <li><a href="#link-2" rel='link-2'>製品について</a></li>
                        <li><a href="#link-3" rel='link-3'>ポイントについて</a></li>
                        <li><a href="#link-4" rel='link-4'>製品について</a></li>
                        <li><a href="#link-5" rel='link-5'>製品について</a></li>
                    </ul>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="lower-faq-panel">
                    <h5 id="link-1">ＩＤについて</h5>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading1">
                                <h4 class="panel-title">
                                    <a class="active" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                        Q.登録した住所やパスワード、ログイン用メールアドレスを変更したいがどうすればよいか？
                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body">
                                    <p><b>A</b> ・ウエストブランドサイトの会員になることでWestのお得なキャンペーン情報やオリジナルコンテンツ、ブランド紹介などを閲覧できます。
                                     また、ブランドサイト内の「ゲーム」や「コミック」「アンケート」等様々なコンテンツを利用するとでWestポイント（Wポイント）が貯まり、貯まったWポイントでお得なキャンペーンにご応募いただけます。</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading2">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                       Q.「ウエストブランドサイト」の会員登録は有料ですか？

                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body">
                                    <p>・「ウエストブランドサイト」への会員登録は無料です。
</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading3">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                        Q.携帯メールアドレスで「ウエストブランドサイト」に登録することはできますか？

                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body">
                                    <p>・携帯メールアドレスで会員登録の手続きをすることは可能です。ただし当サイトはPCやスマートフォン、タブレット向けのコンテンツを豊富にご用意しているため、当サイトを楽しんでいただくには、スマートフォンやタブレットなどの閲覧環境でご利用いただくことをおすすめ致します。
</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                        Q.フィーチャーホンでも「ウエストブランドサイト」を登録・閲覧できますか？

                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                <div class="panel-body">
                                    <p>・フィーチャーフォン専用サイトをご用意しております。ブランド紹介やニュースなどを閲覧いただくことができます。ただしPCやスマートフォン、タブレット用のサイトのようなオリジナルコンテンツはご利用いただくことができません、またWポイントも貯まりません。
</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading5">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                       Q.会員登録したいのですが、メールの返信が届きません。

                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                <div class="panel-body">
                                    <p>"・迷惑メール対策機能により届いていない可能性がございますので、以下の方法にて受信ができるよう再設定をお願いいたします。.
　▶携帯メールメール：設定のドメイン指定受信で""westonline.jp""をご指定ください。メール設定に関してのお問い合わせは、ご利用の各携帯電話会社にお願いいたします。
　▶PCメール：迷惑メールフォルダに""westonline.jp""からのメールが受信されていないかご確認をお願いいたします。
・入力したメールアドレスが正しいかご確認の上、再度お試しいただきますようお願い致します。なお、システムの都合により稀にお時間がかかる場合もございます。 "
</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading6">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                        Q.登録したログイン用のメールアドレスを忘れてしまったためログインできない。

                                    </a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                <div class="panel-body">
                                    <p>・事務局での変更が必要になります。<a href="inquiry.php"><font color="red">こちら</font></a>からご連絡ください。
</p>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                    </div>


                    <h5 id="link-2">製品について</h5>
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="collapse9">
                                <h4 class="panel-title">
                                    <a class="active" data-toggle="collapse" data-parent="#accordion1" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
                                        Q : 会員登録のメールやお問合せの返信が届きません。                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse9">
                                <div class="panel-body">
                                    <p><b>A</b> 迷惑メール対策機能により届いていない可能性がございますので、以下の方法にて受信ができるよう再設定お願いします。</p>
                                    <p>    【携帯電話】 メール設定のドメイン指定受信で"westonline.jp"をご指定ください。メール設定に関してのお問合せは、ご利用の各携帯電話会社にお願い致します。<p>
                                    <p>   【PC】 迷惑メールフォルダにwestonline.jpからのメールが受信がされていないか、ご確認お願いいたします。<p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading10">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                        Q : 登録した自分のメールアドレスや住所の変更方法を教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading11">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                        Q : 遺伝子組み換えされたタバコ葉を使っていますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading12">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                        Q : 製品は御社から直接買えますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading13">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                        Q : ポイントってどのようにつかうんですか。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading14">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                        Q : 会員限定サイトを退会したいのですが、どうすればよいですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <h5 id="link-3">ポイントについて</h5>
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading15">
                                <h4 class="panel-title">
                                    <a class="active" data-toggle="collapse" data-parent="#accordion2" href="#collapse15" aria-expanded="true" aria-controls="collapse15">
                                        Q : 会員登録のメールやお問合せの返信が届きません。                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                <div class="panel-body">
                                    <p><b>A</b> 迷惑メール対策機能により届いていない可能性がございますので、以下の方法にて受信ができるよう再設定お願いします。</p>
                                    <p>    【携帯電話】 メール設定のドメイン指定受信で"westonline.jp"をご指定ください。メール設定に関してのお問合せは、ご利用の各携帯電話会社にお願い致します。<p>
                                    <p>   【PC】 迷惑メールフォルダにwestonline.jpからのメールが受信がされていないか、ご確認お願いいたします。<p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading16">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                        Q : 登録した自分のメールアドレスや住所の変更方法を教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading17">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                        Q : 遺伝子組み換えされたタバコ葉を使っていますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading18">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                        Q : 製品は御社から直接買えますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse18" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading19">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse19" aria-expanded="false" aria-controls="collapse19">
                                        Q : ポイントってどのようにつかうんですか。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading20">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse20" aria-expanded="false" aria-controls="collapse20">
                                        Q : 会員限定サイトを退会したいのですが、どうすればよいですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 id="link-4">製品について</h5>
                    <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading21">
                                <h4 class="panel-title">
                                    <a class="active" data-toggle="collapse" data-parent="#accordion3" href="#collapse21" aria-expanded="true" aria-controls="collapse21">
                                        Q : 会員登録のメールやお問合せの返信が届きません。                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse21" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading21">
                                <div class="panel-body">
                                    <p><b>A</b> 迷惑メール対策機能により届いていない可能性がございますので、以下の方法にて受信ができるよう再設定お願いします。</p>
                                    <p>    【携帯電話】 メール設定のドメイン指定受信で"westonline.jp"をご指定ください。メール設定に関してのお問合せは、ご利用の各携帯電話会社にお願い致します。<p>
                                    <p>   【PC】 迷惑メールフォルダにwestonline.jpからのメールが受信がされていないか、ご確認お願いいたします。<p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading22">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse22" aria-expanded="false" aria-controls="collapse22">
                                        Q : 登録した自分のメールアドレスや住所の変更方法を教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse22" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading23">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse23" aria-expanded="false" aria-controls="collapse23">
                                        Q : 遺伝子組み換えされたタバコ葉を使っていますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse23" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading23">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading24">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse24" aria-expanded="false" aria-controls="collapse24">
                                        Q : 製品は御社から直接買えますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse24" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading24">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading25">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse25" aria-expanded="false" aria-controls="collapse25">
                                        Q : ポイントってどのようにつかうんですか。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse25" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading25">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading26">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse26" aria-expanded="false" aria-controls="collapse26">
                                        Q : 会員限定サイトを退会したいのですが、どうすればよいですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse26" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading26">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <h5 id="link-5">製品について</h5>
                    <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading27">
                                <h4 class="panel-title">
                                    <a class="active" data-toggle="collapse" data-parent="#accordion4" href="#collapse27" aria-expanded="true" aria-controls="collapse27">
                                        Q : 会員登録のメールやお問合せの返信が届きません。                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse27" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading27">
                                <div class="panel-body">
                                    <p><b>A</b> 迷惑メール対策機能により届いていない可能性がございますので、以下の方法にて受信ができるよう再設定お願いします。</p>
                                    <p>    【携帯電話】 メール設定のドメイン指定受信で"westonline.jp"をご指定ください。メール設定に関してのお問合せは、ご利用の各携帯電話会社にお願い致します。<p>
                                    <p>   【PC】 迷惑メールフォルダにwestonline.jpからのメールが受信がされていないか、ご確認お願いいたします。<p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading28">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse28" aria-expanded="false" aria-controls="collapse28">
                                        Q : 登録した自分のメールアドレスや住所の変更方法を教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse28" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading28">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading29">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse29" aria-expanded="false" aria-controls="collapse29">
                                        Q : 遺伝子組み換えされたタバコ葉を使っていますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse29" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading29">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading30">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse30" aria-expanded="false" aria-controls="collapse30">
                                        Q : 製品は御社から直接買えますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse30" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading30">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading31">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse31" aria-expanded="false" aria-controls="collapse31">
                                        Q : ポイントってどのようにつかうんですか。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse31" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading31">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading32">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse32" aria-expanded="false" aria-controls="collapse32">
                                        Q : 会員限定サイトを退会したいのですが、どうすればよいですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse32" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading32">
                                <div class="panel-body">
                                    <p>Lorem Ipsum dolor set amet</p>
                                </div>
                            </div>
                        </div>
                    </div>



















                </div>
            </div>

        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="../../../../../../Users/Hassan/Downloads/Telegram Desktop/assets/js/jquery.js"></script>     
<script src="../../../../../../Users/Hassan/Downloads/Telegram Desktop/assets/js/odometer.min.js"></script>       
<script src="../../../../../../Users/Hassan/Downloads/Telegram Desktop/assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="../../../../../../Users/Hassan/Downloads/Telegram Desktop/assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });
        });
        $('.panel-heading .panel-title a').click(function() {
            //$(this).toggleClass('active');
            $('.panel-heading .panel-title').removeClass('active');
            
        });

        $('.faq-links li a').click(function() {
            var target = $(this).attr('rel');
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#" + target).offset().top - 280
            }, 1000);

        });

    });

</script>
</body>

</html>
