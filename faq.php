                        






<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="header-faq">
                    <div class="inner-page-header margin-15 faq-title">                                
                        <img src="assets/img/images/faq-grey.png" alt="img">
                        <h3>FAQ</h3>
                    </div> 
                    <ul class="faq-links">
                        <li><a href="#link-1" rel='link-1'>ウエストブランドサイトについて</a></li>
                        <li><a href="#link-2" rel='link-2'>登録情報の確認／変更に関して</a></li>
                        <li><a href="#link-3" rel='link-3'>Westポイント（Wポイント)について</a></li>
                        <li><a href="#link-4" rel='link-4'>キャンペーンについて</a></li>
                        <li><a href="#link-5" rel='link-5'>「たばこ」Westについて</a></li>
                    </ul>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="lower-faq-panel">
                    <h5 id="link-1">ウエストブランドサイトについて</h5>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading1">
                                <h4 class="panel-title">
                                    <a class="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    たばこ「West（ウエスト）」ブランドサイトに登録すると何ができるようになるのですか？
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body">
                                    <p>たばこ「West（ウエスト）」ブランドサイトの会員になることで、Westのお得なキャンペーンをはじめインタビュー、Westの販売店（全国のコンビニエンスストア、たばこ店など）、喫煙所マップ、タバコの味に関する情報などを閲覧することができます。</p>
                                    <p>2015年4月27日のサイトリニューアルより、「サラリーマン山崎シゲル」で知られる田中光氏による、書き下ろし漫画「西さん」の連載を新たにご覧いただけるようになりました。また、Westブランドサイト内の「ゲーム」や「コミック」「アンケート」等様々なコンテンツを利用することで、Westポイント（Wポイント）が貯まり、貯まったWポイントでお得なキャンペーンにご応募いただけます。</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading2">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                    たばこ「West（ウエスト）」ブランドサイトの会員登録は有料ですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body">
                                     <p>たばこ「West（ウエスト）」ブランドサイトは、無料でご利用いただくことができます。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading3">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                    携帯（フィーチャーフォン）のメールアドレスでたばこ「West（ウエスト）」ブランドサイトに登録することはできますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body">
                                    <p>携帯のメールアドレスでも会員登録の手続きをすることは可能ですが、携帯サイトでは限定されたコンテンツのみを提供しているため、PCやスマートフォン、タブレットで閲覧いただくことを推奨いたします。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    携帯（フィーチャーフォン）でも「ウエストブランドサイト」を登録・閲覧できますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                <div class="panel-body">
                                    <p>携帯専用サイトをご用意しております。ブランド紹介やニュースなど一部のコンテンツを閲覧いただくことができます。Westブランドサイトの全てのコンテンツをご覧いただくためには、PCやスマートフォン、タブレットでアクセスいただけますようお願いいたします。Westサイト内で利用できるWポイントは、携帯専用サイトではご利用いただけませんこと予めご了承ください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading5">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                    サイト閲覧の推奨環境を教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                <div class="panel-body">
                                    <p><a href="#">こちら</a>をご確認ください。</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <h5 id="link-2">登録情報の確認／変更に関して</h5>
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading7">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                    登録した住所やパスワード、ログイン用メールアドレスを確認したいがどうすればよいか？
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                                <div class="panel-body">
                                <p><a href="#">こちら</a>よりご確認いただけます。</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading8">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                    登録した住所やパスワード、ログイン用メールアドレスを変更したいがどうすればよいか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                                <div class="panel-body">
                                    <p><a href="#">こちら</a>より変更手続きを行ってください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading9">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                    メールマガジンを受信したいのですが？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                                <div class="panel-body">
                                    <p><a href="#">こちら</a>より「メールによる各種ご案内」で「希望する」にチェックを入れてください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading10">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                    メールマガジンの受信設定をしたのに受信できません｡
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                <div class="panel-body">
                                    <p>迷惑メール対策や、アドレス・件名等による自動振分けを行っている場合はそちらの設定をご確認ください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading11">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                    メールマガジンの解除を行いたいのですが？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                <div class="panel-body">
                                    <p><a href="#">こちら</a>より「メールによる各種ご案内」で「希望しない」にチェックを入れてください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading12">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                    ブランドサイト会員を退会したい。また、退会するとどうなりますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                <div class="panel-body">
                                    <p><a href="inquiry.php">お問い合わせフォーム</a>より退会の旨をご連絡ください。退会後は登録されたお客様情報が抹消され、ログイン後にご利用できるすべてのコンテンツがご利用できなくなります。またWポイントは元に戻すことはできませんので予めご了承ください。</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <h5 id="link-3">Westポイント（Wポイント)について</h5>
                    <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading13">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                    Westポイント（Wポイント）とは何ですか？                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                <div class="panel-body">
                                    <p>「ウエストブランドサイト」では、ログインやニュース閲覧、ゲーム等各種コンテンツをお楽しみいただくと貯まるウエストブランドサイト内専用のポイントです。貯めたWポイントは賞品プレゼント等のキャンペーンへの応募などに使用できます。</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading14">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                    登録した自分のメールアドレスや住所の変更方法を教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                <div class="panel-body">
                                    <p>携帯専用サイトをご用意しております。ブランド紹介やニュースなど一部のコンテンツを閲覧いただくことができます。Westブランドサイトの全てのコンテンツをご覧いただくためには、PCやスマートフォン、タブレットでアクセスいただけますようお願いいたします。Westサイト内で利用できるWポイントは、携帯専用サイトではご利用いただけませんこと予めご了承ください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading15">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                    Wポイントに有効期限はありますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                <div class="panel-body">
                                    <p>最終ログインから1年間有効です。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading16">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                    Wポイントを誰かに譲ることはできますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                <div class="panel-body">
                                    <p>他の方にWポイントを譲渡することはできません。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading17">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                    貯めたWポイントはどこで確認できますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                <div class="panel-body">
                                    <p><a href="#">こちら</a>からご確認いただけます。</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5 id="link-4">キャンペーンについて</h5>
                    <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading19">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion4" href="#collapse19" aria-expanded="true" aria-controls="collapse19">
                                    たばこ「West（ウエスト）」が展開しているキャンペーンについて教えてほしい 
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                <div class="panel-body">
                                <p>たばこ「West（ウエスト）」取扱店舗で展開中のコーヒーキット、ライター付きキットのキャンペーンなどについては、<a href="#">こちら</a>よりご確認ください。ブランドサイト内で展開中の各種キャンペーンについては、<a href="#">キャンペーンページ</a>をご参照ください。</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading20">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse20" aria-expanded="false" aria-controls="collapse20">
                                    キャンペーンに応募したが連絡がない
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse20" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                <div class="panel-body">
                                    <p>キャンペーンへの応募については、キャンペーン事務局にて確認させていただきます。お手数ではございますが、こちらの<a href="#">フォーム</a>よりお問い合わせください。</p>
                                </div>
                            </div>
                        </div>
                    </div>



                    <h5 id="link-5">「たばこ」Westについて</h5>
                    <div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading25">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion5" href="#collapse25" aria-expanded="true" aria-controls="collapse25">
                                    たばこ「West（ウエスト）」はどのような銘柄がありますか？                                    
                                    </a>                                
                                </h4>
                            </div>
                            <div id="collapse25" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading25">
                                <div class="panel-body">
                                    <p>ウエスト・レッド(タール10mg/ニコチン0.7mg)
                                    <br>ウエスト・ディープブルー(タール8mg/ニコチン0.6mg)
                                    <br>ウエスト・ブルー(タール6mg/ニコチン0.5mg)
                                    <br>ウエスト・シルバー(タール3mg/ニコチン0.3mg)
                                    <br>ウエスト・ホワイト(タール1mg/ニコチン0.1mg)
                                    <br>ウエスト・ホワイト100(タール1mg/ニコチン0.1mg)
                                    <br>ウエスト・メンソール(タール6mg/ニコチン0.5mg)
                                    <br>ウエスト・メンソールホワイト100(タール1mg/ニコチン0.1mg)
                                    <br>ウエスト・アイスフレッシュエイト(タール8mg/ニコチン0.6mg)
                                    <br>ウエスト・アイスフレッシュワン100(タール1mg/ニコチン0.1mg)
                                    <br>ウエスト・デュエット(タール6mg/ニコチン0.6mg)
                                    <br>以上11銘柄、各380円</p>
                                    <p>詳しくは<a href="#">「ウエストについて」のページ</a>でご案内しております。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading26">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse26" aria-expanded="false" aria-controls="collapse26">
                                    御社から直接買えますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse26" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading26">
                                <div class="panel-body">
                                    <p>弊社から直接ご購入いただくことはできませんので、最寄のコンビニエンスストア、たばこ店、ドラッグストアなどのWest取扱店よりご購入いただけますようお願いいたします。最寄の取扱店が分からない場合は、WestHunt（取扱店検索）よりご確認いただくか、フォームよりお問い合わせください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading27">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse27" aria-expanded="false" aria-controls="collapse27">
                                    たばこ「West（ウエスト）」はどこで購入できますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse27" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading27">
                                <div class="panel-body">
                                    <p>たばこ「West（ウエスト）」は、全国のコンビニエンスストア、たばこ店、ドラッグストアなどでご購入いただけます。</p>
                                    <p>詳しくは<a href="#">こちら</a>よりお近くの販売店を検索できます。</p>
                                    <p>お近くの店舗でお取り扱いがない場合は、<a href="#">お問い合わせフォーム</a>よりお問い合わせください。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading28">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse28" aria-expanded="false" aria-controls="collapse28">
                                    たばこ「West（ウエスト）」はどこの会社が販売していますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse28" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading28">
                                <div class="panel-body">
                                    <p>たばこ「West（ウエスト）」は、イギリスのインペリアル・タバコ・グループの子会社であるインペリアル・タバコ・ジャパン株式会社が日本国内で販売しています。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading29">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse29" aria-expanded="false" aria-controls="collapse29">
                                    インペリアル・タバコ社について教えてください
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse29" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading29">
                                <div class="panel-body">
                                    <p>インペリアル・タバコは1901年に設立された国際的なタバコメーカーです。イギリスに本社を置き、商品はヨーロッパを中心に世界70カ国以上で販売されています。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading30">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse30" aria-expanded="false" aria-controls="collapse30">
                                    たばこ「West（ウエスト）」はどこの国で誕生したタバコですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse30" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading30">
                                <div class="panel-body">
                                    <p>1981年にドイツで誕生して、今では世界中で展開しているたばこブランドです。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading31">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse31" aria-expanded="false" aria-controls="collapse31">
                                    たばこ「West（ウエスト）」はいつから発売していますか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse31" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading31">
                                <div class="panel-body">
                                    <p>日本では、2011年1月から発売しております。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading32">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse32" aria-expanded="false" aria-controls="collapse32">
                                    たばこ「West（ウエスト）」は他のブランドと比較してなぜ安いのですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse32" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading32">
                                <div class="panel-body">
                                    <p>世界70カ国で展開するスケールメリットを生かし、世界中から吟味した素材を効率的に調達すること、生産工程をよりプロセス化すること、パッケージをシンプルにすることで、結果的に高い品質を保ちながら低価格がご提供することができ、値段以上の価値をお客様にご提供しております。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading33">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse33" aria-expanded="false" aria-controls="collapse33">
                                    たばこ「West（ウエスト）」の特徴は？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse33" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading33">
                                <div class="panel-body">
                                    <p>シンプルなつくり、本物の味わい、高品質と低価格の両立の3点です。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading34">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse34" aria-expanded="false" aria-controls="collapse34">
                                    たばこ「West（ウエスト）」はどんな味ですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse34" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading34">
                                <div class="panel-body">
                                    <p>日本人の好みに合うスムースで吸いやすい味わいになっています。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading35">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse35" aria-expanded="false" aria-controls="collapse35">
                                    たばこ「West（ウエスト）」は売れているのですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse35" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading35">
                                <div class="panel-body">
                                    <p>スムースな味わいにご好評をいただいていることに加え、380円という価格も皆様には喜ばれています。全国のコンビニエンスストアでの取り扱いも増え、順調に売れ行きが伸びております。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading36">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse36" aria-expanded="false" aria-controls="collapse36">
                                    なぜ「Wes（ウエスト）」という名前なのですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse36" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading36">
                                <div class="panel-body">
                                    <p>たばこ「West（ウエスト）」は「イギリス（ヨーロッパ）で作られるアメリカンブレンドのたばこ」ですが、ヨーロッパから見てアメリカは西側にあるため、「西側のタバコ」という意味をこめて「West（ウエスト）」と名づけられました。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading37">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse37" aria-expanded="false" aria-controls="collapse37">
                                    たばこ「West（ウエスト）」に1ミリのロングとショ－トがあるが、タールとニコチンの差はあるのでしょうか。タバコのサイズが違うので体に吸収されるタールとニコチンに差があるのですか？
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse37" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading37">
                                <div class="panel-body">
                                    <p>基本的にはタバコのタールとニコチン量に変わりは無く、タバコが長い分味が少しだけマイルドになります。</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading38">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#collapse38" aria-expanded="false" aria-controls="collapse38">
                                    たばこ「West（ウエスト）」以外の製品について教えてください。
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse38" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading38">
                                <div class="panel-body">
                                    <p>基本的にはタバコのタールとニコチン量に変わりは無く、タバコが長い分味が少しだけマイルドになります。</p>
                                </div>
                            </div>
                        </div>
                    </div>






                </div>
            </div>

        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });
        });
        $('.panel-heading .panel-title a').click(function() {
            //$(this).toggleClass('active');
            $('.panel-heading .panel-title a').removeClass('active');
            var newValue = $(this).attr('href').replace('#', '');
            var current = $(this);
            //current.addClass('active');
            if (!$('#' + newValue).hasClass('in')) {
                current.addClass('active');
            } else {
                current.removeClass('active');
            }

        });

        $('.faq-links li a').click(function() {
            var target = $(this).attr('rel');
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#" + target).offset().top - 330
            }, 1000);

        });

    });

</script>
</body>

</html>
