<?php $tab = explode('.php', basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING'])); ?>
<?php if ($tab != 'index2') { ?>
    <div class="loading-indicator">
        <div class="lt"></div>
        <div class="rt"></div>
        <div class="loader"></div>
    </div>
<?php } ?>

<div class="modal fade" id="modal-comics" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <div class="comics-tile">
                        <a href="comics-detail.php">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img_modal_comic01.jpg" height="114" width="315" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                 <i class="fa fa-angle-right"></i>
                                <p class="value-listing-text-heading">東さんの叫び</p>
                                <p>田中光</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="comics-tile">
                        <a href="comics-detail.php">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img_modal_comic02.jpg" height="114" width="315" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                 <i class="fa fa-angle-right"></i>
                                <p class="value-listing-text-heading">喫煙所物語</p>
                                <p>アーノルズはせがわ</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-value" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>価値組生活</h4>
                <p>価値あるタバコを380円で提供するWest。<br>普段のタバコをWestへ乗り換えて、プチ贅沢してみませんか？</p>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <div class="comics-tile">
                        <div class="comics-listing">
                            <img src="assets/img/images/modal-images/img_modal_kachigumi01.jpg" height="102" width="315" alt="img">                                            
                        </div>
                        <div class="value-listing-text">
                            <a href="value.php"> <i class="fa fa-angle-right"></i></a>
                            <p class="value-listing-text-heading">価値組シミュレーター</p>
                            <p>あなたが手にするプチ贅沢を<br>シミュレーションしてみましょう！</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="comics-tile">
                        <div class="comics-listing">
                            <img src="assets/img/images/modal-images/img_modal_kachigumi02.jpg" height="102" width="315" alt="img">                                            
                        </div>                                       
                        <div class="value-listing-text brown">
                            <a href="1500yen.php"> <i class="fa fa-angle-right"></i></a>
                            <p class="value-listing-text-heading">勝ち組コラム</p>
                            <p>気鋭のクリエーター「ゴミ山地球」が挑む！<br>Westで節約して「○○やってみた！」</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
