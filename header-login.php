
<!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

        <!--[if lt IE 9]><meta http-equiv="refresh" content="0;URL='/upgrade'"><![endif]-->

        <?php $tab = explode('.php', basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING'])); ?>
        <title><?php echo ucfirst($tab[0]) ?></title>
        <link rel="shortcut icon" href="assets/img/images/favicon.ico">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/odometer-theme-default.css">
        <link href="assets/fonts/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/responsive.css">         
        <link rel="stylesheet" type="text/css" href="assets/css/slider/slick-slider.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/slider/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/slider/slider.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/nanoscroller.css"/>
        <link rel="stylesheet" href="assets/css/scroll.css">
        <link rel="stylesheet" href="assets/css/bg.php">
        <link rel="stylesheet" href="assets/css/login.css">
        <link rel="stylesheet" href="assets/css/addition_style.css">

        <script src="assets/js/jquery.js"></script>

    </head>

    <body>  <!-- class="initing loading page-landing" data-section="landing"-->      
        <div id="content-inner">
            <div class="header-fixed-top">
                <div class="container-fluid">           
                    <div class="header-page-login">
                        <div class="container-fluid">
                            <div class="header-content">
                                <a class="navbar-brand" href="index.php?set=1"><img src="assets/img/images/grand-logo.png" alt="logo"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>