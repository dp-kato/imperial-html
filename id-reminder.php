<?php include('header-login.php');?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>IDを確認される方</h3>
                </div>
                <div class="panel-content grey-border">
                    <div class="col-md-12">
                        <p class="margin-40">お客様が登録された情報を以下に入力してください。IDの確認結果は、連絡用メールアドレスにお伝えいたします。</p>

                        <p>お客様の個人情報は、弊社にて厳重に管理を行います。</p>
                        <p>また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。</p>
                    </div>
                    <div class="col-md-12">
                        <div class="below-top-list no-border">
                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">お名前<strong>（必須）</strong></th>
                                        <td>
                                            <div class="inner-table-item">
                                                <p class="at-the-rate">姓</p>
                                                <input class="form-control" type="text">
                                                <p class="at-the-rate">名</p>
                                                <input class="form-control" type="text">                                      
                                            </div>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <th scope="row">生年月日 <strong>（必須）</strong><br>（半角数字）</th>
                                        <td>
                                            <div class="inner-table-item">
                                                <input class="form-control" type="text">
                                                <p>(例:1980年3月14日⇒19800314) </p>
                                            </div>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th scope="row">連絡用メールアドレス<strong>（必須）</strong></th>
                                        <td>
                                            <div class="inner-table-item">
                                                <p class="heading-panel">連絡用のメールアドレスをご入力ください。</p>
                                                <input class="form-control" type="text">
                                                <p>※ご記入いただいた内容に対し連絡をさせていただく場合に使用させていただきます。 </p>
                                                <hr>
                                                <p>確認のため、もう一度入力してください。</p>
                                                <input class="form-control" type="text">

                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="change-password-span margin-40">
                                <a class="white-proceed btn-refresh">送信する</a>
                            </div>
                            <div class="col-md-12">
                                <p>※登録時の上記項目の内容をお忘れの方は、お手数ですが、新たにIDのお申込みをお願いいたします。</p>
                                <p>※本サイトにてご提供いただくお客様の個人情報は、弊社にて厳重に管理を行います。</p>
                                <p>また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示、提供いたしません。</p>
                                <p class="margin-40">※本サイトにてご提供いただくお客様の個人情報は、IDの問合せ確認のために使用させていただきます。</p>

                                <p class="red">【画面操作にあたっての注意事項】</p>
                                <p class="red">ブラウザの戻るボタンは使えませんのでご注意ください。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
