
<!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

        <!--[if lt IE 9]><meta http-equiv="refresh" content="0;URL='/upgrade'"><![endif]-->

        <title>West</title>
        <link rel="shortcut icon" href="assets/img/images/favicon.ico">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/odometer-theme-default.css">
        <link href="assets/fonts/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/responsive.css"> 
        <link rel="stylesheet" href="assets/css/bg.php"> 

        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/odometer.min.js"></script>
        <script>
            $(document).ready(function() {

                $('body').bind('mousewheel', function(e) {
                    if (e.originalEvent.wheelDelta / 120 > 0) {

                    } else {
                        /* $("#content").addClass("animated fadeInUp").removeClass("disappear");
                         setTimeout(function() {
                         $("#content").removeClass("animated fadeInUp");
                         $('body').unbind('mousewheel');
                         $('.odometer').html('219');
                         }, 1000);
                         $(".content").addClass("animated fadeOut");
                         setTimeout(function() {
                         $("body").addClass("page-landing-after");
                         }, 1000);*/
                        window.location.href = 'index2.php';
                    }

                });
                $("#btnDropdown").click(function() {
                    /* $("#content").addClass("animated fadeInUp").removeClass("disappear");
                     setTimeout(function() {
                     $("#content").removeClass("animated fadeInUp");
                     $('body').unbind('mousewheel');
                     $('.odometer').html('219');
                     }, 1000);*/
                    window.location.href = 'index2.php';
                    //$(".content").addClass("animated fadeOut");
                    /*setTimeout(function() {
                     $("body").addClass("page-landing-after");
                     }, 1000);*/
                });

                /*var d = new Date(); // for now
                var hours = d.getHours(); // => 9
                var minutes = d.getMinutes(); // =>  30
                var seconds = d.getSeconds(); // => 51
                var img = Math.floor(Math.random() * 7) + 1 + '.jpg';
                if (hours >= 7 && hours <= 18) {
                    $('.bg').css("background-image", "url(assets/img/landing/backgrounds/morning/" + img + ")");
                } else if (hours >= 19) {
                    $('.bg').css("background-image", "url(assets/img/landing/backgrounds/night/" + img + ")");
                } else {
                    $('.bg').css("background-image", "url(assets/img/landing/backgrounds/large/1.jpg)");
                }*/


            });
        </script>

    <body class="initing loading page-landing" data-section="landing">
        <div class="content">
            <div id="scroll-story">
                <section class="move">
                    <div class="bg"></div>
                    <div class="col-md-12">
                        <div class="bg-content">    
                            <div class="bg-content-items">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="bg-logo">
                                            <img src="assets/img/images/logo-west.png" alt="img">
                                        </div>
                                        <div class="bg-content-text">
                                            <img src="assets/img/images/text-wow-page.png" alt="img">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="bg-img-packet">
                                            <img src="assets/img/images/pack-blue.png" alt="img">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>


                    <!--<div class="cig-pack" style="position:absolute;top:100px; left:250px;width:1000px">

                      <div class="bg-content-text">
                          <div style="display:inline-block;width:250px">
                          <img src="assets/img/images/logo-west.png" alt="img">
                          <img src="assets/img/images/text-wow-page.png" alt="img">
                          </div>
                      </div>
                      <div class="bg-img-packet"style="margin-left:300px;">
                          <img src="assets/img/images/pack-blue.png" alt="img">
                      </div>
                  </div>-->










                </section>                  
                <div id="btnDropdown" class="down-arrow">
                    <div class="a"></div>
                    <!-- <div class="b"></div>-->
                </div>
            </div>

        </div>
        <!--<div id="content" class="<?php //echo ($_GET['set'] != 1) ? 'disappear' : '' ?>">

        <?php //include('navigation-wow.php'); ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="page-content-wow row">
                        <div class="col-xs-9">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-campaign.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline">
                                                <img src="assets/img/images/icon-large/campaign.png" alt="img">
                                            </div>
                                        </div>
                                        <div class="page-item-campaign">
                                            <h3>キャンペーン</h3>
                                            <p>貯まったポイントで応募しよう。</p>
                                            <a class="white-proceed">もっと見る</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-game.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline">
                                                <img src="assets/img/images/icon-large/game.png" alt="img">
                                            </div>
                                        </div>
                                        <div class="page-item-game">
                                            <h3>ゲーム</h3>
                                            <p>1日1回の運試し！ ポイントを増やすならやっぱりこれっ！</p>
                                            <a class="white-proceed">もっと見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-comics.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline">
                                                <img src="assets/img/images/icon-large/comics.png" alt="img">
                                            </div>
                                        </div>
                                        <div class="page-item-comics">
                                            <h3>コミック</h3>
                                            <p>シュールな笑いを提供する 田中光氏連載スタート！</p>
                                            <a data-toggle="modal" data-target="#modal-comics" href="#" class="white-proceed">もっと見る</a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-interview.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline">
                                                <img src="assets/img/images/icon-large/interview.png" alt="img">
                                            </div>
                                        </div>
                                        <div class="page-item-interview">
                                            <h3>インタビュー</h3>
                                            <p>○○氏に聞く。価値あるもの を突き詰めるとやっぱりSimple Life!?</p>
                                            <a class="red-proceed">もっと見る</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-value.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline">
                                                <img src="assets/img/images/icon-large/value.png" alt="img">                                                
                                            </div>
                                        </div>
                                        <div class="page-item-value">
                                            <h3>価値組生活</h3>
                                            <p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
                                            <a data-toggle="modal" data-target="#modal-value" href="#" class="white-proceed">もっと見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">                            
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-questionare.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline top-questionare">
                                                <div class="page-item-questionare">
                                                    <img src="assets/img/images/icon-large/questionare.png" alt="img">
                                                    <p class="questionare-heading">アンケート</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="questionare-items">
                                            <div class="col-md-12">
                                                <p>普通だと思っていたけど、実は変わり者・・・？ 2ステップアンケートで新たな自分発見？</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <img src="assets/img/images/img-spiderman.jpg" alt="img">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <img src="assets/img/images/img-batman.jpg" alt="img">
                                            </div>
                                            <div class="questionare-sumbit">
                                                <a class="grey-bg" href="#">回答する</a>
                                                <hr>
                                                <a class="red-proceed">もっと見る</a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-west-brand.jpg" alt="img">
                                        <div class="page-item-description">
                                            <div class="img-inline top-brand">
                                                <img src="assets/img/images/icon-large/west-brand.png" alt="img">                                                
                                            </div>
                                            <div class="page-item-description-brand">
                                                <div class="west-brand-values">
                                                    <p>ウエストについて</p>
                                                    <a class="white-proceed" href="#">もっと見る</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-west-hunt.jpg" alt="img">
                                        <div class="page-item-description-west">
                                            <div class="west-hunt-values">
                                                <img src="assets/img/images/icon-large/west-hunt.png" alt="img">
                                                <h3>West Hunt</h3>
                                                <p>取扱店検索</p>
                                                <a class="red-proceed" href="#">もっと見る</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <ul class="dates-calender">
                                        <li class="calender-header">
                                            <p class="header-text-left">ニュース</p>
                                            <p class="header-text-right">もっと見る</p>
                                        </li>
                                        <li>
                                            <div class="calender-value">
                                                <div class="col-md-3">
                                                    <h3>12</h3>
                                                    <div class="calender-value-month">
                                                        <p>MAR</p>
                                                        <p>2015</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="calender-value-description">
                                                        <p>全国各地の居酒屋でサンプルの配布イベント開催中！ (期間： 4月10日まで)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="calender-value">
                                                <div class="col-md-3">
                                                    <h3>12</h3>
                                                    <div class="calender-value-month">
                                                        <p>MAR</p>
                                                        <p>2015</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="calender-value-description">
                                                        <p>全国各地の居酒屋でサンプルの配布イベント開催中！ (期間： 4月10日まで)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="calender-value">
                                                <div class="col-md-3">
                                                    <h3>12</h3>
                                                    <div class="calender-value-month">
                                                        <p>MAR</p>
                                                        <p>2015</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="calender-value-description">
                                                        <p>全国各地の居酒屋でサンプルの配布イベント開催中！ (期間： 4月10日まで)</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="page-item">
                                        <img src="assets/img/images/img-landmark.jpg" alt="img">
                                        <div class="page-item-description-landmark">
                                            <div class="landmark-values">
                                                <img src="assets/img/images/icon-large/landmark.png" alt="img">
                                                <h3>喫煙所マップ</h3>
                                                <a class="white-proceed">もっと見る</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <ul class="coins-reward">
                                <li><img src="assets/img/images/img-coins-reward.jpg" alt="img">
                                    <div class="reward-description">
                                        <div class="reward-description-item">
                                            <h3>216</h3>
                                            <p><b>獲得ポイント</b></p>
                                            <p>今すぐ参加できるキャンペーン</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="reward-campaign">
                                        <a class="btn-campaign">Apply</a>
                                        <h3>Campaign 1</h3>
                                        <p>Lorem ipsum dolor sit</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="reward-campaign">
                                        <a class="btn-campaign">Apply</a>
                                        <h3>Campaign 2</h3>
                                        <p>Lorem ipsum dolor sit</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="reward-campaign">
                                        <a class="btn-campaign">Apply</a>
                                        <h3>Campaign 3</h3>
                                        <p>Lorem ipsum dolor sit</p>
                                    </div>
                                </li>
                                <li>
                                    <a class="btn-campaign-submit">他のキャンペーンも見る<i class="fa fa-angle-right"></i></a>
                                </li>
                            </ul>
                            <div class="people-switch">
                                <a class="btn-campaign">友達を招待する</a>
                                <img src="assets/img/images/img-people-switch.png" alt="img">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="footer">
                        <ul class="footer-links">
                            <li><a>About us</a></li>
                            <li><a>News</a></li>
                            <li><a>FAQ</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="below-footer">
                        <ul class="footer-links">
                            <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                            <li><a>プライバシーポリシー</a></li>
                        </ul>
                        <p></p>
                    </div>
                    <div class="warning-footer">
                        <div class="warning-text">
                            <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                            <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                            <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->


        <?php include('footer.php'); ?>

        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/matchmedia.js"></script>
        <script src="assets/js/picturefill.js"></script>
        <script src="assets/js/require.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/bootstrap.js"></script>    


    </body>

</html>
