
<!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

    <!--[if lt IE 9]><meta http-equiv="refresh" content="0;URL='/upgrade'"><![endif]-->

    <title>West</title>
    <link rel="shortcut icon" href="assets/img/images/favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/odometer-theme-default.css">
    <link href="assets/fonts/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/responsive.css"> 

    <link rel="stylesheet" href="assets/css/addition_style.css">

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/odometer.min.js"></script>


<body style="background-color:#fff">
    <?php include('navigation.php'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="page-content-wow row animated fadeInUp">
                <div class="col-xs-9">
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="page-item">
                                <img src="assets/img/images/img-campaign.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline">
                                        <img src="assets/img/images/icon-large/campaign.png" alt="img">
                                    </div>
                                </div>
                                <div class="page-item-campaign">
                                    <h3>キャンペーン</h3>
                                    <p>貯まったWポイントで応募して <br>ステキなギフトをゲットしよう！</p>
                                    <a class="white-proceed">もっと見る</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-game.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline">
                                        <img src="assets/img/images/icon-large/game.png" alt="img">
                                    </div>
                                </div>
                                <div class="page-item-game">
                                    <h3>ゲーム</h3>
                                    <p>1日1回の運試し！<br> Wポイントを増やすならやっぱりこれっ！</p>
                                    <a class="white-proceed">もっと見る</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-comics.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline">
                                        <img src="assets/img/images/icon-large/comics.png" alt="img">
                                    </div>
                                </div>
                                <div class="page-item-comics">
                                    <h3>コミック</h3>
                                    <p>シュールな笑いを提供する 田中光氏連載スタート！</p>
                                    <a data-toggle="modal" data-target="#modal-comics" href="#" class="white-proceed">もっと見る</a>
                                </div>
                            </div>
                        </div> 
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-interview.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline">
                                        <img src="assets/img/images/icon-large/interview.png" alt="img">
                                    </div>
                                </div>
                                <div class="page-item-interview">
                                    <h3>インタビュー</h3>
                                    <p>○○氏に聞く。価値あるもの<br>を突き詰めるとやっぱりSimple Life!?</p>
                                    <a href="interview.php" class="red-proceed">もっと見る</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-value.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline">
                                        <img src="assets/img/images/icon-large/value.png" alt="img">                                                
                                    </div>
                                </div>
                                <div class="page-item-value">
                                    <h3>価値組生活</h3>
                                    <p>普段のタバコをWestへ乗り換えて、<br>プチ贅沢してみませんか？ </p>
                                    <a data-toggle="modal" data-target="#modal-value" href="#" class="white-proceed">もっと見る</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">                            
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-questionare.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline top-questionare">
                                        <div class="page-item-questionare">
                                            <img src="assets/img/images/icon-large/questionare.png" alt="img">
                                            <p class="questionare-heading">アンケート</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="questionare-items col-xs-12">
                                    <form action="questionnaire.php" method="POST">
                                    <div id="radioBox">
                                        <div class="col-md-12">
                                            <p>普通だと思っていたけど、実は変わり者・・・？<br>2ステップアンケートで新たな自分発見？</p>
                                        </div>
                                        
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <img src="assets/img/images/img-spiderman.jpg" alt="img" class="img-questionnaire-small" rel="percent-1">
                                            
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <img src="assets/img/images/img-batman.jpg" alt="img" class="img-questionnaire-small" rel="percent-2">
                                        </div>
                                    </div>
                                        <input type="hidden" name="activeVal" id="activeVal"/>
                                        <input type="hidden" name="inactiveVal" id="inactiveVal"/>
                                       
                                    <div class="questionare-sumbit">
                                        <!--<a class="grey-bg" href="javascript:void(0)" id="submitBtn">回答する</a>-->
                                        <input type="submit" class="grey-bg" value="回答する">
                                        <hr>
                                        <a class="red-proceed">もっと見る</a>
                                    </div>
                                    </form>
                                </div>


                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-west-brand.jpg" alt="img">
                                <div class="page-item-description">
                                    <div class="img-inline top-brand">
                                        <img src="assets/img/images/icon-large/west-brand.png" alt="img">                                                
                                    </div>
                                    <div class="page-item-description-brand">
                                        <div class="west-brand-values">
                                            <p>ウエストについて</p>
                                            <a class="white-proceed" href="#">もっと見る</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-west-hunt.jpg" alt="img">
                                <div class="page-item-description-west">
                                    <div class="west-hunt-values">
                                        <img src="assets/img/images/icon-large/west-hunt.png" alt="img">
                                        <h3>West Hunt</h3>
                                        <p>取扱店検索</p>
                                        <a class="red-proceed" href="#">もっと見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <ul class="dates-calender">
                                <li class="calender-header">
                                    <p class="header-text-left">ニュース</p>
                                    <p class="header-text-right">もっと見る</p>
                                </li>
                                <li>
                                    <div class="calender-value">
                                        <div class="col-xs-3">
                                            <h3>12</h3>
                                            <div class="calender-value-month">
                                                <p>MAR</p>
                                                <p>2015</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="calender-value-description">
                                                <p>全国各地の居酒屋でサンプルの配布イベント開催中！<br> (期間： 4月10日まで)</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="calender-value">
                                        <div class="col-xs-3">
                                            <h3>07</h3>
                                            <div class="calender-value-month">
                                                <p>MAR</p>
                                                <p>2015</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="calender-value-description">
                                                <p>セブン-イレブンで「ウエスト缶コーヒー付きキャンペーン」<br>を1月26日～2月8日まで展開中！</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="calender-value">
                                        <div class="col-xs-3">
                                            <h3>03</h3>
                                            <div class="calender-value-month">
                                                <p>MAR</p>
                                                <p>2015</p>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="calender-value-description">
                                                <p>セブンイレブンでキャンペーンを展開中West1パックでボトル<br>缶コーヒーをゲット!!</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-4">
                            <div class="page-item">
                                <img src="assets/img/images/img-landmark.jpg" alt="img">
                                <div class="page-item-description-landmark">
                                    <div class="landmark-values">
                                        <img src="assets/img/images/icon-large/landmark.png" alt="img">
                                        <h3>喫煙所マップ</h3>
                                        <a class="white-proceed">もっと見る</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <ul class="coins-reward">
                        <li><img src="assets/img/images/img-coins-reward.jpg" alt="img">
                            <div class="reward-description">
                                <div class="reward-description-item">
                                    <h3>216</h3>
                                    <p><b>獲得Wポイント</b></p>
                                    <p>今すぐ参加できるキャンペーン</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="reward-campaign">

                                <h3>キャンペーン</h3>
                                <a class="btn-campaign">参加</a>
                                <p>ログインポイントだけで 応募で<br>きる、あなた...</p>
                            </div>
                        </li>
                        <li>
                            <div class="reward-campaign">                                
                                <h3>オープンキャンペーン</h3>
                                <a class="btn-campaign">参加</a>
                                <p>豪華リニューアルオープン キャンペーン。ギフトは松竹梅</p>
                            </div>
                        </li>
                        <li>
                            <div class="reward-campaign">

                                <h3>Campaign 3</h3>
                                <a class="btn-campaign">参加</a>
                                <p>Lorem ipsum dolor sit</p>
                            </div>
                        </li>
                        <li>
                            <a class="btn-campaign-submit">他のキャンペーンも見る<i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                    <div class="people-switch">
                        <a class="btn-campaign">友達に紹介する</a>
                        <img src="assets/img/images/img-people-switch.png" alt="img">
                    </div>
                </div>






            </div>                
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div id="footer">
                <ul class="footer-links">
                    <li><a>About us</a></li>
                    <li><a>News</a></li>
                    <li><a>FAQ</a></li>
                    <li><a>Contact Us</a></li>
                </ul>
            </div>
            <div id="below-footer">
                <ul class="footer-links">
                    <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                    <li><a>プライバシーポリシー</a></li>
                </ul>
                <p></p>
            </div>
            <div class="warning-footer">
                <div class="warning-text">
                    <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                    <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                    <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                </div>
            </div>
        </div>
    </div>


    <?php include('footer.php'); ?>    


    <script src="assets/js/bootstrap.js"></script>    
    <script src="assets/js/Chart.js"></script>
    <script type="text/javascript">
        $('.img-questionnaire-small').click(function(){
            $('.img-questionnaire-small').removeClass('active').addClass('inactive');
            $(this).removeClass('inactive').addClass('active');
            console.log($('.img-questionnaire-small').val());
            $('#activeVal').val($(this).attr('rel'));
            $('#inactiveVal').val($('.img-questionnaire-small.inactive').attr('rel'));
        });
    </script>


</body>

</html>
