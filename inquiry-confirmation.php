<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/inquiry-grey.png" alt="img">
                    <h3>お問い合わせ</h3>
                </div>    
            </div> 
            <div class="col-md-12">
                <div class="below-top-list no-border inquiry-confirmation">
                    <p class="margin-30">入力内容をご確認の上、「送信する」ボタンを押してください。</p>
                    <table class="table">                                    
                        <tbody>
                            <tr>
                                <th scope="row">お問い合わせ内容 <strong>（必須）</strong></th>
                                <td><p>販売店について</p></td>
                            </tr>
                            <tr>
                                <th scope="row">お名前<strong>（必須）</strong></th>
                                <td><p>上巣　太郎 </p></td>
                            </tr>                            
                            <tr>
                                <th scope="row">メールアドレス</th>
                                <td><p> West@gmail.com</p></td>
                            </tr>
                            <tr>
                                <th scope="row">お電話番号 <strong>（必須）</strong></th>
                                <td><p>00-0000-0000</p></td>
                            </tr>                            
                            <tr>
                                <th scope="row">お問い合わせ内容<strong>（必須）</strong></th>
                                <td> 
                                    <p>お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わせ
                                        内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。</p>
                                    <p>お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。</p>
                                    <p>お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わ
                                        せ内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。</p>
                                    <p>お問い合わせ内容が入ります。お問い合わせ内容が入ります。</p>
                                    <p>お問い合わせ内容が入ります。お問い合わせ内容が入ります。お問い合わせ内容が入ります。</p>
                                    <p>お問い合わせ内容が入ります。お問い合わせ内容が入ります。</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="change-password-span">
                        <a class="white-proceed btn-refresh grey">戻る</a>
                        <a class="white-proceed btn-refresh">送信する</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
