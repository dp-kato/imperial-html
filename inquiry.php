<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/inquiry-grey.png" alt="img">
                    <h3>お問い合わせ</h3>
                </div>    
            </div> 
            <div class="col-md-12">
                <div class="below-top-list inquiry-confirmation margin-40 no-border">
                    <p class="margin-30">FAQをご覧になっても問題が解決しない場合には下記よりお問い合わせください。</p>
                    <table class="table">                                    
                        <tbody>
                            <tr>
                                <th scope="row">お問い合わせ内容 <strong>（必須）</strong></th>
                                <td><select class="form-control"> </select></td>
                            </tr>

                            <tr>
                                <th scope="row">お名前<strong>（必須）</strong></th>
                                <td>
                                    <div class="inner-table-item">
                                        <p class="at-the-rate">姓</p>
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">名</p>
                                        <input class="form-control" type="text">                                      
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">メールアドレス</th>
                                <td>
                                    <div class="inner-table-item">
                                        <input class="form-control" type="text">
                                        <p class="at-the-rate">@</p>
                                        <input class="form-control" type="text">
                                        <p class="red">※回答を受け取るメールアドレスを<br>ご記入ください。</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">お電話番号</th>
                                <td>
                                    <div class="inner-table-span">
                                        <input class="form-control" type="text">
                                        <p class="red">※メールでご連絡ができない場合に 　<br>必要となります。</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">お問い合わせ内容<strong>（必須）</strong></th>
                                <td> 
                                    <textarea class="form-control"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="col-md-12">
                        <div class="change-password-span">                       
                            <a class="white-proceed btn-refresh">送信する</a>
                        </div>
                    </div>
                </div>
               <!-- <div class="col-xs-12 margin-40">
                    <div class="col-xs-6">
                        <div class="red-border inquiry-below-buttons">
                            <img src="assets/img/images/inquiry/user.png" alt="img">
                            <p>登録情報の変更についてはこちら</p>
                            <a ><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="red-border inquiry-below-buttons">
                            <img src="assets/img/images/inquiry/gift.png" alt="img">
                            <p>キャンペーンについてはこちら</p>
                            <a ><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>-->
                <div class="col-md-12">
                    <div class="change-password-span">
                        <p>お電話でのお問い合わせ　0570-064-039   受付時間：月-金曜日（土日祝日をのぞく） 10:00-17:00</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
