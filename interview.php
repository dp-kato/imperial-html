<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row interviewWrap">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/interview-grey.png" alt="img">
                    <h3>インタビュー<small>株式会社エートゥジェイ　代表取締役社長　飯澤 満育</small></h3>
                </div>
                <p class="inner-page-header-date">2015.05.01</p>
            </div> 
            <div class="col-md-12">
                <div class="row">
                    <div class="interview-content">
                        <div class="col-xs-9">
                            <div class="interview-banner">
                                <img class="banner-man" src="assets/img/images/interview/img_man_interview01.png " height="296" width="284" alt="img">
                            </div>
                            <div class="interview-banner-text">
                                <img src="assets/img/images/interview/icon-brand-interview.png" alt="img">
                                <h3>Westのシンプルなつくりと<br>ベンチャー経営の成功の極意との共通点</h3>

                            </div>
                            <div class="interview-outline">
                                <p>インターネットの販促支援で高い実績をあげている、今注目のベンチャー企業エートゥジェイ。その創業社長である飯澤氏にお話を伺いました。</p>
                            </div>
                            <div class="question-answer">
                                <p class="question"><b>Q :</b>エートゥジェイはどのようなサービスを展開しているのでしょうか？</p>
                                <div class="center margin-20">
                                    <img src="assets/img/images/interview/img_interview01.png" height="300" width="560" alt="img">
                                </div>
                                <p class="answer">飯澤氏：エートゥジェイでは「ショッピングサイトの開発・運営事業」、「マンガを使った販促支援事業」、「小売店に対する販促支援事業」の3つをサービス展開しています。</p>
                                <p class="answer">「ショッピングサイトの開発・運営事業」では、ECサイトの運営を丸ごと請け負い、成果報酬型の運営代行サービスを展開。「マンガを使った販促支援事業」では、メールマガジンの開封率が低くて困っている企業様や、商品やサービスの内容をうまくユーザーに伝えられない企業様に、マンガコンテンツを提供し課題解決のお手伝いをしています。そして3番目にあげた「小売店に対する販促支援事業」では、さまざまなチャネルのデータを収集、蓄積し、予測分析をして消費行動に刺激を与える支援を行っています。</p>
                            </div>

                            <div id="additional-QuesAns" style="display:none">
                                <div class="question-answer">
                                    <p class="question"><b>Q :</b>いくつもの事業を成功させている飯澤様にとって、成功の極意は何でしょうか？</p>
                                    <div class="center margin-20">
                                        <img src="assets/img/images/interview/img_interview02.png" height="300" width="560" alt="img">
                                    </div>
                                    <p class="answer">飯澤氏：極意というと大変恐縮ですが……。インタビューを受ける前に「West」は、一切の無駄を省いたシンプルなタバコづくりをしていると聞いたんですよね。偶然ですが僕の経営スタイルに非常に似ています。</p>
                                    <p class="answer">先ほどお伝えしましたが、エートゥジェイでは複数の事業展開をしています。コストをできる限り低く抑え、無駄を省かないと複数の事業展開はできません。鍵は“透明性のあるプロセス”と“シンプル”、この2つです。“透明性のあるプロセス”で無駄なものをそぎ落としていく。そして“シンプル”にすることで必要なものが見えてくる。そこを経営者としてジャッジしています。そういう意味では「West」と共通するところがありますね。</p>
                                </div>
                                <div class="question-answer">
                                    <p class="question"><b>Q :</b>今年で8期目になるというエートゥジェイですが、飯澤社長の経営人生のなかで、嬉しかったこと、<br>&emsp;&nbsp;&nbsp;辛かったことを教えてください</p>
                                    <div class="center margin-20">
                                        <img src="assets/img/images/interview/img_interview03.png" height="300" width="560" alt="img">
                                    </div>
                                    <p class="answer">飯澤氏：エートゥジェイというチームとして、志などさまざまなことを共有できる喜びですかね。僕が24歳のとき、2007年にエートゥジェイを立ち上げました。一、二年目は赤字で銀行は事業資金を貸してくれなくて……給料は5万円ほど。当時のオフィス、横浜のマンションの一室で従業員たちと寝起きを共にして、それこそ死にもの狂いで働きました。</p>
                                    <p class="answer">お腹が空いてもご飯を買うお金なんてありません。僕の実家は横浜にあるんですが、彼らを連れて行き、みんなでご飯を食べました。三年目からは黒字になりましたが、それまでの二年間は大変で辛い時期でもありました。ただ、チームとしてさまざまなことを共有できたのは最高の喜びです。今でも忘れられない思い出です。</p>

                                </div>
                                <div class="question-answer">
                                    <p class="question"><b>Q :</b>ところで飯澤社長は愛煙家と聞いていますが、タバコを吸うときのこだわりってありますか</p>
                                    <p class="answer">飯澤氏：仕事に熱中しているときは、タバコをあまり吸わないですね。一段落して、無になりたいなと思ったときに、喫煙所に行って一服。そこでは、ただひたすらボーっとしているだけかな。だから僕のタバコを吸うモチベーションは、ストレスなどとは無縁なんです。リラックスしたいときにタバコを楽しんでいます。他には、自分にとって新鮮で未知な環境や場所に行くと、一服したくなります！</p>
                                </div>
                                <div class="question-answer">
                                    <p class="question"><b>Q :</b>最後に「West」を吸ってみて感想を一言</p>
                                    <div class="center margin-20">
                                        <img src="assets/img/images/interview/img_interview04.png" height="300" width="560" alt="img">
                                    </div>
                                    <p class="answer">飯澤氏：いつもは日本の銘柄のタバコを吸っているんですけど「West」は癖がありませんね。外国の銘柄のタバコをいろいろ吸ったことはありますが、独特な癖があって……。「West」は癖がないから違和感なく普通に吸えるなと驚きました。</p>
                                    <p class="answer">経営と一緒で、癖がなくシンプルだからこそ深い味わい(楽しみ)があるタバコ」ですね。</p>
                                </div>
                                <div class="interview-profile">
                                    <div class="prof-image">
                                        <img src="assets/img/images/interview/img_interview_prof.png" height="200" width="200" alt="img">
                                    </div>
                                    <div class="text-bloc">
                                        <p class="prof-title">株式会社エートゥジェイ<br>代表取締役社長</p>
                                        <p class="prof-name">飯澤 満育</p>
                                        <p class="prof-detail">
                                            明治大学卒業後、世界各国で価格比較メディア、クーポンメディアなどの運営を行う MeziMedia 社（米国）へ入社。日本支社のスタートアップを行い、インターネットマーケティング、大手クライアント開拓に従事。
                                            <br>同社が、米国インターネットメディア大手 ValueClick 社 へ売却されたことに伴い、退社。
                                            <br>当時、欧米を中心に広く展開されていたレシート裏広告に着目。
                                            <br>日本での事業展開を試み、2007年3月、株式会社エートゥジェイを創業し、代表取締役に就任。

                                        </p>
                                    </div>
                                </div>
                                <div class="center">
                                    <a class="btn-previous"><i class="fa fa-caret-left"></i> 前の記事へ</a>
                                    <a class="btn-next">次の記事へ <i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                            <div class="center">
                                <a id="showAdditionalQuesAns" class="btn-read-more">もっと読む</a>
                            </div>







                        </div>
                        <div class="col-xs-3">
                            <ul class="other-interviews">
                                <li class="other-interviews-heading">
                                    インタビュー一覧
                                </li>
                                <li>
                                    <div class="interview-listing">                                                    
                                        <div class="interview-listing-text">
                                            <img src="assets/img/images/interview/img1.jpg" alt="img">
                                            <p class="text-small no-margin">2015.05.01</p>
                                            <p class="text-small head no-margin">たばこを吸うメリット 1位はやっぱり○○</p>
                                            <p class="text-small no-margin red">小栗旬</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="interview-listing">                                                    
                                        <div class="interview-listing-text">
                                            <img src="assets/img/images/interview/img2.jpg" alt="img">
                                            <p class="text-small no-margin">2015.05.01</p>
                                            <p class="text-small head no-margin">たばこを吸うメリット 1位はやっぱり○○</p>
                                            <p class="text-small no-margin red">チュートリアル徳井</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="interview-listing">                                                    
                                        <div class="interview-listing-text">
                                            <img src="assets/img/images/interview/img3.jpg" alt="img">
                                            <p class="text-small no-margin">2015.05.01</p>
                                            <p class="text-small head no-margin">たばこを吸うメリット 1位はやっぱり○○</p>
                                            <p class="text-small no-margin red">チバユウスケ</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="interview-listing">                                                    
                                        <div class="interview-listing-text">
                                            <img src="assets/img/images/interview/img4.jpg" alt="img">
                                            <p class="text-small no-margin">2015.05.01</p>
                                            <p class="text-small head no-margin">たばこを吸うメリット 1位はやっぱり○○</p>
                                            <p class="text-small no-margin red">今田耕司</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
