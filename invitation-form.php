<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row invitationWrap">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>ご友人からの紹介者用　≪特別入力フォーム≫</h3>
                </div>
                <div class="panel-content grey-border">
                    <div class="col-md-12 selectItems margin-30">
                        <p>ご希望するサンプルパックを選択して下さい </p>

                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item01.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・レッド</b><br>タール10mg / ニコチン0.7mg</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item02.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・ディープブルー</b><br>タール8mg / ニコチン0.6㎎</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item03.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・ブルー</b><br>タール6mg / ニコチン0.5mg</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item04.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・シルバー</b><br>タール3mg / ニコチン0.3mg</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item05.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ホワイト</b><br>タール1mg / ニコチン0.1mg</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item06.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・ホワイト100</b><br>タール1mg / ニコチン0.1mg</span>
                                </label>
                            </p>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item07.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・メンソール</b><br>タール6mg /ニコチン0.5mg</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item08.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・メンソール<br>ホワイト100</b><br>タール1mg / ニコチン0.1mg</span>
                                </label>
                            </p>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item09.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・デュエット</b><br>タール6mg / ニコチン0.6mg</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item10.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・<br>アイスフレッシュ エイト</b><br>タール8mg / ニコチン0.6㎎</span>
                                </label>
                            </p>
                        </div>
                        <div class="col-md-3">
                            <div class="image">
                                <span><img src="assets/img/images/invitation/select-item11.png" alt="img"></span>
                            </div>
                            <p>
                                <label>
                                    <input type="radio" name="select-item" value="" placeholder="">
                                    <span><b>ウエスト・<br>アイスフレッシュ ワン100</b><br>タール1mg / ニコチン0.1㎎</span>
                                </label>
                            </p>
                        </div>
                        
                        
                    </div> 
                    <div class="col-md-12">
                        <p class="margin-30">利用規約とプライバシーポリシーをご確認下さい。</p>
                        <p class="margin-30">
                            <a href="#">利用規約（必ずお読み下さい）</a>
                            <br><a href="#">プライバシーポリシー（必ずお読み下さい）</a>
                        </p>
                        <p class="margin-30">ご登録申請にあたり以下の内容について入力をお願いします。</p>
                        <p>お客様の個人情報は、弊社にて厳重に管理を行います。
                        <br>また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。</p>
                    </div>

                    <div class="col-md-12">
                        <div class="below-top-list no-border">
                            <table class="table mypagetable1">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">成人確認書類（写真画像）<br><strong>（必須）</strong></th>
                                        <td class="step2-inner">
                                            <a class="btn btn-default">参照</a>
                                            <label class="margin-30">ファイルが選択されていません。</label>
                                            <p class="heading-panel red">※成人であることが確認できる書類の写真画像をアップロードして下さい。</p>
                                            <p class="heading-panel red margin-30">※ファイルサイズ2MB以下のJPG/GIF/PNG形式の画像がアップロード可能です。</p>

                                            <p>※成人確認書類の一例</p>
                                            <ul class="inner-table-list">
                                                <li>taspo</li>
                                                <li>自動車運転免許証</li>
                                                <li>パスポート</li>
                                                <li>住民票の写し</li>
                                                <li>健康保険証</li>
                                                <li>小型船舶操縦免許証</li>
                                                <li>その他、公的機関から発行され、氏名と満20歳以上であることが確認できる書類</li>
                                            </ul>
                                            <p>※写真画像は、文字や写真がはっきりと写るように、正面からピントを合わせて撮影するようにお願いします。</p>
                                            <p>※姓名表記については、必ず登録する名前と同じ記載のある書類をお使い下さい。</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">お名前<strong>（必須）</strong></th>
                                        <td>
                                            <div class="inner-table-item">
                                                <p class="at-the-rate">姓</p>
                                                <input class="form-control" type="text">
                                                <p class="at-the-rate">名</p>
                                                <input class="form-control" type="text">                                      
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">フリガナ <strong>（必須）</strong> <br>（全角カタカナ）</th>
                                        <td>
                                            <div class="inner-table-item">
                                                <p class="at-the-rate">セイ</p>
                                                <input class="form-control" type="text">
                                                <p class="at-the-rate">メイ</p>
                                                <input class="form-control" type="text">                                      
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">性別 <strong>（必須）</strong></th>
                                        <td>
                                            <input type="radio">
                                            <label>男</label>
                                            <input type="radio">
                                            <label>女</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">生年月日 <strong>（必須）</strong><br>（半角数字）</th>
                                        <td>
                                            <div class="inner-table-item">
                                                <input class="form-control" type="text">
                                                <p>(例:1980年3月14日⇒19800314) </p>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">郵便番号<br>(半角数字、ハイフン不要)</th>
                                        <td>
                                            <div class="inner-table-item">
                                                <input class="form-control" type="text">
                                                <p>例：105-8422⇒1058422</p>
                                                <a class="white-proceed btn-refresh btn-grey">郵便番号から住所検索</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">都道府県 <strong>(必須)</strong></th>
                                        <td><select class="form-control"> </select></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">市区町村等 <strong>(必須)</strong></th>
                                        <td><input class="form-control" type="text"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">丁番地等 <strong>(必須)</strong><br>(地名・町名後の丁番地等は半角数字で入力してください。)</th>
                                        <td>
                                            <div class="inner-table-item">
                                                <input class="form-control" type="text">
                                                <p>（例：海岸1-2-3）</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">建物名</th>
                                        <td><input class="form-control" type="text"></td>
                                    </tr>
                                    <!--<tr>
                                        <th scope="row">PCメールアドレス</th>
                                        <td><input class="form-control" type="text"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">携帯メールアドレス</th>
                                        <td>
                                            <div class="inner-table-item">
                                                <input class="form-control" type="text">
                                                <p class="at-the-rate">@</p>
                                                <select class="form-control"> </select>
                                                <hr>
                                                <p>確認のため、もう一度入力してください。</p>
                                                <input class="form-control" type="text">
                                                <p class="at-the-rate">@</p>
                                                <select class="form-control"> </select>

                                            </div>
                                        </td>
                                    </tr>-->
                                </tbody>
                            </table>
                            <p>ご提供頂く個人情報は、弊社が以下の目的に使用させて頂きます。</p>
                            <p class="margin-20">また、個人を特定しない方法で、マーケティングの統計データとして活用させて頂きます。</p>
                            <ul class="outer-table-list margin-20">
                                <li>サンプルたばこの送付（ご希望の場合のみ）</li>
                                <li>メールによる各種ご案内の送付</li>
                            </ul>

                            <p class="margin-20">※また、個人を特定せず、あくまで統計データとして、今後のサービス向上や製品の品質向上のため、マーケティングに活用させて頂くことがあります。</p>
                            <p class="margin-20">お客様の個人情報の取扱いについてはこちら</p>

                            <table class="table margin-30">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row" rowspan="3">メールによる各種ご案内の送付</th>
                                        <th scope="row">A.キャンペーンに関するご案内</th>
                                        <td>
                                            <input type="radio">
                                            <label>希望する</label>
                                            <input type="radio">
                                            <label>希望しない</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">B.お得な懸賞に関するご案内</th>
                                        <td>
                                            <input type="radio">
                                            <label>希望する</label>
                                            <input type="radio">
                                            <label>希望しない</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">C.新製品に関するご案内</th>
                                        <td>
                                            <input type="radio">
                                            <label>希望する</label>
                                            <input type="radio">
                                            <label>希望しない</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p class="margin-30">※製品サンプルやご案内については、時期や条件等により、お届けできない場合もあります。あらかじめご了承ください。 </p>
                            <table class="table half-head margin-40">                                    
                                <tbody>
                                    <tr>
                                        <th colspan="2" scope="row">いまご愛飲されているたばこのブランド名を右記よりお選びください。</th>
                                        <td>
                                            <select class="form-control">選択してください</select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="change-password-span margin-40">
                            <a class="white-proceed btn-refresh">確認する</a>
                        </div>
                    </div>                  
                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
				<li><a>FAQ</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
