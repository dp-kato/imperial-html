<?php
include('header-login.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="background-wow ">
            <div class="page-content-login row" style="padding-bottom:68px;">
                <div class="panel-body">
                    <div class="panel-header">
                        <div class="change-password-span">
                            <h3>たばこWest（ウエスト）ブランドサイトへようこそ</h3>
                        </div>
                    </div>
                    <div class="panel-content grey-border">
                        <p>このWebサイトでは、インペリアル・タバコ・ジャパンの提供するたばこ</p>
                        <p class="margin-20">「West（ウエスト）」についての情報をお知らせしています。</p>
                        <p class="margin-20">登録後にログインすると、Westのお得なキャンペーン情報やオリジナルコンテンツ、ブランド紹介などを閲覧することができます。</p>

                        <div class="col-xs-6 left">
                            <div class="panel-body">
                                <div class="panel-header-small">
                                    <h4>はじめての方</h4>                               
                                </div>
                                <div class="panel-content red-border">
                                    <div class="col-md-12 left_s">
                                        <div class="col-xs-6">
                                            <div class="margin-30">
                                                <p class="margin-20">満20歳以上の喫煙者の方で</p>
												<p class="margin-30">登録がお済みでない方はこちら</p>
                                            </div>
                                            <div class="touroku-span">
                                                <a class="white-proceed btn-refresh">登録する</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <p class="large margin-30">携帯電話・スマートフォン</p>
                                            <div class="change-password-span margin-30">
                                                <img src="assets/img/images/img-qr-code.jpg" alt="img">
                                            </div>
                                            <p class="notice">QRコードを読み取ると携帯電話・スマートフォン用のWebサイトに接続できます。</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 right margin-20">
                            <div class="panel-body">
                                <div class="panel-header-small">
                                    <h4>ご登録済みの方</h4>                               
                                </div>
                                <div class="panel-content red-border">
								　<p class="margin-15">2015年4月27日より8桁のID番号は廃止となりました。<br>ご登録いただいたメールアドレスにてログインしてください。</p>
                                    <div class="margin-5 panel-form clearfix">
                                        <label>メールアドレス</label>
                                        <input class="form-control login" type="text">
                                    </div>
                                    <div class="margin-5 panel-form clearfix">
                                        <label>パスワード</label>
                                        <input class="form-control login" type="text">
                                    </div>
                                    <input type="checkbox">
                                    <p class="wrapped">次回から自動的にログイン</p>
                                    <div class="margin-5 login-span">
                                        <a class="white-proceed btn-refresh">ログイン</a>
                                    </div>
                                    <ul class="inner-links">
                                        <li>
                                            <a href="#">>パスワードを紛失した方</a>
										</li>
                                        <li>
                                           	<a href="#">>ご登録メールアドレスを忘れた方</a>
										</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
						<div class="col-md-12 release">
							<p>本サイトは成人喫煙者向けのサイトです。未成年の方はご利用いただけません</p>
						</div>
                        <div class="col-md-12 release">
                            <div class="panel-body">
                                <div class="panel-header-small">
                                    <h4>プレスリリース</h4>                               
                                </div>
                                <div class="panel-content red-border">
                                    <p class="red">2014.12.19</p>
                                    <p class="newsrelease">
									   <a href="https://westonline.jp/release/141219_PR_ITJ001.pdf">
									     インペリアル・タバコ・ジャパンでは、たばこ「West」の O2O マーケティング活動において、<br>
                                         次世代型スマートフォン連動ガジェット「PlugAir(プラグエア)」の導入を決定しました。
									   </a>
								    </p>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
				<li><a>FAQ</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    var d = new Date(); // for now
    var hours = d.getHours(); // => 9
    var minutes = d.getMinutes(); // =>  30
    var seconds = d.getSeconds(); // => 51
    var img = Math.floor(Math.random() * 7) + 1 + '.jpg';

    if (hours >= 7 && hours <= 18) {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/morning/" + img + ")");
    } else if (hours >= 19) {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/night/" + img + ")");
    } else if (hours <= 6) {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/night/" + img + ")");
    } else {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/large/1.jpg)");
    }

</script>
</body>

</html>
