<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header margin-40">                                
                    <img src="assets/img/images/icon-mgm-grey.png" alt="img">
                    <h3>友達に紹介する</h3>
                </div>
            </div> 
            <div class="col-md-12">

                <div class="grey-border members">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="member-upper-banner">
                                <!--<h5>お友だち・ご家族を紹介してください</h5>
                                <h4>Westブランドサイトをお友だちをご紹介いただくと、</h4>
                                <h3>もれなく<strong>5000ポイント</strong>をプレゼント！</h3>-->

                                <img src="assets/img/images/members/upper-banner.png" alt="ïmg">
                            </div>

                            <div class="app-flow-button margin-40">
                                <p class="margin-20">お友達にメセージを送ります。お友達のメールアドレスを入力してください。</p>
                            </div>
                            <div class="app-flow-button margin-40">
                                <div class="form-inner-span">
                                    <div class="col-xs-4">
                                        <p>お友達のメールアドレス</p>
                                    </div>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text">
                                        <p>（半角英数字）</p>
                                    </div>
                                </div>
                            </div>
                             <div class="app-flow-button margin-40">
                                <div class="form-inner-span">
                                    <div class="col-xs-4">
                                        <p>お友達のメールアドレス(確認) <br><small>※確認のためもう一度ご入力ください。</small></p>
                                    </div>
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text">
                                        <p>（半角英数字）</p>
                                    </div>
                                </div>
                            </div>
                            <div class="app-flow-button margin-40">
                                <div class="form-inner-span">
                                    <div class="col-xs-4">
                                        <p>件名</p>
                                    </div>
                                    <div class="col-xs-8">                                        
                                        <p>【Westたばこのサンプルがもらえる！】●●さんからのご紹介です</p>
                                    </div>
                                </div>
                            </div>
                            <div class="app-flow-button margin-40">
                                <div class="form-inner-span">
                                    <div class="col-xs-4">
                                        <p>お友達へのメッセージ <br>（100文字以内）</p>
                                    </div>
                                    <div class="col-xs-8">                                        
                                        <div class="member-form grey-border">
                                            <div class="member-form-header">
                                                <p>●●さんからWestサンプルパックプレゼントキャンペーンのご紹介</p>
                                            </div>
                                            <p class="large margin-30">あなたは●●さんからWestブランドサイトの紹介を受けました。</p>
                                            <p class="large margin-20">下記のお友だち紹介用URLからWestブランドサイトにご登録いただくと、もれなくお好きなWestサンプルパックをプレゼントいたします。この機会に是非Westをお試しください。</p>
                                            <p class="large">※すでにWestブランドサイト会員の方やお友達紹介用URL以外から会員登録をされた方は本キャンペーンにお申込いただけませんのでご了承ください。</p>
                                            <p class="large margin-20">■サンプルパックプレゼントキャンペーンURL（お友達紹介用URL）</p>
                                            <p class="large margin-20">https://・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・</p>
                                            <div class="form-footer">
                                                <p>※このメールは成人喫煙者の方にお送りしているメールです、</p>
                                                <p class="margin-30">万が一「非喫煙者」「20歳以下の未成年」の方にこちらのメールが届いた場合は、お手数ですがキャンペーンサイトへ進まず、メールを破棄していただけますようお願い申し上げます</p>
                                                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させ
                                                    る危険性を高めます。 未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。
                                                    周りの人から勧められても決して吸ってはいけません。</p>
                                                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 
                                                    喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="change-password-span margin-40">
                        <p class="margin-30">下記のチェックボックスをご確認いただき、内容のご確認に進んで下さい。</p>
                        <div>
                            <input type="checkbox">
                            <label>紹介メールを送る相手は、成人喫煙者です。</label>
                        </div>
                        
                        <a class="white-proceed btn-refresh grey btn-fix-width">戻る</a>
                        <a class="white-proceed btn-refresh btn-fix-width">同意して内容を確認する</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
</body>

</html>
