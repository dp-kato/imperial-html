<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header margin-40">                                
                    <img src="assets/img/images/icon-mgm-grey.png" alt="img">
                    <h3>友達に紹介する</h3>
                </div>
            </div> 
            <div class="col-md-12">

                <div class="grey-border members">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="member-upper-banner">
                                <!--<h5>お友だち・ご家族を紹介してください</h5>
                                <h4>Westブランドサイトをお友だちをご紹介いただくと、</h4>
                                <h3>もれなく<strong>5000ポイント</strong>をプレゼント！</h3>-->

                                <img src="assets/img/images/members/upper-banner.png" alt="ïmg">
                            </div>
                            <div class="member-lower-banner">
                                <div class="lower-banner-content">
                                    <div class="col-xs-4">
                                        <div class="inner-banner-text-heading margin-15">
                                            <img src="assets/img/images/members/text-banner-inner.jpg" alt="img"/>
                                        </div>
                                        <!--<h3 class="red">ご友人様へ Westブランドサイトを ご紹介ください♪</h3>-->
                                        <p>ご紹介いただいたお友達・ご家族の方が、</p>
                                        <p>ウエストブランドサイトで会員登録をしていただくと、</p>
                                        <p>あなたにもお友達にも素敵なプレゼントをお届けします。</p>
                                        <p>※ご紹介キャンペーンは新規登録の</p>
                                        <p>　お友達・ご家族が対象となります。</p>
                                    </div>
                                    <div class="col-md-8 lower-banner-right center">
                                        <div class="lb-img">
                                            <img src="assets/img/images/members/img-lower-1.png" alt="img">
                                        </div>
                                    </div>
                                </div>
                                <div class="change-password-span margin-40">
                                    <a class="white-proceed btn-refresh">いますぐお友達に紹介する</a>
                                </div>
                            </div>
                            <div class="app-flow-button margin-40">
                                <a class="btn-app-flow">お申し込みの流れ<br> お友達にお伝え下さい！</a>
                            </div>
                        </div>
                    </div>



                    <div class="marginal">
                        <div class="app-flow">
                            <div class="col-md-12">
                                <div class="col-xs-4">
                                    <div class="app-flow-steps next-flow">
                                        <img class="app-flow-number" src="assets/img/images/members/flow-num1.png" alt="img">
                                        <div class="app-flow-header">
                                            <h3>紹介メールを <br>お友達に送信</h3>
                                        </div>
                                        <div class="app-flow-image">
                                            <img src="assets/img/images/members/img-flow-1.png" alt="img">
                                        </div>
                                        <div class="app-flow-text">
                                            <ul>
                                                <li>
                                                    <p>〈いますぐお友達に紹介する〉ボタンを押し、次のページへ進む。</p>
                                                </li>
                                                <li>
                                                    <p>お友達のメールア ドレスを入力し〈送信〉するとあなたに5,000Wポイントプレゼント！</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="app-flow-steps next-flow">
                                        <img class="app-flow-number" src="assets/img/images/members/flow-num2.png" alt="img">
                                        <div class="app-flow-header">
                                            <h3>お友達がWestブランド<br> サイトに会員登録</h3>
                                        </div>
                                        <div class="app-flow-image">
                                            <img src="assets/img/images/members/img-flow-2.png" alt="img">
                                        </div>
                                        <div class="app-flow-text">
                                            <ul>
                                                <br>
                                                <li>                                                       
                                                    <p>お友達が紹介メールに記載された〈お友達紹介用URL〉から会員登録をする。</p>
                                                </li>
                                                <p>※〈お友達紹介用URL〉以外から会員登録された場合は適用されません。</p>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="app-flow-steps">
                                        <img class="app-flow-number" src="assets/img/images/members/flow-num3.png" alt="img">
                                        <div class="app-flow-header">
                                            <h3>お友達とあなたにそれぞれ<br>プレゼントが届きます！</h3>
                                        </div>
                                        <div class="app-flow-image">
                                            <img src="assets/img/images/members/img-flow-3.png" alt="img">
                                        </div>
                                        <div class="app-flow-text">
                                            <ul>
                                                <br>
                                                <li>

                                                    <p>お友達の会員登録が完了すると、あなたには楽天スーパーポイント300ポイント、お友達にはご希望のサンプルパックが届きます。</p>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="app-flow-button margin-40"></div>
                    <div class="change-password-span margin-40">
                        <p class="margin-20">※20歳以上且つ喫煙者の方へのみ、送信が可能です。※すでに入会している方へは送れません。</p>
                        <p class="large margin-20">ボタンをクリックして、お友達へ紹介メッセージを送ろう！</p>
                        <a class="white-proceed btn-refresh">いますぐお友達に紹介する</a>
                    </div>
                    <div class="col-md-12">
                        <div class="members-below margin-40">
                            <p class="p-heading margin-20">利用規約</p>
                            <ul>
                                <li>
                                    <p>ご紹介キャンペーンはウエストブランドサイトに新規で登録する成人喫煙者のお友だち・ご家族で日本国内在住の方に限らせていただきます。</p>
                                </li>
                                <li>
                                    <p>ご紹介するお友だち・ご家族には事前に〈お申込の流れ〉を必ずお伝えいただけますようお願いいたします。</p>
                                </li>
                                <li>
                                    <p>Wポイントはご紹介メール送信後に、あなたに付与されます。</p>
                                </li>
                                <li>
                                    <p>お友だちが会員登録を完了すると、〈お友だちの会員登録完了メール〉をあなたにお送りします。</p>
                                </li>
                                <li>
                                    <p>楽天スーパーポイントは、〈お友だちの会員登録完了メール〉が届いた月の翌月末に獲得することができます。ポイント獲得用URLが記載されたメールをあなたにお送りいたします。そのメールに記載されているURLからポイント獲得の手続きを行ってください。※自動でポイントの付与は自動的に行われませんのでご注意ください。ポイント獲得のためのメールが迷惑メールフォルダに振り分けられてしまう場合がございます。ポイント付与時期には、各フォルダをご確認ください。</p>
                                </li>
                                <li>
                                    <p>お友だちへのサンプルパックのプレゼントは、あなたが送った〈紹介メール〉に記載されている〈お友だち紹介用URL〉からのみお申込が可能ですので、その旨お友だちに必ずお伝えください。</p>
                                </li>
                                <li>
                                    <p>お友だちがすでにウエストブランドサイト会員であったり、 〈お友だち紹介用URL〉以外から会員登録をした場合は本キャンペーンは適用されませんのでご了承ください。</p>
                                </li>
                                <li>
                                    <p>本キャンペーンにおいて不正なお申込や非喫煙者・20歳未満の方への紹介等が行われたと事務局が判断した場合、キャンペーンの適用を無効とさせていただく場合がございます。</p>
                                </li>

                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
</body>

</html>
