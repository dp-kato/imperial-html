<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header margin-40 no-border">                                
                    <img src="assets/img/images/icon-mgm-grey.png" alt="img">
                    <h3>友達に紹介する</h3>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="grey-border members center">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="thanks-text margin-40">
                                <h3 class="red">お友達にご紹介メールを送信いたしました。</h3>
                                <p class="large">ダミーです。この度はご紹介メールをお送りいただきありがとうございます。</p>
                                <p class="large">後ほど、担当者よりご連絡をさせていただきます。</p>
                                <p class="large">今しばらくお待ちくださいますようよろしくお願い申し上げます。</p>
                            </div>
                            <div class="change-password-span">
                                <a class="white-proceed btn-refresh">友達をさらに増やす</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
</body>

</html>
