<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/my-page/icon-my-page-black.png" alt="img">
                    <h3>お客様情報の変更</h3>
                </div>
            </div>  

            <div class="col-md-12">
                <div class="below-top-list">
                    <h3><b>■</b>メールアドレスの変更<span class="red table-sub">※ログインの際に必要となりますので必ずお控えください。</span></h3>
                    <table class="table">                                    
                        <tbody>
                            <tr>
                                <th scope="row">現在のメールアドレス</th>
                                <td><input class="form-control" type="text"></td>
                            </tr>
                            <tr>
                                <th scope="row">新しいメールアドレス</th>
                                <td><input class="form-control" type="text"></td>
                            </tr>
                            <tr>
                                <th scope="row">確認のため再入力</th>
                                <td><input class="form-control" type="text"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="change-password-span">
                        <a class="white-proceed change-password">メールアドレスを変更する</a>
                    </div>
                </div>
                <div class="below-top-list">
                    <h3><b>■</b>パスワードの変更<span class="red table-sub">※ログインの際に必要となりますので必ずお控えください。</span></h3>
                    <table class="table">                                    
                        <tbody>
                            <tr>
                                <th scope="row">現在のパスワード</th>
                                <td><input class="form-control" type="text"></td>
                            </tr>
                            <tr>
                                <th scope="row">新しいパスワード</th>
                                <td><input class="form-control" type="text"></td>
                            </tr>
                            <tr>
                                <th scope="row">確認のため再入力</th>
                                <td><input class="form-control" type="text"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="change-password-span">
                        <a class="white-proceed change-password">パスワードを変更する</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="below-top-list no-border">
                    <h3><b>■</b>登録情報を更新する<span class="table-sub">※サンプルたばこやキャンペーン景品等をお送りするために必要な情報となりますので、正確にご記入をお願いいたします。</span></h3>
                    <table class="table">                                    
                        <tbody>
                            <tr>
                                <th scope="row">郵便番号<br>(半角数字、ハイフン不要)</th>
                                <td colspan="3">
                                    <div class="inner-table-item">
                                        <input class="form-control" type="text">
                                        <p>例：105-8422⇒1058422</p>
                                        <a class="white-proceed change-password btn-light-grey">郵便番号から住所検索</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">都道府県 <strong>(必須)</strong></th>
                                <td colspan="3"><select class="form-control"> </select></td>
                            </tr>
                            <tr>
                                <th scope="row">市区町村等 <strong>(必須)</strong></th>
                                <td colspan="3"><input class="form-control" type="text"></td>
                            </tr>
                            <tr>
                                <th scope="row">丁番地等 <strong>(必須)</strong><br>(地名・町名後の丁番地等は半角数字で入力してください。)</th>
                                <td colspan="3">
                                    <div class="inner-table-item">
                                        <input class="form-control" type="text">
                                        <p>（例：海岸1-2-3）</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">建物名・部屋番号</th>
                                <td colspan="3"><input class="form-control" type="text"></td>
                            </tr>
                            <tr>
                                <th colspan="2" scope="row">1)サンプルたばこの送付  </th>
                                <td colspan="2">
                                    <input type="radio" id="radio0" name="radio">
                                    <label for="radio0"><span></span> 希望する</label> 
                                    <input type="radio" id="radio00" name="radio">
                                    <label for="radio00"><span></span> 希望しない</label> 
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" rowspan="3">2)メールによる<br>各種ご案内の送付</th>
                                <th scope="row" class="tal">（1）キャンペーンに関するご案内</th>
                                <td colspan="2">
                                    <input type="radio" id="radio01" name="radio">
                                    <label for="radio01"><span></span> 希望する</label> 
                                    <input type="radio" id="radio02" name="radio">
                                    <label for="radio02"><span></span> 希望しない</label> 
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" class="tal">（2）お得な懸賞に関するご案内</th>
                                <td colspan="2">
                                    <input type="radio" id="radio03" name="radio">
                                    <label for="radio03"><span></span> 希望する</label> 
                                    <input type="radio" id="radio04" name="radio">
                                    <label for="radio04"><span></span> 希望しない</label> 
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" class="tal">（3）新製品に関するご案内</th>
                                <td colspan="2">
                                    <input type="radio" id="radio05" name="radio">
                                    <label for="radio05"><span></span> 希望する</label> 
                                    <input type="radio" id="radio06" name="radio">
                                    <label for="radio06"><span></span> 希望しない</label> 
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <div class="change-password-span margin-30">
                        <a class="grey-bg btn-grey">戻る</a>
                        <a class="white-proceed btn-refresh">更新する</a>
                    </div>
                    <div class="below-table-text">
                        <p>※製品サンプルやご案内については、時期や条件等により、お届けできない場合もあります。あらかじめご了承ください。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

</script>
</body>

</html>
