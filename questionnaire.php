<?php include('header.php'); 
include('config.php');
$selectedVal = '';
$ip = $_SERVER['REMOTE_ADDR'];
$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
$location = explode(',',$details->loc);
if($location){
    $lat = $location[0];
    $lon = $location[1];
}
$conn = new Connection;
$ip = '123';
$data = $conn->getQuestionnair($ip);
$allData = $conn->getAllQuestionnair();
if(!empty($data)){
    $selectedVal = $data['value'];
}
if(isset($_POST['activeVal'])){
    $selectedVal = $_POST['activeVal'];
    $conn->addQuestionnair($ip,$_POST['activeVal']);
} 

echo $selectedVal;
?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-xs-12">
                <div class="inner-page-header">                                
                    <img src="assets/img/images/questionare-grey.png" alt="img">
                    <h3>アンケート</h3>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="questionnaire-text">
                    <p>多数派？少数派？あなたはどっち？</p>
                    <p>ちょっとした時間に一服しながらお楽しみください！</p>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="banner-questionnaire">                                
                    <h3><b>Q.</b> 臨時ボーナスが出たら‥</h3>
                    <form method = "POST">
                    <div class="questionnaire-banner-img">
                        <div id="radioBox">
                            <div class=" col-xs-4">
                                <div class="center">
                                    <label class="percent" id="percent-1"><?php echo (!empty($selectedVal))?round(($allData['percent-1']/$allData['total'])*100).'<span>%</span>':''?></label>
                                </div>
                                <img class="img-questionnaire <?php echo (!empty($selectedVal))?($selectedVal=='percent-1')?'active':'':''?>" id="img-percent-1" src="assets/img/images/questionnaire/img-questionnaire-1.jpg" alt="img">
                                <div class="img-selection">
                                    <input required type="radio" name="questions" value='貯金する' rel="percent-1" id="radio02" name="radio">
                                    <label for="radio02"><?php echo (!empty($selectedVal))?'':'<span></span>'?> 貯金する</label>                                    
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="canvas-holder" style="text-align:center"></div>
                                <div class="doughnut-center center <?php echo (!empty($selectedVal))?'':'hide'?>" id="canvasText">
                                    <p>West</p>
                                    <p>Questionnaire</p>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="center">
                                    <label class="percent" id="percent-2"><?php echo (!empty($selectedVal))?round(($allData['percent-2']/$allData['total'])*100).'<span>%</span>':''?></label>
                                </div>
                                <img class="img-questionnaire <?php echo (!empty($selectedVal))?($selectedVal=='percent-2')?'active':'':''?>" id="img-percent-2" src="assets/img/images/questionnaire/img-questionnaire-2.jpg" alt="img">
                                <div class="img-selection">
                                    <input required type="radio" name="questions" value='パーッと使う' rel="percent-2" id="radio01" name="radio">
                                    <label for="radio01"><?php echo (!empty($selectedVal))?'':'<span></span>'?> パーッと使う</label>

                                </div>
                        </div>
                        </div>

                    </div>
                    <input type="hidden" name="activeVal" id="activeVal"/>
                    <input type="hidden" name="inactiveVal" id="inactiveVal"/>
                    <div class="col-xs-12 center">
                        <!-- <a href="javascript:void(0)" class="grey-bg btn-questionnaire-banner" id="submitBtn">回答する</a> -->
                        <input type="submit" value="回答する" id="submitBtn" class="grey-bg btn-questionnaire-banner"/>
                    </div>
                </form>
                </div>

            </div>
            <div class="col-xs-12">                           
                <div class="row">                                
                    <div class="slider multiple-item questionnaire-slider-area">
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-xs-12">
                                        <p>実は変わり者・・・？</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-2.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-1.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-xs-12">
                                        <p>自分磨きできてる？</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-3.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-4.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-xs-12">
                                        <p>結婚後の家事したい？</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-5.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-6.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg btn-comics color-white" href="#">回答済み</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-xs-12">
                                        <p>セキュリティソフト使ってる？</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-7.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-8.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg btn-comics color-white" href="#">期間終了</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-xs-12">
                                        <p>職場で他人がやっていて ストレスを感じる行為どっち？</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-9.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-10.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg btn-comics color-white" href="#">期間終了</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-xs-12">
                                        <p>実は変わり者・・・？</p>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-2.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-xs-6">
                                        <img src="assets/img/images/questionnaire/img-tile-1.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg btn-comics color-white" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script src="assets/js/Chart.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    
            
    <?php if(!empty($selectedVal)){?>
        //setTimeout(function(){
           var pieData = [
                {
                    value: <?php echo round(($allData['percent-1']/$allData['total'])*100)?>,
                    color: "#EC9696",
                    highlight: "#FF5A5E",
                },
                {
                    value: <?php echo round(($allData['percent-2']/$allData['total'])*100)?>,
                    color: "#AC0000",
                    highlight: "#5AD3D1",
                },
            ];
            $('#canvas-holder').html('<canvas id="chart-area" width="250" height="250"/>');
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(pieData);
            $('#submitBtn').hide(); 
            $('input[name="questions"]').addClass('hide');
            


        //},1000);
    <?php } ?>
    $('.btn-modal').click(function() {
        var dataTarget = $(this).attr('data-target');
        setTimeout(function() {
            $(dataTarget).find('div.modal-body').removeClass('modal-loading');
            $(dataTarget).find('div.single-item').removeClass('disappear');
            $(dataTarget).find('div.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });

        }, 500);
    });




    /*$('body').on('click', '#submitBtn', function() {
        if( ($('input[name="questions"]:checked').length) == 1){
            $(this).attr('id', 'backBtn');
            $(this).text('バック');
            $('#canvas-holder').html('<canvas id="chart-area" width="250" height="250"/>');
            var asd = $('input[name="questions"]:checked').val();
            var asd1 = $('input[name="questions"]:not(:checked)').val();

            var checkRad = $('input[name="questions"]:checked').attr('rel');
            var unCheckRad = $('input[name="questions"]:not(:checked)').attr('rel');
            $('#' + checkRad).html('60<span>%</span>');
            $('#' + unCheckRad).html('40<span>%</span>');
            $('#img-' + checkRad).addClass('active');
            $('input[name="questions"]').addClass('hide');
            var pieData = [
                {
                    value: 60,
                    color: "#EC9696",
                    highlight: "#FF5A5E",
                    label: asd,
                },
                {
                    value: 40,
                    color: "#AC0000",
                    highlight: "#5AD3D1",
                    label: asd1,
                },
            ];
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(pieData);
            $('#canvasText').removeClass('hide');
            
        } else{
            alert('Select any radio box');
        }
    });
    $('body').on('click', '#backBtn', function() {
        $(this).attr('id', 'submitBtn');
        $(this).text('回答する');
        $('#canvas-holder').html('');
        $('#percent-1').html('00<span>%</span>');
        $('#percent-2').html('00<span>%</span>');
        $('.img-questionnaire').removeClass('active');
        $('input[name="questions"]').removeClass('hide');
        $('input[name="questions"]').attr('checked', false);
        $('#canvasText').addClass('hide');
    });*/

    $('input[name="questions"]').click(function(){
        $('.img-questionnaire-small').removeClass('active').addClass('inactive');
        $(this).removeClass('inactive').addClass('active');
        var checkRad = $('input[name="questions"]:checked').attr('rel');
        var unCheckRad = $('input[name="questions"]:not(:checked)').attr('rel');
        $('#activeVal').val(checkRad);
        $('#inactiveVal').val(unCheckRad);
    });
</script>
</body>

</html>
