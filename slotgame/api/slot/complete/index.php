<?php
/**
 * Author: abetakuya <abe@sisan.co.jp>
 * Date: 15/04/15
 * Time: 14:19
 */
$response = array_merge( $_GET, array(
	'status' => 'OK'
) );
echo json_encode( $response );