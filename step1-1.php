<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>STEP1-1: 利用規約およびプライバシーポリシーへのご同意</h3>
                </div>
                <div class="panel-content grey-border">
                    <div class="col-md-12">
                        <p class="heading-panel red">はじめに</p>
                        <p class="margin-40">当ウェブサイトは、日本国内在住の満20歳以上の成人喫煙者を対象にしています。ご閲覧・ご利用いただくには、また、各種キャンペーン</p>

                        <p>まず、利用規約の内容およびプライバシーポリシーをご確認ください。</p>
                    </div>
                    <div class="col-md-12">
                        <div class="container-panel">
                            <p class="heading-panel red">利用規約</p>
                            <div class="panel-grey margin-40">
                                <p class="heading-panel">利用規約</p>
                                <p class="margin-40">当サイトは、インペリアル・タバコ・ジャパン株式会社（以下当社といいます。）および当サイトの運営に関して業務を委託している当社のビジネスパートナーが運営しています。</p>

                                <p>当サイトには、たばこ製品に関する情報が含まれています。</p>
                                <p>当サイトは未成年喫煙防止の観点から、満20歳以上の喫煙者である方のみを対象としています。</p>
                                <p>当サイトのご利用には、お客様の年齢等を確認後に当社が発行するIDおよびパスワードが必要となります。</p>
                                <p>IDおよびパスワードをお持ちでない方は、当社所定の手続きに従い、発行のお申し込みをしてください。</p>
                                <p class="margin-40">当サイトは、当社からのIDおよびパスワードの発行を受けたお客様ご本人のみがご利用いただけるものですので、お客様ご本人によるIDおよびパスワードの管理、および他の方への貸与等をなされないよう、お願いいたします。</p>

                                <p>当サイトのご利用にあたっては上記および、以下の内容をお読みになり、すべてに同意された場合のみ、ご利用ください。なお当サイトを利用された場合には、本利用規約のすべてに同意いただいたものとみなします。</p>
                                <p>当利用規約の内容は、予告なく変更される場合があります。この場合、変更後の当利用規約が適用されますので、最新の内容をご確認ください</p>
                                <p>なお、当利用規約にご同意いただけない場合には、当サイトのご利用をお控えくださいますようお願いいたします。</p>
                            </div>




                            <p class="heading-panel red">プライバシーポリシー</p>
                            <div class="panel-grey">
                                <p class="heading-panel">プライバシーポリシー</p>
                                <p class="margin-30">インペリアル・タバコ・ジャパン株式会社（「当社」）は、当社が販売促進の為に提供するウェブサイト
                                    （「当社サイト」）運営にあたり、お客様からの信頼を第一と考え、個人情報の保護に関する法律（「個人情報保護法」）に沿って、
                                    お客様の個人情報を厳格に管理し、お客様の希望に沿って取扱うとともに、お客様の個人情報の正確性・機密性の保持に努めています。
                                    「個人情報の保護に関する法律」（「個人情報保護法」）、その他関係する法令を遵守し、お客様の個人情報を厳格に管理し、
                                    取扱います。</p>

                                <p class="heading-panel">１．関係法令等の遵守</p>
                                <p class="margin-30">当社は、お客様の個人情報の取扱いにつきまして、個人情報保護法その他関係法令その他の規範を遵守いたします。</p>

                                <p class="heading-panel">２．グループ会社による共同利用</p>
                                <p class="margin-30">当社は、「３．個人情報の収集・利用」に掲げるお客様の個人情報を「４．個人情報の利用目的」に掲げる目的のために
                                    、関連会社と共同で利用することがあります。その場合であっても、お客様の個人情報は、当社が責任を持って管理いたします。</p>

                                <p class="heading-panel">３．個人情報の収集・利用</p>
                                <p class="margin-30">当社は、「４．個人情報の利用目的」に掲げる目的のために、以下のようなお客様の個人情報を収集し、利用することがあります。</p>

                            </div>
                        </div>




                        <p class="margin-40">内容にご同意いただければ、以下のチェックボックスにチェックを入れ、「同意する」ボタンを押して下さい。</p>

                        <div class="change-password-span margin-40">
                            <div class="margin-40">
                                <input type="checkbox">
                                <label>私は満20歳以上の喫煙者であり、上記利用規約およびプライバシーポリシーに同意します。 </label>
                            </div>
							<div class="btn">
                            	<a class="white-proceed btn-refresh">同意する</a>
							</div>
                        </div>
                    </div>
                    <div class="col-md-12">

                        <p>※登録時の上記項目の内容をお忘れの方は、お手数ですが、新たにIDのお申込みをお願いいたします。</p>
                        <p>※本サイトにてご提供いただくお客様の個人情報は、弊社にて厳重に管理を行います。</p>
                        <p>また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示、提供いたしません。</p>
                        <p class="margin-40">※本サイトにてご提供いただくお客様の個人情報は、IDの問合せ確認のために使用させていただきます。</p>

                        <p class="red">【画面操作にあたっての注意事項】</p>
                        <p class="red">ブラウザの戻るボタンは使えませんのでご注意ください。</p>
                    </div>
                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
				<li><a>FAQ</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
