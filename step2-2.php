<?php include('header-login.php');?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>Step2-2:入力内容の確認</h3>
                </div>
                <div class="panel-content grey-border">
                    <div class="col-md-12">
                        <div class="below-top-list no-border">                          
                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">成人確認書類（写真画像）</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>                                    
                                </tbody>
                            </table>
                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">お名前 </th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">フリガナ</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">性別</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">生年月日</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">郵便番号</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">ご住所</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">建物名</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table">                                    
                                <tbody>
                                    <tr>
                                        <th scope="row">サンプルたばこの送付</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">キャンペーンに関するご案内</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">お得な懸賞に関するご案内</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">新製品に関するご案内</th>
                                        <td>テキストテキストテキスト</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="change-conf-span margin-40">
                                <a class="white-proceed btn-refresh btn-grey">修正する</a>
                                <a class="white-proceed btn-refresh">登録する</a>
                            </div>
                        </div>
                    </div>                   
                </div>
            </div>


        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
				<li><a>FAQ</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

</script>
</body>

</html>
