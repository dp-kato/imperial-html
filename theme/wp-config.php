<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpressnew');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'gc2218');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.-t ?}6<*QnP9zA&-o63+|)bvS#B!yNww3{jF%F{&oRg-v25K.EI|R.~@sG*)?Zt');
define('SECURE_AUTH_KEY',  '^0bXNO-R+[E&[zYK@]=|86y_o0:n*-*Y_|7kV5-&Ie_;9Q%2RgejU@#=;u]^W|Z.');
define('LOGGED_IN_KEY',    'lCq}2Y+PLfI}@iu*}(X=w}}`SB3Q%hI}$<*{cywA):tR;7>.d>@-`dOfi[gI,al(');
define('NONCE_KEY',        'NjirF]?&.L?!e*~$rQax{iWIcsgFn2s{;|Sl4pcBrKBb!5k:ZnbZT#A#2TY^G3WL');
define('AUTH_SALT',        'n#6|nF[;D~7Hwb8;vl#rXCZWWK5IN9gT@`T>hW),-(p&BP90}Gv+r4DED]|-lPd,');
define('SECURE_AUTH_SALT', 'q~M)4&XRpqRNL`@6|</;_-HDYh/jIlU8u_@ul.TKhtINfBq.$;DX5++Hmw|fcC#p');
define('LOGGED_IN_SALT',   '$R2_mS_D{_-=yi]wU_/pu1<hP$+uxbg.A-rMTHx<+G}^((<IRE.|a%l`7wu0J@~,');
define('NONCE_SALT',       '>[Aw0zsC+vExbmJmnMOzz1my~pt,I4-0V~s{mVQ).zHe$=%{O<Ep.m&SQUCEZ*TT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
