
<!DOCTYPE html lang="ja">
<!--[if lte IE 9]><html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

       	<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,300italic,400italic' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/custome1/css/bootstrap.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/custome1/css/styles.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/custome1/css/animate.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/custome1/css/odometer-theme-default.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/custome1/css/responsive.css">
        <script src="<?php bloginfo('template_url'); ?>/custome1/js/odometer.min.js"></script>    
    </head>

    <body>  <!-- class="initing loading page-landing" data-section="landing"-->      
        <div id="content-inner">
        <div class="header-fixed-top">
		    <div class="container-fluid">
		        <div class="header-content">
		            <a class="navbar-brand" href="http://localhost/westPhp/index.php?set=1"><img src="<?php bloginfo('template_url'); ?>/custome1/img/images/grand-logo.png" alt="logo"></a>
		            <ul class="header-coins-counter">
		                <li><p>山田さん</p></li>
		                <li><label class="odometer" id="odometer">000</label><i><img class="animated pulse infinite" src="<?php bloginfo('template_url'); ?>/custome1/img/images/coins.png" alt="img"></i></li>
		                <li><p>獲得ポイント</p></li>
		                <li class="coins-dropdown hide">
		                    <ul class="my-page-options">
		                        <li>
		                            <a href="my-page-1.php">ポイント確認 <small class="fa fa-angle-right"></small></a>
		                        </li>
		                        <li>
		                            <a href="my-page-2.php">登録情報の変更 <small class="fa fa-angle-right"></small></a> 
		                        </li>
		                        <li>
		                            <a href="logout.php">ログアウト <small class="fa fa-angle-right"></small></a> 
		                        </li>
		                    </ul>
		                </li>
		            </ul>
		        </div>
		    </div>
		</div>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		    <div class="container-fluid"> 
		        <div class="navbar-header">
		            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-top">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>

		        </div>

		        <!-- Collect the nav links, forms, and other content for toggling -->
		        <?php $tab = explode('.php', basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']));  

		        ?>
		        <div class="collapse navbar-collapse center" id="navbar-top">
		            <ul class="nav navbar-nav">
		                <li><a class="menu-icons" href="/west/game.php"><i class="icon icon-game"></i><span>ゲーム</span></a></li>
		                <li><a class="menu-icons" href="/west/campaign.php"><i class="icon icon-campaign"></i><span>キャンペーン</span></a></li>
		                <li><a class="menu-icons" href="/west/comics2.php"><i class="icon icon-comics"></i><span>コミック</span></a></li>
		                <li><a class="menu-icons" href="/west/interview.php"><i class="icon icon-interview"></i><span>インタビュー</span></a></li>
		                <li><a class="menu-icons" href="/west/value.php"><i class="icon icon-value"></i><span>価値組生活</span></a></li>
		                <li><a class="menu-icons" href="/west/questionnaire.php"><i class="icon icon-questionare"></i><span>アンケート</span></a></li>
		                <li><a class="menu-icons" href="/west/west-brand.php"><i class="icon icon-west-brand"></i><span>ウエストについて</span></a></li>
		                <li><a class="menu-icons" href="/west/west-hunt.php"><i class="icon icon-west-hunt"></i><span>West Hunt</span></a></li>
		                <li><a class="menu-icons" href="/west/landmark.php"><i class="icon icon-landmark"></i><span>喫煙所マップ</span></a></li>

		            </ul>
		        </div><!-- /.navbar-collapse -->
		    </div><!-- /.container -->
		</nav>
		<script type="text/javascript">
		(function($) {
			$(document).ready( function() {
				setTimeout(function() {
		            $('.odometer').html('219');
		        }, 1000);
			});
		})(jQuery);
		    /*$(document).ready(function() {
		        
		        $('html').click(function() {
		            $('.coins-dropdown').addClass('hide');
		        });

		        $('.header-coins-counter').click(function(e) {
		            e.stopPropagation();
		        });

		        $(".header-coins-counter").hover(function() {
		            $(".coins-dropdown").toggleClass("hide");

		        });


		    });*/
		</script>


            