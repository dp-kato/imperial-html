<?php
/*
Template name: comic
*/
get_header('custome1'); ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="page-content row">
                        <div class="col-md-12">
                            <div class="inner-page-header">                                
                                <img src="<?php bloginfo('template_url'); ?>/custome1/img/images/comics-grey.png" alt="img">
                                <!-- <h3>コミック</h3> -->
                                <h3><?php single_post_title(); ?></h3>
                                <a class="grey-bg btn-inner-page-header">Share</a>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="comics-slider">
                                <ol class="breadcrumb">
                                    <li><a href="#">By <?php the_author_meta( 'user_nicename' , $post->post_author ); ?></a></li>
                                    <li class="active"><?php echo get_the_date('F j, Y', $post->ID); ?></li>
                                </ol>
                                <div class="col-md-12">
                                    <p><?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?><?php endwhile; else: ?>
                                        <?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>
                                    <?php endif; ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-12">
                            <h3 class="author-description">Author 1<span>-Same Author</span></h3>
                            <div class="row">
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-man.jpg" alt="img">                                            
                                        </div>
                                        <div class="new-entry">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/new-tag.png" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-man.jpg" alt="img">                                            
                                        </div>                                       
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-man.jpg" alt="img">                                            
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2-5">
                                    <div class="comics-tile">
                                        <div class="comics-listing">
                                            <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/comics/img-comics-child.jpg" alt="img">
                                        </div>
                                        <div class="comics-listing-text">
                                            <a href="#"> <i class="fa fa-angle-right"></i></a>
                                            <p>Lorem Ipsum dolor sit amet</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->




                    </div>
                </div>

            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="footer">
                        <ul class="footer-links">
                            <li><a>About us</a></li>
                            <li><a>News</a></li>
                            <li><a>FAQs</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="below-footer">
                        <ul class="footer-links">
                            <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                            <li><a>Updated Privacy Policy</a></li>
                        </ul>
                        <p></p>
                    </div>
                    <div class="warning-footer">
                        <div class="warning-text">
                            <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                            <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                            <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php bloginfo('template_url'); ?>/custome1/js/bootstrap.js"></script>      
        
    </body>

</html>
