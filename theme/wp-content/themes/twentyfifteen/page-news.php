<?php
/*
Template name: news
*/
get_header('custome1'); ?>
 
<div class="container-fluid">
    <div class="row">
        <div class="page-content row">
            <div class="col-md-12">
                <div class="inner-page-header">                                
                    <img src="<?php bloginfo('template_url'); ?>/custome1/img/images/news/news-grey.png" alt="img">
                    <!-- <h3>価値組生活<small>1,500円Contents</small></h3> -->
                    <h3><?php single_post_title(); ?></h3>
                </div>
                <p class="inner-page-header-date"><?php echo get_the_date('F j, Y', $post->ID); ?></p>

            </div>
            <?php
            //$template_name = get_post_meta( $post->ID, '_wp_page_template', true );
            ?> 
            <div class="col-md-12">
                <div class="row">
                    <div class="interview-content">
                        <div class="col-md-9">
                            <div class="interview-banner img-1500yen">
                                <!-- <img src="<?php //bloginfo('template_url'); ?>/custome1/img/images/interview/img-banner-1500.png" alt="img" style="width:100%"> -->
                                <?php echo get_the_post_thumbnail( $post->ID, array( 800, 308) ); // Other resolutions?>
                            </div>
                            <!--<div class="interview-banner-text">
                                <img src="assets/img/images/interview/icon-brand-interview.png" alt="img">
                                <h3>“NEW YORK MINUTE”を生きる<br>男たちのために</h3>

                            </div>-->
                            <div class="question-answer">
                                <p class="question"></p>
                                <!-- <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                    運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                                <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                                <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                    運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                    それぞれの投資額に応じてすべて投資家に帰属します。</p> -->
                                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                        <?php /* 

                                            $content = wp_get_single_post( $post->ID);
                                            $trimmed_content = substr($content->post_content, 0, 50);
                                            $trimmed_content2 = substr($content->post_content, 50);
                                            echo $trimmed_content;
                                            */
                                        ?></p>
                                        <?php endwhile; else: ?>
                                        <?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>
                                    <?php endif; ?>
                            </div>
                            <div class="center">
                                
                                <?php previous_post_link('%link', '前の記事を読む'); ?> 
                                <?php next_post_link('%link', '次の記事を読む'); ?> 
                            </div>
                            <div id="additional-QuesAns" style="display:none" >
                                <?php echo $content->post_content; ?>
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <ul class="other-interviews">
                                <li class="other-interviews-heading">
                                    ニュース一覧
                                </li>
                                <?php $pages = get_posts(array(
                                            'post_type' => 'page',
                                            'meta_key' => '_wp_page_template',
                                            'meta_value' => get_post_meta( $post->ID, '_wp_page_template', true ),
                                    ));
                                    foreach($pages as $page){?>
                                <li>
                                    <div class="interview-listing">                                                    
                                        <div class="interview-listing-text">
                                            <?php echo get_the_post_thumbnail( $page->ID, array( 50, 50) );?>
                                            <p class="text-small"><?php echo get_the_date('F j, Y', $page->ID); ?></p>
                                            <p><?php echo $page->post_title?></p>
                                        </div>
                                    </div>
                                </li>
                                <?php }?> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                <li><a>Updated Privacy Policy</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>

<script src="<?php bloginfo('template_url'); ?>/custome1/js/jquery.js"></script>     
<script src="<?php bloginfo('template_url'); ?>/custome1/js/odometer.min.js"></script>       
<script src="<?php bloginfo('template_url'); ?>/custome1/js/bootstrap.js"></script>      
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/custome1/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>



