<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>利用規約</h3>
                </div>
                <div class="panel-content grey-border margin-40">
                    <div class="col-md-12">
                        <div class="privacy">
                            <p class="text margin-40">当サイトは、インペリアル・タバコ・ジャパン株式会社（以下当社といいます。）および当サイトの運営に関して業務を委託している当社のビジネスパートナーが運営しています。</p>
                            <p class="text">当サイトには、たばこ製品に関する情報が含まれています。</p>
                            <p class="text">当サイトは未成年喫煙防止の観点から、満20歳以上の喫煙者である方のみを対象としています。</p>
                            <p class="text">当サイトのご利用には、お客様の年齢等を確認後に当社が発行するIDおよびパスワードが必要となります。</p>
                            <p class="text">IDおよびパスワードをお持ちでない方は、当社所定の手続きに従い、発行のお申し込みをしてください。</p>
                            <p class="text margin-40">当サイトは、当社からのIDおよびパスワードの発行を受けたお客様ご本人のみがご利用いただけるものですので、お客様ご本人によるIDおよびパスワードの管理、および他の方への貸与等
                                をなされないよう、お願いいたします。</p>
                            <p class="text margin-40">当サイトのご利用にあたっては上記および、以下の内容をお読みになり、すべてに同意された場合のみ、ご利用ください。なお当サイトを利用された場合には、本利用規約のすべてに
                                同意いただいたものとみなします。当利用規約の内容は、予告なく変更される場合があります。この場合、変更後の当利用規約が適用されますので、最新の内容をご確認ください。
                                なお、当利用規約にご同意いただけない場合には、当サイトのご利用をお控えくださいますようお願いいたします。</p>
                            <p class="head">著作権等</p>
                            <p class="text margin-40">当サイト内で使用されている著作物、その他一切の情報にかかわる著作権その他の権利は、当社に帰属しています。したがって、法律によって認められる範囲（私的使用など）を超えて、
                                当サイトで使用されている内容を使用することはできません。</p>
                            <p class="head">商標</p>
                            <p class="text margin-40">当サイト内で使用されている商標にかかる権利は、当社に帰属しています。したがって、法律によって認められる場合を除き、当社に無断で使用することはできません。</p>
                            <p class="head">メール</p>
                            <p class="text margin-40">当社からお客様にお送りする電子メールの内容に関する著作物はすべて当社に帰属しており、当社に無断で転用・転送はできません。</p>
                            <p class="head">当サイトへのリンク</p>
                            <p class="text">当サイトは、未成年者喫煙防止の観点から、満20歳以上の喫煙者である方のみを対象としておりますので、</p>
                            <p class="text margin-40">お客様の運営するサイト、ブログ等から当サイトへのリンクを張ることはご遠慮ください。</p>
                            <p class="head">通信料および接続料</p>
                            <p class="text margin-40">当サイトにかかる通信料および接続料はお客様のご負担になります。</p>
                            <p class="head">免責条項</p>
                            <p class="text">当サイトへの情報掲載に際し、当社は細心の注意を払っていますが、これらの情報の正確性を保証するものではありません。また当サイト上の情報は予告なしに 変更される場合、</p>
                            <p class="text margin-40">当サイトの運営が中断・中止される場合があります。当社は、お客様が当サイトを利用された際、または利用できなかった際に、お客様自身あるいは第三者に生じた不利益および損害について、
                                いかなる責任を負うものではありません。</p>
                            <p class="head">個人情報</p>
                            <p class="text margin-40">当サイトでは、お客様からご提供いただいた個人情報を尊重し、「プライバシーポリシー」の各規定に従ってこれを取り扱うことで、個人情報の保護に努めております。
                                詳しくは弊社「プライバシーポリシー」をご覧下さい。</p>
                            <p class="head">個人情報以外の情報</p>
                            <p class="text margin-40">当サイトを通じて、当社にお送りいただいた情報（個人情報を除く）につきましては、当社は、お客様または第三者に対し、検討・報告義務、秘密保持義務、対価の支払いの
                                義務等を負うものではありません。</p>
                            <p class="head">利用環境</p>
                            <p class="text">Windows、Mac OS Xで動く日本語が表示可能なWebブラウザに対応します。</p>
                            <p class="text">また、SSL暗号化通信が利用できる環境が必要となります。</p>
                            <p class="text">コンテンツを快適にご覧いただくために、以下の環境を推奨いたします。</p>
                            <p class="text">・Windowsをお使いの場合:</p>
                            <p class="text">　　推奨OS：Windows 7 , Windows 8</p>
                            <p class="text">　　Microsoft Internet Explorer 9.0以降</p>
                            <p class="text">　　Mozilla FireFox 最新版</p>
                            <p class="text">　　Google Chrome 最新版</p>
                            <p class="text">・Macintoshをお使いの場合</p>
                            <p class="text">　　推奨OS：Mac OS X　10.6以降</p>
                            <p class="text">　　Mozilla FireFox 最新版</p>
                            <p class="text">　　Safari 最新版</p>
                            <p class="text">　　Google Chrome 最新版</p>
                            <p class="text">・携帯電話（フィーチャーフォン）をお使いの場合</p>
                            <p class="text">　　2009年以降発売のNTT Docomo, au, Softbankの携帯電話</p>
                            <p class="text">　　※一部機種ではご利用できない場合もございます。予めご了承ください。</p>













                            <p class="head"></p>
                            <p class="text margin-40"></p>
                            <p class="text"></p>
                        </div>
                    </div>  


                </div>
                <div class="change-password-span">
                    <a class="white-proceed btn-refresh btn-privacy">TOPへ戻る</a>
                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
