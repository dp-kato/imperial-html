                        






<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="west-brandWrap page-content row">
                <div class="inner-page-header margin-15">                                
                    <img src="assets/img/images/west-brand/west-brand-grey.png" alt="img">
                     <h3>ウエストについて</h3>

                     <ul class="subMenu-brand">
                         <li><a href="#">Westの3つのValue</a></li>
                         <li class="active"><a href="#">History</a></li>
                         <li><a href="#">Priducts</a></li>
                     </ul>
                </div>

                <div class="content_history">
                    <div class="history_nav">
                        <h3><img src="assets/img/images/west-brand/bg-history-title.png" height="85" width="1000" alt="History"></h3>
                        <ul class="faq-links">
                            <li><a href="#link-1" rel='link-1'><img src="assets/img/images/west-brand/btn-history01.png" alt="img"></a></li>
                            <li><a href="#link-3" rel='link-3'><img src="assets/img/images/west-brand/btn-history02.png" alt="img"></a></li>
                            <li><a href="#link-4" rel='link-4'><img src="assets/img/images/west-brand/btn-history03.png" alt="img"></a></li>
                            <li><a href="#link-5" rel='link-5'><img src="assets/img/images/west-brand/btn-history04.png" alt="img"></a></li>
                            <li><a href="#link-6" rel='link-6'><img src="assets/img/images/west-brand/btn-history05.png" alt="img"></a></li>
                            <li class="single"><a href="#link-7" rel='link-7'><img src="assets/img/images/west-brand/btn-history06.png" alt="img"></a></li>
                            <li class="single"><a href="#link-8" rel='link-8'><img src="assets/img/images/west-brand/btn-history07.png" alt="img"></a></li>
                            <li class="single"><a href="#link-9" rel='link-9'><img src="assets/img/images/west-brand/btn-history08.png" alt="img"></a></li>
                        </ul>
                    </div>
                    <p id="link-1"><img src="assets/img/images/west-brand/img_history01.jpg" height="298" width="1000" alt="1981 ドイツ生まれのウエスト。"></p>
                    <p id="link-2"><img src="assets/img/images/west-brand/img_history02.jpg" height="571" width="1000" alt="1981-1986 ウエスト誕生。そして、ドイツのリーディングブランドに。"></p>
                    <p id="link-3"><img src="assets/img/images/west-brand/img_history03.jpg" height="552" width="1000" alt="1987-1995 こだわりの味わいを試せ。常識への挑戦を続けるウエスト。"></p>
                    <p id="link-4"><img src="assets/img/images/west-brand/img_history04.jpg" height="781" width="1000" alt="1996-2000 スポンサー契約開始。F１チーム、「ウエスト・マクラーレン・メルセデス」"></p>
                    <p id="link-5"><img src="assets/img/images/west-brand/img_history05.jpg" height="540" width="1000" alt="2001-2003 驚嘆と興奮、そして歓喜の一服を。"></p>
                    <p id="link-6"><img src="assets/img/images/west-brand/img_history06.jpg" height="620" width="1000" alt="2004-2008 チャンスと可能性に満ちあふれた毎日。最初のきっかけを、ウエストと共に。"></p>
                    <p id="link-7"><img src="assets/img/images/west-brand/img_history07.jpg" height="562" width="1000" alt="2009 発売から30年目で、初めてのパッケージデザイン変更。-自らを表現せよ-"></p>
                    <p id="link-8"><img src="assets/img/images/west-brand/img_history08.jpg" height="640" width="1000" alt="2010-2013 得られるものは、驚きのアイデアと体験。欲するものは、ここにある。"></p>
                    <p id="link-9"><img src="assets/img/images/west-brand/img_history09.jpg" height="614" width="1000" alt="2014 再起動 日本限定パッケージが登場。"></p>
                </div>
                
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">


    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });
        });
        $('.panel-heading .panel-title a').click(function() {
            //$(this).toggleClass('active');
            $('.panel-heading .panel-title a').removeClass('active');
            var newValue = $(this).attr('href').replace('#', '');
            var current = $(this);
            //current.addClass('active');
            if (!$('#' + newValue).hasClass('in')) {
                current.addClass('active');
            } else {
                current.removeClass('active');
            }

        });

        $('.faq-links li a').click(function() {
            var target = $(this).attr('rel');
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#" + target).offset().top - 95
            }, 1000);

        });

    });

</script>
</body>

</html>
