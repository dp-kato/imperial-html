                        






<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="west-brandWrap page-content row">
                <div class="inner-page-header margin-15">                                
                    <img src="assets/img/images/west-brand/west-brand-grey.png" alt="img">
                     <h3>ウエストについて</h3>

                     <ul class="subMenu-brand">
                         <li><a href="#">Westの3つのValue</a></li>
                         <li><a href="#">History</a></li>
                         <li class="active"><a href="#">Priducts</a></li>
                     </ul>
                </div>

                <div class="content_product">
                    <div class="item-head">
                        <div class="head_inn">
                            <ul class="itemNav01">
                                <li><a href="#productItem01"><img src="assets/img/images/west-brand/btn_product01.png" height="55" width="35" alt="ウエスト・レッド"></a></li>
                                <li><a href="#productItem02"><img src="assets/img/images/west-brand/btn_product02.png" height="55" width="35" alt="ウエスト・ディープブルー"></a></li>
                                <li><a href="#productItem03"><img src="assets/img/images/west-brand/btn_product03.png" height="55" width="35" alt="ウエスト・ブルー"></a></li>
                                <li><a href="#productItem04"><img src="assets/img/images/west-brand/btn_product04.png" height="55" width="35" alt="ウエスト・シルバー"></a></li>
                                <li><a href="#productItem05"><img src="assets/img/images/west-brand/btn_product05.png" height="55" width="35" alt="ウエスト・ホワイト"></a></li>
                                <li><a href="#productItem05"><img src="assets/img/images/west-brand/btn_product06.png" height="65" width="35" alt="ウエスト・ホワイト100"></a></li>
                            </ul>
                            <ul class="itemNav02">
                                <li><a href="#productItem06"><img src="assets/img/images/west-brand/btn_product07.png" height="55" width="35" alt="ウエスト・メンソール"></a></li>
                                <li><a href="#productItem06"><img src="assets/img/images/west-brand/btn_product08.png" height="65" width="35" alt="ウエスト・メンソールホワイト100"></a></li>
                            </ul>
                            <ul class="itemNav03">
                                <li><a href="#productItem07"><img src="assets/img/images/west-brand/btn_product09.png" height="55" width="35" alt="ウエスト・アイスフレッシュエイト"></a></li>
                                <li><a href="#productItem07"><img src="assets/img/images/west-brand/btn_product10.png" height="65" width="35" alt="ウエスト・アイスフレッシュワン100"></a></li>
                            </ul>
                            <ul class="itemNav04">
                                <li><a href="#productItem08"><img src="assets/img/images/west-brand/btn_product11.png" height="56" width="35" alt="ウエスト・デュエット6"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="itemWrap">
                        <div id="productItem01">
                            <img src="assets/img/images/west-brand/img_product01.png" height="600" width="1000" alt="ウエスト・レッド">
                        </div>
                        <div id="productItem02">
                            <img src="assets/img/images/west-brand/img_product02.png" height="600" width="1000" alt="ウエスト・ディープブルー">
                        </div>
                        <div id="productItem03">
                            <img src="assets/img/images/west-brand/img_product03.png" height="600" width="1000" alt="ウエスト・ブルー">
                        </div>
                        <div id="productItem04">
                            <img src="assets/img/images/west-brand/img_product04.png" height="600" width="1000" alt="ウエスト・シルバー">
                        </div>
                        <div id="productItem05">
                            <img src="assets/img/images/west-brand/img_product05.png" height="600" width="1000" alt="ウエスト・ホワイト ウエスト・ホワイト100">
                        </div>
                        <div id="productItem06">
                            <img src="assets/img/images/west-brand/img_product06.png" height="600" width="1000" alt="ウエスト・メンソール ウエスト・メンソールホワイト100">
                        </div>
                        <div id="productItem07">
                            <img src="assets/img/images/west-brand/img_product07.png" height="600" width="1000" alt="ウエスト・アイスフレッシュエイト ウエスト・アイスフレッシュワン100">
                        </div>
                        <div id="productItem08">
                            <img src="assets/img/images/west-brand/img_product08.png" height="600" width="1000" alt="ウエスト・デュエット6">
                        </div>
                    </div>
                </div>
                
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
