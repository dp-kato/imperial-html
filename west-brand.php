                        






<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="west-brandWrap page-content row">
                <div class="inner-page-header margin-15">                                
                    <img src="assets/img/images/west-brand/west-brand-grey.png" alt="img">
                     <h3>ウエストについて</h3>

                     <ul class="subMenu-brand">
                         <li class="active"><a href="#">Westの3つのValue</a></li>
                         <li><a href="#">History</a></li>
                         <li><a href="#">Priducts</a></li>
                     </ul>
                </div>

                <div class="content_value">
                    <p><img src="assets/img/images/west-brand/img-value01.jpg" height="600" width="1000" alt="Westの3つのValue"></p>
                    <p><img src="assets/img/images/west-brand/img-value02.jpg" height="600" width="1000" alt="シンプルなかたち"></p>
                    <p><img src="assets/img/images/west-brand/img-value03.jpg" height="600" width="1000" alt="高品質と低価格の両立"></p>
                    <p><img src="assets/img/images/west-brand/img-value04.jpg" height="600" width="1000" alt="本物の味わい"></p>

                    <div class="movie">
                        <iframe width="820" height="490" src="https://www.youtube.com/embed/QfAA9QSEMUA" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQ</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.      利用規約 </a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });
        });
        $('.panel-heading .panel-title a').click(function() {
            //$(this).toggleClass('active');
            $('.panel-heading .panel-title a').removeClass('active');
            var newValue = $(this).attr('href').replace('#', '');
            var current = $(this);
            //current.addClass('active');
            if (!$('#' + newValue).hasClass('in')) {
                current.addClass('active');
            } else {
                current.removeClass('active');
            }

        });

        $('.faq-links li a').click(function() {
            var target = $(this).attr('rel');
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#" + target).offset().top - 330
            }, 1000);

        });

    });

</script>
</body>

</html>
