<?php include('header.php'); 
function get_client_ip() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

echo $_SERVER['HTTP_HOST'];exit;
?>
<script
            src="http://maps.googleapis.com/maps/api/js">
        </script>

        <script type="text/javascript">

            function showMap() {
             
                //If HTML5 Geolocation Is Supported In This Browser
                if (navigator.geolocation) {
                        
                    //Use HTML5 Geolocation API To Get Current Position
                    navigator.geolocation.getCurrentPosition(function(position){
                        
                        //Get Latitude From Geolocation API
                        var latitude = position.coords.latitude;
                        
                        //Get Longitude From Geolocation API
                        var longitude = position.coords.longitude;
                        
                        //Define New Google Map With Lat / Lon
                        var coords = new google.maps.LatLng(latitude, longitude);
                        
                        //Specify Google Map Options
                        var mapOptions = {
                            zoom: 15,
                            center: coords,
                            mapTypeControl: true,
                            scrollwheel: false,
                            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL}, mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
                        var iconBase = 'assets/img/images/';
                        var contentString = '<div id="content" class="map-popup">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<div class="map-popup-header">' +
                                '<h1 id="firstHeading" class="firstHeading">（株）Westタバコ店</h1>' +
                                '<p>東京都中央区銀座0の0の00</p>' +
                                '<p>03-0000-0000</p>' +
                                '</div>' +
                                '<div id="bodyContent">' +
                                '<ul class="map-popup-list">' +
                                '<li>' +
                                '<a>' +
                                '<img src="' + iconBase + 'west-hunt/pack1.png"/>' +
                                '</a>' +
                                '</li>' +
                                '<li>' +
                                '<a>' +
                                '<img src="' + iconBase + 'west-hunt/pack2.png"/>' +
                                '</a>' +
                                '</li>' +
                                '<li>' +
                                '<a>' +
                                '<img src="' + iconBase + 'west-hunt/pack3.png"/>' +
                                '</a>' +
                                '</li>' +
                                '</ul>' +
                                '</div>' +
                                '</div>';

                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });


                        var marker = new google.maps.Marker({
                            position: coords,
                            map: map,
                            title: 'Uluru (Ayers Rock)',
                            icon: iconBase + 'west-marker.png',
                        });
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.open(map, marker);
                        });    
                 
                        }
                    );
                    
                }else {
                    
                    //Otherwise - Gracefully Fall Back If Not Supported... Probably Best Not To Use A JS Alert Though :)
                    alert("Geolocation API is not supported in your browser.");
                }
                
            }
            function initialize() {
                
                var mapProp = {
                    center: new google.maps.LatLng(51.508742, -0.120850),
                    zoom: 5,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                var iconBase = 'assets/img/images/';
                var contentString = '<div id="content" class="map-popup">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<div class="map-popup-header">' +
                        '<h1 id="firstHeading" class="firstHeading">（株）Westタバコ店</h1>' +
                        '<p>東京都中央区銀座0の0の00</p>' +
                        '<p>03-0000-0000</p>' +
                        '</div>' +
                        '<div id="bodyContent">' +
                        '<ul class="map-popup-list">' +
                        '<li>' +
                        '<a>' +
                        '<img src="' + iconBase + 'west-hunt/pack1.png"/>' +
                        '</a>' +
                        '</li>' +
                        '<li>' +
                        '<a>' +
                        '<img src="' + iconBase + 'west-hunt/pack2.png"/>' +
                        '</a>' +
                        '</li>' +
                        '<li>' +
                        '<a>' +
                        '<img src="' + iconBase + 'west-hunt/pack3.png"/>' +
                        '</a>' +
                        '</li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });


                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(51.508742, -0.120850),
                    map: map,
                    title: 'Uluru (Ayers Rock)',
                    icon: iconBase + 'west-marker.png',
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            }
            /*google.maps.event.addDomListener(window, 'load', initialize);*/
            showMap();
        </script>
            <div class="container-fluid">
                <div class="row">
                    <div class="page-content row">
                        <div class="col-md-12">
                            <div class="inner-page-header">                                
                                <img src="assets/img/images/west-hunt-grey.png" alt="img">
                                <h3>West Hunt</h3>
                                <a class="grey-bg btn-inner-page-header">Share</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="west-headings">
                                <p>店舗検索ができます。</p>
                                <p>登録されていない店舗は誰よりも先にHUNTしよう！　※スマートフォンでWEST HUNTに参加可能</p>
                            </div>
                            <div class="brand-packets">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p style="line-height:4.5">銘柄を選択する</p>
                                    </div>
                                    <div class="col-md-10">
                                        <ul class="packets">
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack1.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack2.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack3.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack4.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack5.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack6.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack7.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack8.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack9.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack10.png" alt="img"></a>
                                            </li>
                                            <li>
                                                <a><img src="assets/img/images/west-hunt/pack11.png" alt="img"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="west-hunt-search">
                                <div class="row">
                                    <div class="search-item">
                                        <div class="col-md-2">
                                            <p style="line-height:2">都市名・市町村から探す</p>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="west-hunt-search-select">
                                                <select><option>都道府県を選択</option></select>
                                                <select><option>市区町村を選択</option></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="west-mid">
                                            <p class="in-mid">または</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search-item">
                                        <div class="col-md-2">
                                            <p style="line-height:2">キーワードを入力して探す</p>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="west-hunt-search-select">
                                                <select><option>都道府県を選択</option></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="search-btn">
                                        <div class="col-md-offset-2 col-md-2">
                                            <a class="btn-read-more btn-search">検索</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="googleMap" style="width:100%;height:500px;margin-top:20px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="footer">
                        <ul class="footer-links">
                            <li><a>About us</a></li>
                            <li><a>News</a></li>
                            <li><a>FAQs</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="below-footer">
                        <ul class="footer-links">
                            <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                            <li><a>Updated Privacy Policy</a></li>
                        </ul>
                        <p></p>
                    </div>
                    <div class="warning-footer">
                        <div class="warning-text">
                            <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                            <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                            <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('footer.php');?>
        <script src="assets/js/jquery.js"></script>    
        <script src="assets/js/odometer.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>      
        <script type="text/javascript" src="assets/js/slider/slick.js"></script>
        <script type="text/javascript">
            $('.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });
        </script>
    </body>

</html>
