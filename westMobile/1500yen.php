<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-30"> 
                        <img src="assets/img/images/interview-grey.png" alt="img">
                        <h3>価値組生活<small>1,500円Contents</small></h3>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="row">
                    <div class="interview-banner-1500">
                        <img style="" src="assets/img/images/interview/1500yen-small.png" alt="img">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="inner-page-content">                 

                    <div class="question-answer">
                        <p class="question">そもそも投資信託とは？</p>
                        <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                            運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                        <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                        <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                            運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                            それぞれの投資額に応じてすべて投資家に帰属します。</p>
                    </div>

                    <div id="additional-QuesAns" style="display:none">
                        <div class="question-answer">
                            <p class="question">そもそも投資信託とは？</p>
                            <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                            <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                            <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                それぞれの投資額に応じてすべて投資家に帰属します。</p>
                        </div>
                        <div class="question-answer">
                            <div class="center margin-20">
                                <img src="assets/img/images/interview/img-yen.png" alt="img">
                            </div>
                            <p class="question">そもそも投資信託とは？</p>
                            <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                            <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                            <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                それぞれの投資額に応じてすべて投資家に帰属します。</p>
                        </div>
                        <div class="question-answer">
                            <p class="question">そもそも投資信託とは？</p>
                            <p class="answer">「投資信託（ファンド）」とは、一言でいえば「投資家から集めたお金をひとつの大きな資金としてまとめ、
                                運用の専門家が株式や債券などに投資・運用する商品で、その運用成果が投資家それぞれの投資額に応じて分配される仕組みの金融商品」です。</p>
                            <p class="answer margin-30">「集めた資金をどのような対象に投資するか」は、投資信託ごとの運用方針に基づき専門家が行います。</p>
                            <p class="answer">投資信託の運用成績は市場環境などによって変動します。投資信託の購入後に、投資信託の運用がうまくいって利益が得られることもあれば、
                                運用がうまくいかず投資した額を下回って、損をすることもあります。このように、投資信託の運用によって生じた損益は、
                                それぞれの投資額に応じてすべて投資家に帰属します。</p>
                        </div>
                        <div class="center">
                            <a class="white-proceed btn-refresh fixed-width">前の記事を読む</a>
                            <a class="white-proceed btn-refresh fixed-width">次の記事を読む</a>
                        </div>
                    </div>
                    <div class="center">
                        <a id="showAdditionalQuesAns" class="white-proceed btn-refresh fixed-width">もっとみる</a>
                    </div>


                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>1,500円コラム</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/interview/img5.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>1,500円で買える 驚きの商品！ </b></p>
                        </div>
                    </div>
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/interview/img5.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>1,500円で買える 驚きの商品！ </b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
