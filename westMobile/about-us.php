<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row about-usWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-30">                                
                        <img src="assets/img/images/west-brand-grey.png" alt="img">
                        <h3>About us</h3>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                   <div class="row">
                        <div class="about-us-main">
                           <img src="assets/img/images/about/img-about-main.jpg" alt="img">
                       </div>
                   </div>
            </div>
            <div class="about-us-body">
                <div class="about-body-text col-md-12">
                    <div class="about-body-heading">
                        <h3>インペリアルタバコについて</h3>
                        <p class="head">Total Tobacco Company</p>
                    </div>
                    <div class="about-body-content">
                        <p>インペリアルタバコ（Imperial Tobacco Group plc)はイングランド西部のブリストルに本社を置く英国のタバコ会社です。</p>
                        <p>世界160ヵ国以上でマーケットを持ち、紙巻タバコ、ファインカット（手巻き用刻み葉）タバコ、ローリングペーパー（巻紙）では世界トップシェアを誇っています。世界中に51ヵ所の工場を展開し、年間で3200億本の紙巻タバコを生産・販売しています。</p>
                        <p>ブランドラインアップは、紙巻タバコの「ウエスト」、「ダビドフ」、「ゴロワーズ」、「ジタン」、「モンテクリスト」、ファインカットの「ゴールデンバージニア」、ローリングペーパーの「リズラ」（ローリングペーパーでは世界第1位の売上）など。タバコに関するすべての製品を生産するグローバルタバコメーカーです。</p>
                    </div>
                </div>
                <div class="about-us-listing">
                    <img src="assets/img/images/about/img-about-usList.png" alt="">
                </div>  
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
