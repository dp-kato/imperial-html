<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-xs-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/campaign-grey.png" alt="img">
                        <h3>Campaign</h3>
                    </div>
                </div>
            </div> 
            <div class="col-xs-12">
                <div class="inner-page-content">
                    <p class="margin-30">入力項目確認</p>

                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <p class="margin-30">項目</p>
                    <p class="margin-30 head">テキストテキストテキスト</p>
                    <div class="center margin-30">                            
                        <a class="white-proceed btn-refresh fixed-width black">修正する</a>
                    </div>
                    <div class="center margin-30">                            
                        <a class="white-proceed btn-refresh fixed-width">登録する</a>
                    </div>
                </div>          
            </div>

        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
