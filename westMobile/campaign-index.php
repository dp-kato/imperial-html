<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-xs-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/campaign-grey.png" alt="img">
                        <h3>Campaign</h3>
                    </div>
                </div>
            </div> 
            <div class="col-xs-12">
                <div class="inner-page-content">
                    <p>Date</p>
                    <div class="campaign-banner">
                        <img src="assets/img/images/img-campaign-banner.jpg" alt="img">
                        <div class="campaign-banner-text">
                            <p>バナー</p>
                        </div>
                    </div>
                    <p class="head">【概要】</p>
                    <p class="text margin-30">貯めたポイントに応じて好きなコースに応募。
                        総計380名様に当たるウエストスマートライフをプレゼント！</p>
                    <p class="head">【期間】</p>
                    <p class="text margin-30">2015年4月24日(金)～7月31日（金）</p>
                    <p class="head">【応募方法】</p>
                    <p class="text margin-30">A～Cのコースを選んで応募ボタンを押してください。</p>
                    <table class="table table-img-middle margin-40">
                        <tr>
                            <td><img src="assets/img/images/img-campaign-banner.jpg" alt="img" width="100" height="100"></td>
                            <td><p class="dot">Aコース</p></td>
                        </tr>
                        <tr>
                            <td><img src="assets/img/images/img-campaign-banner.jpg" alt="img" width="100" height="100"></td>
                            <td><p class="dot">Bコース</p></td>
                        </tr>
                        <tr>
                            <td><img src="assets/img/images/img-campaign-banner.jpg" alt="img" width="100" height="100"></td>
                            <td><p class="dot">Cコース</p></td>
                        </tr>
                    </table>
                    <div class="center margin-30">                            
                        <a class="white-proceed btn-refresh fixed-width">応募する</a>
                    </div>
                </div>          
            </div>










            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>その他のキャンペーン</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/img-new-campaign.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>テキストテキストテキ ストテキストテキスト ストテキストテキスト テキストテキストテキ ストテキストテキスト ストテキストテキスト</p>
                        </div>
                    </div>
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/img-new-campaign.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>テキストテキストテキ ストテキストテキスト ストテキストテキスト テキストテキストテキ ストテキストテキスト ストテキストテキスト</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
