<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-xs-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/campaign-grey.png" alt="img">
                        <h3>キャンペーン</h3>
                    </div>
                </div>
            </div> 
            <div class="col-xs-12">
                <div>
                    <div class="campaign-banner">
                        <div class="img-banner-campaign">
                            <img src="assets/img/images/campaign/img-banner-1-2.jpg" alt="ïmg"/>
                        </div>
                    </div>
                    <ul class="banner-top-list">
                        <li>必ずもらえる</li>
                        <li>何度でも応募可</li>
                        <li>使用ポイント：500pt</li>
                    </ul>

                    <div class="campaign-content">
                        <h3>西さん監修！イケてる壁紙ラボ</h3>
                        <p class="head">連載コミックのキャラクター西さんが忙しい皆様のために考えた壁紙を毎月更新していきます！</p>
                        <p>普段ヒーロー活動で忙しく、旅行にいけいない西さん。</p>
                        <p>忙しい皆様に少しでもリフレッシュしてもらいたいという気持ちで癒しの風景で壁紙を作成しました。</p>
                        <p>パッケージカラーをイメージした8つの風景という凝りよう！あなたがお吸いの銘柄に合わせてパソコンやスマホをドレスアップしてお楽しみください！</p>
                    </div> 
                    <div class="campaign-box-wrap">
                        <div class="row">
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-1.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio01" name="radio">
                                        <label for="radio01"><span></span> ウエスト・レッド</label> 
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-1.png" alt="img"/>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-2.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio02" name="radio">
                                        <label for="radio02"><span></span> ウエスト・ブルー</label> 
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-2.png" alt="img"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-3.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio03" name="radio">
                                        <label for="radio03"><span></span> ウエスト・ディープブルー</label> 
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-3.png" alt="img"/>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-4.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio04" name="radio">
                                        <label for="radio04"> <span></span> ウエスト・シルバー</label>  
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-4.png" alt="img"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-8.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio05" name="radio">
                                        <label for="radio05"><span></span> ウエスト・ホワイト</label>  
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-5.png" alt="img"/>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-5.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio06" name="radio">
                                        <label for="radio06"><span></span> ウエスト・メンソール</label> 
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-6.png" alt="img"/>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-6.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio07" name="radio">
                                        <label for="radio07"><span></span> ウエスト・アイスフレッシュ</label>   
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-7.png" alt="img"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="campaign-box">
                                    <div class="campaign-box-img">
                                        <img src="assets/img/images/campaign/img-box-7.png" alt="img"/>
                                    </div>
                                    <div class="campaign-box-content">
                                        <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio08" name="radio">
                                        <label for="radio08"><span></span> ウエスト・デュエット</label>   
                                        <div class="campaign-box-pack">
                                            <img src="assets/img/images/campaign/img-pack-8.png" alt="img"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="center margin-30">                            
                                <a class="white-proceed btn-refresh fixed-width">応募する</a>
                            </div>
                        </div>
                    </div>


                    
                </div>          
            </div>



            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>その他のキャンペーン</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <a href="campaign2.php">
                            <div class="new-campaigns-img">
                                <img src="assets/img/images/campaign/img-foot-1.png" alt="img"/>
                            </div>
                            <div class="new-campaigns-text">
                                <p>西さん監修！<br>癒しのひとときプレゼント</p>
                            </div>
                        </a>
                    </div>
                    <div class="new-campaigns">
                        <a href="campaign3.php">
                            <div class="new-campaigns-img">
                                <img src="assets/img/images/campaign/img-foot-2.png" alt="img"/>
                            </div>
                            <div class="new-campaigns-text">
                                <p>あなただけのコミック<br>ウエストプレゼント！</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
