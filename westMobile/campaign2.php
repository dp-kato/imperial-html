<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-xs-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/campaign-grey.png" alt="img">
                        <h3>キャンペーン</h3>
                    </div>
                </div>
            </div> 
            <div class="col-xs-12">
                <div>
                    <div class="campaign-banner">
                        <div class="img-banner-campaign">
                            <img src="assets/img/images/campaign/img-banner-2.jpg" alt="ïmg"/>
                        </div>
                    </div>
                    <ul class="banner-top-list list2">
                        <li>抽選</li>
                        <li>１人１回</li>
                        <li>使用ポイント：1,000pt</li>
                    </ul>

                    <div class="campaign-content">
                        <h3>ブランドサイトリニューアル記念第1弾　西さん監修！癒しのひとときプレゼント</h3>
                        <p class="head">ブランドサイトリニューアルを記念して連載コミックのキャラクター西さんが</p>
                        <p class="head">忙しい皆様のために癒しのひとときをプレゼントいたします！</p>
                        <p>普段ヒーロー活動で忙しく、旅行にいけいない西さん。</p>
                        <p>忙しい皆様に少しでもリフレッシュしてもらいたいという気持ちで</p>
                        <p>癒しのひとときをテーマにプレゼントを厳選！総計380名様にどーんとプレゼントいたします！</p>
                    </div> 
                    <div class="campaign-box-wrap">
                        <div>
                            <div class="row">
                                <div>
                                    <div class="campaign-2">
                                        <div class="campaign-box-img">
                                            <h4>松コース<span><img src="assets/img/images/campaign/ico_course01.png" height="45" width="45" alt="img"></span></h4>
                                            <img src="assets/img/images/campaign/img_course01.png" class="item-image"  alt="img"/>
                                        </div>
                                        <div class="campaign-box-content">
                                            <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio01" name="radio">
                                            <label class="red" for="radio01"><span></span>JTB商品券38,000円相当</label>  
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="campaign-2">
                                        <div class="campaign-box-img">
                                            <h4>竹コース<span><img src="assets/img/images/campaign/ico_course02.png" height="45" width="45" alt="img"></span></h4>
                                            <img src="assets/img/images/campaign/img_course02.png" class="item-image"  alt="img"/>
                                        </div>
                                        <div class="campaign-box-content">
                                            <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio02" name="radio">
                                            <label class="red" for="radio02"><span></span>ウエストオリジナル浴衣</label>  
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="campaign-2">
                                        <div class="campaign-box-img">
                                            <h4>竹コース<span><img src="assets/img/images/campaign/ico_course03.png" height="45" width="45" alt="img"></span></h4>
                                            <img src="assets/img/images/campaign/img_course03.png" class="item-image"  alt="img"/>
                                        </div>
                                        <div class="campaign-box-content">
                                            <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio03" name="radio">
                                            <label class="red" for="radio03"><span></span>西さん監修タオル</label>  
                                        </div>
                                    </div>
                                </div>
                                <div class="center margin-30">                            
                                    <a href="campaign-thanks.php" class="white-proceed btn-refresh fixed-width">応募する</a>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>          
            </div>




            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>その他のキャンペーン</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <a href="campaign.php">
                            <div class="new-campaigns-img">
                                 <img src="assets/img/images/campaign/img-foot-1-2.jpg" alt="img">
                            </div>
                            <div class="new-campaigns-text">
                                <p>西さん監修！<br>イケてる壁紙ラボ</p>
                            </div>
                        </a>
                    </div>
                    <div class="new-campaigns">
                        <a href="campaign3.php">
                            <div class="new-campaigns-img">
                                <img src="assets/img/images/campaign/img-foot-2.png" alt="img"/>
                            </div>
                            <div class="new-campaigns-text">
                                <p>あなただけのコミック<br>ウエストプレゼント！</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
