<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-xs-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/campaign-grey.png" alt="img">
                        <h3>キャンペーン</h3>
                    </div>
                </div>
            </div> 
            <div class="campaign-banner">
                <div class="img-banner-campaign">
                    <img src="assets/img/images/campaign/img-banner-3.png" alt="ïmg"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div>
                    <ul class="banner-top-list list3">
                        <li>抽選で当たる</li>
                        <li>使用ポイント：300pt</li>
                    </ul>

                    <div class="campaign-content">
                        <h3>あなただけのコミックウエストプレゼント！</h3>
                        <p class="head">コミックウエストで人気の『タナカダファミリア』と『アーノルズはせがわ』が</p>
                        <p class="head">あなたのために書き下ろした漫画を色紙でプレゼント！</p>
                        <p>ダミー文章ダミー文章ダミー文章ダミー文章ダミー文章ダミー文章</p>
                        <p>応募の際に入力いただいたフリーアンサーを元に、『タナカダファミリア』と『アーノルズはせがわ』が</p>
                        <p>あなたのために漫画を描き下ろします！</p>
                    </div> 
                    <div class="campaign-box-wrap">
                        <div class="col-xs-12">
                            <div class="row">
                                <div>
                                    <div class="campaign-form">
                                        <div class="content-top-part-1">
                                            <h4 class="margin-15">ご希望のコースを選択してください。</h4>
                                            <div>
                                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio01" name="radio">
                                                <label  for="radio02"><span></span> タナカダファミリア コース</label>
                                            </div>
                                            <div>
                                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio02" name="radio">
                                                <label  for="radio02"><span></span> アーノルズはせがわ コース</label>
                                            </div>
                                        </div>
                                        <div class="content-middle-part-2">
                                            <h4 class="margin-15">あなたのプロフィールを入力してください。</h4>
                                            <div class="margin-15">
                                                <label>性別</label>
                                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio03" name="radio">
                                                <label for="radio02"><span></span> 男性</label>
                                                <input type="radio" name="questions" value='貯金する' rel="percent-1" id="radio04" name="radio">
                                                <label for="radio02"><span></span>女性</label>
                                            </div>
                                            <div class="margin-15">
                                                <label>年齢</label><br>
                                                <input type="text" name="questions" rel="percent-1" id="text01" name="text">
                                                <label>歳</label>
                                            </div>
                                            <div class="margin-15">
                                                <label>職業</label><br>
                                                <input type="text" name="questions" rel="percent-1" id="text02" name="text">
                                            </div>
                                        </div>
                                        <div class="content-third-part-3">
                                            <h3>あなたの身の回りで起きたありえない出来事を教えてください。</h3>
                                            <textarea></textarea>
                                        </div>
                                    </div>
                                    <div class="center margin-30">                            
                                        <a class="white-proceed btn-refresh fixed-width">応募する</a>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>          
                </div>




                <div class="col-xs-12">
                    <div class="row">
                        <div class="slider-header">
                            <h4>その他のキャンペーン</h4>
                        </div>
                    </div>
                    <div class="slider single-item slider-padded">
                        <div class="new-campaigns">
                            <a href="campaign.php">
                                <div class="new-campaigns-img">
                                   <img src="assets/img/images/campaign/img-foot-1-2.jpg" alt="img">
                               </div>
                               <div class="new-campaigns-text">
                                <p>西さん監修！<br>イケてる壁紙ラボ</p>
                                </div>
                            </a>
                    </div>
                    <div class="new-campaigns">
                        <a href="campaign2.php">
                            <div class="new-campaigns-img">
                                <img src="assets/img/images/campaign/img-foot-1.png" alt="img"/>
                            </div>
                            <div class="new-campaigns-text">
                                <p>西さん監修！<br>癒しのひとときプレゼント</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
