<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row kachigumiWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-10"> 
                        <img src="assets/img/images/column-grey.png" alt="img">
                    <h3>価値組生活<br><small>No.2「果たしてビギナーズラックは本当なのか！？」</small></h3>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="inner-page-content">                 
                    <p class="inner-page-header-date">2015.05.01</p>
                    <div class="kachigumi-title">
                        <h3>果たしてビギナーズラックは本当なのか！？</h3>
                    </div>
                    <div class="kachigumi-content margin-20">
                        <p>どうも、こんにちは、ゴミ山地球です。</p>
                        <p>下北沢の、とあるパチンコ屋に来ました。
                        <br>こう見えてもギャンブルをほとんどやったことがありません。</p>
                    </div>
                    <div class="center margin-20">
                        <img src="assets/img/images/column/gomiyama/img_vol02_01.jpg"  alt="img">
                    </div>
                    <div class="kachigumi-content margin-20">
                        <p>前回出会った、競馬好きの駄菓子屋の店主に感化されて
                        <br>今日はスロットをやってみることにしました。</p>
                        <p>ただ、一人でギャンブルをするのも面白くないので
                        <br>今回はギャンブル好きの同居人 楢崎さん（シェアハウス）
                        <br>に真っ向から勝負します。</p>
                    </div>

                    <div id="additional-QuesAns" style="display:none">
                                
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_02.jpg"  alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>では、Westで浮いた3,000円を軍資金に
                            <br>真っ向から勝負をしたいと思います！</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_03.jpg"  alt="img">
                            <img src="assets/img/images/column/gomiyama/txt_vol02_01.png"  alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>では、気合を入れて…
                            <br>いざ出陣！！！！</p>
                            <p>※店内での撮影はできないため、絶妙な表情と的を射たセリフでお楽しみください。</p>
                            <p>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>10分後。
                            </p>
                        </div>
                        <div class="speech-bloc">
                            <div class="image"><img src="assets/img/images/column/gomiyama/ico_vol02_01.jpg"  alt="img"></div>
                            <div class="text-bloc">
                                <p><span>ゴミ山：なんだこれ…。。。
                                    <br>あと1,000円しかねぇーよ…。。</span></p>
                            </div>
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>何も光らない。何も起こらない。</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_04.jpg"  alt="img">
                        </div>

                        <div class="kachigumi-content margin-20">
                            <p>一方、同居人の楢崎は……</p>
                        </div>
                        <div class="speech-bloc">
                            <div class="image"><img src="assets/img/images/column/gomiyama/ico_vol02_02.jpg"  alt="img"></div>
                            <div class="text-bloc">
                                <p><span>楢崎：やばい…あと1,000円しかない…
                                <br>やる前から分かってたんだよ…！！</span></p>
                            </div>
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>3,000円でギャンブルなんかできるわけ
                            <br>ねぇーじゃねぇか！！！！！！！！</p>
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>開始15分。</p>
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>あっさりゴミ山撃沈。
                            <br>ビギナーズラックの『ビ』も出ることなく
                            <br>簡単に3,000円を使いきる。</p>
                        </div>
                        <div class="speech-bloc">
                            <div class="image"><img src="assets/img/images/column/gomiyama/ico_vol02_03.jpg"  alt="img"></div>
                            <div class="text-bloc">
                                <p><span>ゴミ山：なんだこれ…。。。
                                <br>お、お、お、お、終わった…
                                <br>楢崎さんもどうせ俺と同じ結果に…</span></p>
                            </div>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_05.jpg"  alt="img">
                        </div>
                        <div class="speech-bloc">
                            <div class="image"><img src="assets/img/images/column/gomiyama/ico_vol02_04.jpg"  alt="img"></div>
                            <div class="text-bloc">
                                <p><span>ゴミ山：え…え…えぇぇぇぇええええええ！？！？！？！</span></p>
                            </div>
                        </div>
                        <div class="speech-bloc">
                            <div class="image"><img src="assets/img/images/column/gomiyama/ico_vol02_05.jpg"  alt="img"></div>
                            <div class="text-bloc">
                                <p><span>楢崎：ラスト９枚で光ったどおおおおお！！！！！！
                                <br><br>ガチンコ、残り９枚で当てるこの男…！！
                                <br>これぞ正真正銘のギャンブラー！！！！！</span></p>
                            </div>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_06.jpg"  alt="img">
                            <img src="assets/img/images/column/gomiyama/img_vol02_07.jpg"  alt="img">
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_08.jpg"  alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>楢崎：分かるか？ ゴミ山！ 初心者がギャンブラーに勝とうなんて100年早いんや！！</p>
                            <p>ゴミ山：（くそ……ビギナーズラックなんか信じるか……！！）</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_09.jpg"  alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>ゴミ山：せめてタバコだけでもおごってくれ！ Westなら懐が傷まないだろ！！！</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_10.jpg"  alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>楢崎：ほらよ、West。
                            <br>ゴミ山：仏様！！！　お天道様！！　帝釈天様ぁぁぁぁ！！！！！</p>
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>帰宅後。</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gomiyama/img_vol02_11.jpg"  alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>ゴミ山：今回は、たまたま負けただけ！！！　当たれ！当たれ！当たれ！！！！</p>
                            <p><a href="#">Westのサイトにあるスロット</a>でリベンジを試みるゴミ山であった……</p>
                        </div>


                        <div class="center">
                            <a class="white-proceed btn-refresh fixed-width">前の記事を読む</a>
                            <a class="white-proceed btn-refresh fixed-width">次の記事を読む</a>
                        </div>
                    </div>
                    <div class="center">
                        <a id="showAdditionalQuesAns" class="white-proceed btn-refresh fixed-width">もっとみる</a>
                    </div>


                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>1,500円コラム</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/interview/img5.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>1,500円で買える 驚きの商品！ </b></p>
                        </div>
                    </div>
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/interview/img5.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>1,500円で買える 驚きの商品！ </b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
