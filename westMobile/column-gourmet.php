<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row kachigumiWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-10"> 
                        <img src="assets/img/images/column-grey.png" alt="img">
                    <h3>価値組生活<br><small>パクチー入れますか？」二郎インスパイア系タイラーメン「パクチー次郎」がパクチー山盛り過ぎる！</small></h3>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="inner-page-content">                 
                    <p class="inner-page-header-date">2015.05.01</p>
                    <div class="kachigumi-title">
                        <h3>パクチー入れますか？」二郎インスパイア系タイラーメン「パクチー次郎」がパクチー山盛り過ぎる！</h3>
                    </div>
                    <div class="kachigumi-content margin-20">
                        <p>な、なんだこれは〜〜〜っ！！どんぶりに山盛りのパクチー。しかしこれは中野のタイ料理店<br>「タイ屋台９９９（カオカオカオ）」の名物料理<b>「パクチー次郎」。</b></p>
                    </div>
                    <div class="center margin-20">
                        <img src="assets/img/images/column/gourmet/img_vol01_01.jpg" alt="img">
                    </div>
                    <div class="kachigumi-content margin-20">
                        <p class="fzl">「パクチー入れますか？」なんて言われて、つい「マシマシで」と答えた結果がコレだよ。</p>
                    </div>

                    <div id="additional-QuesAns" style="display:none">
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gourmet/img_vol01_02.jpg" alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>緑の未踏峰の下には、確かに麺とスープが隠れている。パクチー好きにはたまらぬマシマシパクチー。酸味と甘味のある薄味のスープはさっぱりとしており、二郎系ラーメンに比べれば意外と食べ進めやすい。</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gourmet/img_vol01_03.jpg" alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>「うまい！これはうまい！」パクチー好きの筆者にとっては、こんなに幸せな食べ物はない。これが店の屋号どおりたったの「９９９円」で食べられる。WESTで浮いた1,500円で私はこの幸せを買いたかったのだ。</b></p>
                            <p class="fzl">店長「1,500円？じゃあ500円の『バケツパクチー』も追加します？」
                            <br>バ、バケツパクチー？</p>
                        </div>
                        <div class="center margin-20">
                            <img src="assets/img/images/column/gourmet/img_vol01_04.jpg" alt="img">
                        </div>
                        <div class="kachigumi-content margin-20">
                            <p>ドーン！と登場、名物料理「バケツパクチー」。これはパクチー次郎どころの騒ぎではない。バケツからあふれてこぼれる緑の魔物……この量、迫力。</p>
                            <p class="fzl">これは麺もスープない100％生パクチーなのだ。</p>
                            <p>え、いや、ちょっとこれは勘弁して下さい……とも言えず。一旦出されたものを食べないわけにはいかない。私は一生で後にも先にもこの日ほどパクチーを食べた日はないだろう。</p>
                            <p>都合1,499円で思うさまパクチー三昧。あなたもいかがだろうか。</p>
                        </div>
                        <div class="kachigumi-profile">
                            <p>タイ屋台９９９（カオカオカオ）
                            <br>東京都中野区中野５-53-10　エイトゥリービル1F
                            <br>12:00～14:30/17:30～翌1:00（月曜定休）
                            <br><a href="https://www.facebook.com/thaiyatai999"><b>https://www.facebook.com/thaiyatai999</b></a>
                            <br></p>
                        </div>


                        <div class="center">
                            <a class="white-proceed btn-refresh fixed-width">前の記事を読む</a>
                            <a class="white-proceed btn-refresh fixed-width">次の記事を読む</a>
                        </div>
                    </div>
                    <div class="center">
                        <a id="showAdditionalQuesAns" class="white-proceed btn-refresh fixed-width">もっとみる</a>
                    </div>


                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>1,500円コラム</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/interview/img5.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>1,500円で買える 驚きの商品！ </b></p>
                        </div>
                    </div>
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/interview/img5.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>1,500円で買える 驚きの商品！ </b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
