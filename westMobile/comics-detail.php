<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/comics-grey.png" width="18" height="18" alt="img">
                        <h3>コミック<small>東さんの叫び</small></h3>
                        <a class=" btn-inner-page-header btn-comic-prof" href="#" data-toggle="modal" data-target="#modal-prof">キャラクター紹介</a>
                    </div>
                </div>
            </div>
            <div class="inner-page-content">
                
                <div class="col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="#">田中 光 </a></li>
                        <li><a href="#">2015.05.01</a></li>
                        <li class="active">vol.6</li>
                    </ol>
                    <div class="slider single-item slider-padded margin-40">
                        <div data-toggle="modal" data-target="#modal-comic">
                            <img src="assets/img/images/comics/img-azuma06.jpg" alt="img"/>
                        </div>
                        <div data-toggle="modal" data-target="#modal-comic">
                            <img src="assets/img/images/comics/img-azuma05.jpg" alt="img"/>
                        </div>
                        <div data-toggle="modal" data-target="#modal-comic">
                            <img src="assets/img/images/comics/img-azuma01.jpg" alt="img"/>
                        </div>
                    </div>


                    <div class="author-story">
                        <h3 class="author-description">東さんの叫び<span>-田中 光</span></h3>
                        <p>この文章はダミー。お笑い芸人・田中 光がネットで発表し、話題を集め始めているシュール系一コママンガ『サラリーマン山崎シゲル』。 </p>
                    </div>
                    <div class="slider slides-nav slider-padded">
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <a href="#" data-toggle="modal" data-target="#modal-comic">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/comics/img_azuma06.jpg" alt="img">                                            
                                    </div>
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <span> <i class="fa fa-angle-right"></i></span>
                                        <p>#東さんの叫び<br>vol.000</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <a href="#" data-toggle="modal" data-target="#modal-comic">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/comics/img_azuma05.jpg" alt="img">
                                    </div>
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <span> <i class="fa fa-angle-right"></i></span>
                                        <p>#東さんの叫び<br>vol.005</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <a href="#" data-toggle="modal" data-target="#modal-comic">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/comics/img_azuma01.jpg" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <span> <i class="fa fa-angle-right"></i></span>
                                        <p>#東さんの叫び<br>vol.001</p>
                                    </div>
                                </a>
                            </div>
                        </div>    
                    </div>









                    <div class="comic-other">
                        <a href="comics-detail2.php">
                            <div class="images">
                                <img src="assets/img/images/comics/img-list-hasegawa.jpg" alt="img"></span>
                                <span> <i class="fa fa-angle-right"></i>
                            </div>
                            <div class="text">
                                <h4>喫煙所物語 <small>- アーノルズはせがわ</small></h4>
                                <p>日常であったことや、思いついた事を
                                <br>出来るだけ笑えるマンガにしてます。</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal-prof" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>サラリーマンヒーロー<br>西さんと東さんのプロフィール</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <img src="assets/img/images/comics/img-prof-nishi.png" alt="img">
                </div>
                <div class="col-md-6">
                    <img src="assets/img/images/comics/img-prof-azuma.png" alt="img">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-comic" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>喫煙所物語</h4>
            </div>
            <div class="modal-body">
                <div class="comics-slider">
                    <div class="col-md-12">
                        <div class="slider single-item2">
                            <div>
                                <p>田中 光 | 2015.05.01 | vol.6</p>
                                <img src="assets/img/images/comics/main_auma06.jpg" alt="img"/>
                            </div>
                            <div>
                                <p>田中 光 | 2015.05.01 | vol.5</p>
                                <img src="assets/img/images/comics/main_auma05.jpg" alt="img"/>
                            </div>
                            <div>
                                <p>田中 光 | 2015.05.01 | vol.1</p>
                                <img src="assets/img/images/comics/main_auma01.jpg" alt="img"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('footer.php'); ?>

<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true,
        asNavFor: '.slides-nav'
    });
    $('.slides-nav').slick({
      dots: false,
      slidesToShow: 1,
      centerMode: false,
      focusOnSelect: true,
      asNavFor: '.single-item',
    });

    $('.single-item2').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });


    $('.responsive').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

</script>
</body>

</html>
