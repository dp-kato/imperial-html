<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12 margin-30">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/comics-grey.png" alt="img">
                        <h3>コミック</h3>
                        <a class="grey-bg btn-inner-page-header">Share</a>
                    </div>
                </div>
            </div>
            <div class="inner-page-content">
                <div class="author-story">
                    <h3 class="author-description">Title</h3>
                    <ol class="breadcrumb">
                        <li><a href="#">Author Name</a></li>
                        <li><a href="#">Date</a></li>
                        <li><a href="#">Series 1</a></li>
                        <li class="active">Series Title</li>
                    </ol>
                </div>

                <div class="slider single-item slider-padded margin-40">
                    <div>
                        <img src="assets/img/images/slider-images/img-slider-cat.png" alt="img"/>
                    </div>
                    <div>
                        <img src="assets/img/images/slider-images/img-slider-cat.png" alt="img"/>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="author-story">
                        <h3 class="author-description">Previous Comics<span>-Same Author</span></h3>
                        <p>この文章はダミーですこの文章はダミーです。 この文章はダミーですこの文章はダミーです。この文章はダミーですこの文章はダミーです。   作者作者</p>
                    </div>
                    <div class="slider responsive slider-padded margin-30">
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-man.jpg" alt="img">                                            
                                </div>
                                <div class="new-entry">
                                    <img src="assets/img/images/comics/new-tag.png" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-child.jpg" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-child.jpg" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-man.jpg" alt="img">                                            
                                </div>                                       
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="comics-tile">
                                <div class="comics-listing">
                                    <img src="assets/img/images/comics/img-comics-child-reading.jpg" alt="img">
                                </div>
                                <div class="comics-listing-text">
                                    <a href="#"> <i class="fa fa-angle-right"></i></a>
                                    <p>Lorem Ipsum dolor sit amet</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="single-story">
                            <div class="single-story-img">
                                <!--<img src="assets/img/images/comics/story-banner.png" alt="img">-->
                            </div>
                            <div class="single-story-text">
                                <a><i class="fa fa-angle-right"></i></a>
                                <p>タイトル</p>                                
                                <p class="small">説明が入ります。説明が入ります。説明が入ります。説明が 入ります。説明が入ります。説明が入ります。</p>
                            </div>
                        </div>
                    </div>










                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>

<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });


    $('.responsive').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

</script>
</body>

</html>
