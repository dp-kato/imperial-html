<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>FAQ</h3>
                </div>
                <div class="panel-content">

                    <div class="header-faq">
                        <div class="col-md-12">
                            <p>カテゴリ</p>
                        </div>
                        <ul class="faq-links">
                            <li><a href="#link-1" rel='link-1'>ウエストブランドサイトについて</a></li>
                            <li><a href="#link-2" rel='link-2'>ログインについて</a></li>
                            <li><a href="#link-3" rel='link-3'>「たばこ」Westについて</a></li>
                        </ul>
                    </div>

                    <div class="col-md-12">
                        <div class="lower-faq-panel">
                            <h5 id="link-1">ウエストブランドサイトについて</h5>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading1">
                                        <h4 class="panel-title">
                                            <a class="active" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                                たばこ「West（ウエスト）」ブランドサイトに登録すると何ができるようになるのですか？
                                            </a>                                
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                        <div class="panel-body">
                                            <p>たばこ「West（ウエスト）」ブランドサイトの会員になることで、Westのお得なキャンペーンをはじめインタビュー、Westの販売店（全国のコンビニエンスストア、たばこ店など）、喫煙所マップ、タバコの味に関する情報などを閲覧することができます。</p>
                                            <p>2015年4月27日のサイトリニューアルより、「サラリーマン山崎シゲル」で知られる田中光氏による、書き下ろし漫画「西さん」の連載を新たにご覧いただけるようになりました。また、Westブランドサイト内の「ゲーム」や「コミック」「アンケート」等様々なコンテンツを利用することで、Westポイント（Wポイント）が貯まり、貯まったWポイントでお得なキャンペーンにご応募いただけます。</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading2">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                                たばこ「West（ウエスト）」ブランドサイトの会員登録は有料ですか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                        <div class="panel-body">
                                           <p>たばこ「West（ウエスト）」ブランドサイトは、無料でご利用いただくことができます。</p>
                                       </div>
                                   </div>
                               </div>
                               <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading3">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                            携帯（フィーチャーフォン）のメールアドレスでたばこ「West（ウエスト）」ブランドサイトに登録することはできますか？
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                    <div class="panel-body">
                                        <p>携帯のメールアドレスでも会員登録の手続きをすることは可能ですが、携帯サイトでは限定されたコンテンツのみを提供しているため、PCやスマートフォン、タブレットで閲覧いただくことを推奨いたします。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading4">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                            携帯（フィーチャーフォン）でも「ウエストブランドサイト」を登録・閲覧できますか？
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                    <div class="panel-body">
                                        <p>携帯専用サイトをご用意しております。ブランド紹介やニュースなど一部のコンテンツを閲覧いただくことができます。Westブランドサイトの全てのコンテンツをご覧いただくためには、PCやスマートフォン、タブレットでアクセスいただけますようお願いいたします。Westサイト内で利用できるWポイントは、携帯専用サイトではご利用いただけませんこと予めご了承ください。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading5">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                            サイト閲覧の推奨環境を教えてください。
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                    <div class="panel-body">
                                        <p><a href="#">こちら</a>をご確認ください。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading6">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                            たばこ「ウエスト」ブランドサイトへの登録方法がわからないので教えてください。
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                    <div class="panel-body">
                                        <p><a href="#">トップページ</a>より、「初めての方」の「登録する」ボタンを押し、ページのガイドに従いご登録をお願いいたします。</p>
                                        <p>登録申請の手順は下記①～③の流れとなります。</p>
                                        <p>①利用規約およびプライバシーポリシーへのご同意
                                        <br>②メールアドレスの登録
                                        <br>③成人確認書類の写真画像アップロードとプロフィール情報の入力</p>
                                        <p>登録申請完了には数日かかる場合がありますので予めご了承ください。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading6-2">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6-2" aria-expanded="false" aria-controls="collapse6-2">
                                            たばこ「ウエスト」ブランドサイトの登録になぜ「成人確認書類」が必要なのですか？
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6-2">
                                    <div class="panel-body">
                                        <p>たばこ「West」ブランドサイトには、タバコ製品および、タバコ製品の販売店に関する情報などが含まれており、未成年喫煙防止の観点から、満20歳以上の喫煙者である方のみを対象としています。そのため、お客様が成人であること、ご本人様であることを「成人確認書類」にて確認させていただいております。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading6-3">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6-3" aria-expanded="false" aria-controls="collapse6-3">
                                            「成人確認書類」とは具体的に何ですか？
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6-3">
                                    <div class="panel-body">
                                        <p>成人確認書類の一例は次の通りです。
                                        <br>　-taspo
                                        <br>　-自動車運転免許証
                                        <br>　-パスポート
                                        <br>　-住民票の写し
                                        <br>　-健康保険証
                                        <br>　-小型船舶操縦免許証
                                        <br>その他、公的機関から発行され、氏名と満20歳以上であることが確認できる書類が該当します。</p>
                                        <p>上記以外でも組み合わせ等によって確認させていただける証明書等がございますので、ご不明な点は<a href="#">お問い合わせフォーム</a>よりお問い合わせください。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading6-4">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6-4" aria-expanded="false" aria-controls="collapse6-4">
                                            カメラ付き携帯電話ではないので、画像を送ることができません、ほかに画像を送る方法はありますか？
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6-4">
                                    <div class="panel-body">
                                        <p>お手数ではございますが、「成人確認書類」をFAXもしくは郵送でお送りいただくことになります。事前に<a href="#">お問い合わせフォーム</a>よりその旨ご連絡をいただけますようお願いいたします。</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <h5 id="link-2">ログインについて</h5>
                        <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading7">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                            会員登録したいのですが、メールの返信が届きません。
                                        </a>                                
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                                    <div class="panel-body">
                                        <p>迷惑メール対策機能により届いていない可能性がございますので、以下の方法にて受信ができるよう再設定をお願いいたします。
                                        <br>　▶携帯メール設定のドメイン指定受信で、"westonline.jp"をご指定ください。メール設定の詳細に関しては、ご利用の各携帯電話会社にお問い合わせをお願いいたします。
                                        <br>　▶PCメール迷惑メールフォルダに、"westonline.jp"からのメールが受信されていないかどうかご確認をお願いいたします。</p>
                                        <p>入力したメールアドレスが正しいかご確認の上、再度お試しいただきますようお願いいたします。なお、システムの都合により稀にお時間がかかる場合もございます。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading8">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                            登録したログイン用のメールアドレスを忘れてしまったためログインできません。
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                                    <div class="panel-body">
                                        <p>事務局での変更が必要になります。<a href="#">こちら</a>からご連絡ください。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading9">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                            パスワードを忘れてしまいました。
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                                    <div class="panel-body">
                                        <p><a href="#">こちら</a>よりパスワード再発行の手続きを行ってください。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading10">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                            パスワードを再設定してもログインできません。
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                    <div class="panel-body">
                                        <p>キャッシュおよびインターネット一時ファイル、Cookie、自動保存されたパスワードの削除をお試しください。</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <h5 id="link-3">「たばこ」Westについて</h5>
                        <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading25">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion3" href="#collapse25" aria-expanded="true" aria-controls="collapse25">
                                            たばこ「West（ウエスト）」はどのような銘柄がありますか？                                    
                                        </a>                                
                                    </h4>
                                </div>
                                <div id="collapse25" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading25">
                                    <div class="panel-body">
                                        <p>ウエスト・レッド(タール10mg/ニコチン0.7mg)
                                            <br>ウエスト・ディープブルー(タール8mg/ニコチン0.6mg)
                                            <br>ウエスト・ブルー(タール6mg/ニコチン0.5mg)
                                            <br>ウエスト・シルバー(タール3mg/ニコチン0.3mg)
                                            <br>ウエスト・ホワイト(タール1mg/ニコチン0.1mg)
                                            <br>ウエスト・ホワイト100(タール1mg/ニコチン0.1mg)
                                            <br>ウエスト・メンソール(タール6mg/ニコチン0.5mg)
                                            <br>ウエスト・メンソールホワイト100(タール1mg/ニコチン0.1mg)
                                            <br>ウエスト・アイスフレッシュエイト(タール8mg/ニコチン0.6mg)
                                            <br>ウエスト・アイスフレッシュワン100(タール1mg/ニコチン0.1mg)
                                            <br>ウエスト・デュエット(タール6mg/ニコチン0.6mg)
                                            <br>以上11銘柄、各380円</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading27">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse27" aria-expanded="false" aria-controls="collapse27">
                                                たばこ「West（ウエスト）」はどこで購入できますか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse27" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading27">
                                        <div class="panel-body">
                                            <p>たばこ「West（ウエスト）」は、全国のコンビニエンスストア、たばこ店、ドラッグストアなどでご購入いただけます。
                                            <br>お近くの店舗でウエストのお取り扱いがない場合は、<a href="#">お問い合わせフォーム</a>よりお問い合わせいただくか、Westブランドサイト内の<a href="#">WestHunt（取扱店検索）</a>よりお探しいただくことができます。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading28">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse28" aria-expanded="false" aria-controls="collapse28">
                                                たばこ「West（ウエスト）」について教えてください。どこの会社が販売しているのですか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse28" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading28">
                                        <div class="panel-body">
                                            <p>たばこ「West（ウエスト）」は、イギリスのインペリアル・タバコ・グループの子会社であるインペリアル・タバコ・ジャパン株式会社が日本国内で販売しています。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading29">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse29" aria-expanded="false" aria-controls="collapse29">
                                                インペリアル・タバコ社について教えてください
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse29" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading29">
                                        <div class="panel-body">
                                            <p>インペリアル・タバコは1901年に設立された国際的なタバコメーカーです。イギリスに本社を置き、商品はヨーロッパを中心に世界70カ国以上で販売されています。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading30">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse30" aria-expanded="false" aria-controls="collapse30">
                                                たばこ「West（ウエスト）」はどこの国で誕生したタバコですか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse30" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading30">
                                        <div class="panel-body">
                                            <p>1981年にドイツで誕生して、今では世界中で展開しているたばこブランドです。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading31">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse31" aria-expanded="false" aria-controls="collapse31">
                                                たばこ「West（ウエスト）」はいつから発売していますか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse31" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading31">
                                        <div class="panel-body">
                                            <p>日本では、2011年1月から発売しております。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading33">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse33" aria-expanded="false" aria-controls="collapse33">
                                                たばこ「West（ウエスト）」の特徴は？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse33" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading33">
                                        <div class="panel-body">
                                            <p>シンプルなつくり、本物の味わい、高品質と低価格の両立の3点です。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading34">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse34" aria-expanded="false" aria-controls="collapse34">
                                                たばこ「West（ウエスト）」はどんな味ですか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse34" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading34">
                                        <div class="panel-body">
                                            <p>日本人の好みに合うスムースで吸いやすい味わいになっています。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading37">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse37" aria-expanded="false" aria-controls="collapse37">
                                                たばこ「West（ウエスト）」に1ミリのロングとショ－トがあるが、タールとニコチンの差はあるのでしょうか。タバコのサイズが違うので体に吸収されるタールとニコチンに差があるのですか？
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse37" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading37">
                                        <div class="panel-body">
                                            <p>基本的にはタバコのタールとニコチン量に変わりは無く、タバコが長い分味が少しだけマイルドになります。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">


    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });
        });
        $('.panel-heading .panel-title a').click(function() {
            //$(this).toggleClass('active');
            $('.panel-heading .panel-title a').removeClass('active');
            var newValue = $(this).attr('href').replace('#', '');
            var current = $(this);
            //current.addClass('active');
            if (!$('#' + newValue).hasClass('in')) {
                current.addClass('active');
            } else {
                current.removeClass('active');
            }

        });
        
        $('.faq-links li a').click(function() {
            var target = $(this).attr('rel');
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("#" + target).offset().top - 55
            }, 1000);

        });

    });

</script>
</body>

</html>
