<?php if ($_REQUEST['set'] != 1) { ?>
    <div class="loading-indicator">
        <div class="lt"></div>
        <div class="rt"></div>
        <div class="loader"></div>
    </div>
<?php } ?>

<div class="modal fade" id="modal-comics" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <div class="comics-tile">
                        <div class="comics-listing">
                            <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                        </div>     
                        <div class="new-entry">
                            <img src="assets/img/images/comics/new-tag.png" alt="img">
                        </div>
                        <div class="comics-listing-text">
                            <a href="comics.php"> <i class="fa fa-angle-right"></i></a>
                            <p>Lorem Ipsum dolor sit amet</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="comics-tile">
                        <div class="comics-listing">
                            <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                        </div>                                       
                        <div class="comics-listing-text brown">
                            <a href="comics.php"> <i class="fa fa-angle-right"></i></a>
                            <p>Lorem Ipsum dolor sit amet</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-value" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>価値組生活</h4>
                <p>普段のタバコをWestへ乗り換えるとどれだけオトクになるのでしょうか。</p>
            </div>
            <div class="modal-body">
                <div class="col-md-6">
                    <div class="comics-tile">
                        <div class="comics-listing">
                            <img src="assets/img/images/modal-images/img-modal-3.png" alt="img">                                            
                        </div>
                        <div class="value-listing-text">
                            <a href="value.php"> <i class="fa fa-angle-right"></i></a>
                            <p class="value-listing-text-heading">Value Creator</p>
                            <p>あなたの「価値組生活」を シミュレーションしてみましょう！</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="comics-tile">
                        <div class="comics-listing">
                            <img src="assets/img/images/modal-images/img-modal-4.png" alt="img">                                            
                        </div>                                       
                        <div class="value-listing-text brown">
                            <a href="1500yen.php"> <i class="fa fa-angle-right"></i></a>
                            <p class="value-listing-text-heading">1500円コンテンツ</p>
                            <p>他のタバコよりCPの高いWESTが 提供する、1,500円の価値</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
