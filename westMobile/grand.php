<?php include('header.php'); ?>
<div id="content" class="">

    <?php //include('navigation.php'); ?>
    <div class="col-xs-12">
        <div class="row"> 
            <div class="page-content animated fadeInUp">
                <div class="col-xs-12 no-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-campaign.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline">
                                <img src="assets/img/images/icon-large/campaign.png" alt="img" width="43" height="51">
                            </div>
                        </div>
                        <div class="page-item-campaign">
                            <h3>キャンペーン</h3>
                            <p>貯まったポイントで応募しよう。</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-left-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-game.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline">
                                <img src="assets/img/images/icon-large/game.png" alt="img" width="44" height="46">
                            </div>
                        </div>
                        <div class="page-item-game">
                            <h3>ゲーム</h3>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-right-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-comics.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline">
                                <img src="assets/img/images/icon-large/comics.png" alt="img" width="46" height="46">
                            </div>
                        </div>
                        <div class="page-item-comics">
                            <h3>コミック</h3>
                            <p>シュールな笑いを提供する 田中光氏連載スタート！</p>
                            <!--<a data-toggle="modal" data-target="#modal-comics" href="#" class="white-proceed">もっと見る</a>-->
                        </div>
                    </div>
                </div> 
                <div class="col-xs-6 no-left-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-landmark.jpg" alt="img">
                        <div class="page-item-description-landmark">
                            <div class="landmark-values">
                                <img class="inner-img" src="assets/img/images/icon-large/landmark.png" alt="img" width="25" height="46">
                                <h3>喫煙所マップ</h3>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xs-6 no-right-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-west-hunt.jpg" alt="img">
                        <div class="page-item-description-west">
                            <div class="west-hunt-values">
                                <img class="inner-img"  src="assets/img/images/icon-large/west-hunt.png" alt="img" width="49" height="49">
                                <h3>West Hunt</h3>
                                <p>取扱店検索</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 no-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-interview.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline">
                                <img src="assets/img/images/icon-large/interview.png" alt="img" width="50" height="49">
                            </div>
                        </div>
                        <div class="page-item-interview">
                            <h3>インタビュー</h3>
                            <p>○○氏に聞く。価値あるもの を突き詰めるとやっぱりSimple Life!?</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 no-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-value.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline">
                                <img src="assets/img/images/icon-large/value.png" alt="img" width="37" height="48">                                                
                            </div>
                        </div>
                        <div class="page-item-value">
                            <h3>価値組生活</h3>
                            <p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit. </p>
                            <!-- <a data-toggle="modal" data-target="#modal-value" href="#" class="white-proceed">もっと見る</a>-->
                        </div>
                    </div>
                </div>                          
                <div class="col-xs-12 no-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-questionare.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline top-questionare">
                                <div class="page-item-questionare">
                                    <img src="assets/img/images/icon-large/questionare.png" alt="img" width="46" height="46">
                                    <p class="questionare-heading">アンケート</p>
                                </div>
                            </div>
                        </div>
                        <div class="questionare-items">
                            <div class="col-md-12">
                                <p>普通だと思っていたけど、実は変わり者・・・？ 2ステップアンケートで新たな自分発見？</p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img src="assets/img/images/img-spiderman.jpg" alt="img">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <img src="assets/img/images/img-batman.jpg" alt="img">
                            </div>
                            <div class="questionare-sumbit">
                                <a class="grey-bg" href="#">回答する</a>                                
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-xs-12 no-gap">
                    <div class="page-item">
                        <img src="assets/img/images/img-west-brand.jpg" alt="img">
                        <div class="page-item-description">
                            <div class="img-inline top-brand">
                                <img src="assets/img/images/icon-large/west-brand.png" alt="img" width="47" height="47">                                                
                            </div>
                            <div class="page-item-description-brand">
                                <div class="west-brand-values">
                                    <p>ウエストについて</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 no-gap">
                    <ul class="dates-calender">
                        <li class="calender-header">
                            <p class="header-text-left">ニュース</p>
                        </li>
                        <li>
                            <div class="calender-value">
                                <div class="col-xs-3 no-gap">
                                    <h3>12</h3>
                                    <div class="calender-value-month">
                                        <p>MAR</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <div class="calender-value-description">
                                        <p>全国各地の居酒屋でサンプルの配布イベント開催中！ (期間： 4月10日まで)</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="calender-value">
                                <div class="col-xs-3 no-gap">
                                    <h3>12</h3>
                                    <div class="calender-value-month">
                                        <p>MAR</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <div class="calender-value-description">
                                        <p>全国各地の居酒屋でサンプルの配布イベント開催中！ (期間： 4月10日まで)</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="calender-value">
                                <div class="col-xs-3 no-gap">
                                    <h3>12</h3>
                                    <div class="calender-value-month">
                                        <p>MAR</p>
                                        <p>2015</p>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <div class="calender-value-description">
                                        <p>全国各地の居酒屋でサンプルの配布イベント開催中！ (期間： 4月10日まで)</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 no-gap">
                    <div class="people-switch">
                        <img src="assets/img/images/img-people-switch.png" alt="img">
                        <a class="btn-campaign">友達を招待する</a>
                    </div>
                </div>

            </div>
        </div>
        
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    $('.btn-modal').click(function() {
        var dataTarget = $(this).attr('data-target');
        setTimeout(function() {
            $(dataTarget).find('div.modal-body').removeClass('modal-loading');
            $(dataTarget).find('div.single-item').removeClass('disappear');
            $(dataTarget).find('div.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });

        }, 500);
    });

</script>
</body>

</html>
