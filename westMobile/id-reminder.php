<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>IDを確認される方へ</h3>
                </div>
                <div class="panel-content">
                    <div class="col-md-12">
                        <p class="margin-30">お客様が登録された情報を以下に入力してください。IDの確認結果は、連絡用メールアドレスにお伝えいたします。</p>
                        <p class="margin-30">お客様の個人情報は、弊社にて厳重に管理を行います。また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。</p>
                        <p>お名前 <i>（必須）</i></p>
                        <div class="col-xs-12 no-gap">
                            <div class="col-xs-2">
                                <label class="in-middle">姓：</label>
                            </div>
                            <div class="col-xs-10 no-gap">
                                <input class="form-control  margin-15" type="text">
                            </div>
                            <div class="col-xs-2">
                                <label class="in-middle">名：</label>
                            </div>
                            <div class="col-xs-10 no-gap">
                                <input class="form-control margin-15" type="text">
                            </div>
                        </div>
                        <p class="margin-15">生年月日 <i>（必須）</i>　半角数字</p>
                        <input class="form-control" type="text">
                        <p class="margin-15">(例:1980年3月14日⇒19800314)</p>
                        <p>連絡用メールアドレス <i>（必須）</i></p>
                        <input class="form-control  margin-15" type="text">
                        <p>※連絡用のメールアドレスをご入力ください。</p>
                        <p>※ご記入いただいた内容に対し連絡をさせて頂く場合に使用させていただきます。</p>

                        <p>連絡用メールアドレス（確認） <i>（必須）</i></p>
                        <input class="form-control  margin-15" type="text">
                        <p>確認のためもう一度ご入力ください。</p>
                        <div class="center margin-20">                            
                            <a class="white-proceed btn-refresh fixed-width margin-30">送信する</a>
                        </div>
                        <p>※登録時の上記項目の内容をお忘れの方は、お手数で　すが、新たにIDのお申込みをお願いいたします。</p>
                        <p>※本サイトにてご提供いただくお客様の個人情報は、弊社にて厳重に管理を行います。また、お客様のご同意なしに守秘義務を負う業務</p>

                        <p>委　託先以外の第三者に開示、提供いたしません。</p>
                        <p>※本サイトにてご提供いただくお客様の個人情報は、IDの問合せ確認のために使用させていただきます。</p>

                        <p><i>【画面操作にあたっての注意事項】</i></p>
                        <p><i>ブラウザの戻るボタンは使えませんのでご注意ください。</i></p>


                    </div>                   
                </div>
            </div>
        </div>	
    </div>	
</div>


<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

</script>
</body>

</html>