
<!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

        <!--[if lt IE 9]><meta http-equiv="refresh" content="0;URL='/upgrade'"><![endif]-->

        <title>West</title>
        <link rel="shortcut icon" href="assets/img/images/favicon.ico">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/style-mobile.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/odometer-theme-default.css">
        <link href="assets/fonts/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/responsive.css"> 

        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/odometer.min.js"></script>
        <script>
            $(document).ready(function() {

<?php if ($_GET['set'] == 1) { ?>
                    setTimeout(function() {
                        $('body').unbind('mousewheel');
                        $("body").addClass("page-landing-after");
                    }, 1000);
<?php } ?>
                $('body').bind('mousewheel', function(e) {
                    if (e.originalEvent.wheelDelta / 120 > 0) {

                    } else {
                        /*$("#content").addClass("animated fadeInUp").removeClass("disappear");
                         setTimeout(function() {
                         $("#content").removeClass("animated fadeInUp");
                         $('body').unbind('mousewheel');
                         $('.odometer').html('219');
                         }, 1000);
                         
                         setTimeout(function() {
                         $("body").addClass("page-landing-after");
                         }, 1000);*/
                        window.location = 'grand.php';
                    }

                });
                $("#btnDropdown").click(function() {
                    /*$("#content").addClass("animated fadeInUp").removeClass("disappear");
                     setTimeout(function() {
                     $("#content").removeClass("animated fadeInUp");
                     $('body').unbind('mousewheel');
                     $('.odometer').html('219');
                     }, 1000);
                     $(".content").addClass("animated fadeOut");
                     setTimeout(function() {
                     $("body").addClass("page-landing-after");
                     }, 1000);*/
                    window.location = 'grand.php';
                });

                var d = new Date(); // for now
                var hours = d.getHours(); // => 9
                var minutes = d.getMinutes(); // =>  30
                var seconds = d.getSeconds(); // => 51
                var img = Math.floor(Math.random() * 7) + 1 + '.jpg';
                if (hours >= 7 && hours <= 18) {
                    $('.bg').css("background-image", "url(assets/img/landing/backgrounds/morning/" + img + ")");
                } else if (hours >= 19) {
                    $('.bg').css("background-image", "url(assets/img/landing/backgrounds/night/" + img + ")");
                } else {
                    $('.bg').css("background-image", "url(assets/img/landing/backgrounds/large/1.jpg)");
                }


            });
        </script>

    <body class="initing loading page-landing" data-section="landing">
        <div class="content <?php echo ($_GET['set'] == 1) ? 'disappear' : '' ?>">
            <div id="scroll-story">
                <section class="move">
                    <div class="bg"></div>
                    <div class="col-md-12">
                        <div class="bg-content">    
                            <div class="bg-content-items">
                                <div class="col-md-12">
                                    <div class="col-xs-12">
                                        <div class="block">
                                            <div class="bg-logo">
                                                <img src="assets/img/images/logo-west.png" alt="img">
                                            </div>
                                        </div>
                                        <div class="block">
                                            <div class="bg-content-text">
                                                <img src="assets/img/images/text-wow-page.png" alt="img">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 align-right">
                                        <div class="bg-img-packet">
                                            <img src="assets/img/images/pack-blue.png" alt="img">
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </section>                  
                <div id="btnDropdown" class="down-arrow">
                    <div class="a"></div>
                    <!-- <div class="b"></div>-->
                </div>
            </div>

        </div>



        <?php include('footer.php'); ?>

        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/matchmedia.js"></script>
        <script src="assets/js/picturefill.js"></script>
        <script src="assets/js/require.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/bootstrap.js"></script>    


    </body>

</html>
