<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>ご友人からの紹介者用<br>≪特別入力フォーム 確認≫</h3>
                </div>
                <div class="panel-content">
                    <div class="col-xs-12">
                        <p>ご希望するサンプルパックを選択して下さい</p>
                    </div>
                    <ul class="invitation-confirm margin-30">
                        <li>
                            <dl class="confirmItem">
                                <dt><img src="assets/img/images/invitation/select-item01.png" alt="img"></dt>
                                <dd>ウエスト・レッド</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>お名前</dt>
                                <dd>テキストテキストテキスト</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>フリガナ</dt>
                                <dd>テキストテキストテキスト</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>性別</dt>
                                <dd>男性</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>生年月日</dt>
                                <dd>19800101</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>郵便番号</dt>
                                <dd>1000001</dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>住所</dt>
                                <dd>テキストテキストテキスト</dd>
                            </dl>
                        </li>
                        <li>
                            <dl class="confirm-mail-select">
                                <dt>メールによる各種ご案内の送付</dt>
                                <dd>
                                    <ul>
                                        <li>
                                            <p>（1）キャンペーンに関するご案内</p>
                                            <p><b>希望する</b></p>
                                        </li>
                                        <li>
                                            <p>（2）新製品に関するご案内</p>
                                            <p><b>希望する</b></p>
                                        </li>
                                        <li>
                                            <p>（3）懸賞に関するご案内</p>
                                            <p><b>希望する</b></p>
                                        </li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>現在ご愛飲されているたばこのブランド名</dt>
                                <dd>テキストテキスト</dd>
                            </dl>
                        </li>
                    </ul>
                    <div class="center margin-20">                            
                        <a class="white-proceed btn-refresh fixed-width">登録する</a>
                    </div>
                    <div class="center margin-15">                            
                        <a class="white-proceed btn-refresh fixed-width black">修正する</a>
                    </div>

                </div>
                
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
