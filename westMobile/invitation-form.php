<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>ご友人からの紹介者用<br>≪特別入力フォーム≫</h3>
                </div>
                <div class="panel-content">
                    <div class="col-xs-12">
                        <p>ご希望するサンプルパックを選択して下さい</p>
                        <ul class="selectItem margin-20">
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item01.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・レッド</b><br>タール10mg / ニコチン0.7mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item02.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・ディープブルー</b><br>タール8mg / ニコチン0.6㎎</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item03.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・ブルー</b><br>タール6mg / ニコチン0.5mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item04.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・シルバー</b><br>タール3mg / ニコチン0.3mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item05.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ホワイト</b><br>タール1mg / ニコチン0.1mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item06.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・ホワイト100</b><br>タール1mg / ニコチン0.1mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item07.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・メンソール</b><br>タール6mg /ニコチン0.5mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item08.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・メンソール<br>ホワイト100</b><br>タール1mg / ニコチン0.1mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item09.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・デュエット</b><br>タール6mg / ニコチン0.6mg</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item10.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・<br>アイスフレッシュ エイト</b><br>タール8mg / ニコチン0.6㎎</span>
                                    </label>
                                </p>
                            </li>
                            <li>
                                <div class="image">
                                    <span><img src="assets/img/images/invitation/select-item11.png" alt="img"></span>
                                </div>
                                <p>
                                    <label>
                                        <input type="radio" name="select-item" value="" placeholder="">
                                        <span><b>ウエスト・<br>アイスフレッシュ ワン100</b><br>タール1mg / ニコチン0.1㎎</span>
                                    </label>
                                </p>
                            </li>

                        </ul>
                    </div>
                    <div class="col-md-12">
                        <p class="margin-20">利用規約とプライバシーポリシーをご確認下さい。</p>
                        <p class="margin-20">
                            <a href="#">利用規約（必ずお読み下さい）</a>
                            <br><a href="#">プライバシーポリシー（必ずお読み下さい）</a>
                        </p>
                        <p class="margin-20">ご登録申請にあたり以下の内容について入力をお願いします。</p>
                        <p>お客様の個人情報は、弊社にて厳重に管理を行います。また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。<br>サンプルたばこやキャンペーン景品等をお送りするために必要な情報となりますので、正確にご記入をお願いいたします。</p>
                    </div>
                    <div class="col-md-12 invitationForm">
                        <p class="margin-30">IDのお申込みにあたり以下の内容について入力をお願いします。</p>
                        <p class="margin-30">お客様の個人情報は、弊社にて厳重に管理を行います。また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。</p>

                        <div class="clearfix">
                            <p>お名前<i>（必須）</i></p>
                            <span class="name">性</span><input class="form-control name margin-20" type="text">
                            <span class="name">名</span><input class="form-control name margin-20" type="text">
                        </div>
                        <div class="clearfix">
                            <p>フリガナ<i>（必須）</i>全角フリガナ</p>
                            <span class="name">セイ</span><input class="form-control name margin-20" type="text">
                            <span class="name">メイ</span><input class="form-control name margin-20" type="text">
                        </div>
                        <p>性別 <i>（必須）</i></p>
                        <div class="panel-content-inner">
                            <input type="radio">
                            <label>男</label>
                            <input type="radio">
                            <label>女</label>
                        </div>
                        <p>生年月日<i>（必須）</i>半角数字</p>
                        <input class="form-control" type="text">
                        <p class="margin-20">(例:1980年3月14日⇒19800314)</p>
                        <p>郵便番号<i>（必須）</i>半角数字・ハイフン不要</p>
                        <input class="form-control margin-20" type="text">
                        <p>都道府県<i>（必須）</i></p>
                        <input class="form-control margin-20" type="text">
                        <p>市区町村<i>（必須）</i></p>
                        <input class="form-control margin-20" type="text">
                        <p>丁番地<i>（必須）</i></p>
                        <input class="form-control margin-20" type="text">
                        <p>(地名・町名後の丁番地等は半角数字で入力してくださ い。)</p>
                        <p>建物名・部屋番号</p>
                        <input class="form-control margin-20" type="text">

                        <p>ご提供頂く個人情報は、弊社が以下の目的に使用させて頂きます。
                        <br>サンプルたばこの送付（ご希望の場合のみ）
                        <br>メールによる各種ご案内の送付</p>
                        <p>また、個人を特定しない方法で、マーケティングの統計データとして活用させて頂きます。
                        <br>お客様の個人情報の取扱いについては<a href="">こちら</a></p>
                        
                        <p><b>■以下よりご希望をお選び下さい。</b></p>
                        <p>メールによる各種ご案内の送付</p>

                        <div class="mail-select margin-20">
                            <p>（1）キャンペーンに関するご案内</p>
                            <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                            <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                            <p>（2）お得な懸賞に関するご案内</p>
                            <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                            <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                            <p>（3）新製品に関するご案内</p>
                            <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                            <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                        </div>
                        <p class="margin-20"><small>※サンプルたばこやご案内については、時期や条件等により、お届けできない場合もあります。あらかじめご了承ください。</small></p>

                        <p>いまご愛飲されているたばこのブランド名を下記よりお選びください。</p>
                        <select class="form-control margin-30"><option value="">選択してください</option></select>

                        <div class="center margin-10">                            
                            <a class="white-proceed btn-refresh fixed-width">確認する</a>
                        </div>
                    </div>


                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
