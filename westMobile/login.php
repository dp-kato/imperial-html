<?php
include('header-login.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="background-wow ">
            <div class="page-content-login content-login">
                <div class="panel-body">
                    <div class="panel-header">
                        <div class="change-password-span">
                            <h3>たばこWest（ウエスト）<br>ブランドサイトへようこそ</h3>
                        </div>
                    </div>
                    <div class="panel-content grey-border">
                        <div class="col-xs-12">
                            <p class="margin-30">このWebサイトでは、インペリアル・タバコ・ジャパンの提供するたばこ「West（ウエスト）」についての情報をお知らせしています。</p>
                            <p class="margin-30">IDを取得してログインすると、Westのブランド紹介やキャンペーン情報などを閲覧することが出来ます。</p>

                            <div class="col-xs-12 no-gap">
                                <div class="panel-body">
                                    <div class="panel-header-small">
                                        <h4>はじめての方</h4>                               
                                    </div>
                                    <div class="panel-content-small red-border">
                                        <div class="col-md-12">
                                            <p class="margin-30">満20歳以上の喫煙者の方で｢ID｣と「パスワード」をお持ちでない方はこちらから登録できます｡</p>
                                            <div class="center margin-30">                            
                                                <a class="white-proceed btn-refresh fixed-width">登録する</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-xs-12 no-gap">
                                <div class="panel-body">
                                    <div class="panel-header-small">
                                        <h4>ご登録済みの方</h4>                               
                                    </div>
                                    <div class="panel-content-small red-border">
                                        <div class="col-xs-12">
                                            <div class="margin-15">
                                                <label>ID </label>
                                            </div>
                                            <div class="margin-15">
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="margin-15">
                                                <label>ルアドレス</label>
                                            </div>
                                            <div class="margin-15">
                                                <input class="form-control" type="text">
                                            </div>  
                                            <div class="panel-content-inner">
                                                <input type="checkbox">
                                                <label>次回から自動的にログイン</label>
                                            </div>
                                            <div class="center margin-40">
                                                <a class="white-proceed btn-refresh fixed-width">送信する</a>
                                            </div>
                                            <p>>  <u>パスワードを紛失した方</u></p>
                                            <p>>  <u>IDを紛失した方</u></p>
                                        </div>

                                    </div>

                                </div>
                            </div>


                            <div class="col-xs-12 no-gap">
                                <div class="panel-body">
                                    <div class="panel-header-small">
                                        <h4>プレスリリース</h4>                               
                                    </div>
                                    <div class="panel-content-small red-border">
                                        <div class="col-xs-12">
                                            <p class="red">2014.12.19</p>
                                            <p>インペリアル・タバコ・ジャパンでは、たばこ「West」の O2O マーケティング活動において、</p>
                                            <p>次世代型スマートフォン連動ガジェット「PlugAir(プラグエア)」の導入を決定しました。</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    var d = new Date(); // for now
    var hours = d.getHours(); // => 9
    var minutes = d.getMinutes(); // =>  30
    var seconds = d.getSeconds(); // => 51
    var img = Math.floor(Math.random() * 7) + 1 + '.jpg';

    if (hours >= 7 && hours <= 18) {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/morning/" + img + ")");
    } else if (hours >= 19) {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/night/" + img + ")");
    } else if (hours <= 6) {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/night/" + img + ")");
    } else {
        $('.background-wow').css("background-image", "url(assets/img/landing/backgrounds/large/1.jpg)");
    }

</script>
</body>

</html>
