<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-20">                                
                        <img src="assets/img/images/icon-mgm-grey.png" alt="img">
                        <h3>友達に紹介する</h3>
                    </div>
                </div>
            </div> 
            <div class="inner-page-content">
                <div class="col-md-12">
                    <div class="row">                        
                        <div class="col-xs-12">
                            <p>入力した内容を確認のうえ、</p>
                            <p class="margin-20">「送信する」ボタンを押してください。</p>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-members margin-30">
                                <div class="app-flow-button">
                                    <div class="form-inner-span">
                                        <p>お友達のメールアドレス</p>
                                        <p><b>West@gmail.com</b></p>
                                    </div>
                                </div>
                                <div class="app-flow-button">
                                    <div class="form-inner-span">
                                        <p>件名</p>                                       
                                        <p>◯◯さんからキャンペーンのご紹介</p>
                                    </div>
                                </div>
                                <div class="form-inner-span">
                                    <p>お友達へのメッセージ（100文字以内）</p>                                    
                                    <div class="member-form grey-border bg-grey">
                                        <div class="member-form-header">
                                            <p>WESTからのご紹介メール</p>
                                        </div>
                                        <p class="large margin-30">あなたは上記紹介者よりWestサイトの紹介を受けました。</p>
                                        <p class="large margin-20">お知り合いの方からの紹介によりサイトに登録すると、サンプルプレゼント！この機会を是非ご利用ください。</p>
                                        <p class="large">■サイト登録のお申し込みはこちらから</p>
                                        <p class="large margin-20">https://westonline.jp/Unique URL</p>
                                        <p class="large">■送信元</p>
                                        <p class="large margin-20">（あなたのメールアドレス）</p>
                                        <div class="form-footer">
                                            <p class="grey">※このメールに掲載の全ての特典およびサービスの内容、利用条件につきましては、</p>
                                            <p class="grey">予告なく変更または中止させていただく場合がございますので、予めご了承ください。</p>
                                            <p class="grey">※紹介者をご存じない場合は、当メールを破棄していただきますようお願いいたします。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p>※20歳以上且つ喫煙者の方へのみ、送信が可能です。</p>
                    <p class="margin-40">※すでに入会している方へは送れません。</p>
                    <div class="change-password-span margin-40">
                        <a class="white-proceed btn-refresh fixed-width">送信する</a>
                        <a class="white-proceed btn-refresh grey fixed-width">リセット</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
</body>

</html>
