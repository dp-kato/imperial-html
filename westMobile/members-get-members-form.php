<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row memberWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-20">                                
                        <img src="assets/img/images/icon-mgm-grey.png" alt="img">
                        <h3>友達に紹介する</h3>
                    </div>
                </div>
            </div> 
            <div class="inner-page-content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="member-upper-banner">
                            <div class="image01">
                                <img src="assets/img/images/members/member-banner01.png" alt="img">
                            </div>
                            <div class="img03">
                                <img src="assets/img/images/members/img-belt.png" alt="img">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <p>お友達にメセージを送ります。</p>
                            <p class="margin-20">お友達のメールアドレスを入力してください。</p>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-members margin-30">
                                <div class="app-flow-button">
                                    <div class="form-inner-span">
                                        <p>お友達のメールアドレス<small>（半角英数字）</small></p>
                                        <input class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="app-flow-button">
                                    <div class="form-inner-span">
                                        <p>お友達のメールアドレス(確認)<small>（半角英数字）</small></p>
                                        <input class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="app-flow-button">
                                    <div class="form-inner-span">
                                        <p>件名</p>                                       
                                        <p>【Westたばこのサンプルがもらえる！】<br>●●さんからのご紹介です</p>
                                    </div>
                                </div>
                                <div class="form-inner-span">
                                    <p>お友達へのメッセージ（100文字以内）</p>                                    
                                    <div class="member-form grey-border">
                                        <div class="member-form-header">
                                            <p>●●さんからWestサンプルパック<br>プレゼントキャンペーンのご紹介</p>
                                        </div>
                                        <p class="large margin-30">あなたは●●さんからWestブランドサイトの紹介を受けました。</p>
                                        <p class="large margin-20">下記のお友だち紹介用URLからWestブランドサイトにご登録いただくと、もれなくお好きなWestサンプルパックをプレゼントいたします。この機会に是非Westをお試しください。<br>※すでにWestブランドサイト会員の方やお友達紹介用URL以外から会員登録をされた方は本キャンペーンにお申込いただけませんのでご了承ください。</p>
                                        <p class="large">■サンプルパックプレゼントキャンペーンURL（お友達紹介用URL）</p>
                                        <p class="large margin-20">https://.................</p>
                                        <div class="form-footer">
                                            <p class="grey">※このメールは成人喫煙者の方にお送りしているメールです、万が一「非喫煙者」「20歳以下の未成年」の方にこちらのメールが届いた場合は、お手数ですがキャンペーンサイトへ進まず、メールを破棄していただけますようお願い申し上げます</p>
                                            <p class="grey">喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。
                                            <br>周りの人から勧められても決して吸ってはいけません。 たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="margin-40">下記のチェックボックスをご確認いただき、内容のご確認に進んで下さい。</p>
                    <div class="change-password-span margin-40">
                        <div class="margin-30">
                            <input type="checkbox">
                            <label>紹介メールを送る相手は、<br>成人喫煙者です。</label>
                        </div>
                        <a class="white-proceed btn-refresh fixed-width">送信する</a>
                        <a class="white-proceed btn-refresh grey fixed-width">戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
</body>

</html>
