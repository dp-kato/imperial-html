<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row memberWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header margin-20">                                
                        <img src="assets/img/images/icon-mgm-grey.png" alt="img">
                        <h3>友達に紹介する</h3>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="inner-page-content center">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="thanks-text">
                                <h3 class="margin-20 red">お友達にご紹介メール<br>を送信いたしました。</h3>
                                <div class="thanks-text-left">
                                    <p class="large">ご紹介メールをお送りいただきましてありがとうございました。</p>
                                    <p class="large">5,000Wポイントを付与いたしましたのでご確認ください。</p>
                                    <p class="large">ご紹介メールをお送りした、お友達・ご家族には〈お申込の流れ〉を事前にお伝えいただいていますか？お友達紹介用URLから会員登録とサンプルパックのお申込みを完了していただけるようご連絡をお願いいたします。</p>
                                </div>
                            </div>
                            <div class="center">
                                <a class="white-proceed btn-refresh fixed-width">他のお友達にも紹介する</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                <li><a>Updated Privacy Policy</a></li>
            </ul>
            <p></p>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
</body>

</html>
