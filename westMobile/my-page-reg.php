<?php include('header.php'); ?>
<div id="content" class="">

    <?php //include('navigation.php'); ?>
    <div class="col-xs-12">
        <div class="row mypage-regWrap"> 
            <div class="page-content">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="inner-page-header">                                
                            <img src="assets/img/images/my-page/icon-my-page-black.png" alt="img">
                            <h3>登録情報の変更</h3>
                        </div>
                    </div>	
                </div>
                <div class="col-xs-12">
                    <div class="inner-page-content">
                        <p>お客様の登録情報はこちらで変更できます。</p>
                        <ul class="margin-20">
                            <li>パスワードの変更。</li>
                            <li>住所等の登録情報変更。</li>
                            <li>弊社からのご案内等郵送物の送付先変更。</li>
                            <li>メールマガジンの登録情報変更。</li>
                        </ul>


                        <p>お客様の個人情報は、弊社にて厳重に管理を行います。</p>
                        <p>また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。</p>	
                        <hr class="margin-15"/>
                        <div class="col-xs-12">
                            <p class="margin-10 reg-bloc-title"><strong class="red">■</strong> メールアドレスの変更</p>
                            <p class="red"><small>※ログインの際に必要となりますので必ずお控えください。</small></p>
                            <p class="margin-10">現在のメールアドレス</p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-10">新しいメールアドレス</p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-10">確認のため再入力</p>
                            <input class="form-control margin-20" type="text">
                        </div>
                        <div class="center margin-10">                            
                            <a class="white-proceed btn-refresh fixed-width btn-reg btn-reg">メールアドレスを変更する</a>
                        </div>
                        <hr class="margin-15"/>
                        <div class="col-xs-12">
                            <p class="margin-10 reg-bloc-title"><strong class="red">■</strong> パスワードの変更</p>
                            <p class="red"><small>※ログインの際に必要となりますので必ずお控えください。</small></p>
                            <p class="margin-10">現在のパスワード</p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-10">新しいパスワード</p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-10">確認のため再入力</p>
                            <input class="form-control margin-15" type="text">
                        </div>
                        <div class="center margin-15">                            
                            <a class="white-proceed btn-refresh fixed-width btn-reg">パスワードを変更する</a>
                        </div>
                        <hr class="margin-15"/>
                        <div class="col-xs-12">
                            <p class="margin-15 reg-bloc-title"><strong class="red">■</strong> 登録情報を更新する</p>
                            <p><small>※サンプルたばこやキャンペーン景品等をお送りするために必要な情報となりますので<span class="red">、正確にご記入をお願いいたします。</span></small></p>
                            <p class="margin-15">郵便番号（半角数字、ハイフン不要）</p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-15">(例:105-8422⇒1058422)</p>
                        </div>
                        <div class="center margin-15">                            
                            <a class="white-proceed btn-refresh fixed-width btn-reg black">郵便番号から住所検索</a>
                        </div>
                        <div class="col-xs-12">
                            <p class="margin-10"> 都道府県<i>（必須）</i></p>
                            <select class="form-control margin-15"></select>
                            <p class="margin-10">市町村名<i>（必須）</i></p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-10">丁番地等<i>（必須）</i></p>
                            <p class="margin-10">(地名・町名後の丁番地等は半角数字で入力してください。) </p>
                            <input class="form-control margin-15" type="text">
                            <p class="margin-10">(例：海岸1-2-3)</p>
                            <p class="margin-10">ビル名・部屋番号 </p>
                            <input class="form-control margin-15" type="text">
                        </div>
                        <div class="col-xs-12">
                            <div class="my-page-form">
                                <div class="mail-select margin-15">
                                    <p>1)サンプルたばこの送付 </p>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                                </div>
                                 <div class="mail-select margin-20">
                                    <p>2)メールによる 各種ご案内の送付  </p>
                                    <p>（1）キャンペーンに関するご案内</p>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                                    <p>（2）お得な懸賞に関するご案内</p>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                                    <p>（3）新製品に関するご案内</p>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望する</label>
                                    <label><input type="radio" name="mail-select" value="" placeholder="">希望しない</label>
                                </div>
                            </div>
                        </div>
                        <div class="center margin-20">                            
                            <a class="white-proceed btn-refresh fixed-width">更新する</a>
                        </div>
                        <p>※製品サンプルやご案内については、時期や条件等により、お届けできない場合もあります。あらかじめご了承ください。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    $('.btn-modal').click(function() {
        var dataTarget = $(this).attr('data-target');
        setTimeout(function() {
            $(dataTarget).find('div.modal-body').removeClass('modal-loading');
            $(dataTarget).find('div.single-item').removeClass('disappear');
            $(dataTarget).find('div.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });

        }, 500);
    });

</script>
</body>

</html>
