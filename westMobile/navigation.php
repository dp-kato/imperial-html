<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid"> 
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-top">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="header-icon"></div>
                <ul class="header-coins-counter">
                    <li><p>山田さん</p></li>
                    <li><label class="odometer" id="odometer">000</label><!--<i><img class="animated pulse infinite" src="assets/img/images/coins.png" alt="img"></i>--></li>
                    <li><p>獲得ポイント</p></li>
                    <li class="coins-dropdown hide">
                        <ul class="my-page-options">
                            <li>
                                <a href="my-page-1.php">ポイント確認 <small class="fa fa-angle-right"></small></a>
                            </li>
                            <li>
                                <a href="my-page-2.php">登録情報の変更 <small class="fa fa-angle-right"></small></a> 
                            </li>
                            <li>
                                <a href="logout.php">ログアウト <small class="fa fa-angle-right"></small></a> 
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <?php $tab = explode('.php', basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING'])); ?>
            <div class="collapse navbar-collapse center" id="navbar-top">
                <ul class="nav navbar-nav">
                <li><a class="menu-icons <?php echo ($tab[0] == 'compaign') ? 'active' : '' ?>" href="campaign.php"><i class="icon icon-campaign"></i><span>キャンペーン</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'game') ? 'active' : '' ?>" href="game.php"><i class="icon icon-game"></i><span>ゲーム</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'comics' || $tab[0] == 'comics2') ? 'active' : '' ?>" href="comics2.php"><i class="icon icon-comics"></i><span>コミック</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'landmark') ? 'active' : '' ?>" href="landmark.php"><i class="icon icon-landmark"></i><span>喫煙所マップ</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'west-hunt') ? 'active' : '' ?>" href="west-hunt.php"><i class="icon icon-west-hunt"></i><span>West Hunt</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'interview') ? 'active' : '' ?>" href="interview.php"><i class="icon icon-interview"></i><span>インタビュー</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'value') ? 'active' : '' ?>" href="value.php"><i class="icon icon-value"></i><span>価値組生活</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'questionnaire') ? 'active' : '' ?>" href="questionnaire.php"><i class="icon icon-questionare"></i><span>アンケート</span></a></li>
                    <li><a class="menu-icons <?php echo ($tab[0] == 'west-brand') ? 'active' : '' ?>" href="west-brand.php"><i class="icon icon-west-brand"></i><span>ウエストについて</span></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </div><!-- /.container -->
</nav>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $('.odometer').html('219');
        }, 1000);
        $('html').click(function() {
            $('.coins-dropdown').addClass('hide');
        });

        $('.header-coins-counter').click(function(e) {
            e.stopPropagation();
        });

        $(".header-coins-counter").hover(function() {
            $(".coins-dropdown").toggleClass("hide");

        });


    });
</script>

