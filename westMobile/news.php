<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/news/news-grey.png" alt="img">
                        <h3>ニュース</h3>
                    </div>
                </div>
            </div> 
            <div class="col-md-12">
                <div class="inner-page-content">
                    <p class="inner-page-header-date">2015.05.01</p>
                    <div class="interview-banner">
                        <img src="assets/img/images/banner-news.png" alt="img">
                    </div>

                    <div class="question-answer">                              
                        <p class="answer">北海道、茨城県のウエストの取り扱いがあるセイコーマート各店舗で、缶コーヒー「グランディア 北海道牛乳のカフェオレ」がもらえるキャンペーンを展開します（3月30日～4月12日まで）。　
                            また、キャンペーンパッケージ裏面にて「ウエスト アイスフレッシュ新発売記念キャンペーン」をご案内しております。</p>
                        <p class="answer margin-20">▶詳しくはこちらへ</p>
                    </div>

                    <div id="additional-QuesAns" style="display:none">
                        <div class="question-answer">
                            <p class="answer">この機会にぜひ「グランディア北海道牛乳のカフェオレ×ウエスト１パックセット（ウエスト・
                                アイスフレッシュエイト 8mg、ウエスト・アイスフレッシュワン100 1mg、ウエスト・メンソール 6mgの3銘柄いずれか。
                                各380円）」をお求めください!!</p>

                        </div>
                        <div class="question-answer">
                            <div class="center margin-20">
                                <img src="assets/img/images/news/img-brand-packet.jpg" alt="img">
                            </div>
                            <p class="answer">北海道、茨城県のウエストの取り扱いがあるセイコーマート各店舗で、缶コーヒー「グランディア 北海道牛乳のカフェオレ」がもらえるキャンペーンを展開します（3月30日～4月12日まで）。　
                                また、キャンペーンパッケージ裏面にて「ウエスト アイスフレッシュ新発売記念キャンペーン」をご案内しております。</p>
                            <p class="answer margin-20">▶詳しくはこちらへ</p>
                            <p class="answer">この機会にぜひ「グランディア北海道牛乳のカフェオレ×ウエスト１パックセット（ウエスト・
                                アイスフレッシュエイト 8mg、ウエスト・アイスフレッシュワン100 1mg、ウエスト・メンソール 6mgの3銘柄いずれか。
                                各380円）」をお求めください!!</p>
                            <p class="answer margin-20">北海道、茨城県のウエストの取り扱いがあるセイコーマート各店舗で、缶コーヒー「グランディア 北海道牛乳のカフェオレ」がもらえるキャンペーンを展開します（3月30日～4月12日まで）。　
                                また、キャンペーンパッケージ裏面にて「ウエスト アイスフレッシュ新発売記念キャンペーン」をご案内しております。</p>
                            <p class="answer">この機会にぜひ「グランディア北海道牛乳のカフェオレ×ウエスト１パックセット（ウエスト・
                                アイスフレッシュエイト 8mg、ウエスト・アイスフレッシュワン100 1mg、ウエスト・メンソール 6mgの3銘柄いずれか。
                                各380円）」をお求めください!!</p>
                            <p class="answer margin-20">※数に限りがございますので、予めご了承ください。</p>
                        </div>

                        <div class="center">
                            <a class="white-proceed btn-refresh fixed-width">前の記事を読む</a>
                            <a class="white-proceed btn-refresh fixed-width">次の記事を読む</a>
                        </div>
                    </div>
                    <div class="center">
                        <a id="showAdditionalQuesAns" class="white-proceed btn-refresh fixed-width">もっと読む</a>
                    </div>


                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="slider-header">
                        <h4>ニュース一覧</h4>
                    </div>
                </div>
                <div class="slider single-item slider-padded">
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/news/img1.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>3月30日（月）ウエスト ・アイスフレッシュ新発売 記念キャ...</b></p>                            
                        </div>
                    </div>
                    <div class="new-campaigns">
                        <div class="new-campaigns-img">
                            <img src="assets/img/images/news/img2.jpg" alt="img"/>
                        </div>
                        <div class="new-campaigns-text">
                            <p>2015.05.01</p>
                            <p><b>セブン-イレブンで「ウ エスト缶コーヒー付き キャンペーン」...</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
