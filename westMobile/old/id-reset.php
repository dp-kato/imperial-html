<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>IDを確認される方</h3>
                </div>
                <div class="panel-content grey-border top-gap">
                    <div class="col-md-12">
                        <div class="change-password-span margin-40">
                            <p>喫煙は、あなたにとって肺がんの原因と一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。</p>
                            <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。 周りの人から勧められても決して吸ってはいけません。</p>
                            <p>たばこの煙は、あなたの周りの人、特に乳幼児、子ども、お年寄りなどの健康に悪影響を及ぼします。 </p>
                            <p class="margin-80">喫煙の際には、周りの人の迷惑にならないように注意しましょう。 </p>

                            <a class="white-proceed btn-refresh grey">閉じる</a>
                        </div>
                    </div>                    
                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                <li><a>Updated Privacy Policy</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
