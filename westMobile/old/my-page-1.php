<?php include('header.php'); ?>
<div class="container-fluid">
                <div class="row">
                    <div class="page-content row">
                        <div class="col-md-12">
                            <div class="inner-page-header">                                
                                <img src="assets/img/images/my-page/icon-my-page-black.png" alt="img">
                                <h3>My Page</h3>
                                <a class="grey-bg btn-inner-page-header">Share</a>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="my-page-coins-list">
                                <ul class="my-page-points-list">
                                    <li class="header-points">
                                        <h4>こんにちは　山田花子さん</h4>                                        
                                    </li>
                                    <li>
                                        <div class="coins-points">
                                            <img src="assets/img/images/my-page/coins-more.png" alt="img">
                                            <p>現在の保有ポイント</p>
                                        </div>
                                        <div class="coins-points-value">
                                            <h2>2000</h2>
                                            <p><b>ポイント</b></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="coins-points">
                                            <img src="assets/img/images/my-page/coins-less.png" alt="img">
                                            <p>今日の獲得ポイント</p>
                                        </div>
                                        <div class="coins-points-value">
                                            <h2>200</h2>
                                            <p><b>ポイント</b></p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">                           
                            <div class="row">  
                                <div class="my-page-slider">
                                <h3 class="slider-heading">ご利用いただけるキャンペーン</h3>
                                <div class="slider multiple-item comics2-slider-area">
                                    <div class="col-md-2-5">
                                        <div class="comics-tile">
                                            <div class="comics-listing">
                                                <img src="assets/img/images/my-page/mypage-img-1.jpg" alt="img">                                            
                                            </div>
                                            <div class="new-entry">
                                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                                            </div>
                                            <div class="mypage-listing-text">
                                                <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                                <p>ウエスト缶コーヒー付き キャンペーン....</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2-5">
                                        <div class="comics-tile">
                                            <div class="comics-listing">
                                                <img src="assets/img/images/my-page/mypage-img-2.jpg" alt="img">                                            
                                            </div>
                                            <div class="new-entry">
                                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                                            </div>
                                            <div class="mypage-listing-text">
                                                <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                                <p>「ウエストオリジナル ライター付きキャ....</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2-5">
                                        <div class="comics-tile">
                                            <div class="comics-listing">
                                                <img src="assets/img/images/my-page/mypage-img-3.jpg" alt="img">                                            
                                            </div>
                                            <div class="new-entry">
                                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                                            </div>
                                            <div class="mypage-listing-text">
                                                <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                                <p>セブン-イレブンで「ウエ スト缶コーヒー付きキャ...</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2-5">
                                        <div class="comics-tile">
                                            <div class="comics-listing">
                                                <img src="assets/img/images/my-page/mypage-img-4.jpg" alt="img">                                            
                                            </div>
                                            <div class="new-entry">
                                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                                            </div>
                                            <div class="mypage-listing-text">
                                                <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                                <p>セイコーマートで「ウエス ト缶コーヒー付きキャ...</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2-5">
                                        <div class="comics-tile">
                                            <div class="comics-listing">
                                                <img src="assets/img/images/my-page/mypage-img-5.jpg" alt="img">                                            
                                            </div>
                                            <div class="new-entry">
                                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                                            </div>
                                            <div class="mypage-listing-text">
                                                <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                                <p>ウエスト缶コーヒー付き キャンペーン....</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2-5">
                                        <div class="comics-tile">
                                            <div class="comics-listing">
                                                <img src="assets/img/images/my-page/mypage-img-1.jpg" alt="img">                                            
                                            </div>
                                            <div class="new-entry">
                                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                                            </div>
                                            <div class="mypage-listing-text">
                                                <a href="#" data-toggle="modal" data-target="#modal-comics2-1" class="btn-modal"> <i class="fa fa-angle-right"></i></a>
                                                <p>ウエスト缶コーヒー付き キャンペーン....</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div class="mypage-tables">
                                <div class="table-heading">
                                    <img class="table-heading-img" src="assets/img/images/my-page/single-coin.png" alt="img">
                                    <h3>ポイント取得履歴</h3>
                                    <p>最新の3件</p>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>取得日</th>
                                            <th>内容</th>
                                            <th>取得ポイント</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2014 5/1</td>
                                            <td class="description">ウエスト缶コーヒー付きキャンペーン</td>
                                            <td>500ポイント</td>
                                        </tr>
                                        <tr>
                                            <td>2014 5/1</td>
                                            <td class="description">ファミリーマートで再起動キャンペーン！West 1パックで「ファミマカフェ」をゲット!!</td>
                                            <td>50ポイント</td>
                                        </tr>
                                        <tr>
                                            <td>2014 5/1</td>
                                            <td class="description">セブンイレブンで「ウエストオリジナルライター付きキャンペーン」</td>
                                            <td>100ポイント</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="below-table-button">
                                    <a class="grey-bg btn-below-table">履歴20件表示</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mypage-tables">
                                <div class="table-heading">
                                    <img class="table-heading-img" src="assets/img/images/my-page/single-coin.png" alt="img">
                                    <h3>ポイント取得履歴</h3>
                                    <p>最新の3件</p>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>取得日</th>
                                            <th>内容</th>
                                            <th>取得ポイント</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2014 5/1</td>
                                            <td class="description">ウエスト缶コーヒー付きキャンペーン</td>
                                            <td>500ポイント</td>
                                        </tr>
                                        <tr>
                                            <td>2014 5/1</td>
                                            <td class="description">ファミリーマートで再起動キャンペーン！West 1パックで「ファミマカフェ」をゲット!!</td>
                                            <td>50ポイント</td>
                                        </tr>
                                        <tr>
                                            <td>2014 5/1</td>
                                            <td class="description">セブンイレブンで「ウエストオリジナルライター付きキャンペーン」</td>
                                            <td>100ポイント</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="below-table-button">
                                    <a class="grey-bg btn-below-table">履歴100件表示</a>
                                </div>
                                <div class="below-table-text">
                                <p>ポイントの有効期限は最終ログインから1年間です。</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="footer">
                        <ul class="footer-links">
                            <li><a>About us</a></li>
                            <li><a>News</a></li>
                            <li><a>FAQs</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="below-footer">
                        <ul class="footer-links">
                            <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                            <li><a>Updated Privacy Policy</a></li>
                        </ul>
                        <p></p>
                    </div>
                    <div class="warning-footer">
                        <div class="warning-text">
                            <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                            <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                            <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                        <h4>コミック</h4>
                        <p>シュールな笑いを提供する田中光氏連載スタート！</p>
                    </div>
                    <div class="modal-body modal-loading">
                        <div class="slider single-item disappear">
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                                    </div>     
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                                    </div>                                       
                                    <div class="comics-listing-text brown">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                        <h4>コミック</h4>
                        <p>シュールな笑いを提供する田中光氏連載スタート！</p>
                    </div>
                    <div class="modal-body modal-loading">
                        <div class="slider single-item disappear" >
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                                    </div>     
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                                    </div>                                       
                                    <div class="comics-listing-text brown">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                                    </div>                                       
                                    <div class="comics-listing-text brown">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('footer.php'); ?>
        <script src="assets/js/jquery.js"></script>     
        <script src="assets/js/odometer.min.js"></script>       
        <script src="assets/js/bootstrap.js"></script>      
        <script type="text/javascript" src="assets/js/slider/slick.js"></script>
        <script type="text/javascript">
            $('.multiple-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });
        </script>
    </body>

</html>
