<?php include('header.php'); ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="page-content row">
                        <div class="col-md-12">
                            <div class="inner-page-header">                                
                                <img src="assets/img/images/my-page/icon-my-page-black.png" alt="img">
                                <h3>My Page</h3>
                                <a class="grey-bg btn-inner-page-header">Share</a>
                            </div>
                        </div>  
                        <div class="col-md-12">
                            <div class="mypage2-top">
                                <h3>登録情報の変更</h3>
                                <p>お客様の登録情報はこちらで変更できます。</p>
                                <ul>
                                    <li>パスワードの変更。</li> 
                                    <li>住所等の登録情報変更。</li> 
                                    <li>弊社からのご案内等郵送物の送付先変更。</li> 
                                    <li>メールマガジンの登録情報変更。</li> 
                                </ul>
                                <div class="below-top-list">
                                    <p>お客様の個人情報は、弊社にて厳重に管理を行います。</p>
                                    <p>また、お客様のご同意なしに守秘義務を負う業務委託先以外の第三者に開示・提供いたしません。</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="below-top-list">
                                <h3><b>■</b>パスワードの変更</h3>
                                <table class="table mypagetable1">                                    
                                    <tbody>
                                        <tr>
                                            <th scope="row">現在のパスワード</th>
                                            <td><input class="form-control" type="text"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">新しいパスワード</th>
                                            <td><input class="form-control" type="text"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">確認のため再入力</th>
                                            <td><input class="form-control" type="text"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="change-password-span">
                                    <a class="white-proceed change-password">パスワードを変更する</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="below-top-list">
                                <h3><b>■</b>登録情報を更新する</h3>
                                <table class="table">                                    
                                    <tbody>
                                        <tr>
                                            <th scope="row">郵便番号<br>(半角数字、ハイフン不要)</th>
                                            <td>
                                                <div class="inner-table-item">
                                                    <input class="form-control" type="text">
                                                    <p>例：105-8422⇒1058422</p>
                                                    <a class="white-proceed change-password">郵便番号から住所検索</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">都道府県 <strong>(必須)</strong></th>
                                            <td><select class="form-control"> </select></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">市区町村等 <strong>(必須)</strong></th>
                                            <td><input class="form-control" type="text"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">丁番地等 <strong>(必須)</strong><br>(地名・町名後の丁番地等は半角数字で入力してください。)</th>
                                            <td>
                                                <div class="inner-table-item">
                                                    <input class="form-control" type="text">
                                                    <p>（例：海岸1-2-3）</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">PCメールアドレス</th>
                                            <td><input class="form-control" type="text"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">携帯メールアドレス</th>
                                            <td>
                                                <div class="inner-table-item">
                                                    <input class="form-control" type="text">
                                                    <p class="at-the-rate">@</p>
                                                    <select class="form-control"> </select>
                                                    <hr>
                                                    <p>確認のため、もう一度入力してください。</p>
                                                    <input class="form-control" type="text">
                                                    <p class="at-the-rate">@</p>
                                                    <select class="form-control"> </select>

                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="below-top-list">
                                <table class="table">                                    
                                    <tbody>
                                        <tr>
                                            <th colspan=2 scope="row">1)ご希望の場合のサンプルたばこの送付</th>
                                            <td>
                                                <input type="radio">
                                                <label>希望する</label>
                                                <input type="radio">
                                                <label>希望しない</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row" rowspan="2">2)たばこに関するご案内やアンケートなどの送付</th>
                                            <th scope="row">A.郵送での送付</th>
                                            <td>
                                                <input type="radio">
                                                <label>希望する</label>
                                                <input type="radio">
                                                <label>希望しない</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">B.PCメールマガジンの送付</th>
                                            <td>
                                                <input type="radio">
                                                <label>希望する</label>
                                                <input type="radio">
                                                <label>希望しない</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="change-password-span">
                                    <a class="grey-bg btn-grey">戻る</a>
                                    <a class="white-proceed btn-refresh">更新する</a>
                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="footer">
                        <ul class="footer-links">
                            <li><a>About us</a></li>
                            <li><a>News</a></li>
                            <li><a>FAQs</a></li>
                            <li><a>Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div id="below-footer">
                        <ul class="footer-links">
                            <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                            <li><a>Updated Privacy Policy</a></li>
                        </ul>
                        <p></p>
                    </div>
                    <div class="warning-footer">
                        <div class="warning-text">
                            <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                            <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                            <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                        <h4>コミック</h4>
                        <p>シュールな笑いを提供する田中光氏連載スタート！</p>
                    </div>
                    <div class="modal-body modal-loading">
                        <div class="slider single-item disappear">
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                                    </div>     
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                                    </div>                                       
                                    <div class="comics-listing-text brown">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                        <h4>コミック</h4>
                        <p>シュールな笑いを提供する田中光氏連載スタート！</p>
                    </div>
                    <div class="modal-body modal-loading">
                        <div class="slider single-item disappear" >
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                                    </div>     
                                    <div class="new-entry">
                                        <img src="assets/img/images/comics/new-tag.png" alt="img">
                                    </div>
                                    <div class="comics-listing-text">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                                    </div>                                       
                                    <div class="comics-listing-text brown">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="comics-tile">
                                    <div class="comics-listing">
                                        <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                                    </div>                                       
                                    <div class="comics-listing-text brown">
                                        <a href="#"> <i class="fa fa-angle-right"></i></a>
                                        <p>Lorem Ipsum dolor sit amet</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php include('footer.php');?>
        <script src="assets/js/jquery.js"></script>     
        <script src="assets/js/odometer.min.js"></script>       
        <script src="assets/js/bootstrap.js"></script>      
        <script type="text/javascript" src="assets/js/slider/slick.js"></script>
        <script type="text/javascript">
            $('.multiple-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });
            
        </script>
    </body>

</html>
