<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>利用規約</h3>
                </div>
                <div class="panel-content grey-border margin-30">
                    <div class="col-md-12">
                        <div class="privacy">
                            <p class="head">プライバシーポリシー</p>
                            <p class="text">インペリアル・タバコ・ジャパン株式会社（「当社」）は、当社が販売促進の為に提供するウェブサイト（「当社サイト」）運営にあたり、お客様からの信頼を第一と考え、個人情報の保護に関する法律
                                （「個人情報保護法」）に沿って、お客様の個人情報を厳格に管理し、お客様の希望に沿って取扱うとともに、お客様の個人情報の正確性・機密性の保持に努めています。 </p>
                            <p class="text margin-30">「個人情報の保護に関する法律」（「個人情報保護法」）、その他関係する法令を遵守し、お客様の個人情報を厳格に管理し、取扱います。</p>
                            <p class="head">１．関係法令等の遵守</p>
                            <p class="text margin-30">当社は、お客様の個人情報の取扱いにつきまして、個人情報保護法その他関係法令その他の規範を遵守いたします。</p>
                            <p class="head">２．グループ会社による共同利用</p>
                            <p class="text margin-30">当社は、「３．個人情報の収集・利用」に掲げるお客様の個人情報を「４．個人情報の利用目的」に掲げる目的のために、関連会社と共同で利用することがあります。その場合であっても、
                                お客様の個人情報は、当社が責任を持って管理いたします。</p>
                            <p class="head">３．個人情報の収集・利用</p>
                            <p class="text">当社は、「４．個人情報の利用目的」に掲げる目的のために、以下のようなお客様の個人情報を収集し、利用することがあります。</p>
                            <p class="text margin-30">お客様の氏名、住所、性別、生年月日、電話番号、Ｅメールアドレス、満20歳以上であることを確認できる公的身分証明書のコピー、喫煙者である旨の申し出、携帯端末情報、
                                お客様のたばこ購入履歴など。</p>
                            <p class="head">４．個人情報の利用目的</p>
                            <p class="text margin-30">当社は、収集したお客様の個人情報を、以下の目的達成に必要な範囲内で利用し、その他の目的には利用いたしません。</p>
                            <ul>
                                <li>お客様が満20歳以上であること又は喫煙者であることの確認</li>
                                <li>お客様からお問合せいただいた場合に回答を行う</li>
                                <li>お客様に対して当社の製品・サービス、キャンペーン等のご案内</li>
                                <li>お客様に対して当社の製品・サービスについてご意見をお伺いする</li>
                                <li>懸賞企画等の当選者への賞品や景品のお届け</li>
                                <li>懸賞企画等の当選者発表</li>
                                <li>懸賞企画等の資料発送</li>
                                <li>個人を特定しない方法によるマーケティングデータの作成</li>
                                <li>当社運営サイト利用時に登録者ご本人であることの確認</li>
                                <li>当社運営サイトにおけるサービスの利用動向等の統計的資料の作成</li>
                                <li>上記各目的を達成するため、関連会社に対して個人情報を提供すること</li>                              
                            </ul>
                            <p class="text margin-30">その他、お客様から書面等（電子メールなど電磁的方法等による場合を含む）により個人情報を取得する場合には、都度、利用目的を明示させていただきます。</p>

                            <p class="text margin-30">お客様から、お電話でご質問、ご意見等をいただいた際に、ご相談内容を録音させていただくことがあります。録音しました内容につきましては、お客様のご相談に正確に対応するために利用させていただきます。</p>
                            <p class="head">５．個人情報の管理</p>
                            <p class="text margin-30">当社は、お客様の個人情報を正確かつ最新の内容に保つよう努めます。</p>
                            <p class="text margin-30">お客様の個人情報の漏えい、滅失または毀損を防止するため、個人情報の保護に関する法律に従い、厳重なセキュリティ対策を実施いたします。</p>
                            <p class="head">◆個人情報の取扱いの委託</p>
                            <p class="text margin-30">「４．個人情報の利用目的」に掲げる目的のために、当社または関連会社は、以下の場合に、個人情報の取扱いを外部（日本以外に所在する委託先も含みます）に委託することがあります。</p>
                            <ol class="margin-30">
                                <li>ウェブサーバ・データベースの管理の委託</li>
                                <li>キャンペーン等、事務局業務の一部委託</li>
                                <li>抽選及び発送業務の委託</li>
                                <li>当社からのお知らせ（メールマガジン、ＤＭ配信等）の業務委託</li>
                                <li>お客様窓口のお問合せ窓口業務の委託</li>
                                <li>その他各サービス業務の委託</li>
                            </ol>
                            <p class="text margin-30">なお、当社および関連会社は、上記業務の委託先との間で、個人情報の取扱いに関する契約を締結した上で、サービス提供以外の目的での情報の利用を禁止し、当該契約の遵守状況について
                                監督を行っています。</p>

                            <p class="head">６．個人情報のお客様以外の第三者への提供</p>
                            <p class="text margin-30">当社は、以下の場合を除き、お客様の個人情報を第三者に提供することはありません。</p>
                            <ul class="margin-30">
                                <li>お客様から事前の同意を得た場合</li>        
                                <li>法令に基づく場合</li>  
                                <li>人の生命、身体または財産の保護のために必要がある場合であって、本人の同意を得ることが困難である場合</li>  
                                <li>公衆衛生の向上または児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難である場合</li>  
                                <li>国の機関もしくは地方公共団体またはその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合であって、本人の同意を得ることにより当該事務の遂行に支
                                    障を及ぼすおそれがある場合</li>  
                                <li>その他個人情報保護法により提供が認められている場合</li>                                  
                            </ul>
                            <p class="head">7．プライバシーポリシーの変更</p>
                            <p class="text margin-30">お客様の個人情報の安全管理を適切に行うため、当社は、本プライバシーポリシーを随時見直し改訂いたします。</p>
                            <p class="head">8．個人情報の取扱いについて</p>
                            <p class="text margin-30">お客様が登録した情報の利用目的につきましては、本プライバシーポリシーをご確認下さい。また、お客様の個人情報に関する確認、訂正又は登録解除に関するお問合せにつきましては
                                下記のメールにて受け付けております。ご本人様であることの確認のため、お問い合わせの際は下記の事項を必ずご記載頂き、お問合せ下さい。</p>
                            <ul class="margin-30">
                                <li>登録された住所</li>  
                                <li>氏名</li>  
                                <li>生年月日</li>  
                                <li>連絡先電話番号</li>  
                            </ul>
                            <p class="text margin-30">お問合せ先 e-mail : west-inquiries@westonline.jp</p>
                            <p class="text margin-30">※ westonline.jpからのメールが受信できるよう、迷惑メールのフィルタリング機能をお確かめいただいた上でお問い合わせくださいますよう、お願い致します。</p>
                            <p class="text margin-30">2014年7月18日</p>
                        </div>
                    </div>  


                </div>
                <div class="change-password-span">
                    <a class="white-proceed btn-refresh btn-privacy">TOPへ戻る</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer" class="bg-footer-grey">
            <ul class="footer-links">
                <li><a>Copyright © 2015 Westonline.jp. All rights reserved.    Terms of Use</a></li>
                <li><a>Updated Privacy Policy</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
