<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/questionare-grey.png" alt="img">
                        <h3>アンケート</h3>
                    </div>
                </div>
            </div>
            <div class="inner-page-content">
                <div class="col-md-12">
                    <p><b>キャッチコピー。説明文が入ります。キャッチコピー。説明文が入ります。キャッチコピー。説明文が入ります。キャッチコピー
                            。説明文が入ります。キャッチコピー。説明文が入ります。キャッチコピー。説明文が入ります。キャッチコピー。説明文が入ります
                            。キャッチコピー。説明文が入ります。キャッチコピー。説明文が入ります。キャッチコピー。説明文が入ります。</b></p>


                    <div class="banner-questionnaire<?php echo (!empty($selectedVal))?' js-questionnaire-result':''?>">                                
                        <h3><b>Q.</b> 臨時ボーナスが出たら‥</h3>
                        <div class="questionnaire-banner-img">
                            <div id="radioBox">
                                <div class="questionnaire-banner-img-item">
                                    <label for="questions01">
                                    <div class="center">
                                       <label class="percent" id="percent-1"><?php echo (!empty($selectedVal))?round(($allData['percent-1']/$allData['total'])*100).'<span>%</span>':''?></label>
                                    </div>
                                    <img class="img-questionnaire active" src="assets/img/images/questionnaire/img-questionnaire-1.jpg" alt="img">
                                    <div class="img-selection">
                                        <input type="radio" id="questions01" name="questions" value='貯金する'>
                                        <span>貯金する</span>
                                    </div>
                                    </label>
                                </div>

                                <div class="canvas-holder-wrapper">
                                    <div id="canvas-holder" style="text-align:center"></div>
                                    <div class="doughnut-center center <?php echo (!empty($selectedVal))?'':'hide'?>" id="canvasText">
                                        <p><img src="assets/img/images/text-doughnut-center.png" height="18" width="53" alt="West Questionnaire"></p>
                                    </div>
                                </div>

                                <div class="questionnaire-banner-img-item">
                                    <label for="questions02">
                                    <div class="center">
                                        <label class="percent" id="percent-2"><?php echo (!empty($selectedVal))?round(($allData['percent-2']/$allData['total'])*100).'<span>%</span>':''?></label>
                                    </div>
                                    <img class="img-questionnaire" src="assets/img/images/questionnaire/img-questionnaire-2.jpg" alt="img">
                                    <div class="img-selection">
                                        <input type="radio" id="questions02" name="questions" value='パーッと使う'>
                                        <span>パーッと使う</span>
                                    </div>
                                    </label>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-12 center">
                            <a href="#" class="grey-bg btn-questionnaire-banner" id="submitBtn">回答する</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 margin-20">                           
                <div class="row">  
                    <div class="slider-header margin-20">
                        <h4>他のアンケート結果</h4>
                    </div>
                    <div class="slider responsive slider-padded">
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-md-12">
                                        <p>実は変わり者・・・？</p>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-2.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-1.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-md-12">
                                        <p>自分磨きできてる？</p>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-3.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-4.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-md-12">
                                        <p>結婚後の家事したい？</p>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-5.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-6.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-md-12">
                                        <p>セキュリティソフト使ってる？</p>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-7.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-8.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-md-12">
                                        <p>職場で他人がやっていて ストレスを感じる行為どっち？</p>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-9.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-10.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2-5">
                            <div class="tile-questionnaire">
                                <div class="questionare-items">
                                    <div class="col-md-12">
                                        <p>実は変わり者・・・？</p>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-2.jpg" alt="img">
                                    </div>
                                    <div class="col-xs-6 ">
                                        <img src="assets/img/images/questionnaire/img-tile-1.jpg" alt="img">
                                    </div>
                                    <div class="submit-questionnaire">
                                        <a class="grey-bg" href="#">回答する</a>                                                    
                                    </div>
                                    <div class="center">
                                        <a class="red-proceed">結果を見る</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script src="assets/js/Chart.js"></script>
<script type="text/javascript">
    $('.responsive').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.btn-modal').click(function() {
        var dataTarget = $(this).attr('data-target');
        setTimeout(function() {
            $(dataTarget).find('div.modal-body').removeClass('modal-loading');
            $(dataTarget).find('div.single-item').removeClass('disappear');
            $(dataTarget).find('div.single-item').slick({
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                adaptiveHeight: true
            });

        }, 500);
    });



    $('#submitBtn').click(function() {
        if ($('#radioBox').hasClass('hide')) {
            $('#submitBtn').text('回答する');
            $('#canvas-holder').html('');
           // $('#radioBox').removeClass('hide');
        } else {
           // $('#radioBox').addClass('hide');
            $('#submitBtn').text('バック');
            $('#canvas-holder').html('<canvas id="chart-area" width="120" height="120"/>');
            $('.banner-questionnaire').addClass("js-questionnaire-result");
            $('#submitBtn').hide(); 
            $('input[name="questions"]').addClass('hide');

            var asd = $('input[name="questions"]:checked').val();
            var asd1 = $('input[name="questions"]:not(:checked)').val();

            var pieData = [
                {
                    value: 40,
                    color: "#EC9696",
                    highlight: "#FF5A5E",
                },
                {
                    value: 60,
                    color: "#AC0000",
                    highlight: "#5AD3D1",
                },
            ];
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(pieData);
            // window.myPie = new Chart(ctx).Pie(pieData);
        }
    });
</script>
</body>

</html>
