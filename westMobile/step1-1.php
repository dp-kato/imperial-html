<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>STEP1-1: 利用規約およびプライバシーポリシ ーへのご同意</h3>
                </div>
                <div class="panel-content">
                    <div class="col-xs-12">
                        <p class="heading-panel red">はじめに</p>
                        <p>当ウェブサイトは、日本国内在住の満20歳以上の成人喫煙者を対象にしています。ご閲覧・ご利用いただくには、また、各種キャンペーン等の応募には、お客様IDを取得していただくことが必要となります。</p>
                        <p class="margin-30">お申し込みの流れについては、こちらでご確認ください。</p>
                        <p class="margin-30">まず、利用規約の内容およびプライバシーポリシーをご確認ください。</p>
                        <p><strong>■</strong> 利用規約（必ずお読み下さい）</p>
                        <p class="margin-30"><strong>■</strong> プライバシーポリシー（必ずお読み下さい）</p>
                        <p class="margin-30">内容にご同意いただければ、以下のチェックボックスにチェックを入れ「同意する」ボタンを押して下さい。</p>
                        <div class="margin-30">
                            <input type="checkbox">
                            <label>私は満20歳以上の喫煙者であり、上記利用規約およびプライバシーポリシーに同意します。 </label>
                        </div>
                        <div class="center margin-30">                            
                            <a class="white-proceed btn-refresh fixed-width">同意する</a>
                        </div>
                        <p>※ 本サイトのID取得が可能なのは、日本国内に お住ま　いの満20歳以上の喫煙者のかたに限らせていただ　きます。</p>
                        <p>※ お申込みに際しては、taspoまたは運転免許証など　満20歳以上であることが確認できる、成人確認書　類の写し（写真画像）が必要となります。</p>
                        <p>※ ID発行には、数日かかる場合があります。</p>
                        <p>※ 利用規約およびプライバシーポリシーに不同意の場　合はお申し込みになれません。</p>





                    </div>


                </div>
            </div>




        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>

</body>

</html>
