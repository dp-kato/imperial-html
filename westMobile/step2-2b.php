<?php include('header-login.php'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="page-content-login row">
            <div class="panel-body">
                <div class="panel-header">
                    <h3>STEP2-2: プロフィール登録</h3>
                </div>
                <div class="panel-content">
                    <div class="col-md-12">
                        <div class="center">
                            <p>携帯メールアドレス</p>
                            <p class="margin-30 red">× × × × ×@docomo.ne.jp</p>
                        </div>
                        <p class="margin-30">ご提供頂く個人情報は、弊社が以下の目的に使用させて頂きます。
                            また、個人を特定しない方法で、マーケティングの統計データとして活用させて頂きます。
                            お客様の個人情報の取扱いについてはこちら</p>
                        <p class="margin-15">■以下よりご希望をお選び下さい。</p>
                        <p>1)ご希望の場合のサンプルたばこの送付</p>
                        <div class="panel-content-inner">
                            <input type="radio">
                            <label>希望する</label>
                            <input type="radio">
                            <label>希望しない</label>
                        </div>
                        <p>2)メールマガジンの送付</p>
                        <div class="col-xs-12">
                            <p>（1）キャンペーンに関するメールマガジン</p>
                            <div class="panel-content-inner">
                                <div class="col-xs-12">
                                    <input type="radio">
                                    <label>希望する</label>
                                    <input type="radio">
                                    <label>希望しない</label>
                                </div>
                            </div>
                            <p>（2）新製品に関するメールマガジン</p>
                            <div class="panel-content-inner">
                                <div class="col-xs-12">
                                    <input type="radio">
                                    <label>希望する</label>
                                    <input type="radio">
                                    <label>希望しない</label>
                                </div>
                            </div>
                            <p>（3）懸賞に関するメールマガジン</p>
                            <div class="panel-content-inner">
                                <div class="col-xs-12">
                                    <input type="radio">
                                    <label>希望する</label>
                                    <input type="radio">
                                    <label>希望しない</label>
                                </div>
                            </div>
                        </div>
                        <p class="margin-30">※製品サンプルやご案内については、時期や条件等により、お届けできない場合もあります。あらかじめご了承ください。</p>
                        <p class="margin-15">いまご愛飲されているたばこについて、パッケージ側面のバーコードの下に記載されている番号を記入してください。(半角数字)</p>
                        <input class="form-control margin-20" type="text">
                        <div class="center margin-20">                            
                            <a class="white-proceed btn-refresh fixed-width">登録する</a>
                        </div>


                    </div>                   
                </div>
            </div>


        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div id="below-footer">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>

    </div>
</div>
</div>
<div class="modal fade" id="modal-comics2-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear">
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comics2-11" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> <img src="assets/img/images/modal-images/btn-modal-close.png" alt="img"></span></button>
                <h4>コミック</h4>
                <p>シュールな笑いを提供する田中光氏連載スタート！</p>
            </div>
            <div class="modal-body modal-loading">
                <div class="slider single-item disappear" >
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-1.png" alt="img">                                            
                            </div>     
                            <div class="new-entry">
                                <img src="assets/img/images/comics/new-tag.png" alt="img">
                            </div>
                            <div class="comics-listing-text">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="comics-tile">
                            <div class="comics-listing">
                                <img src="assets/img/images/modal-images/img-modal-2.png" alt="img">                                            
                            </div>                                       
                            <div class="comics-listing-text brown">
                                <a href="#"> <i class="fa fa-angle-right"></i></a>
                                <p>Lorem Ipsum dolor sit amet</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.multiple-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

</script>
</body>

</html>
