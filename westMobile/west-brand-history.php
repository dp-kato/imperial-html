<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row west-brandWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/west-brand-grey.png" alt="img">
                        <h3>ウエストについて</h3>
                    </div>
                </div>
            </div>
            <div class="subMenu-wrap">
                <ul class="subMenu-brand">
                     <li><a href="#">Westの3つのValue</a></li>
                     <li class="active"><a href="#">History</a></li>
                     <li><a href="#">Priducts</a></li>
                 </ul>
            </div>
            <div class="content_history">
                <h3>History</h3>
                <p class="history01"><img src="assets/img/images/west-brand/img-history01.jpg" alt="1981 ドイツ生まれのウエスト。"></p>
                <p class="history02"><img src="assets/img/images/west-brand/img-history02.jpg" alt="1981-1986 ウエスト誕生。そして、ドイツのリーディングブランドに。">
                </p>
                <p class="history03"><img src="assets/img/images/west-brand/img-history03.jpg" alt="1987-1995 こだわりの味わいを試せ。常識への挑戦を続けるウエスト。"></p>
                <p class="history04"><img src="assets/img/images/west-brand/img-history04.jpg" alt="1996-2000 スポンサー契約開始。F１チーム、「ウエスト・マクラーレン・メルセデス」"></p>
                <p class="history05"><img src="assets/img/images/west-brand/img-history05.jpg" alt="2001-2003 驚嘆と興奮、そして歓喜の一服を。"></p>
                <p class="history06"><img src="assets/img/images/west-brand/img-history06.jpg" alt="2004-2008 チャンスと可能性に満ちあふれた毎日。最初のきっかけを、ウエストと共に。"></p>
                <p class="history07"><img src="assets/img/images/west-brand/img-history07.jpg" alt="2009 発売から30年目で、初めてのパッケージデザイン変更。-自らを表現せよ-"></p>
                <p class="history07"><img src="assets/img/images/west-brand/img-history08.jpg" alt="2010-2013 得られるものは、驚きのアイデアと体験。欲するものは、ここにある。"></p>
                <p class="history08"><img src="assets/img/images/west-brand/img-history09.jpg" alt="2014 再起動 日本限定パッケージが登場。"></p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
