<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row west-brandWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/west-brand-grey.png" alt="img">
                        <h3>ウエストについて</h3>
                    </div>
                </div>
            </div>
            <div class="subMenu-wrap">
                <ul class="subMenu-brand">
                     <li><a href="#">Westの3つのValue</a></li>
                     <li><a href="#">History</a></li>
                     <li class="active"><a href="#">Priducts</a></li>
                 </ul>
            </div>
            <div class="content_product">
                <h3>Products</h3>
                <div class="itemWrap">
                    <div id="productItem01">
                        <img src="assets/img/images/west-brand/img-product01.jpg" height="720" width="640" alt="ウエスト・レッド">
                    </div>
                    <div id="productItem02">
                        <img src="assets/img/images/west-brand/img-product02.jpg" height="720" width="640" alt="ウエスト・ディープブルー">
                    </div>
                    <div id="productItem03">
                        <img src="assets/img/images/west-brand/img-product03.jpg" height="600" width="1000" alt="ウエスト・ブルー">
                    </div>
                    <div id="productItem04">
                        <img src="assets/img/images/west-brand/img-product04.jpg" height="600" width="1000" alt="ウエスト・シルバー">
                    </div>
                    <div id="productItem05">
                        <img src="assets/img/images/west-brand/img-product05.jpg" height="600" width="1000" alt="ウエスト・ホワイト ウエスト・ホワイト100">
                    </div>
                    <div id="productItem06">
                        <img src="assets/img/images/west-brand/img-product06.jpg" height="600" width="1000" alt="ウエスト・メンソール ウエスト・メンソールホワイト100">
                    </div>
                    <div id="productItem07">
                        <img src="assets/img/images/west-brand/img-product07.jpg" height="600" width="1000" alt="ウエスト・アイスフレッシュエイト ウエスト・アイスフレッシュワン100">
                    </div>
                    <div id="productItem08">
                        <img src="assets/img/images/west-brand/img-product08.jpg" height="600" width="1000" alt="ウエスト・デュエット6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
