<?php include('header.php'); ?>
<div class="container-fluid">
    <div class="row west-brandWrap">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/west-brand-grey.png" alt="img">
                        <h3>ウエストについて</h3>
                    </div>
                </div>
            </div>
            <div class="subMenu-wrap">
                <ul class="subMenu-brand">
                     <li class="active"><a href="#">Westの3つのValue</a></li>
                     <li><a href="#">History</a></li>
                     <li><a href="#">Priducts</a></li>
                 </ul>
            </div>
            <div class="content_value">
                <p><img src="assets/img/images/west-brand/img-value01.jpg" alt="Westの3つのValue"></p>
                <p><img src="assets/img/images/west-brand/img-value02.jpg" alt="シンプルなかたち"></p>
                <div class="value01">
                    <h3>シンプルなかたち</h3>
                    <p>ウエストは、シンプルさが特徴です。華美な装飾を控えた、ウエストのシンプルなパッケージは、シックでモダンな印象です。手にも取りやすく、さりげないこだわりまで演出してくれます。また、タバコづくりもシンプル。原料の調達、製造工程の効率化など、あらゆる工程をシンプルにし、無駄を省いています。だから、高品質なのにお求めやすい価格で提供できるのです。</p>
                </div>
                <p><img src="assets/img/images/west-brand/img-value03.jpg" alt="高品質と低価格の両立"></p>
                <div class="value02">
                    <h3>高品質と低価格の両立</h3>
                    <p>ウエストは、本物の味わいにこだわりを持ち、上質な原料を一括調達。各工程においても徹底した品質管理を行っています。しかも、生産効率が向上するように改善も続けています。それらの努力を積み重ねた先に、品質と価格のバランスのとれた価値ある製品が実現しているのです。</p>
                </div>
                <p><img src="assets/img/images/west-brand/img-value04.jpg" alt="本物の味わい"></p>
                <div class="value03">
                    <h3>本物の味わい</h3>
                    <p>本当のおいしさを味わっていただくために、ウエストに使われる良質な葉タバコは、世界中の産地から集められています。選りすぐられた葉タバコを、乾燥させた後に、さらに専門のブレンダーが厳選。入念なブレンドによって、ウエストの高品質な香りと味わいが生まれています。</p>
                </div>
                <div class="movie">
                    <iframe width="300" height="160" src="https://www.youtube.com/embed/QfAA9QSEMUA" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>     
<script src="assets/js/odometer.min.js"></script>       
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });

    $(document).ready(function() {
        $("#showAdditionalQuesAns").click(function() {
            var current = $(this);
            $('#additional-QuesAns').slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    //current.text('Close');
                    current.addClass('hide');
                } else {
                    current.text('Read More');
                }
            });


        });

    });

</script>
</body>

</html>
