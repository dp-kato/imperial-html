<?php
include('header.php');
$ip = $_SERVER['REMOTE_ADDR'];
$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
$location = explode(',', $details->loc);
if ($location) {
    $lat = $location[0];
    $lon = $location[1];
}
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.
    var latitude, longitude;

    function showMap(latitude, longitude) {

        //Get Latitude From Geolocation API
        //var latitude = "<?php echo $lat ?>";

        //Get Longitude From Geolocation API
        //var longitude = "<?php echo $lon ?>";

        //Define New Google Map With Lat / Lon
        var coords = new google.maps.LatLng(latitude, longitude);
        //Specify Google Map Options
        var mapOptions = {
            zoom: 15,
            center: coords,
            mapTypeControl: true,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var iconBase = 'assets/img/images/';
        var contentString = '<div id="content" class="map-popup">' +
                '<div id="siteNotice">' +
                '</div>' +
                '<div class="map-popup-header">' +
                '<h1 id="firstHeading" class="firstHeading">（株）Westタバコ店</h1>' +
                '<p>東京都中央区銀座0の0の00</p>' +
                '<p>03-0000-0000</p>' +
                '</div>' +
                '<div id="bodyContent">' +
                '<ul class="map-popup-list">' +
                '<li>' +
                '<a>' +
                '<img src="' + iconBase + 'west-hunt/pack1.png"/>' +
                '</a>' +
                '</li>' +
                '<li>' +
                '<a>' +
                '<img src="' + iconBase + 'west-hunt/pack2.png"/>' +
                '</a>' +
                '</li>' +
                '<li>' +
                '<a>' +
                '<img src="' + iconBase + 'west-hunt/pack3.png"/>' +
                '</a>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });


        var marker = new google.maps.Marker({
            position: coords,
            map: map,
            title: 'Uluru (Ayers Rock)',
            icon: iconBase + 'west-marker.png',
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });

    }
    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initialize() {
        // Create the autocomplete object, restricting the search
        // to geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
                {types: ['geocode']});
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
        });
    }

// [START region_fillform]
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        latitude = place.geometry.location.lat();
        longitude = place.geometry.location.lng();
        /*for (var component in componentForm) {
         document.getElementById(component).value = '';
         document.getElementById(component).disabled = false;
         }
         
         // Get each component of the address from the place details
         // and fill the corresponding field on the form.
         for (var i = 0; i < place.address_components.length; i++) {
         var addressType = place.address_components[i].types[0];
         if (componentForm[addressType]) {
         var val = place.address_components[i][componentForm[addressType]];
         document.getElementById(addressType).value = val;
         }
         }*/
    }
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);
                autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
                        geolocation));
            });
        }
    }
// [END region_geolocation]

    $(document).ready(function() {
        initialize();
        setTimeout(function() {
            //Get Latitude From Geolocation API
            var latitude = "<?php echo $lat ?>";

            //Get Longitude From Geolocation API
            var longitude = "<?php echo $lon ?>";

            showMap(latitude, longitude);
        }, 3000);

        $('#geoBtn').click(function() {
            showMap(latitude, longitude);
        });
    });

</script>

<style>
    #locationField, #controls {
        position: relative;
    }
    #autocomplete {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
    }
    .label {
        text-align: right;
        font-weight: bold;
        width: 100px;
        color: #303030;
    }
    #address {
        border: 1px solid #000090;
        background-color: #f0f0ff;
        width: 480px;
        padding-right: 2px;
    }
    #address td {
        font-size: 10pt;
    }
    .field {
        width: 99%;
    }
    .slimField {
        width: 80px;
    }
    .wideField {
        width: 200px;
    }
    #locationField {
        height: 20px;
        margin-bottom: 2px;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="page-content">
            <div class="col-md-12">
                <div class="row">
                    <div class="inner-page-header">                                
                        <img src="assets/img/images/west-hunt-grey.png" alt="img">
                        <h3>West Hunt</h3>
                        <a class="grey-bg btn-inner-page-header">Share</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="inner-page-content">    
                    <div class="row">
                        <div class="search-item">
                            <div class="col-xs-12">
                                <div class="west-hunt-search-select">


                                    <div class="search-location">
                                        <a id="icon-gps" class="btn btn-default btn-red-icon-center">Search By GPS Location</a>
                                        <div class="search-by-location">
                                            <div class="col-md-12">
                                                <div id="googleMap" style="width:100%;height:300px;margin-top:20px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="center selection">
                                        <p class="head">OR</p>
                                    </div>

                                    <div class="search-location">
                                        <a id="icon-landmark-white" class="btn btn-default btn-red-icon-center">Search Manually</a>
                                        <div class="search-manual" style="display:none">
                                            <select class="form-control"><option>Select Prefecture</option></select>
                                            <select class="form-control"><option>Select City</option></select>
                                            <div class="packet-selection">
                                                <p>Select Pack</p>
                                                <ul class="packets">
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack1.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack2.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack3.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack4.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack5.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack6.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack7.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack8.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack9.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack10.png" alt="img"></a>
                                                    </li>
                                                    <li>
                                                        <a><img src="assets/img/images/west-hunt/pack11.png" alt="img"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="center margin-30">                            
                                                <a class="white-proceed btn-refresh fixed-width">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-location">
                                        <a id="icon-register" class="btn btn-default btn-red-icon-center">Register Store</a>
                                    </div>
                                    <!--<div id="locationField" class="btn-icon-landmark">
                                        <input id="autocomplete" placeholder="キーワードを入力して探す" onFocus="geolocate()" type="text" class="form-control"></input>
                                    </div>  -->    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="center margin-30">
                         <a class="white-proceed btn-refresh fixed-width" id="geoBtn">検索</a>
                     </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="footer">
            <ul class="footer-links">
                <li><a>About us</a></li>
                <li><a>News</a></li>
                <li><a>FAQs</a></li>
                <li><a>Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div id="below-footer-main">
            <ul class="footer-links">
                <li><a>利用規約</a></li>
                <li><a>プライバシーポリシー</a></li>
            </ul>
            <ul class="footer-links color-grey">
                <li><a>Copyright © 2015 Imperial Tobacco Japan K.K. All Rights Reserve</a></li>
            </ul>
        </div>
        <div class="warning-footer">
            <div class="warning-text">
                <p>喫煙は、あなたにとって肺がんの原因の一つとなり、心筋梗塞・脳卒中の危険性や肺気腫を悪化させる危険性を高めます。 </p>
                <p>未成年者の喫煙は、健康に対する悪影響やたばこへの依存をより強めます。周りの人から勧められても決して吸ってはいけません。</p>
                <p>たばこの煙は、あなたの周りの人、特に乳幼児、子供、お年寄りなどの健康に悪影響を及ぼします。 喫煙の際には、周りの人の迷惑にならないように注意しましょう。</p>
            </div>
        </div>
    </div>
</div>
</div>
<?php include('footer.php'); ?>
<script src="assets/js/jquery.js"></script>    
<script src="assets/js/odometer.min.js"></script>
<script src="assets/js/bootstrap.js"></script>      
<script type="text/javascript" src="assets/js/slider/slick.js"></script>
<script type="text/javascript">
    $('.single-item').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true
    });
    $(document).ready(function(){
        $(".search-manual ul.packets li").click(function(){
            $(this).toggleClass("active");
        });
        
        $("#icon-landmark-white").click(function(){
            $(".search-manual").slideToggle();
            $(".search-by-location").slideToggle();
        });
        
        $("#icon-gps").click(function(){
            $(".search-by-location").slideToggle();
            $(".search-manual").slideToggle();
        });
        
        
    });
</script>
</body>

</html>
